# import random

choices = ["rock", "paper", "scissors"]
print(random.choice(choices))

rock = codesters.Sprite("rock", -150, 150)
rock.set_size(.7)
paper = codesters.Sprite("paper", 0, 150)
paper.set_size(.7)
scissors = codesters.Sprite("scissors", 150, 150)
scissors.set_size(.7)

user_choice = ""

def click(_sprite):
    global user_choice
    user_choice = _sprite.get_image_name()
    print(user_choice)
    
rock.event_click(click)
paper.event_click(click)
scissors.event_click(click)

while user_choice == "":
    stage.wait(.1)

chosen_weapon_text = codesters.Text(f"You chose {user_choice}", 0, -50)

"""
choice_dict = {}
positions = [()]
"""

