weapons = ["rock", "paper", "scissors"]

# FOR LOOPS
for i in range(len(weapons)): # this line is confusing
    print("index", i)

for i in range(len(weapons)): # this line is confusing
    print("item", weapons[i])

# FOR EACH LOOPS
for item in weapons:
    print(item)

# ENUMERATE (python only)
for index, item in enumerate(weapons):
    print(index, item)
