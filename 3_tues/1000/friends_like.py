# This teaches us how to use a list of dictionaries
friends = [
  {
    "name": "Bert",
    "likes": "Resident Evil",
    "age": 32,
  },
  {
    "name": "Gabby",
    "likes": "BotW",
    "age": 32,
  },
  {
    "name": "Oliver",
    "likes": "drawing",
    "age": 13,
  },
]

chosen_name = input("Who would you like to know about? ")
for one_friend in friends:
  if one_friend["name"] == chosen_name: # changed
    name = one_friend["name"]
    likes = one_friend["likes"]
    age = one_friend["age"]
    print(f"{name} is {age} years old, and likes {likes}")
