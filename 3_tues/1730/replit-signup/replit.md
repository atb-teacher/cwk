# Make an account at `replit.com`

* Go to the website
* Sign in using any email
* When it asks you about your experience level, say "beginner"
* Hit the big blue plus button
  * Make a new python project
  * Call it whatever you want
