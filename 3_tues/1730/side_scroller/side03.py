hero = codesters.Sprite("mega_man_ac6")
hero.hide()
hero.set_size(.3)
hero.reflect_x(50)
hero.go_to(-200, -100)
hero.show()
hero.id = "mega man" # NEW CODE

stage.set_background("sky2")

ground = codesters.Rectangle(
    0, # x   
    -250, # y
    500, # width   
    50, # height
    "green", # color
)
ground.id = "grass ground" # NEW CODE

stage.set_gravity(10)

def collision_hero(hero_sprite, other):
    if all([
        hero_sprite.id == "mega man", 
        other.id == "grass ground",
    ]):
        hero_sprite.set_y_speed(0)
        hero_sprite.set_gravity_off()
        
        new_height = other.get_top() + \
        (hero_sprite.get_height() // 2)
        hero_sprite.set_y(new_height)
        
        
hero.event_collision(collision_hero)

def space_key(sprite):
    sprite.jump(10)
    sprite.set_gravity_on()

hero.event_key("space", space_key)

def left_key(sprite):
    sprite.move_left(20)

hero.event_key("left", left_key)

def right_key(sprite):
    if sprite.get_x() < 0:
        sprite.move_right(20)

hero.event_key("right", right_key)

def down_key_minecraft(sprite):
    if sprite.get_y() > (ground.get_top() + sprite.get_height()):
        sprite.set_y_speed(
            -30   
        )

hero.event_key("down", down_key_minecraft)

