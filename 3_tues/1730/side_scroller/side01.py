hero = codesters.Sprite("mega_man_ac6")
hero.hide()
hero.set_size(.3)
hero.reflect_x(50)
hero.go_to(-200, -100)
hero.show()
hero.id = "mega man" # NEW CODE

stage.set_background("sky2")

ground = codesters.Rectangle(
    0, # x   
    -250, # y
    500, # width   
    50, # height
    "green", # color
)
ground.id = "grass ground" # NEW CODE

stage.set_gravity(10)

def collision_hero(hero_sprite, other):
    if all([
        hero_sprite.id == "mega man", 
        other.id == "grass ground",
    ]):
        hero_sprite.set_y_speed(0)
        hero_sprite.set_gravity_off()
        
hero.event_collision(collision_hero)


