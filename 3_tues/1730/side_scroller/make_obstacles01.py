ground = codesters.Rectangle(
    0, # x   
    -250, # y
    500, # width   
    50, # height
    "green", # color
)
ground.id = "grass ground" # NEW CODE

ground.set_gravity_off()

stage.set_gravity(10)
stage.disable_all_walls()

def bouncy_enemy_collision(bouncy_enemy, other):
    if all([
        bouncy_enemy.id == "bouncy enemy",
        other.id == "grass ground",
        ]):
        bouncy_enemy.set_y_speed(
            abs(bouncy_enemy.get_y_speed())
        )

def make_bouncy_enemy():
    new_enemy = codesters.Rectangle(
        270, # x
        random.randint(-100, 250), # y # NEW CODE
        20, # width
        20, # height
        "blue", # color
    )
    new_enemy.set_x_speed(-5)
    new_enemy.id = "bouncy enemy"
    new_enemy.event_collision(bouncy_enemy_collision)
    

while True: # NEW CODE
    stage.wait(0.05) # NEW CODE
    make_bouncy_enemy() # NEW CODE
