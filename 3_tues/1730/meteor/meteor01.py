stage.disable_all_walls()

stage.set_background("mars")

player = codesters.Sprite(
    "ufo", # image
    200, # x
    0, # y
)

player.set_size(0.7)

def up_key(sprite):
    sprite.move_up(20)

player.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)

player.event_key("down", down_key)

# TEMP CODE

one_meteor = codesters.Sprite(
    "meteor1", # image
    -200, # x
    random.randint(-200, 200), # y
)

one_meteor.set_x_speed(4)




