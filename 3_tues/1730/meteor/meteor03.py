game_running = True

stage.disable_all_walls()

stage.set_background("mars")

player = codesters.Sprite(
    "ufo", # image
    200, # x
    0, # y
)

player.set_size(0.7)

def up_key(sprite):
    sprite.move_up(20)

player.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)

player.event_key("down", down_key)

def collision(player, other):
    global game_running
    if other.get_image_name() == "meteor1":
        player.hide()
        game_running = False
    elif other.get_image_name() == "alien1":
        player.say("Yaay!")

player.event_collision(collision)

# while True:
while game_running == True:
    one_meteor = codesters.Sprite(
        "meteor1", # image
        -200, # x
        random.randint(-200, 200), # y
    )
    
    one_meteor.set_x_speed(4)
    stage.wait(2)
