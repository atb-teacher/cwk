# creating a sound
sound_p_bb = codesters.Sound("piano_bb")

sounds = """
piano_c
piano_d
piano_eb
piano_f
piano_g
piano_ab
piano_bb
""".strip().split('\n')

notes = []

for one_sound in sounds:
    notes.append(
        codesters.Sound(one_sound)
    )

def death_music():
    for one_note in reversed(notes):
        one_note.play()
        stage.wait(0.05)

game_running = True

score_text = codesters.Text(
    "Score: 0",
    0, # x
    225, # y
)
score_text.set_color("white")

score = 0

stage.disable_all_walls()

stage.set_background("mars")

player = codesters.Sprite(
    "ufo", # image
    200, # x
    0, # y
)

player.set_size(0.5)

def up_key(sprite):
    if sprite.get_y() < 200:
        sprite.move_up(20)

player.event_key("up", up_key)

def down_key(sprite):
    if sprite.get_y() > -200:
        sprite.move_down(20)

player.event_key("down", down_key)

def collision(player, other):
    global game_running
    global score
    if other.get_image_name() == "meteor1":
        player.hide()
        game_running = False
        death_music()
    elif other.get_image_name() == "alien1":
        score = score + 1
        sound_p_bb.play() # play the sound
        score_text.set_text(
            "Score: " + str(score)
        )
        stage.remove_sprite(other)

player.event_collision(collision)

# while True:
while game_running == True:
    one_meteor = codesters.Sprite(
        "meteor1", # image
        -200, # x
        random.randint(-200, 200), # y
    )
    one_meteor.set_size(0.5)
    one_meteor.set_x_speed(4)
    one_alien = codesters.Sprite(
        "alien1",
        -200, # x
        random.randint(-200, 200), # y
    )
    one_alien.set_size(0.4)
    one_alien.set_x_speed(3)
    stage.wait(2)



