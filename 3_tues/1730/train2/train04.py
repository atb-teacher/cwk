stage.disable_all_walls()
trains_active = True

frog = codesters.Sprite(
    "alien1", # sprite
    -200, # x
    0, # y
)

frog.set_size(0.2)
frog.sprite_cat = "hero"

def left_key(sprite):
    sprite.set_x(
        sprite.get_x() - 20
    )
    
frog.event_key("left", left_key)

def right_key(sprite):
    sprite.set_x(
        sprite.get_x() + 20
    )

frog.event_key("right", right_key)

def up_key(sprite):
    sprite.set_y(
        sprite.get_y() + 20
    )

frog.event_key("up", up_key)

def down_key(sprite):
    sprite.set_y(
        sprite.get_y() - 20
    )

frog.event_key("down", down_key)

end_area = codesters.Rectangle(
    250, # x
    0, # y
    50, # width
    500, # height
    "palegreen", # color
)
end_area.sprite_cat = "win_con"

def down_train(speed=-4):
    # random color
    train_color = random.choice([
        "red",
        "orange",
        "yellow",
        "green",
        "blue",
        "purple",
    ])
    
    # random x coordinate
    x_coord = random.randint(-10, 10) * 20
    
    for i in range(3):
        train_part = codesters.Rectangle(
            x_coord, # x
            500 - (i * 75), # y
            25, # width
            70, # height
            train_color, # color
        )
        train_part.set_y_speed(speed)
        train_part.set_outline_color("black")
        train_part.sprite_cat = "obstacle"

def collision(hero_sprite, other):
    other_cat = other.sprite_cat
    if other_cat == "obstacle":
        frog.say("Ouchie!!", 1)
    if other_cat == "win_con":
        global trains_active
        trains_active = False
        frog.hide()
        frog.go_to(
            -200, # x
            0, # y
        )
        stage.wait(2)
        frog.show()
        trains_active = True
    
frog.event_collision(collision)


def up_train(speed=4):
    # random color
    train_color = random.choice([
        "red",
        "orange",
        "yellow",
        "green",
        "blue",
        "purple",
    ])
    
    # random x coordinate
    x_coord = random.randint(-10, 10) * 20
    
    for i in range(3):
        train_part = codesters.Rectangle(
            x_coord, # x
            -300 - (i * 75), # y
            25, # width
            70, # height
            train_color, # color
        )
        train_part.set_y_speed(speed)
        train_part.set_outline_color("black")
        train_part.sprite_cat = "obstacle"
        

def randtrain():
    if random.randint(0, 1) == 1:
        up_train()
    else:
        down_train()

# Game Loop
while True:
    stage.wait(1)
    if trains_active == True:
        randtrain()

"""
* Make score
* Something happens when you reach the goal area
  * Restart the game when you hit the goal area
    * Make sure that the trains don't spawn for a bit
"""

