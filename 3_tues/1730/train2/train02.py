stage.disable_all_walls()

frog = codesters.Sprite(
    "alien1", # sprite
    -200, # x
    0, # y
)
frog.set_size(0.3)

def left_key(sprite):
    sprite.set_x(
        sprite.get_x() - 20
    )
    
frog.event_key("left", left_key)

def right_key(sprite):
    sprite.set_x(
        sprite.get_x() + 20
    )

frog.event_key("right", right_key)

def up_key(sprite):
    sprite.set_y(
        sprite.get_y() + 20
    )

frog.event_key("up", up_key)

def down_key(sprite):
    sprite.set_y(
        sprite.get_y() - 20
    )

frog.event_key("down", down_key)

end_area = codesters.Rectangle(
    250, # x
    0, # y
    50, # width
    500, # height
    "palegreen", # color
)

def down_train():
    # random color
    train_color = random.choice([
        "red",
        "orange",
        "yellow",
        "green",
        "blue",
        "purple",
    ])
    
    # random x coordinate
    x_coord = random.randint(-10, 10) * 20
    
    for i in range(3):
        train_part = codesters.Rectangle(
            x_coord, # x
            500 - (i * 75), # y
            25, # width
            70, # height
            train_color, # color
        )
        train_part.set_y_speed(-10)
        train_part.set_outline_color("black")

def up_train():
    # random color
    train_color = random.choice([
        "red",
        "orange",
        "yellow",
        "green",
        "blue",
        "purple",
    ])
    
    # random x coordinate
    x_coord = random.randint(-10, 10) * 20
    
    for i in range(3):
        train_part = codesters.Rectangle(
            x_coord, # x
            -300 - (i * 75), # y
            25, # width
            70, # height
            train_color, # color
        )
        train_part.set_y_speed(10)
        train_part.set_outline_color("black")

def randtrain():
    if random.randint(0, 1) == 1:
        up_train()
    else:
        down_train()

# Game Loop
while True:
    stage.wait(1)
    randtrain()


