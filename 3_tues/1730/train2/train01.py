stage.disable_all_walls()

frog = codesters.Sprite(
    "alien1", # sprite
    -200, # x
    0, # y
)
frog.set_size(0.3)

def left_key(sprite):
    sprite.set_x(
        sprite.get_x() - 20
    )
    
frog.event_key("left", left_key)

def right_key(sprite):
    sprite.set_x(
        sprite.get_x() + 20
    )

frog.event_key("right", right_key)

def up_key(sprite):
    sprite.set_y(
        sprite.get_y() + 20
    )

frog.event_key("up", up_key)

def down_key(sprite):
    sprite.set_y(
        sprite.get_y() - 20
    )

frog.event_key("down", down_key)

end_area = codesters.Rectangle(
    250, # x
    0, # y
    50, # width
    500, # height
    "palegreen", # color
)

# TEMP TRAIN CODE

for i in range(3):

    train_part = codesters.Rectangle(
        0, # x
        500 - (i * 75), # y
        25, # width
        70, # height
        "blue", # color
    )
    train_part.set_y_speed(-20)


