stage.set_background_color('black')
question = codesters.Text("test", 0, 180, "lightblue")
a = codesters.Text("A:", -150, 50, "lightblue")
b = codesters.Text("B:", 50, 50, "lightblue")
c = codesters.Text("C:", -150, -50, "lightblue")
d = codesters.Text("D:", 50, -50, "lightblue")

question_list = []
answer_list = []
correct_answer_list = []

question_list.append("How many seasons are there?")
answer_list.append(
    ["2", "3", "4", "5"]
)
correct_answer_list.append("c")

question_list.append("How many seasons of Natuto are there?")
answer_list.append(
    ["2", "4", "6", "8"]
)
correct_answer_list.append("b")

question_list.append("What is my teacher's name?")
answer_list.append(
    ["Axel", "Abby", "Ack", "Alex"]
)
correct_answer_list.append("d")

question_list.append("What's the best place to shop?")
answer_list.append(
    ["Target", "Amazon", "Wal*Mart", "7/11"]
)
correct_answer_list.append("a")

for session in [0, 1, 2, 3]:
    question.set_text(question_list[session])
    stage.wait(2)

print(question_list)


