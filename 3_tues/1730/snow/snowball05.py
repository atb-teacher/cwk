stage.disable_all_walls()

stage.set_background("winter")


villain = codesters.Sprite(
    "evilwizard", # sprite
    199, # x
    150, # y
)
villain.set_size(.5)

villain.health = 10 # NEW
villain.say(villain.health) # NEW

villain_bg = codesters.Circle(
    villain.get_x(), # x
    villain.get_y(), # y
    175, # radius
    "darkred",
)


villain_bg.move_to_back()


hero = codesters.Sprite(
    "dolphin_thing_78b", # sprite
    -199, # x
    -199, # y
)

# hero.flip_right_left()
hero.set_size(.5)

hero.health = 10 # NEW
hero.say(hero.health) # NEW

hero_bg = codesters.Circle(
    hero.get_x(), # x
    hero.get_y(), # y
    175, # radius
    "lightblue",
)
hero_bg.move_to_back()

def left_key(sprite):
    sprite.move_left(20)
    hero_bg.go_to(
        sprite.get_x(),
        sprite.get_y(),
    )
    
hero.event_key("left", left_key)


def right_key(sprite):
    if sprite.get_x() < -50:
        sprite.move_right(20)
        hero_bg.go_to(
            sprite.get_x(),
            sprite.get_y(),
        )
    
hero.event_key("right", right_key)

def up_key(sprite):
    if sprite.get_y() < -50:
        sprite.move_up(20)
        hero_bg.go_to(
            sprite.get_x(),
            sprite.get_y(),
        )

hero.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)
    hero_bg.go_to(
        sprite.get_x(),
        sprite.get_y(),
    )
    
hero.event_key("down", down_key)

def normalize(x_speed, y_speed):
    hypotenuse = math.sqrt(
        x_speed ** 2 +
        y_speed ** 2
    )
    return (
        x_speed / hypotenuse,
        y_speed / hypotenuse
    )

snowballs = []

def prune():
    for one_snowball in snowballs:
        """
        if abs(one_snowball.get_x()) > 250
        or abs(one_snowball.get_y()) > 250: # NEWLINE
        """
        if any([
                abs(one_snowball.get_x()) > 250,
                abs(one_snowball.get_y()) > 250,
            ]):
            stage.remove_sprite(one_snowball)
            snowballs.remove(one_snowball)
    print(len(snowballs))

def collide(snowball, other): # NEW
    if other.get_image_name() == "evilwizard": # NEW
        other.health -= 1 # NEW
        other.say(other.health) # NEW
        stage.remove_sprite(snowball) # NEW

def make_snowball():
    snowball = codesters.Circle(
        hero.get_x(), # x
        hero.get_y(), # y
        25, # radius # CHANGED
        "white", # color # CHANGED
    )
    snowball.set_outline_color("black")
    snowballs.append(snowball)
    x_speed = stage.click_x() - hero.get_x()
    y_speed = stage.click_y() - hero.get_y()
    x_speed, y_speed = normalize(x_speed, y_speed)
    snowball.set_x_speed(x_speed * 5)
    snowball.set_y_speed(y_speed * 5)
    snowball.event_collision(collide) # NEW
    prune()

stage.event_click(make_snowball)


while hero.health > 0 and villain.health > 0:
    stage.wait(1)
    # make villain throw snowball
    
# this runs after the game is over
print("game over!")

"""
# Long Term Goals
  * Make the dolphin & background move
  * Make an end screen
  * Make a "fire" cooldown
  
# Stretch goals
  * Make moving obstacles that block snowballs
  * Make powerups
  * Make the circle change when character gets hit
"""
