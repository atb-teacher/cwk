stage.disable_all_walls()

villain = codesters.Sprite(
    "evilwizard", # sprite
    199, # x
    199, # y
)
villain.set_size(.5)


villain_bg = codesters.Circle(
    villain.get_x(), # x
    villain.get_y(), # y
    175, # radius
    "darkred",
)


villain_bg.move_to_back()


hero = codesters.Sprite(
    "dolphin_thing_78b", # sprite
    -199, # x
    -199, # y
)

# hero.flip_right_left()
hero.set_size(.5)
hero_bg = codesters.Circle(
    hero.get_x(), # x
    hero.get_y(), # y
    175, # radius
    "lightblue",
)
hero_bg.move_to_back()


def normalize(x_speed, y_speed):
    hypotenuse = math.sqrt(
        x_speed ** 2 +
        y_speed ** 2
    )
    return (
        x_speed / hypotenuse,
        y_speed / hypotenuse
    )

snowballs = [] # NEWLINE

def prune(): # NEWLINE
    for one_snowball in snowballs: # NEWLINE
        """
        if abs(one_snowball.get_x()) > 250
        or abs(one_snowball.get_y()) > 250: # NEWLINE
        """
        if any([ # NEWLINE
                abs(one_snowball.get_x()) > 250, # NEWLINE
                abs(one_snowball.get_y()) > 250, # NEWLINE
            ]): # NEWLINE
            stage.remove_sprite(one_snowball) # NEWLINE
            snowballs.remove(one_snowball) # NEWLINE
    print(len(snowballs)) # NEWLINE

def make_snowball():
    snowball = codesters.Circle(
        hero.get_x(), # x
        hero.get_y(), # y
        50, # radius
        "gray", # color
    )
    snowballs.append(snowball) # NEWLINE
    x_speed = (stage.click_x() - hero.get_x())
    y_speed = (stage.click_y() - hero.get_y())
    x_speed, y_speed = normalize(x_speed, y_speed)
    snowball.set_x_speed(x_speed * 5)
    snowball.set_y_speed(y_speed * 5)
    prune()

stage.event_click(make_snowball)



"""
# Goals
  * Make the dolphin & background move
  * Give them health
  * Make the snowwballs take away health
  * Make an end screen
  * Make a "fire" coldown
  
# Stretch goals
  * Make moving obstacles that block snowballs
  * Make powerups
  * Make the circle change when character gets hit
"""
