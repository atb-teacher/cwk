"""
Group Work Goals
* Give the hero and villain health
* create a function to allow the villain to throw snowballs
* create collision function(s) so that the characters can damage each other
"""


stage.disable_all_walls()

stage.set_background("winter")


villain = codesters.Sprite(
    "evilwizard", # sprite
    199, # x
    150, # y
)
villain.set_size(.5)


villain_bg = codesters.Circle(
    villain.get_x(), # x
    villain.get_y(), # y
    175, # radius
    "darkred",
)


villain_bg.move_to_back()


hero = codesters.Sprite(
    "dolphin_thing_78b", # sprite
    -199, # x
    -199, # y
)

# hero.flip_right_left()
hero.set_size(.5)
hero_bg = codesters.Circle(
    hero.get_x(), # x
    hero.get_y(), # y
    175, # radius
    "lightblue",
)
hero_bg.move_to_back()


def normalize(x_speed, y_speed):
    hypotenuse = math.sqrt(
        x_speed ** 2 +
        y_speed ** 2
    )
    return (
        x_speed / hypotenuse,
        y_speed / hypotenuse
    )

snowballs = []

def prune():
    for one_snowball in snowballs:
        """
        if abs(one_snowball.get_x()) > 250
        or abs(one_snowball.get_y()) > 250: # NEWLINE
        """
        if any([
                abs(one_snowball.get_x()) > 250,
                abs(one_snowball.get_y()) > 250,
            ]):
            stage.remove_sprite(one_snowball)
            snowballs.remove(one_snowball)
    print(len(snowballs))

def make_snowball():
    snowball = codesters.Circle(
        hero.get_x(), # x
        hero.get_y(), # y
        25, # radius # CHANGED
        "white", # color # CHANGED
    )
    snowball.set_outline_color("black") # NEW
    snowballs.append(snowball)
    x_speed = stage.click_x() - hero.get_x()
    y_speed = stage.click_y() - hero.get_y()
    x_speed, y_speed = normalize(x_speed, y_speed)
    snowball.set_x_speed(x_speed * 5)
    snowball.set_y_speed(y_speed * 5)
    prune()

stage.event_click(make_snowball)





"""
# Long Term Goals
  * Make the dolphin & background move
  * Give them health
  * Make the snowwballs take away health
  * Make an end screen
  * Make a "fire" coldown
  
# Stretch goals
  * Make moving obstacles that block snowballs
  * Make powerups
  * Make the circle change when character gets hit
"""


