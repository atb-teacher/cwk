hero_x = -50
hero_y = -100

user_click_x = input("What is the x pos of your click? ")
user_click_y = input("What is the y pos of your click? ")

print(
    "The snowball will start at", hero_x, hero_y
)

user_click_x = int(user_click_x)
user_click_y = int(user_click_y)

x_speed = user_click_x - hero_x
y_speed = user_click_y - hero_y

print(
    "The x/y speed will be", x_speed, y_speed
)

hypotenuse = math.sqrt(
    x_speed**2 + y_speed**2    
)

x_speed_normalized = x_speed / hypotenuse
y_speed_normalized = y_speed / hypotenuse

print(
    "The normalized x/y speed will be", 
    x_speed_normalized, y_speed_normalized
)



