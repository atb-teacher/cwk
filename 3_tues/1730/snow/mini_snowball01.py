tage.disable_all_walls()

hero = codesters.Sprite("alien1_masked", -100, 0)
hero.health = 10
hero.say(hero.health)

villain = codesters.Sprite("alien2", 100, 0)
villain.health = 10
villain.say(villain.health)


def collide(snowball, other_sprite):
    if other_sprite.get_image_name() != snowball.owner:
        other_sprite.health -= 1
        other_sprite.say(other_sprite.health)

def throw_snowball(_owner, direction=1):
    new_snowball = codesters.Circle(
        _owner.get_x(),
        _owner.get_y(),
        50,
        "gray",
    )
    new_snowball.owner = _owner.get_image_name()
    new_snowball.set_x_speed(2 * direction)
    new_snowball.event_collision(collide)
    
    
while True:
    stage.wait(1)
    throw_snowball(_owner=villain, direction=-1)
