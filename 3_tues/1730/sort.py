def generate_random_number_list():
    nums = list(range(101, 110))
    random.shuffle(nums)
    return nums
    
sprite = codesters.Sprite("alien1")
random_number_list = generate_random_number_list()
sprite.say(random_number_list)
