# =========== SETUP SPRITES =============

user_sprite = codesters.Sprite("alien2", -150, 0)
sprite = codesters.Sprite("alien1", 150, 0)

# =========== CHOICES =============
options = ["rock", "paper", "scissors"]


user_choice = None
while user_choice not in options:
    user_choice = sprite.ask("Choose: rock, paper, or scissors")

computer_choice = random.choice(options)

# =========== SHOW CHOICES =============

sprite.load_image(computer_choice)
user_sprite.load_image(user_choice)
sprite.say(f"Computer chose {computer_choice}", 2)

# =========== SEE WHO WON =============

rps_dictionary = {
    "rock": ["paper", "scissors"],
    "paper": ["scissors", "rock"],
    "scissors": ["rock", "paper"],
}

if user_choice == computer_choice:
    sprite.say("Draw!")

elif user_choice == rps_dictionary[computer_choice][0]:
    sprite.say("You win!")

elif user_choice == rps_dictionary[computer_choice][1]:
    sprite.say("You lose!")

