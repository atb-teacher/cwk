import random
sprite = codesters.Sprite("robot", 200, 200)
def make_snowball():

    
    robot_x = sprite.get_x()
    robot_y = sprite.get_y()
    
        
    # compare the click position to 0, 0
    """
    snowball_speed_x = stage.click_x()
    snowball_speed_y = stage.click_y()
    """
    
    # compare the click position to the robot's position
    snowball_speed_x = stage.click_x() - robot_x
    snowball_speed_y = stage.click_y() - robot_y
    
    snowball_speed_x = snowball_speed_x / 10
    snowball_speed_y = snowball_speed_y / 10
    
    # make the snowball come from the robot
    snowball = codesters.Circle(robot_x, robot_y, 10, "white")
    
    # make the snowball come from 0, 0
    # snowball = codesters.Circle(0, 0, 10, "white")
    
    snowball.set_outline_color("black")
    """
    snowball = codesters.Circle(
        0, # x position
        0, # y position
        10, # diameter
        "gray", # color
    )
    """
    snowball.set_x_speed(snowball_speed_x)
    snowball.set_y_speed(snowball_speed_y)
    stage.wait(.5)
    sprite.go_to(
            random.randint(-200, 200),
            random.randint(-200, 200),
        )
        
stage.event_click(make_snowball)
stage.disable_all_walls()

