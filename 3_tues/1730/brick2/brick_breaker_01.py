x_offset = 70
y_offset = 40

for i in range(0, 7):
    for j in range(0, 4):
        
        one_brick = codesters.Rectangle(
            -210 + (x_offset * i), # x
            150 - (y_offset * j), # y
            60, # width
            30, # height
            "red", # color
        )
        

paddle = codesters.Rectangle(
    0, # x
    -200, # y
    100, # width
    15, # height
    "blue", # color
)

ball = codesters.Circle(
    -25, # x
    -50, # y
    20, # diameter
    "yellow", # color
)

ball.set_outline_color("black")
ball.set_x_speed(1)
ball.set_y_speed(2)


"""
# STRETCH GOALS
* Stagger Offset variable
* Make "Make Bricks" function
"""



