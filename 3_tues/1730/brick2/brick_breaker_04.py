x_offset = 70
y_offset = 40
game_running = True

# ===== SETUP SCORE =====

score = 0

score_display = codesters.Text(
    "Score: 0", # text
    0, # x
    235, # y
    "black", # color
)

def update_score(score_change_amount):
    global score
    score += score_change_amount
    score_display.set_text(
        f"Score: {score}"
    )
    
# ===== SETUP DEATH BOX =====

death_box = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    20, # height
    "darkslategray", # color
)

# ===== SETUP BRICKS =====

rows = 4
cols = 7
for i in range(cols):
    for j in range(rows):
        
        one_brick = codesters.Rectangle(
            -210 + (x_offset * i), # x
            150 - (y_offset * j), # y
            60, # width
            30, # height
            "red", # color
        )
        

paddle = codesters.Rectangle(
    0, # x
    -200, # y
    100, # width
    15, # height
    "blue", # color
)

ball = codesters.Circle(
    -25, # x
    -50, # y
    20, # diameter
    "yellow", # color
)

ball.set_outline_color("black")
ball.set_x_speed(1)
ball.set_y_speed(2)

def bounce_off_paddle():
    # increase speed
    ball.set_y_speed(
        ball.get_y_speed * -1.3
    )
    abs_ball_speed = abs(ball.get_y_speed())
    if abs_ball_speed > 3:
        print("too fast!")
    
    
# ===== SETUP GAME OVER FUNCTION =====

def game_over():
    # TEMPORARY CODE
    print("Game over!!!")
    
# ===== SETUP BALL COLLISION =====

def ball_collision(_ball, _other_thing):
    other_thing_color = _other_thing.get_color()
    if other_thing_color == "red":
        # hitting a brick
        #bounce
        _ball.set_y_speed(
            _ball.get_y_speed() * -1
        )
        # remove brick
        stage.remove_sprite(_other_thing)
        update_score(1)
        
    elif other_thing_color == "blue":
        _ball.set_y_speed(
            _ball.get_y_speed() * -1
        )
    elif other_thing_color == "darkslategray":
        game_over()

ball.event_collision(ball_collision)

def left_key(sprite):
    sprite.move_left(20)

paddle.event_key("left", left_key)


def go_to_mouse():
    x_pos = stage.mouse_x()
    paddle.set_x(
        x_pos
    )
    
stage.event_mouse_move(go_to_mouse)


"""
# STRETCH GOALS
* Stagger Offset variable
* Make "Make Bricks" function
"""


