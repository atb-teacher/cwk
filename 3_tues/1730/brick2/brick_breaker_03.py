x_offset = 70
y_offset = 40

for i in range(0, 7):
    for j in range(0, 4):
        
        one_brick = codesters.Rectangle(
            -210 + (x_offset * i), # x
            150 - (y_offset * j), # y
            60, # width
            30, # height
            "red", # color
        )
        

paddle = codesters.Rectangle(
    0, # x
    -200, # y
    100, # width
    15, # height
    "blue", # color
)

ball = codesters.Circle(
    -25, # x
    -50, # y
    20, # diameter
    "yellow", # color
)

ball.set_outline_color("black")
ball.set_x_speed(1)
ball.set_y_speed(2)

def bounce_off_paddle():
    # increase speed
    ball.set_y_speed(
        ball.get_y_speed * -1.3
    )
    abs_ball_speed = abs(ball.get_y_speed())
    if abs_ball_speed > 3:
        print("too fast!")
    
    

def ball_collision(_ball, _other_thing):
    other_thing_color = _other_thing.get_color()
    if other_thing_color == "red":
        # hitting a brick
        #bounce
        _ball.set_y_speed(
            _ball.get_y_speed() * -1
        )
        # remove brick
        stage.remove_sprite(_other_thing)
        
    elif other_thing_color == "blue":
        _ball.set_y_speed(
            _ball.get_y_speed() * -1
        )

ball.event_collision(ball_collision)

def left_key(sprite):
    sprite.move_left(20)

paddle.event_key("left", left_key)

"""
def go_to_mouse():
    x_pos = stage.mouse_x()
    paddle.set_x(
        x_pos
    )
    
stage.event_mouse_move(go_to_mouse)
"""

"""
# STRETCH GOALS
* Stagger Offset variable
* Make "Make Bricks" function
"""


