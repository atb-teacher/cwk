# index_two_strings.py
import random

string1 = 'rickastley'
string2 = '_' * len(string1) # 10 underscores

output = ""
for index in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
  active_string = random.choice([string1, string2])
  output = output + active_string[index]

print(output)