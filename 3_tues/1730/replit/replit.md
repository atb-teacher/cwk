# Commands
  * `ls` to list files
    * `ls` is like looking at your file manager
  * `cd` to change directory
    * `cd` is like double clicking a folder
  * `clear` to clear the screen
    * can also be done with `ctrl-l`
  * `python` or `python3` to run a `.py` file
