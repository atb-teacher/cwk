sprite = codesters.Sprite("ballerina1", 0, 100)
sprite.set_size(.4)
sprite.grounded = False

# sprite = codesters.Rectangle(x, y, width, height, "color")
lava = codesters.Rectangle(0, -225, 500, 50, "red")
lava.set_gravity_off()
stage.set_gravity(10)

platform = codesters.Rectangle(0, -150, 100, 25, "blue")
platform.set_gravity_off()

def collision_function(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "red":
        sprite.say("Ouch!")

    if my_var == "blue":
        sprite.set_y_speed(0)
        sprite.set_gravity_off()
        sprite.grounded = True
        
sprite.event_collision(collision_function)


def space_bar_function_pizza(sprite):
    sprite.grounded = False
    sprite.set_y_speed(2)
    stage.wait(.5)
    sprite.set_gravity_on()
    sprite.set_y_speed(0)
    sprite.grounded = True
    """
    sprite.grounded = ??
    something about y speed
    something about gravity
    """

def space_bar_function_cheesecake(sprite):
    if sprite.grounded == True:
        sprite.grounded = False
        sprite.set_y_speed(10)
        sprite.set_gravity_on()

sprite.event_key("space", space_bar_function_pizza)



