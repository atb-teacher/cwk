stage.set_background("beach3")

# blueprint
class Question:
    def __init__(
        self, # NEW LINE
        _prompt,
        _a,
        _b,
        _c,
        _d,
        _answer,
        ):
        self.prompt = _prompt
        self.a = _a
        self.b = _b
        self.c = _c
        self.d = _d
        self.answer = _answer
        
question_list = [
    Question(
        _prompt = "How many MLB baseball teams are there?",
        _a = "18",
        _b = "20",
        _c = "25",
        _d = "30",
        _answer = "30",
    ),
    Question(
        _prompt = "How old is Saanvi?",
        _a = "12",
        _b = "13",
        _c = "14",
        _d = "15",
        _answer = "13",
    ),
    Question(
        _prompt = "How many games are on Nintendo 64?",
        _a = "393",
        _b = "412",
        _c = "465",
        _d = "519",
        _answer = "393",
    ),
]
        

