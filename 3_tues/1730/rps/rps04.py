player_sprite = codesters.Sprite(
    "alien1_masked",
    -150, # x
    0, # y
)
player_text = codesters.Text(
    "You",
    -150,
    75,
)

enemy_sprite = codesters.Sprite(
    "evilwizard",
    150, # x
    0, # y
)
enemy_sprite.set_size(.7)

enemy_text = codesters.Text(
    "Enemy",
    150,
    75,
)

weapons = ["rock", "paper", "scissors"]

user_weapon = stage.ask("Choose a weapon")
while user_weapon not in weapons:
    user_weapon = stage.ask("Choose a weapon")

enemy_weapon = random.choice(weapons)

user_weapon_sprite = codesters.Sprite(
    user_weapon,
    -150, # x
    150, # y
)

enemy_weapon_sprite = codesters.Sprite(
    enemy_weapon,
    150, # x
    150, # y
)

result_text = codesters.Text(
    "",
    0,
    -100,
)

if user_weapon == enemy_weapon:
    result_text.set_text("Tie!")

if user_weapon == "rock":
    if enemy_weapon == "paper":
        result_text.set_text("You lose!")
    elif enemy_weapon == "scissors":
        result_text.set_text("You win!")
