weapons = ["rock", "paper", "scissors"]

user_weapon = stage.ask("Choose a weapon")
while user_weapon not in weapons:
    user_weapon = stage.ask("Choose a weapon")

print(user_weapon)
enemy_weapon = random.choice(weapons)
print(f"Enemy chose {enemy_weapon}")

if user_weapon == "rock":
    if enemy_weapon == "paper":
        print("You lose")
