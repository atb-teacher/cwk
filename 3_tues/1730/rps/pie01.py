import random
seasons = ["strawberry", "blueberry"]
foods = ["strawberry pie", "blueberry pie"]

print(f"choose a pie: {foods}")
chosen_pie = input("What kind of pie do you want? ")
while chosen_pie not in foods:
  chosen_pie = input("What kind of pie do you want? ")
in_season = random.choice(seasons)
print(f"What's in season? {in_season}")

if in_season == "blueberry":
  if chosen_pie == "strawberry pie":
    print("Not in season, try blueberry!")
  elif chosen_pie == "blueberry pie":
    print("Good choice!")

elif in_season == "strawberry":
  if chosen_pie == "strawberry pie":
    print("Good choice!")
  elif chosen_pie == "blueberry pie":
    print("Try strawberry!")
