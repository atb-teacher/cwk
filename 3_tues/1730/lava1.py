sprite = codesters.Sprite("ballerina1", 0, 100)
sprite.set_size(.4)

# sprite = codesters.Rectangle(x, y, width, height, "color")
lava = codesters.Rectangle(0, -225, 500, 50, "red")
lava.set_gravity_off()
stage.set_gravity(10)

platform = codesters.Rectangle(0, -150, 100, 25, "blue")
platform.set_gravity_off()

def collision_function(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "red":
        sprite.say("Ouch!")
        
    if my_var == "green":
        sprite.say("Goal!")
    
    if my_var == "blue":
        sprite.set_y_speed(0)
        sprite.set_gravity_off()
        
    # add any other actions...
    
sprite.event_collision(collision_function)
