food_dictionary = {
    "Ezra": ["Hot dogs", "Cheese"],
    "Alex": ["Burritos", "Fish"],
    "Aaryan": ["Paneer", "Oranges"],
    "Ben": ["Mac 'n cheese", "Chili"],
}

user_name = input("What's your name? ")
user_food = input("What are you eating? ")
left_side_food = food_dictionary[user_name][0]
right_side_food = food_dictionary[user_name][1]
if user_food == left_side_food:
    print("Yum!")
elif user_food == right_side_food:
    print("Ugh!")


"""
1) Ask the user their name
  * Assume they type a name in the dictionary
2) Ask the user what they are eating
  * Assume they type a food in the dictionary
3) Tell them if they like that food or not
"""


"""
# print everything to the left of the colon
for one_key in food_dictionary.keys():
    print(one_key)

# print everything to the right of the colon
for one_value in food_dictionary.values():
    print(one_value)

# print everything to the right of the colon
for one_key in food_dictionary.keys():
    print(one_key)
    print(food_dictionary[one_key])
"""
