# ===== SETUP STAGE =====

# lets stuff pass through the edges of the screen
stage.disable_all_walls()
stage.set_gravity(3)


# ===== SETUP POTATO =====
potato = codesters.Sprite(
    "alien1",
    -150, # x
    -150, # y
)
potato.set_size(0.5)
potato.grounded = False

# ===== GROUNDED TEXT =====

grounded_text = codesters.Text(
    "Grounded: False",
    -170, # x
    200, # y
)

def refresh_grounded():
    grounded_text.set_text(
        f"Grounded: {potato.grounded}"
    )



floor = codesters.Rectangle(
    0, # x
    -250, # y
    1000, # width
    100, # height
    "darkslateblue", # color
)

floor.set_gravity_off()



def pop_up():
    potato.set_y(
        (potato.get_height()//2) + floor.get_top()
    )

def collision_potato(potato_sprite, other_thing):
    other_thing_color = other_thing.get_color()
    if other_thing_color == "darkslateblue":
        potato_sprite.set_y_speed(0)
        potato_sprite.set_gravity_off()
        pop_up()
        potato_sprite.grounded = True
        refresh_grounded()
        
potato.event_collision(collision_potato)


def jump_chicken(potato_sprite):
    potato_sprite.jump(7)
    potato_sprite.set_gravity_on()
    potato_sprite.grounded = False
    refresh_grounded()

potato.event_key("space", jump_chicken)

def make_obs():
    base_height = floor.get_top() + 25
    added_height = random.randint(0, 2) * 100
    height = base_height + added_height
    obstacle = codesters.Rectangle(
        150, # x
        height, # y
        50, # width
        50, # height
        "blue", # color
    )
    obstacle.set_gravity_off()
    obstacle.set_x_speed(-4)


while True: # game loop
    stage.wait(1)
    make_obs()

"""
* Prevent infinite jump
* Keep track of "grounded", if the alien is on the ground
* Make obstacles
"""

