while True:
    user_pet = input("What's better, cats or dogs?")
    if user_pet == "cats":
        print("correct!")
        break
    else:
        print("incorrect, try again")


"""
while True:
    user_number = input("Give me a number: ")
    if user_number.isdigit() == True:
        break

print(f"Your number was {user_number}")
"""


"""
for num in [0, 1, 2, 3, 4, 5, 6]:
    if num == 3:
        break
    print(num)
"""
