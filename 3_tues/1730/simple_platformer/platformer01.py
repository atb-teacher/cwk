potato = codesters.Sprite("alien1")
potato.set_size(0.5)

stage.set_gravity(3)

floor = codesters.Rectangle(
    0, # x
    -250, # y
    1000, # width
    100, # height
    "darkslateblue", # color
)

floor.set_gravity_off()

def collision_potato(potato_sprite, other_thing):
    other_thing_color = other_thing.get_color()
    if other_thing_color == "darkslateblue":
        potato_sprite.set_y_speed(0)
        potato_sprite.set_gravity_off()
        
potato.event_collision(collision_potato)