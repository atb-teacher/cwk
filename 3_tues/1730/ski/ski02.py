stage.set_background_color("white")
stage.disable_all_walls()

hero = codesters.Sprite(
    "ski_5b5", # image name
    0, # x
    150, # y
)

hero.set_size(0.15)

obs_strings = [
    "rock",
    "pinetree1",
    "pinetree3",
    "pinetree4",
    "pinetree7",
]

obstacles = []

def make_obstacle():
    one_obstacle = codesters.Sprite(
        random.choice(obs_strings), # image name
        500, # x
        500, # y
    )
    
    one_obstacle.set_size(
        random.randint(2, 4) * 0.1
    )
    
    one_obstacle.go_to(
        random.randint(-200, 200), # x
        -250, # y
    )
    
    one_obstacle.set_y_speed(2)
    
    obstacles.append(one_obstacle)

def left_key():
    for one_obstacle in obstacles:
        one_obstacle.move_right(20)
    
stage.event_key("left", left_key)


while True:
    make_obstacle()
    stage.wait(1)
