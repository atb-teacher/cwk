stage.set_background_color("white")
stage.disable_all_walls()

hero = codesters.Sprite(
    "ski_5b5", # image name
    0, # x
    150, # y
)

hero.set_size(0.15)

obs_strings = [
    "rock",
    "pinetree1",
    "pinetree3",
    "pinetree4",
    "pinetree7",
]

obstacles = []

def make_obstacle():
    one_obstacle = codesters.Sprite(
        random.choice(obs_strings), # image name
        500, # x
        500, # y
    )
    
    one_obstacle.set_size(
        random.randint(2, 4) * 0.1
    )
    
    one_obstacle.go_to(
        random.randint(-200, 200), # x
        -250, # y
    )
    
    one_obstacle.set_y_speed(2)
    
    obstacles.append(one_obstacle)

# ===== MOVEMENT =====

left_key_pressed = False
right_key_pressed = False

def left_key():
    global left_key_pressed
    left_key_pressed = True
    
stage.event_key("left", left_key)

def release_left(sprite):
    global left_key_pressed
    left_key_pressed = False

hero.event_key_release("left", release_left)

def right_key():
    global right_key_pressed
    right_key_pressed = True

stage.event_key("right", right_key)

def release_right(sprite):
    global right_key_pressed
    right_key_pressed = False
    
hero.event_key_release("right", release_right)

# ===== PRUNING =====

def prune_obstacles():
    for one_obstacle in obstacles:
        if one_obstacle.get_y() > 250:
            stage.remove_sprite(one_obstacle)
            obstacles.remove(one_obstacle)

# ===== MAIN LOOP =====

counter = 0
while True:
    counter = counter + 1
    stage.wait(0.05)
    if counter % 20 == 0:
        make_obstacle()
        print(len(obstacles))# TEMP CODE
        prune_obstacles()
    if left_key_pressed == right_key_pressed:
        x_speed = 0
    elif left_key_pressed == True:
        x_speed = 5
    else:
        x_speed = -5
    for one_obstacle in obstacles:
        one_obstacle.set_x_speed(x_speed)

