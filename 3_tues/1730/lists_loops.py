name_list = ['yena', 'aaryan', 'exaveer', 'ben']
favorite_color = ['black', 'red', 'blue', 'green']
favorite_sport = ['tennis', 'cricket', 'esports', 'none']

# index 4
name_list.append('rohan')
favorite_color.append('cyan')
favorite_sport.append('soccer')

# index 5
name_list.append('alex')
favorite_color.append('orange')
favorite_sport.append('starcraft')

for index in range(len(name_list)):
    print(f"{name_list[index]} likes {favorite_color[index]} and {favorite_sport[index]}")
'''
import pprint
letter_list = ['a', 'b', 'c', 'd', 'e', 'f']
number_list = [0, 10, 20, 30, 40, 50]
# for index in [0, 1, 2, 3]:
for index in range(len(letter_list)):
    print(letter_list[index], number_list[index])
'''

