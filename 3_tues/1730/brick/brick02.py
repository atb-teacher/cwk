# ===== MAKE OBJECTS =====

ball = codesters.Circle(0, 0, 30, "blue")

paddle = codesters.Rectangle(
    0, # x
    -200, # y
    100, # width
    20, # height
    "purple",
)

bottom = codesters.Rectangle(
    0, # x
    -250, # y
    500,
    10,
    "DarkSlateGray",
)

print(
    "\n".join(
        dir(
            paddle
        )
    )
)

# ===== MAKE BRICKS =====

for y_pos in range(125, 225 + 1, 25):
    for x_pos in range(-200, 200 + 1, 50):
        one_brick = codesters.Rectangle(
            x_pos, # x
            y_pos, # y
            40, # width
            18, # height
            "darkred",
        )

# ===== PADDLE MOVEMENT =====

def right_key_walle(sprite):
    if sprite.x < 200:
        sprite.move_right(20)
    
paddle.event_key("right", right_key_walle)

def left_key_aladdin(sprite):
    if sprite.x > -200:
        sprite.move_left(20)
    
paddle.event_key("left", left_key_aladdin)

# ===== START GAME =====

def bounce(in_ball, in_rect):
    if in_ball.y > in_rect.y:
        direction = 1
    else:
        direction = -1
        
    in_ball.set_y_speed(
        abs(
            in_ball.get_y_speed()
        ) * direction
    )
    

def collision_worldcup(ball, other_thing):
    other_thing_color = other_thing.get_color()
    if other_thing_color == "darkred":
        ball.say("I hit a brick")
        bounce(ball, other_thing)
        stage.remove_sprite(other_thing)
    if other_thing_color == "purple":
        ball.say("I hit the paddle")
        bounce(ball, other_thing)
    if other_thing_color == "DarkSlateGray":
        stage.remove_sprite(ball)

# def collision_worldcup_old(ball, other_thing):
#     other_thing_color = other_thing.get_color()
#     if other_thing_color == "darkred":
#         ball.say("I hit a brick")
#         ball.set_y_speed(
#             ball.get_y_speed() * -1
#         )
#     if other_thing_color == "purple":
#         ball.say("I hit the paddle")
#         ball.set_y_speed(
#             ball.get_y_speed() * -1
#         )

ball.event_collision(collision_worldcup)

ball.set_x_speed(-2)
ball.set_y_speed(-2)


