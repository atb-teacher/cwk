class CardPlace:
    def __init__(
        self,
        x_pos,
        y_pos,
        sprite_name,
    ):
        self.card = codesters.Rectangle(
            x_pos, # x
            y_pos, # y
            50, # width
            50, # height
            "blue",
        )
        self.sprite = codesters.Sprite(
            sprite_name,
            x_pos, # x
            y_pos, # y
        )
        self.sprite.set_size(.3)
        self.sprite.hide()
        self.hidden = "sprite"
        self.card.parent = self
        
# test_card = CardPlace(x_pos=100, y_pos=100, sprite_name="bat")

sprites = ["alien1", "blackcat", "butterfly"]

sprites = sprites * 2

random.shuffle(sprites)

print(sprites)

locations = []
# for x_pos in range(-150, 150 + 1, 150):
for x_pos in [-150, 0, 150]:
    for y_pos in [150, -50]:
        locations.append((x_pos, y_pos))

cards = []
for sprite, location in zip(sprites, locations):
    cards.append(
        CardPlace(
            x_pos = location[0],
            y_pos = location[1],
            sprite_name = sprite
        )
    )

clicked_cards = []

def click(sprite):
    global clicked_cards
    card_place = sprite.parent
    clicked_cards.append(card_place)
    if card_place.hidden == "sprite":
        card_place.hidden = "card"
        card_place.sprite.show()
        card_place.card.hide()
        stage.wait(2)
    if len(clicked_cards) > 1:
        print([c.sprite.get_name() for c in clicked_cards])
        if clicked_cards[0].sprite.get_name() == clicked_cards[1].sprite.get_name():
            print("good job!")
        clicked_cards = []
        for one_card in cards:
            one_card.sprite.hide()
            one_card.card.show()
            one_card.hidden = "sprite"

for one_card in cards:
    one_card.card.event_click(click)


