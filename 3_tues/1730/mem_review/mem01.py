class CardPlace:
    def __init__(
        self,
        x_pos,
        y_pos,
        sprite_name,
    ):
        self.card = codesters.Rectangle(
            x_pos, # x
            y_pos, # y
            50, # width
            50, # height
            "blue",
        )
        self.sprite = codesters.Sprite(
            sprite_name,
            x_pos, # x
            y_pos, # y
        )
        self.sprite.set_size(.3)
        self.sprite.hide()
        self.hidden = "sprite"
        self.card.parent = self


sprites = ["alien1", "blackcat", "butterfly"]

sprites = sprites * 2

random.shuffle(sprites)

print(sprites)

locations = []
for x_pos in [-150, 0, 150]:
    for y_pos in [150, -50]:
        locations.append((x_pos, y_pos))

print(
    list(zip(sprites, locations))
)

cards = []
for sprite, location in zip(sprites, locations):
    cards.append(
        CardPlace(
            x_pos = location[0],
            y_pos = location[1],
            sprite_name = sprite
        )
    )

