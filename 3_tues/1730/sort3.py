def swap(in_list, index1, index2):
    
    """Swaps items at index1 and index2"""
    
    in_list[index1], in_list[index2] = (
        in_list[index2],
        in_list[index1]
    )

def generate_random_number_list():
    
    """creates a shuffled list of numbers, 101 thru 110"""
    
    nums = list(range(101, 111))
    random.shuffle(nums)
    return nums
    

def generate_mostly_sorted_list():
    
    """creates a mostly sorted list of numbers
    with only two numbers out of order
    because of one swap
    the number at index 0 is always swapped
    with another random index""" 
    
    nums = list(range(101, 111))
    randindex = random.randrange(1, len(nums))
    swap(nums, 0, randindex)
    return nums
 
def simple_sort_new_list(unsorted_list):
    sorted_list = []
    while len(unsorted_list) > 0:
        smallest_num = unsorted_list[0]
        smallest_num_index = 0
        for index, num in enumerate(unsorted_list):
            if num < smallest_num:
                smallest_num = num
                smallest_num_index = index
        sorted_list.append(smallest_num)
        unsorted_list.pop(smallest_num_index)
    return sorted_list

sprite = codesters.Sprite("alien1")
random_number_list = generate_random_number_list()
sorted_list = simple_sort_new_list(random_number_list)
sprite.say(sorted_list)

