# ===== SETUP STAGE =====

stage.set_background("space2")

# ===== SETUP UFO =====

# controlled with WASD
ufo_sprite = codesters.Sprite(
    "ufo", # sprite name
    -200, # x
    200, # y
)
ufo_sprite.set_size(0.3)

# ===== SETUP SHIP =====

# controlled with arrow keys
ship_sprite = codesters.Sprite(
    "spaceship", # sprite name
    200, # x
    -200, # y
)

ship_sprite.set_size(0.3)

# ===== SETUP UFO CONTROLS =====

def left_key_ufo(in_ufo):
    in_ufo.set_rotation(90)
    in_ufo.move_left(20)

ufo_sprite.event_key("left", left_key_ufo)

def down_key_ufo(in_ufo):
    in_ufo.set_rotation(180)
    in_ufo.move_down(20)
    
ufo_sprite.event_key("down", down_key_ufo)

def right_key_ufo(in_ufo):
    in_ufo.set_rotation(-90)
    in_ufo.move_right(20)
    
ufo_sprite.event_key("right", right_key_ufo)

def up_key_ufo(in_ufo):
    in_ufo.set_rotation(0)
    in_ufo.move_up(20)
    
ufo_sprite.event_key("up", up_key_ufo)


