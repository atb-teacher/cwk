# ===== SETUP STAGE =====

stage.set_background("space2")
stage.disable_all_walls()
obs_list = []

# ===== SETUP UFO =====

# controlled with WASD
ufo_sprite = codesters.Sprite(
    "ufo", # sprite name
    -200, # x
    200, # y
)
ufo_sprite.set_size(0.3)
ufo_sprite.up_rot = 0
ufo_sprite.left_rot = 90
ufo_sprite.down_rot = 180
ufo_sprite.right_rot = 270
ufo_sprite.health = 5


# ===== SETUP SHIP =====

# controlled with arrow keys
ship_sprite = codesters.Sprite(
    "spaceship", # sprite name
    #200, # x
    #-200, # y
    0,
    0,
)

ship_sprite.set_size(0.3)
ship_sprite.offset = 45
ship_sprite.up_rot = 0 + ship_sprite.offset
ship_sprite.left_rot = 90 + ship_sprite.offset
ship_sprite.down_rot = 180 + ship_sprite.offset
ship_sprite.right_rot = -90 + ship_sprite.offset
ship_sprite.health = 5

# ===== SETUP HEALTH TEXT =====

ufo_health_text = codesters.Text(
    "UFO Health: 5",
    -100,# x
    220,# y
)
ufo_health_text.set_color("skyblue")

ship_health_text = codesters.Text(
    "Ship Health: 5",
    100,# x
    220,# y
)
ship_health_text.set_color("skyblue")

def update_health_text():
    ufo_health_text.set_text(
        f"UFO Health: {ufo_sprite.health}"
    )
    ship_health_text.set_text(
        f"Ship Health: {ship_sprite.health}"
    )

# ===== SETUP GAME OVER =====

def end_screen():
    end_text = codesters.Text(
        # EDIT THE LINE BELOW
        f"{ufo_sprite.health}, {ship_sprite.health < 0}",
        0,
        0,
    )
    end_text.set_size(3)
    end_text.set_color("skyblue")
        

def check_game_over():
    ufo_dead = ufo_sprite.health <= 0
    ship_dead = ship_sprite.health <= 0
    if ufo_dead or ship_dead:
        for one_obs in obs_list:
            stage.remove_sprite(one_obs)
        if ufo_dead:
            ufo_sprite.hide()
        if ship_dead:
            ship_sprite.hide()
        end_screen()

# ===== SETUP UFO CONTROLS =====

def left_key_ufo(in_ufo):
    in_ufo.set_rotation(in_ufo.left_rot)
    in_ufo.move_left(20)

ufo_sprite.event_key("a", left_key_ufo)

def down_key_ufo(in_ufo):
    in_ufo.set_rotation(in_ufo.down_rot)
    in_ufo.move_down(20)
    
ufo_sprite.event_key("s", down_key_ufo)

def right_key_ufo(in_ufo):
    in_ufo.set_rotation(in_ufo.right_rot)
    in_ufo.move_right(20)
    
ufo_sprite.event_key("d", right_key_ufo)

def up_key_ufo(in_ufo):
    in_ufo.set_rotation(in_ufo.up_rot)
    in_ufo.move_up(20)
    
ufo_sprite.event_key("w", up_key_ufo)

# ===== SETUP UFO LASER =====

def green_laser_funny101(green_laser, other_thing):
    
    other_thing_color = other_thing.get_color()
    if other_thing_color == "red":
        stage.remove_sprite(green_laser)
    elif other_thing.get_image_name() == "spaceship":
        stage.remove_sprite(green_laser)
        ship_sprite.health -= 1
        update_health_text()
        check_game_over()
        


def ufo_fire(in_ufo):
    green_laser = codesters.Sprite(
        "green_laser_34a", # image
        500, # x
        500, # y
    )
    green_laser.event_collision(green_laser_funny101)
    green_laser.set_size(0.2)
    green_laser.go_to(
        ufo_sprite.get_x(),
        ufo_sprite.get_y(),
    )
    ufo_rot = ufo_sprite.get_rotation()
    if ufo_rot == ufo_sprite.up_rot:
        green_laser.set_y_speed(-2)
    elif ufo_rot == ufo_sprite.down_rot:
        green_laser.set_y_speed(2)
    elif ufo_rot == ufo_sprite.right_rot:
        green_laser.set_x_speed(-2)
        green_laser.set_rotation(90)
    elif ufo_rot == ufo_sprite.left_rot:
        green_laser.set_x_speed(2)
        green_laser.set_rotation(90)


ufo_sprite.event_key("f", ufo_fire)

# ===== SETUP SHIP CONTROLS =====

def left_key_ship(in_ship):
    in_ship.set_rotation(in_ship.left_rot)
    in_ship.move_left(20)
    
ship_sprite.event_key("left", left_key_ship)

def right_key_ship(in_ship):
    in_ship.set_rotation(in_ship.right_rot)
    in_ship.move_right(20)
    
ship_sprite.event_key("right", right_key_ship)

def up_key_ship(in_ship):
    in_ship.set_rotation(in_ship.up_rot)
    in_ship.move_up(20)
    
ship_sprite.event_key("up", up_key_ship)

def down_key_ship(in_ship):
    in_ship.set_rotation(in_ship.down_rot)
    in_ship.move_down(20)
    
ship_sprite.event_key("down", down_key_ship)

# ===== SETUP SHIP LASER =====

codesters.Sprite("red_laser_19d")

"""
def green_laser_funny101(green_laser, other_thing):
    
    other_thing_color = other_thing.get_color()
    if other_thing_color == "red":
        stage.remove_sprite(green_laser)
    elif other_thing.get_image_name() == "spaceship":
        stage.remove_sprite(green_laser)
        ship_sprite.health -= 1
        update_health_text()
        check_game_over()
        


def ufo_fire(in_ufo):
    green_laser = codesters.Sprite(
        "green_laser_34a", # image
        500, # x
        500, # y
    )
    green_laser.event_collision(green_laser_funny101)
    green_laser.set_size(0.2)
    green_laser.go_to(
        ufo_sprite.get_x(),
        ufo_sprite.get_y(),
    )
    ufo_rot = ufo_sprite.get_rotation()
    if ufo_rot == ufo_sprite.up_rot:
        green_laser.set_y_speed(-2)
    elif ufo_rot == ufo_sprite.down_rot:
        green_laser.set_y_speed(2)
    elif ufo_rot == ufo_sprite.right_rot:
        green_laser.set_x_speed(-2)
        green_laser.set_rotation(90)
    elif ufo_rot == ufo_sprite.left_rot:
        green_laser.set_x_speed(2)
        green_laser.set_rotation(90)


ufo_sprite.event_key("f", ufo_fire)
"""

# ===== SETUP OBSTACLES =====

area = 180
for i in range(50): # run 50 times
    one_obs = codesters.Rectangle(
        random.randint(-1 * area, area), # x
        random.randint(-1 * area, area),# y
        random.randint(20, 30), # width
        random.randint(20, 30), # height
        "red", # color
    )
    obs_list.append(one_obs)

"""
TO DO:
* Work on endscreen
* Work on preventing collision

"""


