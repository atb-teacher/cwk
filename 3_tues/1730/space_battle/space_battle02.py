# ===== SETUP STAGE =====

stage.set_background("space2")

# ===== SETUP UFO =====

# controlled with WASD
ufo_sprite = codesters.Sprite(
    "ufo", # sprite name
    -200, # x
    200, # y
)
ufo_sprite.set_size(0.3)
ufo_sprite.up_rot = 0
ufo_sprite.left_rot = 90
ufo_sprite.down_rot = 180
ufo_sprite.right_rot = 270

# ===== SETUP SHIP =====

# controlled with arrow keys
ship_sprite = codesters.Sprite(
    "spaceship", # sprite name
    #200, # x
    #-200, # y
    0,
    0,
)

ship_sprite.set_size(0.3)
ship_sprite.offset = 45
ship_sprite.up_rot = 0 + ship_sprite.offset
ship_sprite.left_rot = 90 + ship_sprite.offset
ship_sprite.down_rot = 180 + ship_sprite.offset
ship_sprite.right_rot = -90 + ship_sprite.offset

# ===== SETUP UFO CONTROLS =====

def left_key_ufo(in_ufo):
    in_ufo.set_rotation(in_ufo.left_rot)
    in_ufo.move_left(20)

ufo_sprite.event_key("a", left_key_ufo)

def down_key_ufo(in_ufo):
    in_ufo.set_rotation(in_ufo.down_rot)
    in_ufo.move_down(20)
    
ufo_sprite.event_key("s", down_key_ufo)

def right_key_ufo(in_ufo):
    in_ufo.set_rotation(in_ufo.right_rot)
    in_ufo.move_right(20)
    
ufo_sprite.event_key("d", right_key_ufo)

def up_key_ufo(in_ufo):
    in_ufo.set_rotation(in_ufo.up_rot)
    in_ufo.move_up(20)
    
ufo_sprite.event_key("w", up_key_ufo)

"""
# TEMP CODE
def ufo_fire(in_ufo):
    green_laser = codesters.Sprite("green_laser_34a")


ufo_sprite.event_key("f", ufo_fire)
"""
# ===== SETUP SHIP CONTROLS =====

def left_key_ship(in_ship):
    in_ship.set_rotation(in_ship.left_rot)
    in_ship.move_left(20)
    
ship_sprite.event_key("left", left_key_ship)

# ===== SETUP OBSTACLES =====

area = 180
for i in range(50): # run 50 times
    one_obs = codesters.Rectangle(
        random.randint(-1 * area, area), # x
        random.randint(-1 * area, area),# y
        random.randint(20, 30), # width
        random.randint(20, 30), # height
        "red", # color
    )

"""
# Main goals
* Make the lasers
  red_laser = codesters.Sprite("red_laser_19d")
  green_laser = codesters.Sprite("green_laser_34a")
  * positioning it to where the UFO is
  * make it properly sized
  * angle the laser properly
* Give the UFO and ship health
  * let them take damage

# Advanced goal
  * health bar
"""

