import random
def random_word_fun():
    words = "apple aardvark arizona alligator".split()
    random_word = random.choice(words)
    return random_word

game_running = True

frank = codesters.Sprite("frankenstein", 180, 150)

guessed_letters = []
guessed_output = codesters.Text("", -150, 0)
active_word = random_word_fun()
presented_word = codesters.Text("", -150, 190)

while game_running == True:
    presented_word.set_text(active_word)
    output = "Guessed letters:\n" + ", ".join(guessed_letters)
    guessed_output.set_text(output)
    user_guess = frank.ask("Guess a letter: ")
    guessed_letters.append(user_guess)


