# =========== SETUP SPRITES =============

user_sprite = codesters.Sprite("alien2", -150, 0)
sprite = codesters.Sprite("alien1", 150, 0)

# =========== CHOICES =============

user_choice = sprite.ask("Choose: rock, paper, or scissors")
computer_choice = random.choice(["rock", "paper", "scissors"])

# =========== SHOW CHOICES =============

sprite.load_image(computer_choice)
user_sprite.load_image(user_choice)
sprite.say(f"Computer chose {computer_choice}", 2)

# =========== SEE WHO WON =============

if user_choice == computer_choice:
    sprite.say("Draw!")
    
elif computer_choice == "rock":
    # computer chooses rock
    if user_choice == "paper":
        sprite.say("I lose!")
    elif user_choice == "scissors":
        sprite.say("I win!")

elif computer_choice == "paper":
    # computer chooses paper
    if user_choice == "rock":
        sprite.say("I win!")
    elif user_choice == "scissors":
        sprite.say("You win!")

elif computer_choice == "scissors":
    # computer chooses scissors
    if user_choice == "rock":
        sprite.say("You win!")
    elif user_choice == "paper":
        sprite.say("I win!")
