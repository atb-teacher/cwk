sprite = codesters.Sprite("alien1")
user_choice = sprite.ask("Choose: rock, paper, or scissors")
computer_choice = random.choice(["rock", "paper", "scissors"])
sprite.say(f"Computer chose {computer_choice}", 2)

if computer_choice == "rock":
    # computer chooses rock
    if user_choice == "rock":
        sprite.say("Draw!")
    elif user_choice == "paper":
        sprite.say("I lose!")
    elif user_choice == "scissors":
        sprite.say("I win!")

elif computer_choice == "paper":
    # computer chooses paper
    if user_choice == "rock":
        sprite.say("I win!")
    elif user_choice == "paper":
        sprite.say("Draw!")
    elif user_choice == "scissors":
        sprite.say("You win!")

elif computer_choice == "scissors":
    if user_choice == "rock":
        sprite.say("You win!")
    elif user_choice == "paper":
        sprite.say("I win!")
    elif user_choice == "scissors":
        sprite.say("Draw!")
