def swap(in_list, index1, index2):
    in_list[index1], in_list[index2] = (
        in_list[index2],
        in_list[index1]
    )

def generate_random_number_list():
    nums = list(range(101, 111))
    print("before shuffle", nums)
    random.shuffle(nums)
    print("after shuffle", nums)
    return nums
    

def generate_mostly_sorted_list():
    nums = list(range(101, 111))
    randindex = random.randrange(1, len(nums))
    swap(nums, 0, randindex)
    return nums
 
def simple_sort_new_list(unsorted_list):
    sorted_list = []
    smallest_num = unsorted_list[0]
    smallest_num_index = 0
    for index, num in enumerate(unsorted_list):
        if num < smallest_num:
            smallest_num = num
            smallest_num_index = index
    sorted_list.append(smallest_num)
    unsorted_list.pop(smallest_num_index)
    return sorted_list

sprite = codesters.Sprite("alien1")

mostly_sorted_list = generate_mostly_sorted_list()
small_sorted_list = simple_sort_new_list(mostly_sorted_list)

sprite.say(small_sorted_list)
