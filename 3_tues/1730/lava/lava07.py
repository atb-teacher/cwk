stage.set_gravity(10)

# ========== MAKE THE HERO ========== 

hero = codesters.Sprite(
    "alien2", # sprite
    0, # x
    100, # y
)
hero.set_size(0.4)
hero.alive = True
hero.score = 0

# ========== SETUP "GROUNDED" ========== 

hero.grounded = False

grounded_text = codesters.Text(
    f"grounded: {hero.grounded}",
    -175, # x
    200, # y
)

def update_grounded_text():
    grounded_text.set_text(f"grounded: {hero.grounded}")
    
# ========== SETUP SCORE ========== 

score_text = codesters.Text(
    f"score: {hero.score}",
    -175, # x
    175, # y
)

def update_score():
    score_text.set_text(f"score: {hero.score}")

# ========== SETUP LAVA ========== 

lava = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    50, # height
    "red", # color
)
lava.set_gravity_off()

# ========== SETUP 1ST PLATFORM ========== 

platform = codesters.Rectangle(
    0, # x
    0, # y
    100, # width
    20, # height
    "blue"
)

platform.set_gravity_off()
platform.set_x_speed(1)
platform.set_y_speed(-.3)

# ========== SETUP ALIEN COLLISION ========== 

def alien_collision(alien, other_thing):
    
    # if touching blue platform
    platform_color = other_thing.get_color() == "blue"
    
    # if at least 25 pixels above platform
    alien_above = alien.y - 25 > other_thing.y
    
    # if the alien is going down
    falling = alien.get_y_speed() < 0
    
    # if all the above are true and the alien isn't grounded
    if platform_color and alien_above and falling and not alien.grounded:
        alien.grounded = True
        update_grounded_text()
        alien.set_gravity_off()
        while alien.grounded:
            alien.set_x_speed(
                other_thing.get_x_speed()
            )
            alien.set_y_speed(
                other_thing.get_y_speed()
            )
            stage.wait(0.1)
            
    # if touching lava
    if other_thing.get_color() == "red":
        stage.remove_sprite(alien)
        hero.alive = False
        
hero.event_collision(alien_collision)

# ========== SETUP PLATFORM COLLISION ========== 

def platform_collision(platform, other_thing):
    if other_thing.get_color() == "red":
        stage.remove_sprite(platform)
        # if hero.alive:
        if hero.alive == True:
            hero.score += 1
            update_score()

platform.event_collision(platform_collision)

# ========== SETUP JUMP ========== 

def space_bar(sprite):
    if sprite.grounded == True:
        sprite.grounded = False # new code
        update_grounded_text() # new code
        sprite.jump(15)
        sprite.set_gravity_on()

hero.event_key("space", space_bar)

# ========== MAIN GAME LOOP ========== 

# while hero.alive
while hero.alive == True:
    
    # ========== MAKE A PLATFORM ========== 
    
    platform = codesters.Rectangle(
        random.randint(-3, 3) * 20, # x
        175, # y
        100, # width
        20, # height
        "blue"
    )
    
    platform.set_gravity_off()
    
    platform.set_x_speed(random.choice([-2, -1, 1, 2]))
    platform.set_y_speed(-1)
    platform.event_collision(platform_collision)
    
    stage.wait(2) # WAIT 2 SECONDS

print(hero.score)

score_text = codesters.Text(
    f"Your score was {hero.score}",
    0, # x
    0, # y
)
score_text.set_size(2)



