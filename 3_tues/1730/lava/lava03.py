stage.set_gravity(10)

hero = codesters.Sprite(
    "alien2", # sprite
    0, # x
    100, # y
)
hero.set_size(0.4)
hero.grounded = False

grounded_text = codesters.Text(
    f"grounded: {hero.grounded}",
    -175,
    200,
)

lava = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    50, # height
    "red", # color
)
lava.set_gravity_off()

platform = codesters.Rectangle(
    0, # x
    0, # y
    100, # width
    20, # height
    "blue"
)

platform.set_gravity_off()

platform.set_x_speed(1)
platform.set_y_speed(-1)

def alien_collision(alien, other_thing):
    if other_thing.get_color() == "blue":
        alien.set_gravity_off()
        alien.set_x_speed(
            other_thing.get_x_speed()
        )
        alien.set_y_speed(
            other_thing.get_y_speed()
        )
    if other_thing.get_color() == "red":
        stage.remove_sprite(alien)
        
hero.event_collision(alien_collision)

def platform_collision(platform, other_thing):
    if other_thing.get_color() == "red":
        stage.remove_sprite(platform)

platform.event_collision(platform_collision)

def space_bar(sprite):
    sprite.jump(15)
    sprite.set_gravity_on()

hero.event_key("space", space_bar)
