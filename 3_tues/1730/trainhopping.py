alien = codesters.Sprite("alien1")
alien.set_size(.2)

"""
def left_key(sprite):
    sprite.move_left(20)
    # add other actions...
    
alien.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)
    # add other actions...
    
alien.event_key("right", right_key)
"""

def left_key(sprite):
    sprite.set_x(sprite.get_x() - 20)
    sprite.say(sprite.get_x())

alien.event_key("left", left_key)

def right_key(sprite):
    sprite.set_x(sprite.get_x() + 20)
    sprite.say(sprite.get_x())

alien.event_key("right", right_key)



