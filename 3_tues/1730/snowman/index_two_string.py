# index_two_strings.py
import random

string1 = 'abcdefg'
string2 = "ABCDEFG"

output = ""
for index in [0, 1, 2, 3, 4, 5, 6]:
  active_string = random.choice([string1, string2])
  output = output + active_string[index]

print(output)
