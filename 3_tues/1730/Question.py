class Question:
    def __init__(self, in_q, in_a, in_b, in_c, in_d):
        self.question = in_q
        self.a = in_a
        self.b = in_b
        self.c = in_c
        self.d = in_d
        self.correct_answer = None

    def set_correct_answer(in_answer):
        self.correct_answer = in_answer
