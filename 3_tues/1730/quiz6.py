stage.set_background_color('black')
sprite = codesters.Sprite("alien1", 0, -200)

score = 0
question = codesters.Text("test", 0, 180, "lightblue")
a = codesters.Text("A:", -150, 50, "lightblue")
b = codesters.Text("B:", 50, 50, "lightblue")
c = codesters.Text("C:", -150, -50, "lightblue")
d = codesters.Text("D:", 50, -50, "lightblue")
display_questions = [a, b, c, d]

class Question:
	def __init__(self, in_question, in_a, in_b, in_c, in_d, in_answer):
		self.question = in_question
		self.a = in_a
		self.b = in_b
		self.c = in_c
		self.d = in_d
		self.answer = in_answer

question_list = []
question_list.append(
	Question(
		"How many seasons are there?", "2", "3", "4", "5", "c"
	)
)

question_list.append(
	Question(
		"What is my teacher's name?", "Axel", "Abby", "Ack", "Alex", "d"
	)
)

def ask_questions():
    global score
	for one_question in question_list:
		a.set_text(one_question.a)
		b.set_text(one_question.b)
		c.set_text(one_question.c)
		d.set_text(one_question.d)
		question.set_text(one_question.question)
		stage.wait(2)

ask_questions()
