class Dog:
  def __init__(self, gender, size, color, cuteness_level):
    self.gender = gender
    self.size = size
    self.color = color
    self.cuteness_level = cuteness_level
    

Bertie = Dog(gender="boy", size="small", color="white", cuteness_level=6)

Potato = Dog(gender="boy", size="big", color="white", cuteness_level=11)

Teddy = Dog(gender="boy", size="medium", color="black", cuteness_level=10)

Ms_Puggles = Dog(gender="girl", size="small", color="blonde", cuteness_level=11)
