alien = codesters.Sprite(
    "alien1_masked", # image
    0, # x
    0, # y
)

alien.set_size(.3)

def left_key_pasta(sprite):
    sprite.set_x(
        sprite.get_x() - 20
    )
    
alien.event_key("left", left_key_pasta)

def right_key_beetroot(sprite):
    sprite.set_x(
        sprite.get_x() + 20
    )
    
alien.event_key("right", right_key_beetroot)

def up_key_sushi(sprite):
    sprite.set_y(
        sprite.get_y() + 20
    )

alien.event_key("up", up_key_sushi)

def down_key_tacos(sprite):
    sprite.set_y(
        sprite.get_y() - 20
    )
    
alien.event_key("down", down_key_tacos)

"""
y_start = 50
y_separation = 85


t1 = codesters.Rectangle(
    50, # x
    y_start + y_separation * 0, # y
    25, # width
    75, # height
    "blue",
)

t2 = codesters.Rectangle(
    50, # x
    y_start + y_separation * 1, # y
    25, # width
    75, # height
    "blue",
)

t3 = codesters.Rectangle(
    50, # x
    y_start + y_separation * 2, # y
    25, # width
    75, # height
    "blue",
)
"""

y_start = 50
y_separation = 85

for num in [0, 1, 2]:
    new_car = codesters.Rectangle(
        50, # x
        y_start + y_separation * num, # y
        25, # width
        75, # height
        "blue",
    )
    # temp code
    new_car.set_y_speed(-5)
    


