stage.disable_all_walls()

alien = codesters.Sprite(
    "alien1_masked", # image
    -200, # x
    0, # y
)

alien.set_size(.3)

def alien_collision_andor(_alien, _other_thing):
    if all(
        [hasattr(_other_thing, "dangerous"),
        _other_thing.dangerous == True,
        ]
    ):
        alien.hide()
    if _other_thing.get_color() == "blue":
        alien.hide()
        
alien.event_collision(alien_collision_andor)

def left_key_pasta(sprite):
    sprite.set_x(
        sprite.get_x() - 20
    )
    
alien.event_key("left", left_key_pasta)

def right_key_beetroot(sprite):
    sprite.set_x(
        sprite.get_x() + 20
    )
    
alien.event_key("right", right_key_beetroot)

def up_key_sushi(sprite):
    if alien.get_y() < 100:
        sprite.set_y(
            sprite.get_y() + 20
        )

alien.event_key("up", up_key_sushi)

def down_key_tacos(sprite):
    if alien.get_y() > -100:
        sprite.set_y(
            sprite.get_y() - 20
        )
    
alien.event_key("down", down_key_tacos)

"""
y_start = 50
y_separation = 85


t1 = codesters.Rectangle(
    50, # x
    y_start + y_separation * 0, # y
    25, # width
    75, # height
    "blue",
)

t2 = codesters.Rectangle(
    50, # x
    y_start + y_separation * 1, # y
    25, # width
    75, # height
    "blue",
)

t3 = codesters.Rectangle(
    50, # x
    y_start + y_separation * 2, # y
    25, # width
    75, # height
    "blue",
)
"""

def make_train_down(
        color,
        x_pos = 0,
    ):
    y_start = 300
    y_separation = 85
    
    for num in [0, 1, 2]:
        new_car = codesters.Rectangle(
            x_pos, # x
            y_start + y_separation * num, # y
            25, # width
            75, # height
            color,
        )
        new_car.dangerous = True
        # temp code
        new_car.set_y_speed(-10)
        

def make_train_up(
        color,
        x_pos = 0,
    ):
    y_start = -300
    y_separation = 85
    
    for num in [0, 1, 2]:
        new_car = codesters.Rectangle(
            x_pos, # x
            y_start - y_separation * num, # y
            25, # width
            75, # height
            color,
        )
        new_car.dangerous = True
        # temp code
        new_car.set_y_speed(10)

while True:
    stage.wait(.5)
    make_train_up(
        random.choice(
            ["red", "green", "purple", "pink", "skyblue"]
        ),
        x_pos = random.randint(-4, 4) * 40,
    )
    stage.wait(.5)
    make_train_down(
        random.choice(
            ["red", "green", "purple", "pink", "skyblue"]
        ),
        x_pos = random.randint(-4, 4) * 40,
    )
