stage.disable_all_walls()

game_running = True

alien = codesters.Sprite(
    "alien1_masked", # image
    -200, # x
    0, # y
)

alien.set_size(.3)

alien.health = 3
alien.invincible = False

health_text = codesters.Text(
    f"Health: {alien.health}",
    -200, # x
    200, # y
)

goal_area = codesters.Rectangle(
    250, # x
    0, # y
    50, # width
    500, # height
    "lightgreen", # color
)

goal_area.finish = True

candy = codesters.Sprite(
    "candy",
    random.randint(-4, 4) * 40,
    random.randint(-2, 2) * 40,
)
candy.set_size(.3)


def update_health():
    health_text.set_text(f"Health: {alien.health}")

def alien_collision_andor(_alien, _other_thing):
    global game_running
    if hasattr(_other_thing, "dangerous"):
        if _other_thing.dangerous == True:
            if alien.invincible == False:
                alien.invincible = True
                alien.health -= 1 # remove one health
                if alien.health <= 0:
                    game_running = False
                update_health()
                stage.wait(2)
                alien.invincible = False
    if hasattr(_other_thing, "finish"):
        if _other_thing.finish == True:
            game_running = False
    if _other_thing.get_image_name() == "candy":
        alien.health += 2
        update_health()
        candy.go_to(
                random.randint(-4, 4) * 40,
                random.randint(-2, 2) * 40,
        )
        
        
alien.event_collision(alien_collision_andor)

def left_key_pasta(sprite):
    sprite.set_x(
        sprite.get_x() - 20
    )
    
alien.event_key("left", left_key_pasta)

def right_key_beetroot(sprite):
    sprite.set_x(
        sprite.get_x() + 20
    )
    
alien.event_key("right", right_key_beetroot)

def up_key_sushi(sprite):
    if alien.get_y() < 100:
        sprite.set_y(
            sprite.get_y() + 20
        )

alien.event_key("up", up_key_sushi)

def down_key_tacos(sprite):
    if alien.get_y() > -100:
        sprite.set_y(
            sprite.get_y() - 20
        )
    
alien.event_key("down", down_key_tacos)

def make_train_down(
        color,
        x_pos = 0,
    ):
    y_start = 300
    y_separation = 85
    
    for num in [0, 1, 2]:
        new_car = codesters.Rectangle(
            x_pos, # x
            y_start + y_separation * num, # y
            25, # width
            75, # height
            color,
        )
        new_car.dangerous = True
        # temp code
        new_car.set_y_speed(-10)
        

def make_train_up(
        color,
        x_pos = 0,
    ):
    y_start = -300
    y_separation = 85
    
    for num in [0, 1, 2]:
        new_car = codesters.Rectangle(
            x_pos, # x
            y_start - y_separation * num, # y
            25, # width
            75, # height
            color,
        )
        new_car.dangerous = True
        # temp code
        new_car.set_y_speed(10)

while game_running == True:
    stage.wait(.5)
    make_train_up(
        random.choice(
            ["red", "green", "purple", "pink", "skyblue"]
        ),
        x_pos = random.randint(-4, 4) * 40,
    )
    stage.wait(.5)
    make_train_down(
        random.choice(
            ["red", "green", "purple", "pink", "skyblue"]
        ),
        x_pos = random.randint(-4, 4) * 40,
    )

background_rect = codesters.Rectangle(
    0,
    0,
    300,
    100,
    "gray",
)
background_rect.set_opacity(.5)

finish_text = codesters.Text(
    "",
    0,
    0,
)


finish_text.set_size(3)
goal_area.hide()


if alien.health > 0:
    stage.set_background("beach2")
    finish_text.set_text("You win!")
else:
    stage.set_background("hauntedhouseinterior")
    finish_text.set_text("You lose!")

