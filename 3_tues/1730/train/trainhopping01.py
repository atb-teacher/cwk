alien = codesters.Sprite(
    "alien1_masked", # image
    0, # x
    0, # y
)

alien.set_size(.3)

def left_key_pasta(sprite):
    sprite.set_x(
        sprite.get_x() - 20
    )
    
alien.event_key("left", left_key_pasta)

def right_key_beetroot(sprite):
    sprite.set_x(
        sprite.get_x() + 20
    )
    
alien.event_key("right", right_key_beetroot)

def up_key_sushi(sprite):
    sprite.set_y(
        sprite.get_y() + 20
    )

alien.event_key("up", up_key_sushi)

def down_key_tacos(sprite):
    sprite.set_y(
        sprite.get_y() - 20
    )
    
alien.event_key("down", down_key_tacos)
