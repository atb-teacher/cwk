stage.disable_all_walls()

alien = codesters.Sprite(
    "alien1_masked", # image
    -200, # x
    0, # y
)

alien.set_size(.3)

alien.health = 10
alien.invincible = False

health_text = codesters.Text(
    f"Health: {alien.health}",
    -200, # x
    200, # y
)

def update_health():
    health_text.set_text(f"Health: {alien.health}")

def alien_collision_andor(_alien, _other_thing):
    if hasattr(_other_thing, "dangerous"):
        if _other_thing.dangerous == True:
            if alien.invincible == False:
                alien.invincible = True
                alien.health -= 1 # remove one health
                update_health()
                stage.wait(2)
                alien.invincible = False

        
alien.event_collision(alien_collision_andor)

def left_key_pasta(sprite):
    sprite.set_x(
        sprite.get_x() - 20
    )
    
alien.event_key("left", left_key_pasta)

def right_key_beetroot(sprite):
    sprite.set_x(
        sprite.get_x() + 20
    )
    
alien.event_key("right", right_key_beetroot)

def up_key_sushi(sprite):
    if alien.get_y() < 100:
        sprite.set_y(
            sprite.get_y() + 20
        )

alien.event_key("up", up_key_sushi)

def down_key_tacos(sprite):
    if alien.get_y() > -100:
        sprite.set_y(
            sprite.get_y() - 20
        )
    
alien.event_key("down", down_key_tacos)

def make_train_down(
        color,
        x_pos = 0,
    ):
    y_start = 300
    y_separation = 85
    
    for num in [0, 1, 2]:
        new_car = codesters.Rectangle(
            x_pos, # x
            y_start + y_separation * num, # y
            25, # width
            75, # height
            color,
        )
        new_car.dangerous = True
        # temp code
        new_car.set_y_speed(-10)
        

def make_train_up(
        color,
        x_pos = 0,
    ):
    y_start = -300
    y_separation = 85
    
    for num in [0, 1, 2]:
        new_car = codesters.Rectangle(
            x_pos, # x
            y_start - y_separation * num, # y
            25, # width
            75, # height
            color,
        )
        new_car.dangerous = True
        # temp code
        new_car.set_y_speed(10)

while True:
    stage.wait(.5)
    make_train_up(
        random.choice(
            ["red", "green", "purple", "pink", "skyblue"]
        ),
        x_pos = random.randint(-4, 4) * 40,
    )
    stage.wait(.5)
    make_train_down(
        random.choice(
            ["red", "green", "purple", "pink", "skyblue"]
        ),
        x_pos = random.randint(-4, 4) * 40,
    )
