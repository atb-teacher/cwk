stage.disable_all_walls()

# ========== SETUP ALIEN ==========
alien = codesters.Sprite("alien1", -200, 0)
alien.set_size(.2)

# ========== SETUP BRAIN ==========
brain = codesters.Circle(100, 100, 10, "pink")

# ========== SETUP LIVES ==========
lives = 1
lives_text = codesters.Text(f"lives: {lives}", -200, 200)

# ========== SETUP MOVEMENT ==========
def left_key(sprite):
    sprite.set_x(sprite.get_x() - 20)
    sprite.say(sprite.get_x())

alien.event_key("left", left_key)

def right_key(sprite):
    sprite.set_x(sprite.get_x() + 20)
    sprite.say(sprite.get_x())

alien.event_key("right", right_key)

def up_key(sprite):
    sprite.set_y(sprite.get_y() + 20)
    
alien.event_key("up", up_key)

def down_key(sprite):
    sprite.set_y(sprite.get_y() - 20)
    
alien.event_key("down", down_key)

# ========== SETUP COLLISION ==========
def collision(me, not_me):
    global lives
    not_me_color = not_me.get_color() 
    if not_me_color == "blue": # collide with train
        me.say("Ouch!")
        me.set_height(5)
    elif not_me_color == "pink": # collide with brain
        lives += 1
        me.set_height(me.get_height() + 5)
        lives_text.set_text(f"lives: {lives}")

# ========== SETUP TRAIN ==========
def make_train(x_pos):
    train = []
    
    for y_pos in [300, 350, 400]: # runs three times
        # each train will have a different y position
        train.append(codesters.Rectangle(x_pos, y_pos, 20, 45, "blue"))

    for train_car in train:
        # make every blue rectangle fall
        train_car.set_y_speed(-5)

    return train


    
alien.event_collision(collision)

while True:
    # every one second, create a train
    # with a random left-right position
    stage.wait(1)
    make_train(random.choice([-100, 0, 100]))
    

