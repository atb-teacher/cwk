words = [
    "roblox",
    "graveyard",
    "fire",
    "rickastley",
    #"supercalifragilisticexpialidocious",
    #"pneumonoultramicroscopicsilicovolcanoconiosis",
]

guesses = 6
guess_text = codesters.Text(
    f"Guesses: {guesses}", -195, 220
)

answer = random.choice(words)
print(answer)
representation = []
for one_letter in answer:
    representation.append("_")

representation_text = codesters.Text(
    " ".join(representation), 0, -100,
    # representation, 0, -100,
)

sprite = codesters.Sprite("alien1", 200, 0)

while "_" in representation and guesses >= 1:
    user_guess = sprite.ask("Guess a letter")
    
    if user_guess in answer:
        sprite.say("Correct!")
        for i in range(len(answer)):
            if answer[i] == user_guess:
                representation[i] = user_guess
        representation_text.set_text(" ".join(representation))
    else:
        guesses = guesses - 1
        print("guesses: ", guesses)
        guess_text.set_text(f"Guesses: {guesses}")
