tongue = codesters.Sprite("alien1", -200, 100)
mask = codesters.Sprite("alien1_masked", 0, 100)
tall = codesters.Sprite("alien2", 200, 100)

stage.disable_all_walls()

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "red":
        sprite.say("Ouch!")
        
    if my_var == "blue":
        sprite.say("I'm healed!")
        

for one_alien in [tongue, mask, tall]:
# for one_alien in [mask, tall]:
    one_alien.set_size(.5)
    one_alien.event_collision(collision)


# for x_pos in range(-200, 201, 200):
for x_pos in [-200, 0, 200]:
    laser = codesters.Rectangle(x_pos, -200, 10, 40,
        random.choice(["red", "blue"]))
    laser.set_y_speed(20)

