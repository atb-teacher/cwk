import random
sprite = codesters.Sprite("ballerina1", 0, 100)
sprite.set_size(.4)
sprite.grounded = False
blue_platforms = []
# sprite = codesters.Rectangle(x, y, width, height, "color")
lava = codesters.Rectangle(0, -225, 500, 50, "red")
lava.set_gravity_off()

stage.set_gravity(10)

platform = codesters.Rectangle(0, -50, 250, 25, "blue")
platform.set_gravity_off()
platform.set_x_speed(-2)
platform.set_y_speed(-2)
blue_platforms.append(platform)

def collision_function(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "red":
        sprite.say("Ouch!")

    if my_var == "blue":
        if sprite.get_y_speed() < 0:
            sprite.set_y_speed(0)
            sprite.set_gravity_off()
            sprite.grounded = True
            sprite.set_x_speed(platform.get_x_speed())
            sprite.set_y_speed(platform.get_y_speed())
        
sprite.event_collision(collision_function)

def collision_function_platform(in_platform, hit_sprite):
    if hit_sprite.get_color() == "red":
        stage.remove_sprite(in_platform)
        blue_platforms.remove(in_platform)

platform.event_collision(collision_function_platform) # new

def space_bar_function_cheesecake(sprite):
    if sprite.grounded == True:
        sprite.grounded = False
        sprite.set_y_speed(15)
        sprite.set_x_speed(0)
        sprite.set_gravity_on()

sprite.event_key("space", space_bar_function_cheesecake)

while True:
    stage.wait(1)
    if len(blue_platforms) < 3:
        platform = codesters.Rectangle(random.randint(-100, 100), 100, 250, 25, "blue")
        blue_platforms.append(platform)
        platform.set_gravity_off()
        platform.set_x_speed(-2)
        platform.set_y_speed(-2)
        platform.event_collision(collision_function_platform) # new
