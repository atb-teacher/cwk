# ===== SETUP GAME =====

stage.set_gravity(5)
game_running = False

# ===== SETUP FLOOR/CEILING =====

floor = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    25, # height
    "orange", # color
)
floor.set_gravity_off()

ceiling = codesters.Rectangle(
    0, # x
    250, # y
    500, # width
    25, # height
    "orange", # color
)
ceiling.set_gravity_off()

# ===== SETUP "CLICK TO START" TEXT =====

start_text = codesters.Text(
    "tap or click to start\nthe game",
    0, # x
    100, # y
)

# ===== SETUP HERO =====

hero = codesters.Sprite("astronaut4")
hero.set_gravity_off()
hero.set_y_speed(0)
hero.flip_right_left()
hero.set_size(0.3)


def click():
    global game_running
    if game_running == False:
        game_running = True
        hero.set_gravity_on()
        stage.remove_sprite(start_text)
        game_loop()

stage.event_click(click)

def make_obstacles():
    pass

def game_loop():
    while True:
        stage.wait(1)
        make_obstacles()

"""
# sprite = codesters.Rectangle(x, y, width, height, "color")
"""

