# ===== SETUP GAME =====

stage.set_gravity(5)
game_running = False
previous_y = 0
stage.disable_all_walls()
high_score = 0

# ===== SETUP FLOOR/CEILING =====

floor = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    25, # height
    "orange", # color
)
floor.set_gravity_off()

ceiling = codesters.Rectangle(
    0, # x
    250, # y
    500, # width
    25, # height
    "orange", # color
)
ceiling.set_gravity_off()

# ===== SETUP "CLICK TO START" TEXT =====

start_text = codesters.Text(
    "tap or click to start\nthe game",
    0, # x
    100, # y
)

# ===== SETUP SCORE TEXT =====

score_text = codesters.Text(
    "Score: 10",
    -150, # x
    210, # y
)

def update_score():
    score_text.set_text(
        f"Score: {hero.score}"
    )

# ===== SETUP HERO =====

hero = codesters.Sprite("astronaut4")
hero.set_gravity_off()
hero.set_y_speed(0)
hero.flip_right_left()
hero.set_size(0.3)
hero.score = 10 # NEW CODE

def collision(sprite, other_thing):
    other_thing_color = other_thing.get_color()
        
    if other_thing_color == "orange":
        end_game()

        
hero.event_collision(collision)

def end_game():
    hero.hide()
    hero.set_gravity_off()
    hero.set_y_speed(0)
    hero.set_y(0)
    global game_running
    game_running = False
    score_text.hide()
    start_text.show()
    start_text.set_text(
        f"""Your score was {hero.score}
Click to start again"""
    )
    start_text.move_to_front()



def click():
    global game_running
    if game_running == False:
        game_running = True
        score_text.show()
        hero.score = 10
        update_score()
        hero.show()
        hero.set_gravity_on()
        start_text.hide()
        game_loop()
    else:
        hero.jump(5)
        hero.score = hero.score + 10
        score_text.move_to_front()
        update_score()

stage.event_click(click)

def make_obstacles(speed, gap_size_lower, gap_size_upper):
    global previous_y
    new_y = previous_y + random.randint(-50, 50)
    print(new_y)
    if -150 < new_y < 150:
        previous_y = new_y # NEW CODE
    gap_size = random.randint(gap_size_lower, gap_size_upper)
    upper_gap = new_y + gap_size//2
    lower_gap = new_y - gap_size//2
    upper_rect_length = abs(250 - upper_gap)
    lower_rect_length = abs(-250 - lower_gap)
    upper_rect = codesters.Rectangle(
        250, # x # CHANGED
        250 - (upper_rect_length // 2 ), # y
        250, # width
        upper_rect_length, # height
        "orange",
    )
    upper_rect.set_gravity_off()
    lower_rect = codesters.Rectangle(
        250, # x # CHANGED
        -250 + (lower_rect_length // 2), # y
        250, # width
        lower_rect_length, # height
        "orange",
    )
    lower_rect.set_gravity_off()
    upper_rect.set_x_speed(-1 * speed)
    lower_rect.set_x_speed(-1 * speed)
    score_text.move_to_front()
    start_text.move_to_front()
    


def game_loop():
    global game_running
    while game_running == True:
        stage.wait(1)
        make_obstacles(
            speed=5,
            gap_size_lower=200,
            gap_size_upper=300,
        )

"""

* Create a score that goes up
  * Create a text object for that

"""


