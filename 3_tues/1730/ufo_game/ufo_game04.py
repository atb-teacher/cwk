# ===== SETUP GAME =====

stage.set_gravity(5)
game_running = False
previous_y = 0
stage.disable_all_walls()

# ===== SETUP FLOOR/CEILING =====

floor = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    25, # height
    "orange", # color
)
floor.set_gravity_off()

ceiling = codesters.Rectangle(
    0, # x
    250, # y
    500, # width
    25, # height
    "orange", # color
)
ceiling.set_gravity_off()

# ===== SETUP "CLICK TO START" TEXT =====

start_text = codesters.Text(
    "tap or click to start\nthe game",
    0, # x
    100, # y
)

# ===== SETUP HERO =====

hero = codesters.Sprite("astronaut4")
hero.set_gravity_off()
hero.set_y_speed(0)
hero.flip_right_left()
hero.set_size(0.3)


def collision(sprite, other_thing):
    other_thing_color = other_thing.get_color()
        
    if other_thing_color == "orange":
        sprite.hide()
        sprite.set_gravity_off()
        sprite.set_y_speed(0)
        sprite.set_y(0)
        global game_running
        game_running = False
        
hero.event_collision(collision)


def click():
    global game_running
    if game_running == False:
        game_running = True
        hero.show()
        hero.set_gravity_on()
        stage.remove_sprite(start_text)
        game_loop()
    # NEW CODE
    else:
        hero.jump(5)

stage.event_click(click)

def make_obstacles(speed, gap_size_lower, gap_size_upper):
    global previous_y
    new_y = previous_y + random.randint(-50, 50)
    print(new_y)
    if -150 < new_y < 150:
        previous_y = new_y # NEW CODE
    gap_size = random.randint(gap_size_lower, gap_size_upper)
    upper_gap = new_y + gap_size//2
    lower_gap = new_y - gap_size//2
    upper_rect_length = abs(250 - upper_gap)
    lower_rect_length = abs(-250 - lower_gap)
    upper_rect = codesters.Rectangle(
        250, # x # CHANGED
        250 - (upper_rect_length // 2 ), # y
        250, # width
        upper_rect_length, # height
        "orange",
    )
    upper_rect.set_gravity_off()
    lower_rect = codesters.Rectangle(
        250, # x # CHANGED
        -250 + (lower_rect_length // 2), # y
        250, # width
        lower_rect_length, # height
        "orange",
    )
    lower_rect.set_gravity_off()
    upper_rect.set_x_speed(-1 * speed)
    lower_rect.set_x_speed(-1 * speed)
    


def game_loop():
    global game_running
    while game_running == True:
        stage.wait(1)
        make_obstacles(
            speed=5,
            gap_size_lower=460,
            gap_size_upper=500,
        )

"""

* Terrain to tome from the right
* Click to make the astronaut jump
* Create a score that goes up
  * Create a text object for that

"""

