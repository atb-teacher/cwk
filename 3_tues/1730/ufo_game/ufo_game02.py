# ===== SETUP GAME =====

stage.set_gravity(5)
game_running = False
previous_y = 0

# ===== SETUP FLOOR/CEILING =====

floor = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    25, # height
    "orange", # color
)
floor.set_gravity_off()

ceiling = codesters.Rectangle(
    0, # x
    250, # y
    500, # width
    25, # height
    "orange", # color
)
ceiling.set_gravity_off()

# ===== SETUP "CLICK TO START" TEXT =====

start_text = codesters.Text(
    "tap or click to start\nthe game",
    0, # x
    100, # y
)

# ===== SETUP HERO =====

hero = codesters.Sprite("astronaut4")
hero.set_gravity_off()
hero.set_y_speed(0)
hero.flip_right_left()
hero.set_size(0.3)


def click():
    global game_running
    if game_running == False:
        game_running = True
        hero.set_gravity_on()
        stage.remove_sprite(start_text)
        game_loop()

stage.event_click(click)

def make_obstacles():
    global previous_y
    new_y = previous_y + random.randint(-50, 50)
    gap_size = random.randint(200, 300)
    upper_gap = new_y + gap_size//2
    lower_gap = new_y - gap_size//2
    upper_rect_length = abs(250 - upper_gap)
    lower_rect_length = abs(-250 - lower_gap )
    print(upper_rect_length, lower_rect_length)
    upper_rect = codesters.Rectangle(
        0, # x # change later
        250 - (upper_rect_length // 2 ), # y
        50, # width
        upper_rect_length, # height
        "orange",
    )
    upper_rect.set_gravity_off()
    lower_rect = codesters.Rectangle(
        0, # x # change later
        -250 + (lower_rect_length // 2), # y
        50, # width
        lower_rect_length, # height
        "orange",
    )
    lower_rect.set_gravity_off()
    upper_rect.set_x_speed(-1)
    lower_rect.set_x_speed(-1)


def game_loop():
    
    while True:
        stage.wait(1)
        make_obstacles()

"""

* Terrain to tome from the right
* Click to make the astronaut jump
* Create a score that goes up
  * Create a text object for that

"""

