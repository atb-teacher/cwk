words = [
    "roblox",
    "graveyard",
    "fire",
    "rickastley",
    "supercalifragilisticexpialidocious",
    "pneumonoultramicroscopicsilicovolcanoconiosis",
]

answer = random.choice(words)

representation = "_ " * len(answer)

representation_text = codesters.Text(
    representation, 0, -100,    
)

sprite = codesters.Sprite("alien1", 200, 0)

user_guess = sprite.ask("Guess a letter")

if user_guess in answer:
    sprite.say("Correct!")
    # update the representation variable
    # hint: look at update_lines.py
    representation_text.set_text(representation)




