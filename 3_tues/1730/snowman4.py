"""
words = [
    "roblox",
    "graveyard",
    "fire",
    "rickastley",
    #"supercalifragilisticexpialidocious",
    #"pneumonoultramicroscopicsilicovolcanoconiosis",
]

guesses = 6
guess_text = codesters.Text(
    f"Guesses: {guesses}", -195, 220
)

answer = random.choice(words)
print(answer)
representation = []
for one_letter in answer:
    representation.append("_")

representation_text = codesters.Text(
    " ".join(representation), 0, -100,
    # representation, 0, -100,
)

sprite = codesters.Sprite("alien1", 200, 0)

while "_" in representation and guesses >= 1:
    user_guess = sprite.ask("Guess a letter")
    
    if user_guess in answer:
        sprite.say("Correct!")
        for i in range(len(answer)):
            if answer[i] == user_guess:
                representation[i] = user_guess
        representation_text.set_text(" ".join(representation))
    else:
        guesses = guesses - 1
        print("guesses: ", guesses)
        guess_text.set_text(f"Guesses: {guesses}")

print("representation", representation)
print("guesses", guesses)

end_text = codesters.Text("", 0, 0)

if guesses > 0:
    end_text.set_text("You win!")
else:
    end_text.set_text("You lose!")
"""
def draw_lower_body():
    lower_body = codesters.Circle(
        -125, # x pos
        0, # y pos
        70, # size
        "white",
    )
    lower_body.set_outline_color("black")

draw_lower_body()

def draw_upper_body():
    upper_body = codesters.Circle(
        -125, # x pos
        50, # y pos
        60, # size
        "white",
    )
    upper_body.set_outline_color("black")

draw_upper_body()

def draw_head():
    head = codesters.Circle(
        -125,
        100,
        50,
        "white",
    )
    head.set_outline_color("black")

draw_head()

# replace all the parts that say "pass" with making shapes

def draw_hat():
    pass

draw_hat()

def draw_nose():
    pass

draw_nose()

def draw_eye():
    head = codesters.Circle(
        -110,
        100,
        5,
        "black",
    )
    head.set_outline_color("black")

draw_eye()
