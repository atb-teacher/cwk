"""
Goals:
4) The hero's health goes down if they get hit by the snowball
"""

hero = codesters.Sprite(
    "superhero2", # sprite
    -200, # x
    -190, # y
)

hero.set_size(.5)
hero.health = 5
hero.say(hero.health)


villain = codesters.Sprite(
    "evilwizard", # sprite
    200, # x
    140, # y
)

villain.set_size(.6)


villain.health = 5
villain.say(villain.health)



def left_key_minecraft(sprite):
    sprite.move_left(20)
    
hero.event_key("left", left_key_minecraft)

def right_key_roblox(sprite_fortnite):
    if sprite_fortnite.get_x() < 0:
        sprite_fortnite.move_right(20)
    
hero.event_key("right", right_key_roblox)

def up_key_minecraft(sprite):
    if sprite.get_y() < 0:
        sprite.move_up(20)
    
hero.event_key("up", up_key_minecraft)

def down_key_minecraft(sprite):
    sprite.move_down(20)
    
hero.event_key("down", down_key_minecraft)



def normalize(x, y):
    hyp = math.sqrt(
        x**2 + y**2
    )
    return x/hyp, y/hyp

def hero_snowball_collide(snowball, other):
    if other.get_name() == "evilwizard":
        other.health -= 1
        other.say(other.health)
 
def villain_make_snowball():
    # make a circle where the villain is
    # give the circle x/y speed
    
    villain_x = villain.get_x()
    villain_y = villain.get_y()
    
    snowball = codesters.Circle(
        villain_x,
        villain_y,
        10, # diameter
        "white",
    )
    
    snowball.set_outline_color("black")
    
    snowball.set_x_speed(-5)
    snowball.set_y_speed(-5)
    

def villain_snowball_collide():
    # the collision function for the villain's snowball
    pass

def make_snowball():
    
    hero_x = hero.get_x()
    hero_y = hero.get_y()
    
    snowball_speed_x = stage.click_x() - hero_x
    snowball_speed_y = stage.click_y() - hero_y
    
    snowball_speed_x, snowball_speed_y = normalize(
        snowball_speed_x, snowball_speed_y
    )
    
    snowball = codesters.Circle(
        hero_x, # start x
        hero_y, # start y
        10, # diameter
        "white", # color
    )
    
    snowball.event_collision(
        hero_snowball_collide   
    )
    snowball.set_outline_color("black")
    snowball.set_x_speed(snowball_speed_x * 5)
    snowball.set_y_speed(snowball_speed_y * 5)

stage.event_click(make_snowball)

stage.disable_all_walls()

while True:
    stage.wait(1)
    villain_make_snowball()
