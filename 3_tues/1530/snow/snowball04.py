hero = codesters.Sprite(
    "superhero2", # sprite
    -200, # x
    -190, # y
)

hero.set_size(.5)

villain = codesters.Sprite(
    "evilwizard", # sprite
    200, # x
    190, # y
)

villain.set_size(.6)

def left_key_minecraft(sprite):
    sprite.move_left(20)
    
hero.event_key("left", left_key_minecraft)

def right_key_roblox(sprite_fortnite):
    if sprite_fortnite.get_x() <0:
        sprite_fortnite.move_right(20)
    
hero.event_key("right", right_key_roblox)

def up_key_minecraft(sprite):
    if sprite.get_y() < 0:
        sprite.move_up(20)
    
hero.event_key("up", up_key_minecraft)

def down_key_minecraft(sprite):
    sprite.move_down(20)
    
hero.event_key("down", down_key_minecraft)

def make_snowball():
    
    hero_x = hero.get_x()
    hero_y = hero.get_y()
    
    snowball_speed_x = stage.click_x() - hero_x
    snowball_speed_y = stage.click_y() - hero_y
    
    snowball = codesters.Circle( # new
        hero_x, # start x
        hero_y, # start y
        10, # diameter
        "white", # color
    )
    snowball.set_outline_color("black")
    snowball.set_x_speed(snowball_speed_x)
    snowball.set_y_speed(snowball_speed_y)

stage.event_click(make_snowball)
