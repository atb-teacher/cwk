bricks = []
for y_pos in [200, 180, 160, 140]:
    for x_pos in [-200, -150, -100, -50, 0, 50, 100, 150, 200]:
        bricks.append(codesters.Rectangle(
            x_pos,# x pos
            y_pos,# y pos
            45,# width
            15,# height
            "darkred",
        ))

ball = codesters.Circle(
        3,    # x
        -100, # y
        25,   # diameter
        "black",
)

ball.set_x_speed(2) # new
ball.set_y_speed(1) # new

def collision(ball_mario, rect_minions):
    rect_color = rect_minions.get_color() 
    
    if rect_color == "darkred":
        stage.remove_sprite(rect_minions)
        ball_mario.set_y_speed(
            ball_mario.get_y_speed() * -1
        )
    
ball.event_collision(collision)

paddle = codesters.Rectangle(
    0, # x
    -200,# y
    120,# width
    20,# height
    "blue",# color
)

def left_key_frozen2(sprite_obi_wan):
    sprite_obi_wan.move_left(20)

paddle.event_key("left", left_key_frozen2)


def right_key_henry(sprite_danger):
    sprite_danger.move_right(20)

paddle.event_key("right", right_key_henry)


# Unnecessary Code
def say_position_minecraft(sprite_zelda):
    print(sprite_zelda.get_x())
    sprite_zelda.set_color(
        random.choice(["blue", "green", "yellow"])
    )
    
paddle.event_key("a", say_position_minecraft)


