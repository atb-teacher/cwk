stage.set_background("winter")

stage.disable_all_walls()

all_flakes = []

def random_flake():
    options = [
        "snowflake1",
        "snowflake2",
        "snowflake3",
        # "frosted_flake_c23",
        # "FrostedFLakes_eda",
    ]
    return random.choice(options)

while True:
    stage.wait(.2)
    new_flake = codesters.Sprite(
        random_flake(), # name
        random.randint(-500, 500), # x
        250, # y
    )
    all_flakes.append(new_flake)
    new_flake.set_y_speed(-1)
    wind = random.choice([-3, 0, 3])
    for one_flake in all_flakes:
        one_flake.set_x_speed(
            wind + random.randint(-1, 1)
        )


