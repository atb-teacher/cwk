# blueprint
class Question:
    def __init__(self, prompt, a, b, c, d, answer):
        self.prompt = prompt
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.answer = answer
        
question_list = [
    Question(
        prompt = "Which of these countries borders Romania?",
        a = "Poland",
        b = "Moldova",
        c = "Turkiye",
        d = "Tanzania",
        answer = "Moldova",
    ),
    Question(
        prompt = "How many subspecies of tigers are there?",
        a = "9",
        b = "250",
        c = "3",
        d = "17",
        answer = "9",
    ),
    Question(
        prompt = "Which college owns scratch?",
        a = "Yale",
        b = "Texas Tech",
        c = "Stanford",
        d = "MIT",
        answer = "MIT",
    ),
]

