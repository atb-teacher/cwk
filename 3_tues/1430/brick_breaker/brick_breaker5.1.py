bricks = []
for y_pos in [200, 180, 160, 140]:
    for x_pos in [-200, -150, -100, -50, 0, 50, 100, 150, 200]:
        bricks.append(codesters.Rectangle(
            x_pos,# x pos
            y_pos,# y pos
            45,# width
            15,# height
            "darkred",
        ))

ball = codesters.Circle(
        3,    # x
        -100, # y
        25,   # diameter
        "black",
)

ball.set_x_speed(2) # new
ball.set_y_speed(1) # new

def collision_the_strokes(ball_mario, rect_minions):
    if rect_minions.get_color() == "darkred":
        stage.remove_sprite(rect_minions) # remove
        new_y_speed = ball_mario.get_y_speed() * -1 # get new vel
        ball_mario.set_y_speed(new_y_speed) # apply new vel
    if rect_minions.get_color() == "blue":
        new_y_speed = ball_mario.get_y_speed() * -1 # get new vel
        ball_mario.set_y_speed(new_y_speed) # apply new vel

    
    # temp
    # ball_mario.say("I hit you!")
    # rect_minions.say("I got hit!")

ball.event_collision(collision_the_strokes)

paddle = codesters.Rectangle(
    0, # x
    -200,# y
    120,# width
    20,# height
    "blue",# color
)

def left_key_frozen2(sprite_obi_wan):
    sprite_obi_wan.move_left(20)

paddle.event_key("left", left_key_frozen2)


def right_key_henry(sprite_danger):
    sprite_danger.move_right(20)

paddle.event_key("right", right_key_henry)




