# Introductions

1) Your name
2) (optional) Where you are from
3) Your experience with coding
4) One piece of media you like (a book, a movie, a tv show, a video game) and why you like it

# This Class
## Schedule

We will have

| Time         | Section     |
|--------------|-----------|
| Intros |    12:00 - 12:10   |
| Python      |  12:10 - 12:50 |
| Break #1      | 12:50 - 1:00 |
| Web dev|  1:10 - 1:50 |
| Break #2      | 1:50 - 2:00 |
| End stuff | 2:00 - 2:30 |

* replit
* codepen

# REPLIT account
# Commands
  * `ls` to list files
    * `ls` is like looking at your file manager
  * `cd` to change directory
    * `cd` is like double clicking a folder
  * `clear` to clear the screen
    * can also be done with `ctrl-l`
  * `python` or `python3` to run a `.py` file


# HTML, CSS, JS

* html is the "skeleton"
* js is the "muscle"
* css is the "fur"

* html mostly places rectangles
* css mostly makes the rectangles look nice
* js does programming stuff to the rectangles
