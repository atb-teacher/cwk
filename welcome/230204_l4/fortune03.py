options = """
Absolutely!
Never!
It will not happen
Ask again later
Perhaps
"""

options = options.strip('\n').split('\n')

user_choice = input("Ask your question: ")

# version 1
random_number = random.randint(
    0, # it starts with index 0
    len(options) - 1 # the highest index we can use
)

print(options[random_number])

"""
# version 2
print(
    random.choice(
        options
    )
)

# version 3
random_number = random.randrange(
    len(options) # the highest index we can use
)

print(options[random_number])
"""
