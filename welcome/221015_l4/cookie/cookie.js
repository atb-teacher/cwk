let cookies = 0;
let cookieSpan = document.getElementById("cookie-num");
let cookiePic = document.getElementById("cookie");

function increaseCookie() {
  cookies++;
  cookieSpan.innerText = cookies;
}

cookiePic.onclick = increaseCookie;

setInterval(increaseCookie, 1000);
