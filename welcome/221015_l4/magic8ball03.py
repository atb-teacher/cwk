import random
import time

possible_responses = [
  "yes",
  "no",
  "ask a friend",
  "try again",
]

# print(possible_responses)

user_question = input("Type your question\n> ")

for dotnum in [1, 2, 3, 4]:
  print("." * dotnum)
  time.sleep(.2)

print(
  random.choice(
    possible_responses 
  )
)
