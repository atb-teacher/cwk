possible_responses = [
  "yes",
  "no",
  "ask a friend",
  "try again",
]

print(possible_responses)

responses_str = """
yes
no
ask a friend
try again
"""

possible_responses = responses_str.split("\n")

print(possible_responses)
