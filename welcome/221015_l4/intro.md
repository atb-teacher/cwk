# Introductions

1) Your name
2) (optional) Where you are from
3) Your experience with coding
4) One piece of media you like (a book, a movie, a tv show, a video game) and why you like it

# This Class
## Schedule

We will have

| Time         | Section     |
|--------------|-----------|
| Intros |    1:00 - 1:10   |
| Python      |  1:10 - 1:50 |
| Break #1      | 1:50 - 2:00 |
| Web dev|  2:10 - 2:50 |
| Break #2      | 2:50 - 3:00 |
| End stuff | 3:00 - 3:30 |

* replit
* codepen

# REPLIT account

# HTML, CSS, JS

* html is the "skeleton"
* js is the "muscle"
* css is the "fur"

* html mostly places rectangles
* css mostly makes the rectangles look nice
* js does programming stuff to the rectangles