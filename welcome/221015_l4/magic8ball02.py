import random

possible_responses = [
  "yes",
  "no",
  "ask a friend",
  "try again",
]

# print(possible_responses)

user_question = input("Type your question\n> ")

print(
  random.choice(
    possible_responses 
  )
)
