sprite = codesters.Sprite("alien1_masked")

# If you see : you are making a rule
# indented code follows the rule

if 1 == 0:
    sprite.say(
        "a", # what they say
        2, # how much time
    )
    
if 1 == 1:
    sprite.say(
        "b", # what they say
        2, # how much time
    )
    
counter = 0
# repeat as long as counter
# does not equal 3
while counter != 3:
    sprite.say(counter)
    stage.wait(1)
    counter = counter + 1