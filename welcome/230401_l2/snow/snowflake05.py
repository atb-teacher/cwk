stage.set_background("winter")
stage.disable_all_walls()

my_snowman = codesters.Sprite(
    "snowman", # image name
    0, # x
    -120, # y
)

my_snowman.set_size(0.8)

def move_left_minecraft():
    my_snowman.move_left(10)
    
stage.event_key("left", move_left_minecraft)

while True: # forever loop

    stage.wait(1)

    my_snowflake = codesters.Sprite(
        "snowflake1", # image name
        random.randint(-200, 200), # x
        300, # y
    )
    
    my_snowflake.set_size(0.8)
    
    my_snowflake.set_y_speed(-2)
    

