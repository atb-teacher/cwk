stage.set_background("winter")
stage.disable_all_walls()

while True: # forever loop

    stage.wait(1)

    my_snowflake = codesters.Sprite(
        "snowflake1", # image name
        random.randint(-200, 200), # x
        300, # y
    )
    
    my_snowflake.set_size(0.8)
    
    my_snowflake.set_y_speed(-2)
    
    
