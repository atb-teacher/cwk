package space.codekingdoms.alexteacher8.magicwand2;

import com.codekingdoms.nozzle.utils.Random;
import com.codekingdoms.nozzle.utils.Direction;
import java.lang.Math;
import java.lang.Integer;
import java.lang.Float;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import com.codekingdoms.nozzle.utils.ProjectileType;
import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	
	public void onUseItem() {
		
		ItemStack wand = new ItemStack(Material.STICK);
		if (getItemInMainHand().equals(wand)) {
			
			throwProjectile(ProjectileType.LARGE_FIREBALL);
			
		}
		
	
	}
	
	
}
