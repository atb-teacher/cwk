PLAYER:

package space.codekingdoms.alexteacher8.zombiefight;

import com.codekingdoms.nozzle.utils.ArmorSet;
import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.Material;

public class Player extends BasePlayer {
	
	public void onRunCommand( String command, String[] args ) {
		
		if (command.equals("/start")) {
			getGame().currPlayer = this;
			equipFullArmorSet(ArmorSet.DIAMOND);
			equipItem(Material.DIAMOND_SWORD);
			getGame().startGame();
			
		}
		
	
	}
	
	
}


GAME:

package space.codekingdoms.alexteacher8.zombiefight;

import com.codekingdoms.nozzle.base.BaseGame;
import org.bukkit.Location;

public class Game extends BaseGame {
	
	public Player currPlayer;
	
	public void spawnZombie() {
		
		Zombie z = new Zombie();
		z.spawn(world, currPlayer.getLocation());
	
	}
	
	public void startGame() {
		
		removeAllMobs();
		disableMobSpawning();
		int zombieCount = 10;
		for (int i=0; i<zombieCount; i++ ) {
			setTimeout(
			() -> {
				spawnZombie();
			}, i*5);
		}
	}
}

ZOMBIE:

package space.codekingdoms.alexteacher8.zombiefight;

import com.codekingdoms.nozzle.base.BaseZombie;
import org.bukkit.Material;

public class Zombie extends BaseZombie {
	
	public void onSpawn() {
		
		setBaby(true);
		setMaxHealth(1);
		equipItem(Material.WOOD_SWORD);
	
	}
	
	
}
