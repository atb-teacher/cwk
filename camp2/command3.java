package space.codekingdoms.alexteacher8.advancedcommands;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Player extends BasePlayer {
	
	public void onRunCommand( String command, String[] args ) {
		
		if (command.equals("/strike")) {
			
			world.strikeLightning(getTargetBlock().getLocation());
			
		}
		
		else if (command.equals("/invis")) {
			
			applyPotionEffect(PotionEffectType.INVISIBILITY, 600, 1);
			
		}
		
		//Super speed
		else if (command.equals("/speed")) {
			
			setWalkSpeed(0.5f);
			
		}
		
		//Full food and health
		else if (command.equals("/heal")) {
			
			setFoodLevel(20);
			setHealth(20);
			
		}
		
		//Get a sword
		else if (command.equals("/sword")) {
			
			addItemToInventory(new ItemStack(Material.DIAMOND_SWORD));
			
		}
		
	
	}
	
	
}
