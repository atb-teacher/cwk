package space.codekingdoms.alexteacher8.commandexample;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Player extends BasePlayer {
	
	public void onRunCommand( String command, String[] args ) {
		
		if (command.equals("/strike")) {
			
			world.strikeLightning(getTargetBlock().getLocation());
			
		}
		
		else if (command.equals("/gofast")) {
			
			setWalkSpeed(0.5f);
			
		}
		
		else if (command.equals("/sword")) {
			
			addItemToInventory(new ItemStack(Material.DIAMOND_SWORD));
			
		}
		
		else if (command.equals("/menu")) {
			
			sendMessage("/strike - Lightning strike");
			sendMessage("/gofast - Go faster");
			sendMessage("/sword - Get a sword");
		}
		
	
	}
	
	
}
