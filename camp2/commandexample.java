package space.codekingdoms.alexteacher8.commandexample;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	public void onRunCommand(String command, String[] args) {
		if (command.equals("/makelightning")) {
			world.strikeLightning(getTargetBlock().getLocation());
		}	
	}
}
