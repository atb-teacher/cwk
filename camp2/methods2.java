package space.codekingdoms.alexteacher8.methods;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.entity.EntityType;
import java.lang.Float;
import java.lang.Integer;
import java.lang.Math;
import com.codekingdoms.nozzle.utils.Direction;
import com.codekingdoms.nozzle.utils.Random;

public class Player extends BasePlayer {
	
	String mobSpawn; // add this line
	
	public void onRunCommand( String command, String[] args ) {
		
		if (command.equals("/clearmobs")) {
			
			getGame().removeAllMobs();
			
		}
		
		else if (command.equals("/invis")) {
			
			applyPotionEffect(PotionEffectType.INVISIBILITY, 600, 1);
			
		}
		
		else if (command.equals("/togglespawn")) {  // add this line
			if (mobSpawn == "no") {  // add this line
				mobSpawn = "yes";  // add this line
			}  // add this line
			else {  // add this line
				mobSpawn = "no";  // add this line
			}  // add this line
		}  // add this line
		
	
	}
	
	public void startup() {
		
		clearInventory();
		addItemToInventory(Material.TORCH);
		addItemToInventory(Material.STONE_PICKAXE);
	
	}
	
	public void onJoin() {
		
		startup();
		mobSpawn = "no";  // add this line

	
	}
	
	public void onRespawn() {
		
		startup();
	
	}
	
	public void changeBlock( Material newMaterial ) {
		
		getGame().setBlockTypeAtLocation(newMaterial, getTargetBlock().getLocation());
	
	}
	
	public void onLeftClick() {
		
		changeBlock(Material.OAK_LEAVES);
		if (mobSpawn == "yes") {  // add this line
			Panda p = new Panda();
			p.spawn(world, getLocation());
		}  // add this line
	
	}
	
	public int countCreatures( EntityType type ) {
		
		int count = 0;
		for (Entity e : world.getNearbyEntities(getLocation(), 5, 5, 5)) {
			
			if (e.getType() == type) {
				
				count = count + 1;
				
			}
			
			
		}
		
		return count;
	
	}
	
	public void onRightClick() {
		
		sendMessage("There are " + (countCreatures(EntityType.PANDA) + " pandas nearby"));
	
	}
	
	
}
