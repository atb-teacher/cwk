# ===== SETUP STAGE =====

stage.set_gravity(10)

# ===== SETUP UFO/PARROT =====

parrot = codesters.Sprite("ufo")
parrot.set_size(.5)
parrot.set_gravity_on()

# ===== SETUP BOUNDARIES =====

top_boundary = codesters.Rectangle(
	0, # x-coord
	250, # y-coord
	500, # width
	100, # height
	"orange", # color
)
top_boundary.set_gravity_off()

bottom_boundary = codesters.Rectangle(
	0, # x-coord
	-250, # y-coord
	500, # width
	100, # height
	"orange", # color
)
bottom_boundary.set_gravity_off()

# ===== SETUP OBSTACLES =====

def create_obstacle():
	x_pos = 300
	y_pos = random.randint(-200, 200)
	width = 50
	height = random.randint(1, 3) * 50
	new_obstacle = codesters.Rectangle(x_pos, y_pos, width, height, "orange")
	new_obstacle.set_x_speed(-5)
	

# ===== SETUP JUMP ======

def space_bar(sprite):
    sprite.jump(7.5)
    
parrot.event_key("space", space_bar)

# ===== SETUP COLLISION ======

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color()
    if my_var == "orange":
        # sprite.say("Ouch!")
		sprite.hide()
        
parrot.event_collision(collision)

# ===== SETUP GAME LOOP =====

while True:
	stage.wait(3) # experiment with this number
	create_obstacle()

