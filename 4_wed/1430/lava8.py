# SETUP STAGE
stage.set_gravity(5)
stage.disable_all_walls()

# SETUP ALIEN
alien = codesters.Sprite("alien2", 0, 120)
alien.set_size(0.5)
alien.grounded = False
alien.platform = None


# SETUP PLATFORMS

platform_list = []

platform = codesters.Rectangle(0, - 30, 75, 10, "blue")
platform.set_gravity_off()
platform.name = "Chicago"
platform.left_side = platform.get_x() - (platform.get_width()/2)
platform.right_side = platform.get_x() + (platform.get_width()/2)
platform_list.append(platform)


platform2 = codesters.Rectangle(175, 150, 75, 10, "blue")
platform2.set_gravity_off()   
platform2.set_x_speed(3)
platform2.set_y_speed(-1)
platform2.name = "NYC"
platform_list.append(platform2)

def name_gen():
    adjectives = ["quick", "slow", "brown"]
    animals = ["fox", "cat", "dog", "mouse", "bear"]
    return random.choice(adjectives) + random.choice(animals)

def create_platform():
    new_platform = codesters.Rectangle(175, 150, 125, 10, "blue")
    new_platform.set_gravity_off()   
    new_platform.set_x_speed(3)
    new_platform.set_y_speed(-1)
    new_platform.name = name_gen()
    print(new_platform.name)
    platform_list.append(new_platform)

# SETUP GRAVITY TEXTBOX
grounded_text = codesters.Text(f"Grounded: {alien.grounded}", 
    -180, 200) # changed
def refresh_grounded_text():
    grounded_text.set_text(f"Grounded: {alien.grounded}")

# SETUP PLATFORM TEXTBOX
platform_text = codesters.Text(f"Platform: {alien.platform}", 
    -180, 175) # changed
def refresh_platform_text(): # changed
    platform_text.set_text(f"Platform: {alien.platform}")

# SETUP COLLISION
def collision(me, not_me):
    not_me_color = not_me.get_color()
    if all([
        # colliding with something blue
        not_me_color == "blue",
        # not moving up
        me.get_y_speed() <= 0,
        # the alien is slightly above the platform
        me.get_y() > not_me.get_y() + 10,
        # the alien's not too far left
        me.get_x() > not_me.get_x() - (not_me.get_width()/2),
        # the alien's not too far right
        me.get_x() < not_me.get_x() + (not_me.get_width()/2),
    ]):
        
        # make it like you're standing on a floor
        me.set_gravity_off()
        me.set_y_speed(0)
        
        # handle the alien's state, in air or on ground
        alien.grounded = True
        refresh_grounded_text()
        
        # snap to correct height
        legs = me.get_height() / 2 - 5
        me.set_y(not_me.get_y() + legs)
        
        # take platforms speed
        me.set_x_speed(not_me.get_x_speed())
        
        # remember which platform
        alien.platform = not_me.name
        refresh_platform_text()

alien.event_collision(collision)
# sprite = codesters.Sprite("candy")

def space_bar_grinch():
    if alien.grounded == True:
        alien.jump(10) 
        alien.grounded = False
        alien.set_gravity_on()
        refresh_grounded_text()
        alien.platform = None
        refresh_platform_text()

stage.event_key("space", space_bar_grinch)



def check_platform_bounce():
    if platform2.get_x() > 150 and platform2.get_x_speed() > 0:
        platform2.set_x_speed(platform2.get_x_speed() * -1)
        if alien.platform == platform2.name:
            alien.set_x_speed(platform2.get_x_speed())
    elif platform2.get_x() < -150 and platform2.get_x_speed() < 0:
        platform2.set_x_speed(platform2.get_x_speed() * -1)
        if alien.platform == platform2.name:
            alien.set_x_speed(platform2.get_x_speed()) 

#while True:
for i in range(1000):
    stage.wait(.1)
    check_platform_bounce()
    create_platform()

        
"""
GOALS:
  * Make platforms move down
  * Let the alien lose
"""
