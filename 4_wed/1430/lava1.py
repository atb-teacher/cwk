alien = codesters.Sprite("alien2", 0, 100)
alien.set_size(0.5)
platform = codesters.Rectangle(0, -30, 75, 10, "blue")

stage.set_gravity(5)

platform.set_gravity_off()

def collision(me, not_me):
    not_me_color = not_me.get_color()
    me.say(not_me_color)
    me.set_gravity_off()
    me.set_y_speed(0)

alien.event_collision(collision)
