# Warmup 1

Make a program where a sprite asks the user what number grade they have, and then tells them if they got an A/B/C/D/F

Hint: create a sprite, and then use `sprite.ask("stuff")` and `sprite.say("stuff")`
