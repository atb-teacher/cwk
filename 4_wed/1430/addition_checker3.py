"""
sprite = codesters.Sprite("alien1")
num1 = random.randint(1, 5)
num2 = random.randint(1, 5)
keep_looping = True
while keep_looping == True:
    user_guess = sprite.ask(f"What is {num1} + {num2}?")
    user_guess = int(user_guess)
    if user_guess == num1 + num2:
        sprite.say("correct!")
        keep_looping = False
    else:
        sprite.say("wrong!", 2)
"""

# this code is less readable
sprite = codesters.Sprite("alien1")
num1 = random.randint(1, 5)
num2 = random.randint(1, 5)
user_guess = sprite.ask(f"What is {num1} + {num2}?")
user_guess = int(user_guess)
while user_guess != num1 + num2:
    sprite.say("wrong!", 2) # say "wrong" for 2 seconds
    user_guess = sprite.ask(f"What is {num1} + {num2}?")
    user_guess = int(user_guess)
sprite.say("correct!")


