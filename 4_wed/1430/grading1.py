"""
# ASK FOR NUMBER

sprite = codesters.Sprite("alien1")
user_answer_str = sprite.ask("give me a number")
user_answer_int = int(user_answer_str)
if user_answer_int > 10:
    sprite.say("That's bigger than 10")
"""

# ASK FOR LETTER
sprite = codesters.Sprite("alien1")
user_answer_str = sprite.ask("give me a letter")
if user_answer_str.capitalize() == "A":
    sprite.say("Your score is between 90 and 100")
