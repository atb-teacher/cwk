# SETUP STAGE

stage.set_gravity(5)
stage.disable_all_walls()

# SETUP ALIEN

alien = codesters.Sprite("alien2", 0, 100)
alien.set_size(0.5)
alien.gravity = True

# SETUP PLATFORM

platform = codesters.Rectangle(0, -30, 75, 10, "blue")
platform.set_gravity_off()

# SETUP GRAVITY TEXTBOX
gravity_text = codesters.Text(f"Gravity: {alien.gravity}", 
    -200, 200)
def refresh_gravity_text():
    gravity_text.set_text(f"Gravity: {alien.gravity}")

def collision(me, not_me):
    not_me_color = not_me.get_color()
    if not_me_color == "blue" and me.get_y_speed() < 0:
        me.say(not_me_color)
        me.set_gravity_off()
        alien.gravity = False
        refresh_gravity_text()
        me.set_y_speed(0)

alien.event_collision(collision)

def space_bar():
    if alien.gravity == False:
        alien.jump(10) 
        alien.gravity = True
        alien.set_gravity_on()
        refresh_gravity_text()

stage.event_key("space", space_bar)

"""
GOALS:
  * Debug getting stuck in the platform
  * Make platforms move
"""

