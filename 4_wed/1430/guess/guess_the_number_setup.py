import random

answer = random.randint(1, 100)

"""
options = ["MCU", "Star Wars", "Harry Potter"] # don't need this
favorite = "Harry Potter" # this is the same as answer = random.randint

while True: # same
  print("Guess my favorite movie") # similar
  for index, one_option in enumerate(options): # skip
    print(index + 1, one_option) # skip
  
  user_choice = input(":") #similar/same
  if user_choice != favorite: # similar
    print("Incorrect")
  else:
    print("Correct!")
"""
