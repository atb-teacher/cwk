# guess_my_favorite.py
"""
Goal: ask the user to guess your favorite movie (give them options). Tell them if they are right.
"""

options = ["MCU", "Star Wars", "Harry Potter"]

"""
print("Guess which is my favorite movie:")
for one_option in options:
  print(one_option)

"""
for index, one_option in enumerate(options):
  print(index + 1, one_option)

user_choice = input(":")
print(user_choice)
# you can program the response
# set your favorite
# have an if conditional
