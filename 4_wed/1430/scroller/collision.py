class Hero:
  def __init__(self):
    self.health = 5

  def say_health(self):
    print(f"I have {self.health} health.")

class Thing:
  def __init__(self, type_param):
    self.type = type_param

my_hero = Hero()
my_danger = Thing("danger")
my_powerup = Thing("powerup")

def collision(sprite, hit_sprite):
  my_type = hit_sprite.type
  if my_type == "danger":
    sprite.health -= 1
  if my_type == "powerup":
    sprite.health += 1
  sprite.say_health()

collision(my_hero, my_danger)
collision(my_hero, my_powerup)



"""
def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "red":
        sprite.say("Ouch!")
        
    if my_var == "green":
        sprite.say("Goal!")
        
    # add any other actions...
"""
