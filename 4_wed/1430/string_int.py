>>> type("abc")
<class 'str'>
>>> type(5)
<class 'int'>
>>> "5" == 5
False
>>> int("5") == 5
True
>>> "7" + "4" # string + string
'74'
>>> 3 + 8 # int + int
11
>>> 3 + "5" # int + string
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'int' and 'str'
>>> 3 + int("5") # int + (string converted to int)
8
