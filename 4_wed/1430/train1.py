hero = codesters.Sprite("alien1", -200, 0)
hero.set_size(.3)

def right_key(sprite):
    sprite.move_right(25)
    sprite.say(sprite.get_x())
    
hero.event_key("right", right_key)

def left_key(sprite):
    sprite.move_left(25)
    sprite.say(sprite.get_x())
    
hero.event_key("left", left_key)


train = codesters.Rectangle(
    random.choice([0, -50, 50, -100, 100]), # x
    200, # y
    20,# width
    80,# height
    "blue", # color
    )
    
train.set_y_speed(-10)
