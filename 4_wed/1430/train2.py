# ========== SETUP HERO ==========

hero = codesters.Sprite("alien1", -200, 0)
hero.set_size(.3)

# ========== SETUP MOVEMENT ==========

def up_key(sprite):
    sprite.move_up(20)
    
def down_key(sprite):
    sprite.move_down(20)

def right_key(sprite):
    sprite.move_right(25)
    sprite.say(sprite.get_x())
    
def left_key(sprite):
    sprite.move_left(25)
    sprite.say(sprite.get_x())

hero.event_key("up", up_key)
hero.event_key("down", down_key)
hero.event_key("right", right_key)
hero.event_key("left", left_key)

# ========== SETUP COLLISION ==========

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "blue":
        sprite.say("Ouch!")
        
    if my_var == "green":
        sprite.say("Goal!")
        
hero.event_collision(collision)

# ========== SETUP TRAIN ==========

train = codesters.Rectangle(
    random.choice([0, -50, 50, -100, 100]), # x
    200, # y
    20,# width
    80,# height
    "blue", # color
    )
    
train.set_y_speed(-10)
