sprite = codesters.Sprite("alien1")
num1 = random.randint(1, 5)
num2 = random.randint(1, 5)
keep_looping = True # new line
while keep_looping == True: # new line
    user_guess = sprite.ask(f"What is {num1} + {num2}?") # changed line
    user_guess = int(user_guess) # changed line
    if user_guess == num1 + num2: # changed line
        sprite.say("correct!") # changed 
        keep_looping = False # new line
