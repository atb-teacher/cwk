# SET UP SPRITE

sprite = codesters.Sprite("ufo", 0, -175)
sprite.set_size(0.3)

# SET UP STAGE

stage.disable_all_walls()
stage.set_background("space")

# SET UP MOVEMENT

def left_sushi(sprite):
    sprite.move_left(20)

def right_fries(sprite):
    sprite.move_right(20)
    
sprite.event_key("left", left_sushi)
sprite.event_key("right", right_fries)


while 1 == 1:
    asteroid = codesters.Sprite(
        "asteroid", 
        random.randint(-150, 150),
        250,
    )
    asteroid.set_y_speed(-10)
    stage.wait(1)




