# SET UP VARIABLES

SPAWN_HEIGHT = 290
score = 0

# SET UP SPRITE

sprite_roblox = codesters.Sprite("ufo", 0, -175)
sprite_roblox.set_size(0.3)
sprite_roblox.health = 10
movement_bonus = 0

# SET UP STAGE

stage.disable_all_walls()
stage.set_background("space")

# SET UP SCORE BOX

score_text = codesters.Text(f"score: {score}", -180, 230)
score_text.set_outline_color("black")
score_text.set_fill_color("white")

# SET UP MOVEMENT

def left_sushi(sprite):
    sprite.move_left(20 + movement_bonus)

def right_fries(sprite):
    sprite.move_right(20 + movement_bonus)
    
sprite_roblox.event_key("left", left_sushi)
sprite_roblox.event_key("right", right_fries)

# SET UP COLLISION

def collision(me, not_me):
    global movement_bonus
    
    not_me_name = not_me.get_image_name()
    
    if not_me_name == "anemone":
        sprite_roblox.health = sprite_roblox.health + 10
        stage.remove_sprite(not_me)
        
    elif not_me_name == "asteroid":
        sprite_roblox.health = sprite_roblox.health - 1
        stage.remove_sprite(not_me)
        
    elif not_me_name == "present3":
        sprite_roblox.health = sprite_roblox.health + 1
        if movement_bonus < 10:
            movement_bonus = movement_bonus + 2
        stage.remove_sprite(not_me)

    
sprite_roblox.event_collision(collision)

# SINGLE-FIRE ANEMONE SPRITE

tempsprite = codesters.Sprite(
    "anemone", 
    0, 
    SPAWN_HEIGHT,
)
tempsprite.set_size(
    random.randint(2, 4) * .1
)
tempsprite.set_y_speed(-4)

# loop forever
counter = 0
while True:
    
    # CREATE AN ASTEROID
    if counter % 6 == 0:
        SPAWN_X = random.randint(-150, 150)
        asteroid = codesters.Sprite(
            "asteroid", 
            SPAWN_X, 
            SPAWN_HEIGHT,
        )
        asteroid.set_size(
            random.randint(3, 5) * .1
        )
        
        asteroid.set_y_speed(-2)
        
        if SPAWN_X < 0:
            asteroid.set_x_speed(random.randint(0, 5) * .2)
        else:
            asteroid.set_x_speed(random.randint(-5, 0) * .2)
            
        # asteroid.set_x_speed(random.randint(-2, 2))
        
    # CREATE A POWERUP
    if counter % 60 == 0:
        SPAWN_X = random.randint(-150, 150)
        powerup = codesters.Sprite(
            "present3",
            SPAWN_X, 
            SPAWN_HEIGHT,
        )
        
        powerup.set_size(.3)
        
        powerup.set_y_speed(-3)
        

    # ANOUNCE HEALTH
    sprite_roblox.say(sprite_roblox.health)
    
    # WAIT BEFORE LOOPING AGAIN
    stage.wait(.05)
    counter = counter + 1
    score = score + 1
    score_text.set_text(f"score: {score}")

"""
GOALS

* Make a powerup that makes our ship go faster
  * The powerup comes occasionally
* Put a score in the top left corner
"""
# print(dir(score_text))
