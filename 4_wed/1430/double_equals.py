>>> # a single equals sign tells a variable to point at info
>>> my_num = 5
>>> # now the variable my_num is pointing at the integer 5
>>> # two equals signs is a yes/no or true/false question
>>> # is 5 equal to 5?
>>> 5 == 5
True
>>> # is 5 equal to 6?
>>> 5 == 6
False
>>> # is 5 equal to what my_num is pointing at?
>>> 5 == my_num
True
