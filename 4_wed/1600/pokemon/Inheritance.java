
public class Inheritance
{
    public Inheritance() { // constructor class
        System.out.println("I am being created!");
    }
    
    public static void main(String args[]) { // main method
        // System.out.println("hello! pikachu!");
        Pet Rocky = new Pet("large", "Rocky", true);
        Rocky.printDescription();
        System.out.println(Rocky.hunger);
        Rocky.makeNoise();
        System.out.println(Rocky.hunger);
        
        
        Dog Rocky2 = new Dog("large", "Rocky");
        Rocky2.printDescription();
        System.out.println(Rocky2.hunger);
        Rocky2.makeNoise();
        System.out.println(Rocky2.hunger);
        Rocky2.eatFood();
        System.out.println(Rocky2.hunger);

        
    }
}

class Pet {
    public String size;
    public String name;
    public boolean mammal;
    public int hunger;
    public Pet(String size, String name, boolean mammal) {
        this.size = size;
        this.name = name;
        this.mammal = mammal;
        this.hunger = 10;
    }
    
    public void printDescription() {
        System.out.println(
            String.format(
                "A %s pet named %s", this.size, this.name
            )
        );
    }
    
    public void makeNoise() {
        System.out.println("I don't know what noise to make");
    }
}

class Dog extends Pet {
    public Dog(String size, String name) {
        super(size, name, true);
    }
    
    public void makeNoise() {
        hunger++;
        System.out.println("Bork bork bork!");
    }
    
    public void eatFood() {
        hunger = hunger - 5;
        System.out.println("Om nom nom!");
    }
    
    public void printDescription() {
        super.printDescription();
        System.out.println(
            String.format(
                "%s is a great dog!",
                this.name
            )
        );
    }
}