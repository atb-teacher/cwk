import java.util.Scanner;
import java.util.Arrays;

public class Pokemon
{
    public static enum Type {FIRE, WATER, GRASS, GLITCH};
    public static enum Diff {EASY, MEDIUM, HARD};
    public int lV;
    public int hP; 
    public int aP;
    public int dP;
    public int sP;
    public String name;
    public Type pokeType;
    public Pokemon(Type inType)
    {
        pokeType = inType;
        lV = 5;
    }
    
    public void printDescription() {
        System.out.println(
            String.format(
                "This is a %s type pokemon",
                getType()
            )
        );
    }
    
    public String getType() {
        switch (pokeType) {
            case FIRE:
                return "fire";
            case WATER:
                return "water";
            case GRASS:
                return "grass";
            default:
                return "none";
        }
    }
    
    public static void main(String args[]) throws Exception {
        
        GameSession mainSession = new GameSession();
        mainSession.run();
        
        /*
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Choose (e)asy, (m)edium, or (h)ard:");
        String difficultyStr = myScanner.nextLine().toLowerCase();
        String[] diff = {"e", "m", "h", "easy", "medium", "hard"};
        while (!Arrays.asList(diff).contains(difficultyStr)) {
            System.out.println("Choose (e)asy, (m)edium, or (h)ard:");
            difficultyStr = myScanner.nextLine();
        }
        Pokemon.Diff gameDiff;
        int goalWins;
        switch (difficultyStr) {
            case "e":
            case "easy":
                gameDiff = Pokemon.Diff.EASY;
                goalWins = 1;
                break;
            case "m":
            case "medium":
                gameDiff = Pokemon.Diff.MEDIUM;
                goalWins = 10;
                break;
            case "h":
            case "hard":
                gameDiff = Pokemon.Diff.HARD;
                goalWins = 20;
                break;
            default:
                throw new Exception("unacceptable game difficulty string");
        }
        
        Pokemon playerPokemon;
        String typeString = "Fire";
        System.out.println("choose a pokemon");
        
        playerPokemon = StarterFactory.MakeStarter(typeString);
        playerPokemon.printDescription();
        
        System.out.println("What do you want to do?");
        System.out.println("1) Fight.");
        System.out.println("2) Quit.");
        String playerChoice = myScanner.nextLine().toLowerCase();
        
        String[] chooseFight = {"1", "f", "fight"};
        String[] chooseEnd = {"2", "q", "quit"};
        if (Arrays.asList(chooseFight).contains(playerChoice)) {
            System.out.println("Fight");
        } else if (Arrays.asList(chooseEnd).contains(playerChoice)) {
            System.out.println("end game loop");
        }
        */

    }
    
    // public static PokeBuilder builder() {
        // return new PokeBuilder();
    // }
    
    // static class PokeBuilder {
        // public int lV = 1;
        // public int hP = 1; 
        // public int aP = 1;
        // public int dP = 1;
        // public int sP = 1;
        // public String name = "none";
        // public Type pokeType = Type.GRASS;
        
        // public PokeBuilder setName(String nameParam) {
            // this.name = nameParam;
            // return this;
        // }
        
        // public Pokemon build() {
            // return new Grookey();
        // }
    // }
}

class Grookey extends Pokemon
{
    public Grookey() {
        super(Pokemon.Type.GRASS);
        name = "Grookey";
        hP = 50;
        aP = 65;
        dP = 40;
        sP = 65;
    }
}

class Scorbunny extends Pokemon
{
    public Scorbunny() {
        super(Pokemon.Type.FIRE); 
        name = "Scorbunny";
        hP = 50;
        aP = 71;
        dP = 40;
        sP = 69;
    }
}

class Sobble extends Pokemon
{
    public Sobble() {
        super(Pokemon.Type.WATER);
        name = "Sobble";
        hP = 50;
        aP = 40;
        dP = 40;
        sP = 70;
    }
}

class Missingno extends Pokemon
{
    public Missingno() {
        super(Pokemon.Type.GLITCH);
    }
}

class StarterFactory {
    public static Pokemon MakeStarter(String typeString) {
        System.out.println(typeString);
        switch (typeString) {
            case "Fire":
                return new Scorbunny();
            case "Water":
                return new Sobble();
            case "Grass":
                return new Grookey();
            default:
                return new Missingno();
        }
    }
}

class Player {
    private boolean alive;
    public Pokemon playerPoke;
    
    Player(Pokemon inPoke) {
        playerPoke = inPoke;
    }
}

class Item {

}

class SavePoint {
    private Player playerSave;
    private Pokemon pokeSave;
    private Item[] itemSave;
    SavePoint(Player mainPlayer, Pokemon mainPoke, Item[] items) {
        playerSave = mainPlayer;
        pokeSave = mainPoke;
        itemSave = items;
    }
    
    public Player restorePlayer() {
        return playerSave;
    }
    
    public Pokemon restorePokemon() {
        return pokeSave;
    }
    
    public Item[] restoreItems() {
        return itemSave;
    }
}

class GameSession {
    Pokemon.Diff gameDiff;
    Scanner myScanner;
    Pokemon playerPokemon;


    public void run() throws Exception{
        myScanner = new Scanner(System.in);
        setGameDiff();
        setStarter();
        gameLoop();
    }
    
    public void setGameDiff() throws Exception{
        System.out.println("Choose (E)asy, (M)edium, or (H)ard:");
        String difficultyStr = myScanner.nextLine().toLowerCase();
        String[] diff = {"e", "m", "h", "easy", "medium", "hard"};
        while (!Arrays.asList(diff).contains(difficultyStr)) {
            System.out.println("Choose (E)asy, (M)edium, or (H)ard:");
            difficultyStr = myScanner.nextLine();
        }
        int goalWins;
        switch (difficultyStr) {
            case "e":
            case "easy":
                gameDiff = Pokemon.Diff.EASY;
                goalWins = 1;
                break;
            case "m":
            case "medium":
                gameDiff = Pokemon.Diff.MEDIUM;
                goalWins = 10;
                break;
            case "h":
            case "hard":
                gameDiff = Pokemon.Diff.HARD;
                goalWins = 20;
                break;
            default:
                throw new Exception("unacceptable game difficulty string");
        }
    }
    
    public void setStarter() {
        String typeString = "Fire";
        System.out.println("choose a pokemon");
        /*
        String pokeStr = myScanner.nextLine().toLowerCase();
        String[] diff = {"gr", "sc", "so"};
        while (!Arrays.asList(diff).contains(difficultyStr)) {
            System.out.println("Choose (r)");
            difficultyStr = myScanner.nextLine();
        }
        int goalWins;
        switch (difficultyStr) {
            case "e":
            case "easy":
                gameDiff = Pokemon.Diff.EASY;
                goalWins = 1;
                break;
            case "m":
            case "medium":
                gameDiff = Pokemon.Diff.MEDIUM;
                goalWins = 10;
                break;
            case "h":
            case "hard":
                gameDiff = Pokemon.Diff.HARD;
                goalWins = 20;
                break;
        */
        playerPokemon = StarterFactory.MakeStarter(typeString);
        playerPokemon.printDescription();
    }
    
    public void gameLoop() {
        System.out.println("What do you want to do?");
        System.out.println("1) Fight.");
        System.out.println("2) Quit.");
        String playerChoice = myScanner.nextLine().toLowerCase();
        
        String[] chooseFight = {"1", "f", "fight"};
        String[] chooseEnd = {"2", "q", "quit"};
        if (Arrays.asList(chooseFight).contains(playerChoice)) {
            System.out.println("Fight");
        } else if (Arrays.asList(chooseEnd).contains(playerChoice)) {
            System.out.println("end game loop");
        }
    }
}