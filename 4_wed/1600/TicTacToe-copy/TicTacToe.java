import java.awt.*; // graphics stuff
import java.awt.event.*; // click event stuff
import javax.swing.*; // graphics stuff

public class TicTacToe extends JFrame {
    // setup number of rows/cols
    // setup width/height/line thickness etc
    // create a board
    // create 3 turn states: cross, o, empty
    /*
     * turn states are useful for 2 reasons:
     * 1) keeping track of cell state
     * 2) keeping track of turns
     * 
     */
    int ROWS = 3;
    int COLS = 3;
    
    // graphics drawing stuff
    int CELL_SIZE = 100;
    int CANVAS_WIDTH = CELL_SIZE * COLS;
    int CANVAS_HEIGHT = CELL_SIZE * ROWS;
    int GRID_WIDTH = 8;
    int GRID_WIDTH_HALF = GRID_WIDTH / 2;
    
    // symbols are displayed in a cell
    // padding is btw symbol and border
    int CELL_PADDING = CELL_SIZE/6;
    int SYMBOL_SIZE = CELL_SIZE - CELL_PADDING * 2;
    int SYMBOL_STROKE_WIDTH = 8;
    
    public static enum Seed {
        EMPTY, CROSS, O
    }
    private Seed[][] board;
    private Seed currentPlayer;
    public enum GameState {
        PLAYING, O_WON, CROSS_WON, TIE
    }
    private GameState currentState;
    
    private TicTacToeWinCheck checker;
    
    private DrawCanvas canvas;
    
    private JLabel statusBar;
    
    class DrawCanvas extends JPanel {
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            setBackground(Color.WHITE);
            g.setColor(Color.LIGHT_GRAY);
            // use two loops to draw the grid
            // 1st loop
            for (int row=1; row< ROWS; row++) {
                g.fillRoundRect(
                    0, // x
                    CELL_SIZE * row - GRID_WIDTH_HALF, // y
                    CANVAS_WIDTH-1, // width 
                    GRID_WIDTH, // height 
                    GRID_WIDTH, // rounding stuff 
                    GRID_WIDTH // rounding stuff
                );
            }
            for (int col=1; col<COLS; col++) {
                g.fillRoundRect(
                    CELL_SIZE * col - GRID_WIDTH_HALF, // x
                    0, // y
                    GRID_WIDTH, // width
                    CANVAS_HEIGHT-1, // height
                    GRID_WIDTH, // rounding stuff
                    GRID_WIDTH // rounding stuff
                );
            }
            
            Graphics2D g2d = (Graphics2D)g;
            g2d.setStroke(
                new BasicStroke(
                    SYMBOL_STROKE_WIDTH,
                    BasicStroke.CAP_ROUND,
                    BasicStroke.JOIN_ROUND
                )
            );
            for (int row = 0; row < ROWS; row++) {
                for (int col = 0; col < COLS; col++) {
                    int x1 = col * CELL_SIZE + CELL_PADDING;
                    int y1 = row * CELL_SIZE + CELL_PADDING;
                    if (board[row][col] == Seed.O) {
                        g2d.setColor(Color.BLUE);
                        g2d.drawOval(x1, y1, SYMBOL_SIZE, SYMBOL_SIZE);
                    }
                    else if (board[row][col] == Seed.CROSS) {
                        g2d.setColor(Color.RED);
                        int x2 = (col + 1) * CELL_SIZE - CELL_PADDING;
                        int y2 = (row + 1) * CELL_SIZE - CELL_PADDING;
                        g2d.drawLine(x1, y1, x2, y2);
                        g2d.drawLine(x2, y1, x1, y2);
                    }
                }
            }
        }
    }
    
    public boolean checkWinTesting(Seed[][] inBoard) {
        if (inBoard[0][0] == Seed.CROSS &&
            inBoard[0][1] == Seed.CROSS &&
            inBoard[0][2] == Seed.CROSS) {
                return true;
            }
        return false;
    }
    
    public boolean checkTie() {
        // Return the answer to "Is this game tied?"
        for (int row=0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                if (board[row][col] == Seed.EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public TicTacToe() {
        /*
         * setting stuff up as drawable in the GUI
         */
        canvas = new DrawCanvas();
        canvas.setPreferredSize(
            new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT)
        );
        
        canvas.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int mouseX = e.getX();
                int mouseY = e.getY();
                int rowSelected = mouseY / CELL_SIZE;
                int colSelected = mouseX / CELL_SIZE;
                
                if (currentState == GameState.PLAYING) {
                    if (board[rowSelected][colSelected] == Seed.EMPTY) {
                        board[rowSelected][colSelected] = currentPlayer;
                        checker = new TicTacToeWinCheck(board);
                        if (checker.checkBoard(TicTacToe.Seed.CROSS)) {
                            statusBar.setText("X won");
                            currentState = GameState.CROSS_WON;
                        }
                        else if (checker.checkBoard(TicTacToe.Seed.O)) {
                            statusBar.setText("O won");
                            currentState = GameState.O_WON;
                        }
                        if (currentState == GameState.PLAYING) {
                            if (checkTie()) {
                                currentState = GameState.TIE;
                                statusBar.setText("Tie!");
                            }
                        }
                        if (currentState == GameState.PLAYING) {
                            // swap currentPlayer
                            if (currentPlayer == Seed.CROSS) {
                                currentPlayer = Seed.O;
                                statusBar.setText("O's turn");
                            }
                            else {
                                currentPlayer = Seed.CROSS;
                                statusBar.setText("X's turn");
                            }
                        }
                    }
                } else {
                    initGame();
                    statusBar.setText("X's turn");
                }
                repaint();
            }
        });
        
        statusBar = new JLabel("  ");
        statusBar.setFont(new Font(Font.DIALOG_INPUT, Font.BOLD, 15));
        statusBar.setBorder(BorderFactory.createEmptyBorder(2, 5, 4, 5));
        statusBar.setText("X's turn");
        
        Container cp = getContentPane();
        cp.setLayout(new BorderLayout());
        cp.add(canvas, BorderLayout.CENTER);
        cp.add(statusBar, BorderLayout.PAGE_END);
        
        /*
         * PokemonBuilder myBuild = new PokemonBuilder();
         * PokemonBuilder.setType(Pokemon.Types.GRASS);
         * PokemonBuilder.setHp(30);
         * PokemonBuilder.setDp(25);
         * Pokemon myIvysaur = PokemonBuilder.build();
         */
        
        /*
         * Pokemon myIvysaur = new Pokemon(
         *    30,
         *    25,
         *    Pokemon.Types.GRASS,
         *  )
         */
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setTitle("Tic Tac Toe");
        setVisible(true);
        
        board = new Seed[ROWS][COLS];
        initGame();
    }
    public void initGame() {
        /*
         * setting up an empty board
         */
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                board[row][col] = Seed.EMPTY;
            }
        }
        // board[1][2] = Seed.O;
        // board[0][1] = Seed.CROSS;
        // print stuff out
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                System.out.print(board[row][col] + "; ");
            }
            System.out.println();
        }
        
        currentState = GameState.PLAYING;
        currentPlayer = Seed.CROSS;
    }
    public static void main(String[] args) {
        new TicTacToe();
    }
}

class TicTacToeWinCheck
{
    /*
    public enum Seed {
        EMPTY, CROSS, O
    }
    
    public Seed[][] board;
    
    public static void main(String[] args) {
        TicTacToeWinCheck myChecker = new TicTacToeWinCheck();
        myChecker.printBoard();

    } 
    
    public TicTacToeWinCheck()
    {
        board = new Seed[3][3]; // INSTANTIATE BOARD
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                board[i][j] = Seed.EMPTY;
            }
        }
        // board[y][x]
        board[0][0] = Seed.CROSS;
        board[0][1] = Seed.CROSS;
        board[0][2] = Seed.CROSS;
    }
    */
    
    TicTacToe.Seed[][] board;
    
    public TicTacToeWinCheck(TicTacToe.Seed[][] inBoard) {
        board = inBoard;
    }
    
    public boolean checkBoard(TicTacToe.Seed token) {
        boolean foundWin = false;
        // loop 1
        if (!foundWin) {
            for (int i=0; i<3; i++) {
                foundWin = checkDir(
                    0, // startX
                    i, // startY
                    1, // xDelta
                    0, // yDelta
                    0, // counter
                    3, // winNum
                    token // turn
                );
                if (foundWin) {
                    break;
                }
            }
        }
        
        // loop 2
        if (!foundWin) {
            for (int i=0; i<3; i++) {
                foundWin = checkDir(
                    i, // startX
                    0, // startY
                    0, // xDelta
                    1, // yDelta
                    0, // counter
                    3, // winNum
                    token // turn
                );
                if (foundWin) {
                    break;
                }
            }
        }
        if (!foundWin) {
            // diag 1
            foundWin = checkDir(
                    0, // startX
                    0, // startY
                    1, // xDelta
                    1, // yDelta
                    0, // counter
                    3, // winNum
                    token // turn
            );
        }
        if (!foundWin) {
            // diag 2
            foundWin = checkDir(
                    2, // startX
                    0, // startY
                    -1, // xDelta
                    1, // yDelta
                    0, // counter
                    3, // winNum
                    token // turn
            );
        }
        
        return foundWin;
    }
    /*
    public void printBoard() {
        for (int i=0; i<board.length; i++) {
            for (int j=0; j<board[0].length; j++) {
                System.out.print(board[i][j]);
                System.out.print("\t");
            }
            System.out.println();
        }
    }
    */
    public boolean checkDir(
        int startX, 
        int startY, 
        int xDelta, 
        int yDelta, 
        int counter, 
        int winNum, 
        TicTacToe.Seed turn) {
        if (board[startY][startX] == turn) { // we found the seed we want
            // check if we've found enough
            if (counter + 1 == winNum) {
                return true;// it's true that someone won
            }
            else {
                return checkDir( // keep looking
                    startX + xDelta, // move to next place
                    startY + yDelta, // move to next place
                    xDelta, // keep direction
                    yDelta, // keep direction
                    counter + 1, // we found one more seed
                    winNum, // the required number to win stays the same
                    turn); // the turn stays the same
            }
        } else {
            return false;
        }
    }
}


/*
 * 
 *                     // TEMPORARY CODE
                    currentState = GameState.TIE;
                    for (int i=0; i<ROWS; i++) {
                        for (int j=0; j<COLS; j++) {
                            if (board[j][i] == Seed.EMPTY) {
                                currentState = GameState.PLAYING;
                            }
                        }
                    }
        */
       
