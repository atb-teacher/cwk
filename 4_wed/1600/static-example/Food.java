import java.util.HashMap; // import the HashMap class

public class Food
{
    public static HashMap<String, String> foodTaste = new HashMap<String, String>();
    public String name;
    
    public static void setupFoodTastes() {
        System.out.println("setting up tastes");
        foodTaste.put("pizza", "good");
        foodTaste.put("steamed broccoli", "bad");
        foodTaste.put("cafeteria lunch", "okay");
    }
    
    public Food(String inName) {
        name = inName;
    }
    
    public void sayWhatYouAre() {
        System.out.println(name);
        System.out.println("I taste " + foodTaste.get(name));
    }
    
    public static void main(String[] args) {
        Food.setupFoodTastes();
        Food pizza = new Food("pizza");
        pizza.sayWhatYouAre();
    }
}
