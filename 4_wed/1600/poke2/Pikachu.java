public class Pikachu extends pokemon
{
    public Pikachu() {
        super("Pikachu", pokemon.Type.ELECTRIC);
        ID = 1;
        hp = 35;
        lv = 5;
        aP = 89;
        dP = 15;
        sP = 50;
    }
}
