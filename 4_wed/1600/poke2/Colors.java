import java.util.HashMap; // import the HashMap class
import java.util.HashSet;

public class Colors
{
    public static void main(String[] args) {
        /*
        HashMap<String, String> thingsWithColors = new HashMap<String, String>();
        thingsWithColors.put("green", "grass");
        thingsWithColors.put("blue", "sky");
        System.out.println(thingsWithColors);
        
        HashSet<String> typesWeakToFire = new HashSet<String>();
        typesWeakToFire.add("grass");
        typesWeakToFire.add("bug");
        typesWeakToFire.add("steel");
        typesWeakToFire.add("ice");
        
        System.out.println(
            typesWeakToFire.contains("water")
        );
        */
        HashMap<pokemon.Type, HashSet> isStrongAgainst = new HashMap<pokemon.Type, HashSet>();
        isStrongAgainst.put(pokemon.Type.FIRE, new HashSet<pokemon.Type>());
        isStrongAgainst.get(pokemon.Type.FIRE).add(pokemon.Type.GRASS);
        isStrongAgainst.get(pokemon.Type.FIRE).add(pokemon.Type.BUG);
        isStrongAgainst.get(pokemon.Type.FIRE).add(pokemon.Type.STEEL);
        isStrongAgainst.get(pokemon.Type.FIRE).add(pokemon.Type.ICE);
        
        
    }
}
