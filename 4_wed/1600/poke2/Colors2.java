import java.util.HashMap; // import the HashMap class

public class Colors2
{
    public static void main(String[] args) {
        HashMap<String, String> thingsWithColors = new HashMap<String, String>();
        thingsWithColors.put("green", "grass");
        thingsWithColors.put("blue", "sky");
        System.out.println(thingsWithColors);
    }
}
