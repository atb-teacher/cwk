import java.util.HashMap; // import the HashMap class
import java.util.HashSet;

public class Move
{
    public static HashMap<pokemon.Type, HashSet> isStrongAgainst = new HashMap<pokemon.Type, HashSet>();
    
    public static void setupIsStrongAgainst() {
        System.out.println("Setting up strengths/weaknesses");
        isStrongAgainst.put(pokemon.Type.FIRE, new HashSet<pokemon.Type>());
        pokemon.Type[] typesFire = {pokemon.Type.GRASS, pokemon.Type.BUG, pokemon.Type.STEEL, pokemon.Type.ICE};
        
        for (int i=0; i<typesFire.length; i++) {
            isStrongAgainst.get(pokemon.Type.FIRE).add(typesFire[i]);
        }
        
        
        isStrongAgainst.put(pokemon.Type.GRASS, new HashSet<pokemon.Type>());
        pokemon.Type[] typesGrass = {pokemon.Type.WATER};

        for (int i=0; i<typesGrass.length; i++) {
            isStrongAgainst.get(pokemon.Type.GRASS).add(typesGrass[i]);
        }

    }
    
    public pokemon.Type type;
    public int averageDamage;
    public String name; // new
    
    public Move(String inName, pokemon.Type inType, int inDamage) { // changed
        type = inType;
        name = inName; // new
        averageDamage = inDamage; // new
    }
    
    public void Attack(pokemon attackingPokemon, pokemon defendingPokemon) {
        int power = 1;
        System.out.println(
            String.format(
                "Type %s is strong against %s? %b",
                type,
                defendingPokemon.pokeType,
                isStrongAgainst.get(type).contains(defendingPokemon.pokeType)
            )
        );
        if ( isStrongAgainst.containsKey( type ) ) {
            if ( isStrongAgainst.get( type ).contains(defendingPokemon.pokeType) ) {
                power = 2;
            }
        }
        System.out.println(
            String.format(
                "Attacking with %d power",
                power
            )
        );
    }
}

class Ember extends Move {
    
    public Ember() {
        super(
            "Ember", // name
            pokemon.Type.FIRE, // type
            10 // average damage
        );
    }
    
    public void Attack() {
    }
}

class VineWhip extends Move {
    
    public VineWhip() {
        super(
            "Vine Whip", // name
            pokemon.Type.GRASS, // type
            10 // average damage
        );
    }
    
    public void Attack() {
    }
}

class WaterGun extends Move {
    
    public WaterGun() {
        super(
            "Water Gun", // name
            pokemon.Type.WATER, // type
            10 // average damage
        );
    }
    
    public void Attack() {
    }
}