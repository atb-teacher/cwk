public class CheckPoint {
    private Player playerSave;
    private pokemon pokeSave;
    private Item[] itemSave;
    CheckPoint(Player mainPlayer, pokemon mainPoke, Item[] items) {
        playerSave = mainPlayer;
        pokeSave = mainPoke;
        itemSave = items;
    }
    
    public Player RestorePlayer() {
        return playerSave;
    }
    
    public pokemon restorePokemon() {
        return pokeSave;
    }
    
    public Item[] restoreItems() {
        return itemSave;
    }
}