

public class TicTacToeWinCheck
{
    /*
    public enum Seed {
        EMPTY, CROSS, O
    }
    
    public Seed[][] board;
    
    public static void main(String[] args) {
        TicTacToeWinCheck myChecker = new TicTacToeWinCheck();
        myChecker.printBoard();

    } 
    
    public TicTacToeWinCheck()
    {
        board = new Seed[3][3]; // INSTANTIATE BOARD
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                board[i][j] = Seed.EMPTY;
            }
        }
        // board[y][x]
        board[0][0] = Seed.CROSS;
        board[0][1] = Seed.CROSS;
        board[0][2] = Seed.CROSS;
    }
    */
    public boolean checkBoard() {
        boolean foundWin = false;
        // loop 1
        if (!foundWin) {
            for (int i=0; i<3; i++) {
                foundWin = checkDir(
                    0, // startX
                    i, // startY
                    1, // xDelta
                    0, // yDelta
                    0, // counter
                    3, // winNum
                    Seed.CROSS // turn
                );
                if (foundWin) {
                    break;
                }
            }
        }
        
        // loop 2
        if (!foundWin) {
            for (int i=0; i<3; i++) {
                foundWin = checkDir(
                    i, // startX
                    0, // startY
                    0, // xDelta
                    1, // yDelta
                    0, // counter
                    3, // winNum
                    Seed.CROSS // turn
                );
                if (foundWin) {
                    break;
                }
            }
        }
        if (!foundWin) {
            // diag 1
            checkDir(
                    0, // startX
                    0, // startY
                    1, // xDelta
                    1, // yDelta
                    0, // counter
                    3, // winNum
                    Seed.CROSS // turn
            );
        }
        if (!foundWin) {
            // diag 2
            checkDir(
                    2, // startX
                    0, // startY
                    -1, // xDelta
                    1, // yDelta
                    0, // counter
                    3, // winNum
                    Seed.CROSS // turn
            );
        }
        
        return foundWin;
    }
    
    public void printBoard() {
        for (int i=0; i<board.length; i++) {
            for (int j=0; j<board[0].length; j++) {
                System.out.print(board[i][j]);
                System.out.print("\t");
            }
            System.out.println();
        }
    }
    
    public boolean checkDir(
        int startX, 
        int startY, 
        int xDelta, 
        int yDelta, 
        int counter, 
        int winNum, 
        Seed turn) {
        if (board[startY][startX] == turn) { // we found the seed we want
            // check if we've found enough
            if (counter + 1 == winNum) {
                return true;// it's true that someone won
            }
            else {
                return checkDir( // keep looking
                    startX + xDelta, // move to next place
                    startY + yDelta, // move to next place
                    xDelta, // keep direction
                    yDelta, // keep direction
                    counter + 1, // we found one more seed
                    winNum, // the required number to win stays the same
                    turn); // the turn stays the same
            }
        } else {
            return false;
        }
    }
}
