TO DO:

* Adjust the pokemon "attack" method
  * Figure out if we want to replace the pokemon "attack" method, or have it receive info from move attack method

* Fix pokemon attack order
  * Make sure that it's clear what's going on when enemy attacks
  * Don't let pokemon attack after they faint

* Setup not very effective/effective/super effective
  * We could use an enum for that

* Put move array in pokemon constructor




    /*
    public boolean isCatchable(Item pokeBall) {
        int combined = lv * health;
        if (pokeBall.getType() == "basic" && combined < 25) {
            return true;
        }
        else if (pokeBall.getType() == "master" && combined < 200) {
            return true;
        }
        return false;
    }
    */
