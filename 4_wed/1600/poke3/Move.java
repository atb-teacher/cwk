import java.util.HashMap; // import the HashMap class
import java.util.HashSet;

public class Move
{
    public static HashMap<pokemon.Type, HashSet> isStrongAgainst = new HashMap<pokemon.Type, HashSet>();
    
    public static void setupIsStrongAgainst() {
        
        /* ===== SETUP FIRE STRENGTHS ===== */
        
        isStrongAgainst.put(pokemon.Type.FIRE, new HashSet<pokemon.Type>());
        pokemon.Type[] typesFire = {pokemon.Type.GRASS, pokemon.Type.BUG, pokemon.Type.STEEL, pokemon.Type.ICE};
        
        for (int i=0; i<typesFire.length; i++) {
            isStrongAgainst.get(pokemon.Type.FIRE).add(typesFire[i]);
        }
        
        /* ===== SETUP GRASS STRENGTHS ===== */
        
        isStrongAgainst.put(pokemon.Type.GRASS, new HashSet<pokemon.Type>());
        pokemon.Type[] typesGrass = {pokemon.Type.WATER, pokemon.Type.GROUND, pokemon.Type.ROCK};

        for (int i=0; i<typesGrass.length; i++) {
            isStrongAgainst.get(pokemon.Type.GRASS).add(typesGrass[i]);
        }
        
        /* ===== SETUP WATER STRENGTHS ===== */
        
        isStrongAgainst.put(pokemon.Type.WATER, new HashSet<pokemon.Type>());
        pokemon.Type[] typesWater = {pokemon.Type.FIRE, pokemon.Type.GROUND, pokemon.Type.ROCK};

        for (int i=0; i<typesWater.length; i++) {
            isStrongAgainst.get(pokemon.Type.WATER).add(typesWater[i]);
        }

        /* ===== SETUP NORMAL STRENGTHS ===== */
        
        isStrongAgainst.put(pokemon.Type.NORMAL, new HashSet<pokemon.Type>());

    }
    
    public pokemon.Type type;
    public int averageDamage;
    public String name; // new
    
    public Move(String inName, pokemon.Type inType, int inDamage) { // changed
        type = inType;
        name = inName; // new
        averageDamage = inDamage; // new
    }
    
    public void Attack(pokemon attackingPokemon, pokemon defendingPokemon) {
        boolean strongBool;
        strongBool = false;
        System.out.println(
            String.format(
                "Type %s is strong against %s? %b", // THIS IS DEBUG INFO, PRINT SOMETHING BETTER
                type,
                defendingPokemon.pokeType,
                isStrongAgainst.get(type).contains(defendingPokemon.pokeType)
            )
        );
        if ( isStrongAgainst.containsKey( type ) ) {
            if ( isStrongAgainst.get( type ).contains(defendingPokemon.pokeType) ) {
                strongBool = true;
            }
        }
        int damageTaken;
        double attackDefenseRatio;
        if (defendingPokemon.dP >= attackingPokemon.aP) {
            damageTaken = 1;
        }
        else {
            attackDefenseRatio = (double) attackingPokemon.aP / defendingPokemon.dP;
            damageTaken = (int) (attackDefenseRatio * 1.5);
        }
        if (strongBool) {
            damageTaken *= 2;
            System.out.println("It's super effective!");
        }
        defendingPokemon.health = defendingPokemon.health - damageTaken;
        System.out.println(
            String.format(
                "%s has %d health",
                defendingPokemon.name,
                defendingPokemon.health
            )
        );
    }
}

/*
   2 fire moves
   2 water moves
   2 grass moves
   a couple normal moves to throw in
*/

class Ember extends Move {
    
    public Ember() {
        super(
            "Ember", // name
            pokemon.Type.FIRE, // type
            10 // average damage
        );
    }
}

class VineWhip extends Move {
    
    public VineWhip() {
        super(
            "Vine Whip", // name
            pokemon.Type.GRASS, // type
            10 // average damage
        );
    }
}

class WaterGun extends Move {
    
    public WaterGun() {
        super(
            "Water Gun", // name
            pokemon.Type.WATER, // type
            10 // average damage
        );
    }
}

class Fire1 extends Move {
    
    public Fire1() {
        super(
            "Fire1", // name
            pokemon.Type.FIRE, // type
            10 // average damage
        );
    }
}

class Fire2 extends Move {
    
    public Fire2() {
        super(
            "Fire2", // name
            pokemon.Type.FIRE, // type
            10 // average damage
        );
    }
}
class Water1 extends Move {
    
    public Water1() {
        super(
            "Water1", // name
            pokemon.Type.WATER, // type
            10 // average damage
        );
    }
}

class Grass1 extends Move {
    
    public Grass1() {
        super(
            "Grass1", // name
            pokemon.Type.GRASS, // type
            10 // average damage
        );
    }
}

class Pound extends Move {
    
    public Pound() {
        super(
            "Pound", // name
            pokemon.Type.NORMAL, // type
            10 // average damage
        );
    }
}
