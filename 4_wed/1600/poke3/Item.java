public class Item {
    public static enum ItemType {HEALING, ANTIDOTE, BUFF};
    public Item.ItemType type;
    public Item() {
        
    }
    public void Use() {
        
    }
}

class Potion extends Item {
    int healAmount;
    public Potion(int healAmountParam) {
        super();
        healAmount = healAmountParam;
        type = Item.ItemType.HEALING;
    }
    public void Use(pokemon inPoke) {
        inPoke.increaseHealth(healAmount);
    }
}