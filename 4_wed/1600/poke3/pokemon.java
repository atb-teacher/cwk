import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;

public class pokemon
{
    public static enum Type {FIRE, WATER, GRASS, GLITCH, ELECTRIC, GHOST, BUG, STEEL, ICE, GROUND, ROCK, NORMAL};
    public static enum Difficulty {EASY, NORMAL, HARD};
    public ArrayList<Move> moves = new ArrayList<Move>();
    public int ID;
    public int hp; // hitpoints
    public int lv; // level
    public int aP; // attack power
    public int dP; // defense power
    public int sP; // speed
    public int health = -1; // current hitpoints
    public String name;
    public Type pokeType;
    public boolean alive;
    
    /* Make a pokemon without specifying moves */
    public pokemon(String inName, Type inType)
    {
        alive = true;
        name = inName;
        pokeType = inType;
        moves.add(new Pound());
    }
    
    /* Make a pokemon specifying moves */
    public pokemon(String inName, Type inType, ArrayList<Move> inMoves) {
        alive = true;
        name = inName;
        pokeType = inType;
        moves = inMoves;
    }
    
    public boolean updateAlive() {
        alive = health > 0;
        return alive;
    }
    
    public void printDescription() {
        System.out.println(
            String.format(
                "%s is a %s type pokemon",
                name,
                getType()
            )
        );
    }
    
    public String getType() {
        switch (pokeType) {
            case FIRE:
                return "fire";
            case WATER:
                return "water";
            case GRASS:
                return "grass";
            case GLITCH:
                return "ă̴̮̾̀̆́̈́͊̕̕o̷̢̮̞͓͎̙̿͆̍͊̿́͋̕e̶̻̮̓̅̈́̏̒̒̚ḯ̶̡̨̛͙̭͉͓̹̦̫͚̝̾̀̀̚n̴̫̯̒̿͌̇̃̐͛́́̚͠ỏ̶̰̣͚̲̰̏̓͆̄͜͝ͅi̸̩̲̺̦̞͔̱͓̭̭̦̫̤͒͜͝d̴͖̜̦̘̖͕͎̞̬̥͇̏s̸̢̮̩̞̤͉͔̥̭̪̮̱͚͒̆̍̈́̕į̴̲̠̇̓̐̃͐̃́͝͝o̵̢͎͓̹̯͉͓̐̊̿̓̇̃̓̾͐̏͑̂̒̕͜͝h̷̨̨̡̗̼̟̠̣̻̥̮̘͉̊͠ͅa̵̛̺͇̪͚̖̳̋̃̓̈̃̾̈́̑̆́̀͒͝d̶̖̮̯̟̠̝̼͈̍̏̉͋̈̏́̀́̕͘̕ḯ̸͍̼̘̹̻͙̰̈̓̇̔̉̔̃̉͘̕͜͝͝";
            default:
                return "none";
        }
    }
    
    
    
    public boolean isCatchable(String ballType) {
        int combined = lv * health;
        if (ballType == "basic" && combined < 25) {
            return true;
        }
        else if (ballType == "master" && combined < 200) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public void attack(pokemon otherPoke) {
        int damageTaken;
        double attackDefenseRatio;
        if (otherPoke.dP >= aP) {
            damageTaken = 1;
        }
        else {
            attackDefenseRatio = (double) aP / otherPoke.dP;
            damageTaken = (int) (attackDefenseRatio * 1.5);
        }
        otherPoke.health = otherPoke.health - damageTaken;
    }
    
    public void setMaxHealth() {
        health = hp;
    }
    
    public int getHealth() {
        return health;
    }
    
    public void increaseHealth(int inHealth) {
        health += inHealth;
        if (health > hp) {
            health = hp;
        }
    }
    
    public void decreaseHealth(int inDamage) {
        health -= inDamage;
    }
    
    public static void main(String args[]) throws Exception {
        GameSession mainSession = new GameSession();
        mainSession.run();
    }
}



class StarterFactory {
    public static pokemon MakeStarter(String typeString) {
        switch (typeString) {
            case "Fire":
                return new Scorbunny();
            case "Water":
                return new Sobble();
            case "Grass":
                return new Grookey();
            default:
                return new Missingno();
        }
    }
}

class WildFactory {
    public static String[] options = {"Grookey"};
    public static pokemon MakeWild(String typeString) {
        switch (typeString) {
            case "Grookey":
                return new Grookey();
            default:
                return new Pikachu();
            }
    }
}



class Pikachu extends pokemon
{
    public Pikachu() {
        super("Pikachu", pokemon.Type.ELECTRIC);
        ID = 1;
        hp = 35;
        lv = 5;
        aP = 89;
        dP = 15;
        sP = 50;
    }
}


// import java.util.List;
class Grookey extends pokemon
{    
    public Grookey() {

        super(
            "Grookey", 
            pokemon.Type.GRASS, 
            new ArrayList<Move>(
                List.of(
                    new VineWhip(),
                    new Grass1(),
                    new Pound()
                )
            )
        );
        hp = 40;
        lv = 5;
        aP = 65;
        dP = 50;
        sP = 65;
    }
}

class Scorbunny extends pokemon
{
    public Scorbunny() {
        super("Scorbunny", pokemon.Type.FIRE);
        hp = 55;
        lv = 5;
        aP = 71;
        dP = 40;
        sP = 69;
    }
}

class Sobble extends pokemon
{
    public Sobble() {
        super("Sobble", pokemon.Type.WATER);
        hp = 45;
        lv = 5;
        aP = 40;
        dP = 40;
        sP = 70;
    }
}


class Missingno extends pokemon
{
    public Missingno() {
        super("Missingno", pokemon.Type.GLITCH);
        hp = 35;
        lv = 5;
        aP = 89;
        dP = 15;
        sP = 50;
    }
}



class Vulpix extends pokemon
{
    public Vulpix() {
        super("Vulpix", pokemon.Type.FIRE);
        hp = 35;
        lv = 5;
        aP = 89;
        dP = 15;
        sP = 50;
    }
}

class Staryu extends pokemon {
    public Staryu() {
        super("Staryu", pokemon.Type.WATER);
        hp = 40;
        lv = 5;
        aP = 55;
        dP = 60;
        sP = 75;
    }
}


