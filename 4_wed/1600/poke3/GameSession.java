import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
import java.util.ArrayList;

public class GameSession {
    public pokemon.Difficulty gameDiff;
    public Scanner myScanner;
    public pokemon playerPokemon;
    public ArrayList<Item> inventory;
    
    public void run() throws Exception{
        myScanner = new Scanner(System.in);
        Move.setupIsStrongAgainst();
        setGameDiff();
        setStarter();
        gameLoop();
    }
    
    public void setupInv() {
        // TEMP CODE
        inventory = new ArrayList();
        for (int i=0; i<3; i++) {
            inventory.add(
                new Potion(15 + (i*5))
            );
        }
    }
    
    public void setGameDiff() throws Exception {
        System.out.println("Choose your difficuly: Easy, Normal, Hard");
        String difficultyStr = myScanner.nextLine();
        String[] Difficulty = {"Easy", "Normal", "Hard", "easy", "normal", "hard", "E", "N", "H", "e", "n", "h"};
        while (!Arrays.asList(Difficulty).contains(difficultyStr)) {
            System.out.println("Choose your difficulty: Easy, Normal, or Hard");
            difficultyStr = myScanner.nextLine();
        }
        int goalWins;
        switch (difficultyStr) {
            case "e":
            case "easy":        
            case "Easy":
            case "E":
                gameDiff = pokemon.Difficulty.EASY;
                goalWins = 5;
                break;
            case "n":
            case "normal":
            case "Normal":
            case "N":
                gameDiff = pokemon.Difficulty.NORMAL;
                goalWins = 10;
                break;
            case "h":
            case "hard":
            case "Hard":
            case "H":
                gameDiff = pokemon.Difficulty.HARD;
                goalWins = 20;
                break;
            default:
                throw new Exception("This is not a difficulty setting.");
        }
    }
    
    public void setStarter() {
        String typeString = "";
        System.out.println("Choose you pokemon");
        String pokemoncharacter = myScanner.nextLine();
        String[] pokemon = {"Grookey", "grookey", "Sobble", "sobble", "Scorbunny", "scorbunny"};
        while (!Arrays.asList(pokemon).contains(pokemoncharacter)) {
            System.out.println("Choose your pokemon");
            pokemoncharacter = myScanner.nextLine();
        }
        // System.out.println(pokemoncharacter);
        switch (pokemoncharacter) {
            case "Grookey":
                typeString = "Grass";
                break;
            case "Sobble":
                typeString = "Water";
                break;
            case "Scorbunny":
                typeString = "Fire";
                break;
            default:
                typeString = "MISSINGNO";
                break;
        }
        playerPokemon = StarterFactory.MakeStarter(typeString);
        playerPokemon.setMaxHealth();
        
        playerPokemon.printDescription();
    }
    
    public pokemon generateWildPokemon() {
        Random rand = new Random();
        return WildFactory.MakeWild(
            WildFactory.options[rand.nextInt(WildFactory.options.length)]
        );
    }

    public Move chooseMove(pokemon inPokemon) {
        System.out.println("Choose a move");
        for (int i=0; i<inPokemon.moves.size(); i++) {
            System.out.println(
                String.format(
                    "%d) %s",
                    i,
                    inPokemon.moves.get(i).name
                )
            );
        }
        int chosenMove = Integer.parseInt(myScanner.nextLine());
        return inPokemon.moves.get(chosenMove);
    }
    
    public void fightPokemon() {
        // setup
        pokemon wildPokemon = generateWildPokemon();
        wildPokemon.setMaxHealth();
        System.out.println(
            String.format(
                "Fighting %s, a %s type pokemon",
                wildPokemon.name,
                wildPokemon.pokeType
            )
        );
        // fight loop
        boolean fighting = true;
        String playerChoice = "fight";
        while (fighting) {
            System.out.println("choose (fight), use (item), or (run):"); // CHANGED
            playerChoice = myScanner.nextLine();
            String[] options = {"fight", "run", "item"}; // CHANGED
            while (!Arrays.asList(options).contains(playerChoice)) {
                System.out.println("choose (fight), use (item), or (run):"); // CHANGED
                playerChoice = myScanner.nextLine();
            }
            
            Move pokeMove;
            if (playerChoice.equals("fight")) {
                if (playerPokemon.sP >= wildPokemon.sP) {
                    pokeMove = chooseMove(playerPokemon);
                    pokeMove.Attack(playerPokemon, wildPokemon);
                    wildPokemon.updateAlive();
                    if (wildPokemon.alive) {
                        pokeMove = wildPokemon.moves.get(0);
                        pokeMove.Attack(wildPokemon, playerPokemon);
                        playerPokemon.updateAlive();
                    }
                } else {
                    pokeMove = wildPokemon.moves.get(0);
                    pokeMove.Attack(wildPokemon, playerPokemon);
                    playerPokemon.updateAlive();
                    if (playerPokemon.alive) {
                        pokeMove = chooseMove(playerPokemon);
                        pokeMove.Attack(playerPokemon, wildPokemon);
                        wildPokemon.updateAlive();
                    }
                }
                if (!wildPokemon.alive || !playerPokemon.alive) { // wild pokemon is dead or player pokemon is dead
                    fighting = false;
                    if (!wildPokemon.alive) {
                        System.out.println("Wild Pokemon defeated!");
                    }
                    if (!playerPokemon.alive) {
                        System.out.println("Player Pokemon defeated!");
                    }
                }
            }
            else if (playerChoice.equals("run")) {
                System.out.println("running away");
                fighting = false;
            }
            else if (playerChoice.equals("item")) {
                useItemFromInventory();
            }
        }
        // think more about when fighting ends
        
        /*
         * 
        */
        
        /*
        We have access to the "playerPokemon" variable,
        so we can create a variable called "wildPokemon"
        and use our factory to make the wild pokemon.
        We can have them take turns fighting each other
        until one is out of health.
        
        We can add a "get health" method to the
        pokemon, which will allow us to update
        */
        
    }
    
        
    public void gameLoop() {
        System.out.println("What do you want to do?");
        System.out.println("1) Fight.");
        System.out.println("2) Quit.");
        String playerChoice = myScanner.nextLine().toLowerCase();
        String[] chooseFight = {"1", "f","fight"};
        String[] chooseEnd = {"2", "q", "quit"};
        if (Arrays.asList(chooseFight).contains(playerChoice)) {
            fightPokemon();
        }else if (Arrays.asList(chooseEnd).contains(playerChoice)) {
            System.out.println("end game loop");
        }
    }
}

