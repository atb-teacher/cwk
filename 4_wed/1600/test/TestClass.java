
public class TestClass
{
    public static void main(String[] args) {
        String favPie = "strawberry";
        String color = "unchanged";
        switch(favPie) {
            case "blueberry":
                System.out.println("blue!");
                color = "blue";
                break;
            case "strawberry":
                System.out.println("red!");
                color = "red";
                break;
            default:
                System.out.println("no match!");
                color = "default!";
                break;
        }
        System.out.println(color);
    }
}
