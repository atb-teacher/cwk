 

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class pokemon
{
    public static enum Type {FIRE, WATER, GRASS, GLITCH, ELECTRIC, GHOST};
    public static enum Difficulty {EASY, NORMAL, HARD};
    public int ID;
    public int hp; // hitpoints
    public int lv; // level
    public int aP; // attack power
    public int dP; // defense power
    public int sP; // speed
    public int health = -1; // current hitpoints
    public String name;
    public Type pokeType;
    public pokemon(Type inType)
    {
        pokeType = inType;
    }
    
    public void printDescription() {
        System.out.println(
            String.format(
                "This is a %s type pokemon",
                getType()
            )
        );
    }
    
    public String getType() {
        switch (pokeType) {
            case FIRE:
                return "fire";
            case WATER:
                return "water";
            case GRASS:
                return "grass";
            case GLITCH:
                return "ă̴̮̾̀̆́̈́͊̕̕o̷̢̮̞͓͎̙̿͆̍͊̿́͋̕e̶̻̮̓̅̈́̏̒̒̚ḯ̶̡̨̛͙̭͉͓̹̦̫͚̝̾̀̀̚n̴̫̯̒̿͌̇̃̐͛́́̚͠ỏ̶̰̣͚̲̰̏̓͆̄͜͝ͅi̸̩̲̺̦̞͔̱͓̭̭̦̫̤͒͜͝d̴͖̜̦̘̖͕͎̞̬̥͇̏s̸̢̮̩̞̤͉͔̥̭̪̮̱͚͒̆̍̈́̕į̴̲̠̇̓̐̃͐̃́͝͝o̵̢͎͓̹̯͉͓̐̊̿̓̇̃̓̾͐̏͑̂̒̕͜͝h̷̨̨̡̗̼̟̠̣̻̥̮̘͉̊͠ͅa̵̛̺͇̪͚̖̳̋̃̓̈̃̾̈́̑̆́̀͒͝d̶̖̮̯̟̠̝̼͈̍̏̉͋̈̏́̀́̕͘̕ḯ̸͍̼̘̹̻͙̰̈̓̇̔̉̔̃̉͘̕͜͝͝";
            default:
                return "none";
        }
    }
    
    
    
    public boolean isCatchable(String ballType) {
        int combined = lv * health;
        if (ballType == "basic" && combined < 25) {
            return true;
        }
        else if (ballType == "master" && combined < 200) {
            return true;
        }
        else {
            return false;
        }
    }
    
    public void attack(pokemon otherPoke) {
        int damageTaken;
        double attackDefenseRatio;
        if (otherPoke.dP >= aP) {
            damageTaken = 1;
        }
        else {
            attackDefenseRatio = (double) aP / otherPoke.dP;
            damageTaken = (int) (attackDefenseRatio * 1.5);
        }
        otherPoke.hp = otherPoke.hp - damageTaken;
    }
    
    public void setMaxHealth() {
        health = hp;
    }
    
    /*
    public boolean isCatchable(Item pokeBall) {
        int combined = lv * health;
        if (pokeBall.getType() == "basic" && combined < 25) {
            return true;
        }
        else if (pokeBall.getType() == "master" && combined < 200) {
            return true;
        }
        return false;
    }
    */
   
    public static void main(String args[]) throws Exception {
        GameSession mainSession = new GameSession();
        mainSession.run();
        //Scanner myScanner = new Scanner(System.in);
        //System.out.println("Choose your difficulty: Easy, Normal, Hard");
        //String difficultyStr = myScanner.nextLine();
        //String[] Difficulty = {"Easy", "Normal", "Hard", "easy", "normal", "hard", "E", "N", "H", "e", "n", "h"};
        //while (!Arrays.asList(Difficulty).contains(difficultyStr)) {
            //System.out.println("This is not a difficulty setting, please try again.");
            //difficultyStr = myScanner.nextLine();
    }
        //pokemon.Difficulty gameDiff;
        //int goalWins;
        //switch (difficultyStr) {
            //case "e":
            //case "easy":        
            //case "Easy":
            //case "E":
                //gameDiff = pokemon.Difficulty.EASY;
                //goalWins = 5;
                //break;
            //case "n":
            //case "normal":
            //case "Normal":
            //case "N":
                //gameDiff = pokemon.Difficulty.NORMAL;
                //goalWins = 10;
                //break;
            //case "h":
            //case "hard":
            //case "Hard":
            //case "H":
                //gameDiff = pokemon.Difficulty.HARD;
                //goalWins = 20;
                //break;
            //default:
                //throw new Exception("This is not a difficulty setting.");
        }
        //System.out.println("Choose you pokemon");
        //pokemon playerPokemon;
        //String typeString = "";
        //String pokemoncharacter = myScanner.nextLine();
        //String[] pokemon = {"Grookey", "grookey", "Sobble", "sobble", "Scorbunny", "scorbunny"};
        //while (!Arrays.asList(pokemon).contains(pokemoncharacter)) {
            //System.out.println("Choose your pokemon");
            //pokemoncharacter = myScanner.nextLine();
        //}
        //switch (pokemoncharacter) {
            //case "Grookey":
                //typeString = "Grass";
            //case "Sobble":
                //typeString = "Water";
            //case "Scorbunny":
                //typeString = "Fire";
            //default:
                //typeString = "MISSINGNO";
        //}
        //System.out.println("choose a pokemon");
        //playerPokemon = StarterFactory.MakeStarter(typeString);
        //playerPokemon.printDescription();
        //System.out.println("What do you want to do?");
        //System.out.println("1) Fight.");
        //System.out.println("2) Quit.");
        //String playerChoice = myScanner.nextLine().toLowerCase();
        //String[] chooseFight = {"1", "f","fight"};
        //String[] chooseEnd = {"2", "q", "quit"};
        //if (Arrays.asList(chooseFight).contains(playerChoice)) {
            //System.out.println("Fight");
        //}else if (Arrays.asList(chooseEnd).contains(playerChoice)) {
            //System.out.println("end game loop");
        //}
    //}
    
    //public static PokeBuilder builder() {
        //return new PokeBuilder();
    //}

    //static class PokeBuilder {
        //public int lv = 1;
        //public int hp = 1;
        //public int aP = 1;
        //public int dP = 1;
        //public int sP = 1;
        //public String name = "Missingno";
        //public Type pokeType = Type.GLITCH;
        
        //public PokeBuilder setName(String nameParam) {
            //this.name = nameParam;
            //return this;
        //}
        
        //public pokemon build() {
            //return new Missingno();
        //}
    //}
//}



class Grookey extends pokemon
{
    public Grookey() {
        super(pokemon.Type.GRASS);
        hp = 40;
        lv = 5;
        aP = 65;
        dP = 50;
        sP = 65;
    }
}

class Scorbunny extends pokemon
{
    public Scorbunny() {
        super(pokemon.Type.FIRE);
        hp = 55;
        lv = 5;
        aP = 71;
        dP = 40;
        sP = 69;
    }
}

class Sobble extends pokemon
{
    public Sobble() {
        super(pokemon.Type.WATER);
        hp = 45;
        lv = 5;
        aP = 40;
        dP = 40;
        sP = 70;
    }
}

class Missingno extends pokemon
{
    public Missingno() {
        super(pokemon.Type.GLITCH);
        hp = 35;
        lv = 5;
        aP = 89;
        dP = 15;
        sP = 50;
    }
}

class Pikachu extends pokemon
{
    public Pikachu() {
        super(pokemon.Type.ELECTRIC);
        ID = 1;
        hp = 35;
        lv = 5;
        aP = 89;
        dP = 15;
        sP = 50;
    }
}

class Vulpix extends pokemon
{
    public Vulpix() {
        super(pokemon.Type.FIRE);
        hp = 35;
        lv = 5;
        aP = 89;
        dP = 15;
        sP = 50;
    }
}

class Staryu extends pokemon {
    public Staryu() {
        super(pokemon.Type.WATER);
        hp = 40;
        lv = 5;
        aP = 55;
        dP = 60;
        sP = 75;
    }
}

public class StarterFactory {
    public static pokemon MakeStarter(String typeString) {
        switch (typeString) {
            case "Fire":
                return new Scorbunny();
            case "Water":
                return new Sobble();
            case "Grass":
                return new Grookey();
            default:
                return new Missingno();
        }
    }
}

puclass WildFactory {
    public static String[] options = {"Pikachu"};
    public static pokemon MakeWild(String typeString) {
        switch (typeString) {
            case "Pikachu":
                return new Pikachu();
            default:
                return new Pikachu();
            }
    }
}



class Item {
    
}



class GameSession {
    pokemon.Difficulty gameDiff;
    Scanner myScanner;
    pokemon playerPokemon;
    
    
    public void run() throws Exception{
        myScanner = new Scanner(System.in);
        setGameDiff();
        setStarter();
    }
    
    public void setGameDiff() throws Exception {
        System.out.println("Choose your difficuly: Easy, Normal, Hard");
        String difficultyStr = myScanner.nextLine();
        String[] Difficulty = {"Easy", "Normal", "Hard", "easy", "normal", "hard", "E", "N", "H", "e", "n", "h"};
        while (!Arrays.asList(Difficulty).contains(difficultyStr)) {
            System.out.println("Choose your difficulty: Easy, Normal, or Hard");
            difficultyStr = myScanner.nextLine();
        }
        int goalWins;
        switch (difficultyStr) {
            case "e":
            case "easy":        
            case "Easy":
            case "E":
                gameDiff = pokemon.Difficulty.EASY;
                goalWins = 5;
                break;
            case "n":
            case "normal":
            case "Normal":
            case "N":
                gameDiff = pokemon.Difficulty.NORMAL;
                goalWins = 10;
                break;
            case "h":
            case "hard":
            case "Hard":
            case "H":
                gameDiff = pokemon.Difficulty.HARD;
                goalWins = 20;
                break;
            default:
                throw new Exception("This is not a difficulty setting.");
        }
    }
    
    public void setStarter() {
        String typeString = "";
                System.out.println("Choose you pokemon");
        pokemon playerPokemon;
        String pokemoncharacter = myScanner.nextLine();
        String[] pokemon = {"Grookey", "grookey", "Sobble", "sobble", "Scorbunny", "scorbunny"};
        while (!Arrays.asList(pokemon).contains(pokemoncharacter)) {
            System.out.println("Choose your pokemon");
            pokemoncharacter = myScanner.nextLine();
        }
        System.out.println(pokemoncharacter);
        switch (pokemoncharacter) {
            case "Grookey":
                typeString = "Grass";
                break;
            case "Sobble":
                typeString = "Water";
                break;
            case "Scorbunny":
                typeString = "Fire";
                break;
            default:
                typeString = "MISSINGNO";
                break;
        }
        System.out.println(typeString);
        playerPokemon = StarterFactory.MakeStarter(typeString);
        playerPokemon.setMaxHealth();
        playerPokemon.printDescription();
        System.out.println("What do you want to do?");
        System.out.println("1) Fight.");
        System.out.println("2) Quit.");
        String playerChoice = myScanner.nextLine().toLowerCase();
        String[] chooseFight = {"1", "f","fight"};
        String[] chooseEnd = {"2", "q", "quit"};
        if (Arrays.asList(chooseFight).contains(playerChoice)) {
            System.out.println("Fight");
        }else if (Arrays.asList(chooseEnd).contains(playerChoice)) {
            System.out.println("end game loop");
        }
    }
    
    public pokemon generateWildPokemon() {
        Random rand = new Random();
        return WildFactory.MakeWild(
            WildFactory.options[rand.nextInt(WildFactory.options.length)]
        );
        
        //return WildFactory.MakeWild("Pikachu");
    }
    /*
     * 
     * class WildFactory {
    public static String[] options = {"Pikachu"};
    public static pokemon MakeWild(String typeString) {
        switch (typeString) {
            case "Pikachu":
                return new Pikachu();
            } 
    }
}
     */
    
    public void fightPokemon() {
        System.out.println("Fight");
        pokemon wildPokemon = generateWildPokemon();
        wildPokemon.setMaxHealth();
        /*
        We have access to the "playerPokemon" variable,
        so we can create a variable called "wildPokemon"
        and use our factory to make the wild pokemon.
        We can have them take turns fighting each other
        until one is out of health.
        
        We can add a "get health" method to the
        pokemon, which will allow us to update
        */
        
    }
    
        
    public void gameLoop() {
        System.out.println("What do you want to do?");
        System.out.println("1) Fight.");
        System.out.println("2) Quit.");
        String playerChoice = myScanner.nextLine().toLowerCase();
        String[] chooseFight = {"1", "f","fight"};
        String[] chooseEnd = {"2", "q", "quit"};
        if (Arrays.asList(chooseFight).contains(playerChoice)) {
            fightPokemon();
        }else if (Arrays.asList(chooseEnd).contains(playerChoice)) {
            System.out.println("end game loop");
        }
    }
    }
    //public static PokeBuilder builder() {
        //return new PokeBuilder();
    //}