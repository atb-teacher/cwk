sprite = codesters.Sprite(
    "chick3", # image name    
    0, # x
    200, # y
)

def left_rice(sprite):
    previous_speed = sprite.get_x_speed()
    sprite.set_x_speed(
        previous_speed - 0.5
    )
    
sprite.event_key(
    "left", # left key on keyboard
    left_rice, # our instructions to go left
)
