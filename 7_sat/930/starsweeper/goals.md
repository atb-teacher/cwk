## Main goals

* Create a list
  * Use the list to track what is and is not a mine
  * Have one place in the list for each star
* Animate explosions


## Stretch goals
* End screen
* Radar
