# Main goals

* Make the health go down when you get hit with a barrel
* Give the hero 2 seconds invincibility after taking damage

## Stretch goals

* Make barrels that launch in from the sides

## Advanced goals

* Create an enemy pirate that chases you
* Let the player throw barrels
