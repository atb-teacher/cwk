# Goals
* Make the panther move
* Make the trees stop the player
* Make the panther stop the player



# Stretch Goals
* Give Amelia health (hearts)
* Make powerup items
* Make abilities

# Advanced Goals
* Make multiple levels
* Make the friend betray you
* Make an ice level
* Make Amelia able to distract the panther