# Goals

* Enemies should follow the ship
* Enemies should damage the ship
* Lasers should damage the enemies
* Time delay on lasers
* Make robots spawn on edges
