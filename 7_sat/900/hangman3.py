# the list of possible words
possible_words = [
    "apple",
    "lasagna",
    "basketball",
    "coding",
    "country",
    "jazzfusion",
    ]

def end_screen(won):
    stage.clear()
    if won == True:
        end_text = codesters.Text("YOU WIN!", 0, 0)
    else:
        end_text = codesters.Text("LOSE!", 0, 0)
        

# choosing a random word from the list
word = random.choice(possible_words)

# making the underscore list
presented_word = []
for letter in word:
    presented_word.append('_')

finished = False # ADDED

guess_counter = 0

while finished == False: # CHANGED
    stage.clear()
    # making it as a presentable string
    presented_word_string = " ".join(presented_word)
    word_sprite = codesters.Text(presented_word_string, -100, 100)

    # let the user guess
    guess = stage.ask("Give me a letter", 0, 0)
    guess_counter += 1
    
    # check if letter is correct
    if guess in word:
        print("correct!")
        print(presented_word)
        for i in range(len(word)): # give all indices we can use
            if guess == word[i]: # compares that index in the word to the guessed letter
                presented_word[i] = guess # overwrite the underscores

    if "".join(presented_word) == word: # ADDED
        print("You won!") # ADDED
        print(f"It took you {guess_counter} guesses!") # ADDED
        finished = True # ADDED

stage.clear()
# making it as a presentable string
presented_word_string = " ".join(presented_word)
word_sprite = codesters.Text(presented_word_string, -100, 100)
stage.wait(2)
end_screen(won=True)

"""
TODO:
* area for guessed wrong letters
* draw the snowman as they guess wrong
* count guessed correct vs. guessed incorrect letters
* tell the user the minimum number of guesses needed for a word
* create a win screen and a lose screen
"""




