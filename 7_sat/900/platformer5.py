# MAKE SPRITES

sprite = codesters.Sprite("astronaut7", 0, 150)
sprite.set_size(0.15)

def set_sides(in_platform):
    in_platform.left_side = in_platform.get_x() - (in_platform.get_width() / 2 )
    in_platform.right_side = in_platform.get_x() + (in_platform.get_width() / 2 )


platforms = []
for x, y, width, height in [
    (0, 0, 100, 25),
    (0, -100, 100, 25),
    (0, -225, 500, 50),
]:
    platform1 = codesters.Rectangle(x, y, width, height, "darkred")
    platforms.append(platform1)
    platform1.set_gravity_off()
    set_sides(platform1)

# HANDLE GRAVITY

stage.set_gravity(10)

# DEBUGGING TOUCHING GROUND

sprite.grounded = False
grounded_text = codesters.Text(f"grounded: {sprite.grounded}", -175, 200)

def update_grounded_text():
    grounded_text.set_text(f"grounded: {sprite.grounded}")

def collision(in_sprite, hit_sprite):
    shape_color = hit_sprite.get_color()
    if shape_color == "darkred":
        in_sprite.set_y_speed(0)
        in_sprite.set_gravity_off()
        # HANDLE GROUNDED
        sprite.grounded = True
        update_grounded_text()
        

sprite.event_collision(collision)

def jump_clash_royale():
    if sprite.grounded == True: # if the sprite is on the ground
        sprite.grounded = False # the sprite is no longer on the ground
        update_grounded_text()
        sprite.jump(15)
        sprite.set_gravity_on()
    
stage.event_key("space", jump_clash_royale)
stage.event_key("up", jump_clash_royale)

def left_key(sprite):
    sprite.move_left(20)
    if sprite.get_x() < platform1.left_side:
        sprite.set_gravity_on()
sprite.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)
    if sprite.get_x() > platform1.right_side:
        sprite.set_gravity_on()
sprite.event_key("right", right_key)

