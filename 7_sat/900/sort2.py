# SETUP SPRITE

sprite = codesters.Sprite("alien1", 0, -100)
sprite2 = codesters.Sprite("alien1", 0, 100)

def randomize_first_item_position(list_param):
    first_item = list_param.pop(0)
    list_length = len(list_param)
    random_index = random.randrange(list_length + 1)
    list_param.insert(random_index, first_item)

def sort_smallest_item(list_param):
    smallest_item = list_param[0]
    index_of_smallest = 0
    for index in range(len(list_param)):
        if list_param[index] < smallest_item:
            smallest_item = list_param[index]
            index_of_smallest = index
    
    # temp code
    return f"the smallest is {smallest_item}" + \
    f" at index {index_of_smallest}"
        

# SETUP LIST

sorted_list = list(range(101, 111))

# sprite.say(sorted_list)

# stage.wait(2)

randomize_first_item_position(sorted_list)

sprite.say(sorted_list)

stage.wait(2)

sprite2.say(
    sort_smallest_item(
        sorted_list
    )
)
