# SETUP SPRITE

sprite = codesters.Sprite("alien1")

def randomize_first_item_position(list_param):
    first_item = list_param.pop(0)
    list_length = len(list_param)
    random_index = random.randrange(list_length + 1)
    list_param.insert(random_index, first_item)
    

# SETUP LIST

sorted_list = list(range(101, 111))

sprite.say(sorted_list)

stage.wait(2)

randomize_first_item_position(sorted_list)

sprite.say(sorted_list)
