# ========== SETUP BACKGROUND ==========

stage.set_background("space")

# ========== SETUP SPRITES ==========

shuttle = codesters.Sprite("spaceshuttle", -210, 210)
shuttle.set_size(0.20)
shuttle.offset = 45
shuttle.set_rotation(0 + shuttle.offset)


ufo = codesters.Sprite("ufo")
ufo.set_size(0.25)


# ========== SETUP CONTROLS ==========

def shuttle_up_batman():
    shuttle.move_up(20)
    shuttle.set_rotation(0 + shuttle.offset)
    

stage.event_key("w", shuttle_up_batman)

def shuttle_down_encanto():
    shuttle.move_down(20)
    shuttle.set_rotation(180 + shuttle.offset)
    
    
stage.event_key("s", shuttle_down_encanto)

def shuttle_left():
    sprite.move_left(20)
    
    
def shuttle_right():
    
def left_key(sprite):
    # add other actions...
    
sprite.event_key("left", left_key)

# ========== SETUP WALLS ==========

size = 175
for i in range(20):
    sprite = codesters.Rectangle(
        random.randint(-1 * size, size), # x-coordinate
        random.randint(-1 * size, size), # y-coordinate
        random.randint(10, 30), # width
        random.randint(10, 30), # height
        "darkred",
    )


