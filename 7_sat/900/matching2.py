sprites = []
for i in range(2):
    sprites.append(codesters.Sprite("evilknight", 500, 500))
    sprites.append(codesters.Sprite("ufo", 500, 500))

for one_sprite in sprites:
    one_sprite.set_size(.2)

"""
# sprite = codesters.Rectangle(x, y, width, height, "color")
sprite = codesters.Rectangle(-150, 0, 50, 75, "blue")
sprite = codesters.Rectangle(-75, 0, 50, 75, "blue")
sprite = codesters.Rectangle(0, 0, 50, 75, "blue")
sprite = codesters.Rectangle(75, 0, 50, 75, "blue")
sprite = codesters.Rectangle(150, 0, 50, 75, "blue")
"""
"""
# for x_pos in range(-150, 151, 75):
for x_pos in [-150, -75, 0, 75, 150]:
    sprite = codesters.Rectangle(x_pos, 0, 50, 75, "blue")
    
for x_pos in [-150, -75, 0, 75, 150]:
    sprite = codesters.Rectangle(x_pos, 100, 50, 75, "blue")
    
for x_pos in [-150, -75, 0, 75, 150]:
    sprite = codesters.Rectangle(x_pos, 200, 50, 75, "blue")
"""


for y_pos in [0, 100, 200]:
    for x_pos in [-150, -75, 0, 75, 150]:
        sprite = codesters.Rectangle(x_pos, y_pos, 50, 75, "blue")
