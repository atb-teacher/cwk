# ========== SETUP STAGE ==========

stage.disable_all_walls()

# ========== SETUP CHARACTERS ==========

fire_lady = codesters.Sprite("evilwitch", 200, 0)
wizard = codesters.Sprite("wizard", -200, 0)
wizard.flip_right_left()

# ========== SETUP FIREBALL ==========

def make_fireball(sprite):
    fireball = codesters.Sprite("sun", 600, 600)
    fireball.set_size(.1)
    fireball.set_x(sprite.get_x() - 60)
    fireball.set_y(sprite.get_y() + 40)
    fireball.set_x_speed(-5)

def shoot_fireball(sprite):
    make_fireball(sprite)

fire_lady.event_key_press("space", shoot_fireball)

def make_iceball(sprite):
    iceball = codesters.Sprite("c6e7fdf106dc4dfead292824e17a33d2",
    600, 600)
    iceball.set_size(.1)
    iceball.set_x(sprite.get_x() + 40)
    iceball.set_y(sprite.get_y() + 70)
    iceball.set_x_speed(5)

def shoot_iceball(sprite):
    make_iceball(sprite)


wizard.event_key_press("a", shoot_iceball)


"""
When we push the spacebar
* The fire lady shoots a fireball
* The fireball goes left
"""

