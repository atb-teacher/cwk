# SETUP SPRITES

sprite = codesters.Sprite("alien1", 0, -100)
sprite2 = codesters.Sprite("alien1", 0, 100)

def randomize_first_item_position(list_param):
    """Make a slightly unsorted list
    so we can practice sorting the smallest number"""
    first_item = list_param.pop(0)
    list_length = len(list_param)
    random_index = random.randrange(list_length + 1)
    list_param.insert(random_index, first_item)

def sort_smallest_item(list_param):
    """Sorting the smallest number"""
    smallest_item = list_param[0]
    index_of_smallest = 0
    for index, num_item in enumerate(list_param):
        if num_item < smallest_item:
            smallest_item = num_item
            index_of_smallest = index
    
    # instead of saying where the smallest number is,
    # we should move it to the beginning
    return f"the smallest is {smallest_item}" + \
    f" at index {index_of_smallest}"


def simple_sort(list_param):
    """an inefficient sorting algorithm"""
    output = []
    while len(list_param) > 0:
        smallest_item = list_param[0]
        index_of_smallest = 0
        for index, num_item in enumerate(list_param):
            if num_item < smallest_item:
                smallest_item = num_item
                index_of_smallest = index
        output.append(smallest_item)
        list_param.pop(index_of_smallest)
    return output

# SETUP LIST

sorted_list = list(range(101, 111))
randomized_list = list(range(101, 111))
random.shuffle(randomized_list)


sprite2.say(
    randomized_list
)

stage.wait(2)

output_list = simple_sort(randomized_list)

sprite.say(
    output_list
)
