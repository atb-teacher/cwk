# ========== SETUP BACKGROUND ==========

stage.set_background("space")
stage.disable_all_walls()

# ========== SETUP SPRITES ==========

shuttle = codesters.Sprite("spaceshuttle", -210, 210)
shuttle.set_size(0.20)
shuttle.offset = 45
shuttle.set_rotation(0 + shuttle.offset)
shuttle.health = 3

ufo = codesters.Sprite("ufo")
ufo.set_size(0.25)
ufo.health = 3


# ========== SETUP SHUTTLE CONTROLS ==========

def shuttle_up_batman():
    shuttle.move_up(20)
    shuttle.set_rotation(0 + shuttle.offset)
    

stage.event_key("w", shuttle_up_batman)

def shuttle_down_encanto():
    shuttle.move_down(20)
    shuttle.set_rotation(180 + shuttle.offset)
    
    
stage.event_key("s", shuttle_down_encanto)

def shuttle_left():
    shuttle.move_left(20)
    shuttle.set_rotation(90 + shuttle.offset)
    
stage.event_key("a", shuttle_left)

def shuttle_right():
    shuttle.move_right(20)
    shuttle.set_rotation(270 + shuttle.offset)

stage.event_key("d", shuttle_right)


# ========== SETUP UFO CONTROLS ==========
ufo.offset = 90

def ufo_up():
    ufo.move_up(20)
    ufo.set_rotation(0 + ufo.offset)

stage.event_key("up", ufo_up)

def ufo_down():
    ufo.move_down(20)
    ufo.set_rotation(180 + ufo.offset)

stage.event_key("down", ufo_down)

def ufo_left():
    ufo.move_left(20)
    ufo.set_rotation(90 + ufo.offset)

stage.event_key("left", ufo_left)

def ufo_right():
    ufo.move_right(20)
    ufo.set_rotation(270 + ufo.offset)

stage.event_key("right", ufo_right)

# ========== SETUP WALLS ==========

size = 175
for i in range(20):
    sprite = codesters.Rectangle(
        random.randint(-1 * size, size), # x-coordinate
        random.randint(-1 * size, size), # y-coordinate
        random.randint(10, 30), # width
        random.randint(10, 30), # height
        "darkred",
    )


def shot_collision(shot, other):
    my_var = other.get_color() 
    if my_var == "darkred":
        stage.remove_sprite(shot)
        
    if other.get_image_name() == "spaceshuttle" \
    and shot.owner == "ufo":
        shuttle.health -= 1
        if shuttle.health <= 0:
            stage.remove_sprite(shuttle)
    
    if other.get_image_name() == "ufo" \
    and shot.owner == "spaceshuttle":
        ufo.health -= 1
        if ufo.health <= 0:
            stage.remove_sprite(ufo)


def make_shot(in_sprite, sprite_angle):
    # MAKE A NEW SHOT
    shot = codesters.Sprite("sun", 600, 600)
    shot.event_collision(shot_collision)
    shot.set_size(0.05)
    shot.owner = in_sprite.get_image_name()
    shot.set_x(in_sprite.get_x())
    shot.set_y(in_sprite.get_y())

    if sprite_angle == 0 or sprite_angle == 360: # bottom down
        shot.set_y_speed(-20)
    elif sprite_angle == 90: # bottom pointing right
        shot.set_x_speed(20)
    elif sprite_angle == 180: # bottom up
        shot.set_y_speed(20)
    elif sprite_angle == 270: # bottom pointing left
        shot.set_x_speed(-20)    

while True:
    ufo.say(ufo.health)
    shuttle.say(shuttle.health)
    
    stage.wait(1)
    
    make_shot(
        ufo,
        ufo.get_rotation()
    )
    
    make_shot(
        shuttle,
        (shuttle.get_rotation() - shuttle.offset + 180) % 360
    )


