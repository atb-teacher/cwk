t = turtle.Turtle()

t.color("blue")

"""
for letter in ['a', 'b', 'c', 'd']:
    t.forward(50)
    t.left(90)
"""

def make_square(length):
    for i in range(4): # run 4 times
        t.forward(length)
        t.left(90)

def make_square_recursive(length):
    make_square(length)
    if length > 10:
        make_square_recursive(length - 5)
        
# make_square_recursive(length=30)

"""
alien = codesters.Sprite("alien1")

def blastoff(countdown_num):
    stage.wait(1)
    if countdown_num > 0:
        alien.say(countdown_num)
        blastoff(countdown_num - 1)
    else:
        alien.say("BLASTOFF" + "!" * 20)

blastoff(3)
"""

def make_triangle(length):
    for i in range(3):
        t.forward(length)
        t.left(120)
        
# make_triangle(100)

def make_triangle_recursive(degrees):
    t.setheading(degrees)
    make_triangle(50)
    if degrees > 10:
        make_triangle_recursive(degrees -10)

make_triangle_recursive(360)



