ice_string = "c6e7fdf106dc4dfead292824e17a33d2"

# ========== SETUP STAGE ==========

stage.disable_all_walls()
stage.set_background("castle")

# ========== SETUP CHARACTERS ==========

fire_lady = codesters.Sprite("evilwitch", 200, 0)
fire_lady.health = 5
wizard = codesters.Sprite("wizard", -200, 0)
wizard.health = 5
wizard.flip_right_left()
characters = [fire_lady, wizard]

# ========== SETUP FIREBALL ==========

def make_fireball(sprite):
    fireball = codesters.Sprite("sun", 600, 600)
    fireball.set_size(.1)
    fireball.set_x(sprite.get_x() - 60)
    fireball.set_y(sprite.get_y() + 40)
    fireball.set_x_speed(-5)

fire_lady.event_key_press("space", make_fireball)

def make_iceball(sprite):
    iceball = codesters.Sprite("c6e7fdf106dc4dfead292824e17a33d2",
    600, 600)
    iceball.set_size(.1)
    iceball.set_x(sprite.get_x() + 40)
    iceball.set_y(sprite.get_y() + 70)
    iceball.set_x_speed(5)

wizard.event_key_press("a", make_iceball)

# ========== SETUP COLLISION ==========

def collision_witch(person, attack):
    attack_type = attack.get_image_name() 
    if attack_type == ice_string:
        person.health = person.health - 1
        person.say(person.health)
        stage.remove_sprite(attack)
    
fire_lady.event_collision(collision_witch)

def collision_wizard(person, attack):
    attack_type = attack.get_image_name() 
    if attack_type == "sun":
        person.health = person.health - 1
        person.say(person.health)
        stage.remove_sprite(attack)
    
wizard.event_collision(collision_wizard)

counter = 0
x_speeds = [-3, -2, -1, 0, 1, 2, 3, 2, 1, 0, -1, -2]
game_running = True
while game_running == True:
    x_speed = x_speeds[counter % len(x_speeds)] # confusing
    for person in characters:
        person.set_x_speed(x_speed)
    stage.wait(.2)
    counter = counter + 1
    for person in characters:
        if person.health <= 0:
            game_running = False

for person in characters:
    person.set_x_speed(0)

"""
* aiming system
* movement

"""
