# Snowball Game
player = codesters.Sprite("robot", -200, -200)
player.health = 5

enemy_bg = codesters.Circle(200, 200, 100, "blue")
enemy = codesters.Sprite("hedgehog", 200, 200)
enemy.set_size(.8)
enemy.health = 5

stage.disable_all_walls()

def up_key():
    player.move_up(20)
    # add other actions...
    
# stage.event_key("w", up_key)
stage.event_key("g", up_key)

def left_key():
    player.move_left(20)
    # add other actions...
    
# stage.event_key("a", left_key)
stage.event_key("f", left_key)


def down_key():
    player.move_down(20)
    # add other actions...
    
# stage.event_key("s", down_key)
stage.event_key("h", down_key)

def right_key():
    player.move_right(20)
    # add other actions...
    
#stage.event_key("d", right_key)
stage.event_key("c", right_key)



def calc_vector(in_x, in_y):
    x_throw = in_x - player.get_x()
    y_throw = in_y - player.get_y()
    return [x_throw, y_throw]

def calc_magnitude(in_vector):
    return math.sqrt(in_vector[0] * in_vector[0] + in_vector[1] * in_vector[1])


def normalize(in_vector, in_mag):
    in_mag /= 10
    if in_mag == 0:
        return in_vector
    return [in_vector[0] / in_mag, in_vector[1] / in_mag]

def throw():
    
    throw_vector = calc_vector(
        stage.click_x(),
        stage.click_y(),
    )
    mag = calc_magnitude(throw_vector)
    
    normalized_vector = normalize(throw_vector, mag)
    snowball = codesters.Circle(player.get_x(), player.get_y(), 25, "gray")
    snowball.set_x_speed(normalized_vector[0])
    snowball.set_y_speed(normalized_vector[1])
    
def enemy_throw():
        snowball = codesters.Circle(200, 200, 25, "gray")
        snowball.set_x_speed(-1 * random.randint(1, 8))
        snowball.set_y_speed(-1 * random.randint(1, 8))
    
stage.event_click(throw)

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "gray":
        sprite.health -= 1
        stage.remove_sprite(hit_sprite)
        
player.event_collision(collision)
enemy.event_collision(collision)

for i in range(10):
    enemy_throw()
    player.say(f"My health is {player.health}")
    enemy.say(f"My health is {enemy.health}")
    stage.wait(1)
