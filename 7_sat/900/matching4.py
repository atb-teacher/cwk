sprites = []
for i in range(2):
    for sprite_name in ["evilknight", "ufo", "alien1_masked",
    "anemone", "butterfly", "shark7", "snowman",
    "present1", "rock", "scissors"]:
        sprites.append(codesters.Sprite(sprite_name, 500, 500))
    
    
for one_sprite in sprites:
    one_sprite.set_size(.2)

random.shuffle(sprites)

for y_pos in [-100, 0, 100, 200]: # added -100
    for x_pos in [-150, -75, 0, 75, 150]:
        codesters.Rectangle(x_pos, y_pos, 50, 75, "blue")
        sprite = sprites.pop()
        sprite.set_position(x_pos, y_pos)
        sprite.move_to_front()

