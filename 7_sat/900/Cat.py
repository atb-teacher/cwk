class Cat:
    def __init__(self, stamina_param, hunger_param):
        print("hello, world!")
        self.stamina = stamina_param
        self.hunger = hunger_param

    def feed(self):
        print("om nom nom")
        self.stamina += 1
        self.hunger -= 1

    def leap(self):
        if self.stamina > 5:
            print("the cat lept!")
            self.stamina -= 1
        else:
            print("the cat is too tired")


