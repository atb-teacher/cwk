class Cat:
  def __init__(self, _hunger):
    self.hunger = _hunger

  def feed(self, _food_num):
    self.hunger = self.hunger - _food_num

  def speak(self):
    if self.hunger > 5:
      print("meow")
    else:
      print("purrr")

myCat = Cat(_hunger = 7)
myCat.speak()
