# SETUP SPRITE

sprite = codesters.Sprite("alien1", 0, -250)
sprite.set_size(.3)

# SETUP TEXT BOXES

q_text = codesters.Text("question", 0, 200)
a_text = codesters.Text("a: stuff", -150, 150)
b_text = codesters.Text("b: stuff", 150, 150)
c_text = codesters.Text("c: stuff", -150, 50)
d_text = codesters.Text("d: stuff", 150, 50)

# SETUP QUESTIONS AND ANSWERS

questions = []
answers = []
correct_answers = []

# INDEX 0

questions.append("What year was the first Star Wars movie released?")

answers.append(
    [
        "1970",
        "1973",
        "1977",
        "1981",
    ]
)

correct_answers.append("c")

# INDEX 1

questions.append("Who directed the first Star Wars movie?")

answers.append(
    [
        "Lucas",
        "Spielberg",
        "Mark Hamil",
        "Harrison Ford",
    ]
)

correct_answers.append("a")

# INDEX 2

questions.append("What color was Yoda's lightsaber?")

answers.append(
    [
        "Red",
        "Blue",
        "Green",
        "Purple",
    ]
)

correct_answers.append("c")


# Loop over questions, answers

# for index in [0, 1, 2]:
for index in range(len(questions)):
    q_text.set_text(questions[index])
    a_text.set_text(f"A: {answers[index][0]}")
    b_text.set_text(f"B: {answers[index][1]}")
    c_text.set_text(f"C: {answers[index][2]}")
    d_text.set_text(f"D: {answers[index][3]}")
    answer = sprite.ask("Which letter is correct?")
    if answer == correct_answers[index]:
        sprite.say("Correct!", 3)
    else:
        sprite.say("Incorrect!", 3)


# Let's not worry about how this code works
print("list of questions:")
print(*list(enumerate(questions)), sep="\n")
print()
print("list of possible answers:")
print(*list(enumerate(answers)), sep="\n")
print()
print("list of correct answers:")
print(*list(enumerate(correct_answers)), sep="\n")


"""
questions = ["q1", "q2", "q3"]
answers = []

for i in range(1, 4):
    set_of_answers = []
    for letter in "abcd":
        set_of_answers.append(letter + str(i))
    answers.append(set_of_answers)
    
for i in range(len(questions)):
    print(questions[i])
    print(answers[i])
    stage.wait(1)
"""
