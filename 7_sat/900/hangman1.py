# the list of possible words
possible_words = [
    "apple",
    "lasagna",
    "basketball",
    "coding",
    "country",
    "jazzfusion",
    ]

# choosing a random word from the list
word = random.choice(possible_words)

# making the underscore list
presented_word = []
for letter in word:
    presented_word.append('_')
    
while True:

    # making it as a presentable string
    presented_word_string = " ".join(presented_word)
    word_sprite = codesters.Text(presented_word_string, -100, 100)

    # let the user guess
    guess = stage.ask("Give me a letter", 0, 0)
    
    # check if letter is correct
    if guess in word:
        print("correct!")
        print(presented_word)
        for i in range(len(word)):
            if guess == word[i]:
                presented_word[i] = guess




