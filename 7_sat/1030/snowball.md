# Goals

* Make snowballs reduce health variable
* Put in a barrier in the middle

# Advanced versions

* Make health bars change based on health
* Make health bars move properly
* Add powerups that float around
* Add a super snowball
