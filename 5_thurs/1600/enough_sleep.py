# Example: Enough sleep

user_sleep = input("How much did you sleep last night?")
user_sleep_int(user_sleep)

if user_sleep_int < 10:
    print("That's too little!")
elif user_sleep_int > 14:
    print("That's too much!")
else:
    print("Great!")

"""
Challenge:
Write a program that asks someone the number grade they got
on a test (like 72 or 91 or something)
and tells them their letter grade
(A/B/C/D/F)

Advanced:
Tell them + and -
Have situations if the number is bigger than 100 or smaller than 0
"""

