lien = codesters.Sprite(
    "alien1_masked", # image name
    -150, # x
    -50, # y
)

floor = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    100, # height
    "darkred", # color
)

floor.set_gravity_off()

stage.set_gravity(10)

stage.disable_left_wall()

# ===== SETUP GROUNDED =====

alien.grounded = False

grounded_text = codesters.Text(
    f"grounded: {alien.grounded}", # text
    -160, # x
    200, # y
)

# ===== SETUP HEALTH =====

alien.health = 5
alien.inv = True

health_text = codesters.Text(
    f"health: {alien.health}", # text
    -160, # x
    180, # y
)

alien.jumps = 0 # NEW
alien.say(alien.jumps) # NEW

def collision_chicken(sprite_icecream, other_pizza):
    other_color_icepops = other_pizza.get_color()
    if other_color_icepops == "darkred": # COLLIDE W/GROUND
        sprite_icecream.set_gravity_off()
        sprite_icecream.set_y_speed(0)
        sprite_icecream.grounded = True
        grounded_text.set_text(
            f"grounded: {sprite_icecream.grounded}"
        )
        alien.jumps = 2 # NEW
        alien.say(alien.jumps) # NEW
        alien.set_y(
            floor.get_top() + (alien.get_height()//2)    
        )
    if other_color_icepops == "blue": # COLLIDE W/BLUE ENEMY
        pass

alien.event_collision(collision_chicken)

def space_bar_dubai(sprite_tokyo):
    if alien.jumps > 0: # NEW
        alien.jumps -= 1 # NEW
        alien.say(alien.jumps) # NEW
        sprite_tokyo.jump(10)
        sprite_tokyo.set_gravity_on()
        sprite_tokyo.grounded = False
        grounded_text .set_text(f"grounded: {sprite_tokyo.grounded}")
    
alien.event_key("space", space_bar_dubai)

def left_key(sprite):
    sprite.move_left(20)

alien.event_key("left", left_key)

def right_key(sprite):
    if sprite.get_x() < 0:
        sprite.move_right(20)

alien.event_key("right", right_key)

def make_bouncy_enemy():
    height = random.randint(-100, 250)   # NEW
    new_enemy = codesters.Rectangle(
        300, # x
        height, # y   # CHANGED
        20, # width
        20, # height
        "blue", # color
    )
    new_enemy.id = "bouncy enemy"
    new_enemy.set_x_speed(-5)

def make_hovering_enemy():
    height = -100 + 50 * random.randint(0, 4)
    new_enemy = codesters.Rectangle(
        300, # x
        height, # y
        20, # width
        20, # height
        "blue", # color
    )
    new_enemy.id = "hovering enemy"
    new_enemy.set_gravity_off()
    new_enemy.set_x_speed(-5)

while True:
    stage.wait(.5)
    make_bouncy_enemy()
    stage.wait(.5)
    make_hovering_enemy()
