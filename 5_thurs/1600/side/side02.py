sprite = codesters.Sprite(
    "alien1_masked", # image name
    -150, # x
    -50, # y
)

floor = codesters.Rectangle(
    0, # x
    -250, # y
    500, # width
    100, # height
    "darkred", # color
)

floor.set_gravity_off()

stage.set_gravity(10)


def collision_chicken(sprite_icecream, other_pizza):
    other_color_icepops = other_pizza.get_color()
    if other_color_icepops == "darkred":
        sprite_icecream.set_gravity_off()
        sprite_icecream.set_y_speed(0)
    if other_color_icepops == "blue": # NEW
        sprite_icecream.say("ouch!", 2) # NEW

sprite.event_collision(collision_chicken)

def space_bar_dubai(sprite_tokyo):
    sprite_tokyo.jump(10)
    sprite_tokyo.set_gravity_on()
    
sprite.event_key("space", space_bar_dubai)




"""
Dubai
Tokyo
Sana Barbra
London
"""

"""
GOALS:

* the sprite can move left/right a bit
* generate obstacles

ADVANCED GOALS:
* drop "down key" to drop
* add double jump
* add health
* create an end screen
"""


