floor = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "darkred", # color
)

floor.id = "main floor"
floor.set_gravity_off()

stage.set_gravity(10)
stage.disable_left_wall()

def make_bouncy_enemy():
    height = random.randint(-100, 250)   # NEW
    new_enemy = codesters.Rectangle(
        300, # x
        height, # y   # CHANGED
        20, # width
        20, # height
        "blue", # color
    )
    new_enemy.id = "bouncy enemy"
    new_enemy.set_x_speed(-5)

def make_hovering_enemy():
    height = -100 + 50 * random.randint(0, 4)
    new_enemy = codesters.Rectangle(
        300, # x
        height, # y
        20, # width
        20, # height
        "blue", # color
    )
    new_enemy.id = "hovering enemy"
    new_enemy.set_gravity_off()
    new_enemy.set_x_speed(-5)

while True:
    stage.wait(.5)
    make_bouncy_enemy()
    stage.wait(.5)
    make_hovering_enemy()



