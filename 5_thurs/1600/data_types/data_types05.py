# ====== DATA TYPES =======

# string

print("=" * 10, "STRINGS", "=" * 10)

str0 = 'this isn\'t a list'

str1 = 'this is a string'

str2 = "this is also a string"

str3 = """


this is a multiline string

"""

string_name = "f-string"

# the part in curlies is replaced by the varible's value
str4 = f"this is an {string_name}"

# loop over the strings and print them out
for one_string in [str1, str2, str3, str4]:
  print(type(one_string))
  print(one_string)

print()
print("=" * 10, "BOOLEANS", "=" * 10)
print()

bool1 = True
print(bool1)
print(type(bool1))

# booleans are just numbers
# print(1 + 1 + 1 + 0)
print(True + True + True + False)

bool2 = 10 > 20
print(bool2)
print(type(bool2))

# if bool1 is True, run the indented code
if bool1:
  print("bool1 is true")

# if bool2 is True, run the indented code
if bool2:
  print("bool2 is true")
  
print()
print("and/or stuff")
print()

two_plus_two_is_four = True
two_plus_two_is_five = False
grass_is_green = True
grass_is_blue = False


print(
  two_plus_two_is_four and grass_is_green
)

print(
  two_plus_two_is_four and grass_is_blue
)

print(
  two_plus_two_is_five or grass_is_green
)

print(
  two_plus_two_is_five or grass_is_blue
)


