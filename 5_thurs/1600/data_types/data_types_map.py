student_map = {
  "Blake":     ["pizza", "teal"],
  "Derek":     ["burgers", "cyan"],
  "Adithya":   ["crab", "cyan"],
  "Ephrata":   ["pizza", "teal"],
}

print(student_map.keys()) # prints everything left of the :
print()
print(student_map.values()) # prints everything right of the :

for one_name in student_map.keys():
  print(
    "{n} wants to eat {f} in a {c} room".format(
      n=one_name,
      f=student_map[one_name][0],
      c=student_map[one_name][1],
    )
  )


student_name = input("Give me a student name: ")
if student_name in student_map.keys():
  print(
        "{n} wants to eat {f} in a {c} room".format(
      n=student_name,
      f=student_map[student_name][0],
      c=student_map[student_name][1],
    )
  )
