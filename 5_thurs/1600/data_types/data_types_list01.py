my_bag = []
for i in range(3): # run 3 times
  my_bag.append("apple") # put one apple in the bag

my_bag.append("orange")

print(my_bag)

print(
  my_bag.pop()
)

print(my_bag)

my_bag.insert(0, "grapefruit") # adding "grapefruit" to the beginning

print(my_bag) # prints the entire bag
print(my_bag[0]) # print what is at position 0
print(my_bag[1]) # print what is at position 1

abc_list = list('abcdefghijklmnopqrstuvwxyz')
