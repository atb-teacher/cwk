# ====== DATA TYPES =======

# string

str0 = 'this isn\'t a list'

str1 = 'this is a string'

str2 = "this is also a string"

str3 = """
this is a multiline string
"""

string_name = "f-string"

str4 = f"this is an {string_name}"

for one_string in [str1, str2, str3, str4]:
  print(type(one_string))
  print(one_string)
