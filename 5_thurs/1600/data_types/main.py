"""

This file is called "main.py". This file is what runs when we hit the big green "run" button above.

We can run files by importing them. To run a file called "banana_pants.py" that is right next to our "main.py" file, we can say "import banana_pants" (note the missing .py).

"""

print("running main.py, and any files imported by main.py")

# import data_types_float
# import data_types_int
# import data_types_list
# import data_types_map
# import data_types
# import Pet
