my_num = 0
for i in range(10): # loop 10 times
  my_num = my_num + 0.1
print(my_num)
# print(round(my_num))

# ===== Use A Fraction =====

import fraction

my_fraction = fraction.Fraction(1, 10)
print(my_fraction)

my_num2 = fraction.Fraction(0, 1)
for i in range(10):
  my_num2 = my_num2 + my_fraction

print(my_num2)
