# Integer = positive or negative numbers, or zero, with no decimal point or fraction

num1 = 5
print(num1, type(num1))

num2 = 3.5
print(num2, type(num2))

num3 = -7
print(num3, type(num3))

num4 = "100"
print(num4, type(num4))

# print("5" + 7)

print(num1.numerator)
print(num1.denominator)
