# Goals
## Main Goals

* Place towers
* Make the towers shoot enemies

## Stretch Goals

* Create a tower shop
* Allow the user to place towers
* Make a "ready" button and waves

## Advanced Goals

* Create different types of enemies
* Create powerups that appear suddenly, that you can click on
* Create different types of towers that have different strengths
