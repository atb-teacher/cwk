class Question:
    def __init__(
        self,
        _prompt,
        _a,
        _b,
        _c,
        _d,
        _answer,
    ):
        self.prompt = _prompt
        self.a = _a
        self.b = _b
        self.c = _c
        self.d = _d
        self.answer = _answer
        
Dereks_q = Question(
    _prompt = "What is the smallest state in the USA?",
    _a = "Rhode Island",
    _b = "Hawaii",
    _c = "Connecticut",
    _d = "Maine",
    _answer = "Rhode Island",
)

Adithyas_q = Question(
    _prompt = "What was the original 1st colony called in the USA?",
    _a = "Georgetown",
    _b = "Virginia",
    _c = "Jamestown",
    _d = "New York",
    _answer = "Jamestown",
)

Ephratas_q = Question(
    _prompt = "The name 'Canada' comes from an Iroquois word meaning what?",
    _a = "hat",
    _b = "ice or snow",
    _c = "house or island",
    _d = "settlement or village",
    _answer = "settlement or village",
)

Blakes_q = Question(
    _prompt = "Which country is bigger than the USA?",
    _a = "Brazil",
    _b = "Mexico",
    _c = "Russia",
    _d = "China",
    _answer = "Russia",
)
    
# temp code
for one_question in [Dereks_q, Adithyas_q, Ephratas_q, Blakes_q]:
    print(one_question.prompt)
    stage.wait(1)
    print(one_question.answer)
    stage.wait(1)


