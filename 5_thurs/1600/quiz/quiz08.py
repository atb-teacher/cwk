class Question:
    def __init__(
        self,
        _prompt,
        _a,
        _b,
        _c,
        _d,
        _answer,
    ):
        self.prompt = _prompt
        self.a = _a
        self.b = _b
        self.c = _c
        self.d = _d
        self.answer = _answer
        
Dereks_q = Question(
    _prompt = "What is the smallest state in the USA?",
    _a = "Rhode Island",
    _b = "Hawaii",
    _c = "Connecticut",
    _d = "Maine",
    _answer = "Rhode Island",
)

Adithyas_q = Question(
    _prompt = "What was the original 1st colony called in the USA?",
    _a = "Georgetown",
    _b = "Virginia",
    _c = "Jamestown",
    _d = "New York",
    _answer = "Jamestown",
)

Ephratas_q = Question(
    _prompt = "The name 'Canada' comes from an Iroquois word meaning what?",
    _a = "hat",
    _b = "ice or snow",
    _c = "house or island",
    _d = "settlement or village",
    _answer = "settlement or village",
)

Blakes_q = Question(
    _prompt = "Which country is bigger than the USA?",
    _a = "Brazil",
    _b = "Mexico",
    _c = "Russia",
    _d = "China",
    _answer = "Russia",
)

# ===== SETUP TEXT =====

text_prompt = codesters.Text(
    "prompt",
    0, # x
    200, # y
)

text_a = codesters.Text(
    "a",
    -125, # x
    100, # y
)

text_b = codesters.Text(
    "b",
    125, # x
    100, # y
)

text_c = codesters.Text(
    "c",
    -125, # x
    0, # y
)

text_d = codesters.Text(
    "d",
    125, # x
    0, # y
)


# ===== SETUP GAME LOOP =====

waiting = True

question_index = 0

questions = [Dereks_q, Adithyas_q, Ephratas_q, Blakes_q]

# ===== SETUP CLICK =====

def next_q():
    global waiting
    waiting = False # get past the while loop

for one_text in [text_a, text_b, text_c, text_d]:
    one_text.event_click(next_q)


while question_index < len(questions):

    text_prompt.set_text(
        questions[question_index].prompt
    )
    
    text_a.set_text(
        f"A) {questions[question_index].a}"
    )
    
    text_b.set_text(
        f"B) {questions[question_index].b}"
    )
    
    text_c.set_text(
        f"C) {questions[question_index].c}"   
    )
    
    text_d.set_text(
        f"D) {questions[question_index].d}"   
    )
    
    question_index += 1
    
    while waiting == True:
        stage.wait(0.1)
    waiting = True

"""
===
* keep a score
====
* create an end screen
"""


