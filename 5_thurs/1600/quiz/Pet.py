# my own pet blueprint

class Pet:
  def __init__(self, _name, _mammal):
    self.name = _name
    self.mammal = _mammal

class Dog(Pet):
  def __init__(self, _name, _breed):
    super().__init__(
      _name = _name,
      _mammal = True,
    )

our_dog = Dog(
  _name = "Garfield the Second",
  _breed = "Golden Retriever",
)

print(our_dog.mammal)
"""
Ideal pet:
 dog
"""
