# num = 0

# def increase_num():
#     global num
#     num += 1
    
# stage.event_click(increase_num)

# while True:
#     stage.wait(1)
#     print(num)

counter = 0
counter_text = codesters.Text(str(counter))

waiting = True

def stop_waiting():
    global waiting
    waiting = False

stage.event_click(stop_waiting)

while True:
    while waiting == True:
        stage.wait(.1)
    waiting = True
    counter += 1
    counter_text.set_text(str(counter))
