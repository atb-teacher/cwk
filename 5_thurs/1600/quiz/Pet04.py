# my own pet blueprint

class Pet:
  def __init__(self, _name, _mammal):
    print("Inside Pet __init__")
    print(f"{_name=} {_mammal=}")
    self.name = _name
    self.mammal = _mammal

  def announce(self):
    print(
      f"{self.name} is a mammal? {self.mammal}"
    )

class Dog(Pet):
  def __init__(self, _name, _breed):
    print("Inside Dog __init__")
    print(f"{_name=} {_breed=}")
    super().__init__(
      _name = _name,
      _mammal = True,
    )
    self.breed = _breed

  def announce(self):
    print(
      f"{self.name} is a {self.breed} type of dog"
    )

Dereks_dog = Dog(
  _name = "Garfield the Second",
  _breed = "Golden Retriever",
)

Ephratas_parrot = Pet(
  _name = "Ephrata Jr.",
  _mammal = False,
)

Adithyas_dog = Dog(
  _name = "Monkey",
  _breed = "German Shepherd",
)

Blakes_dog = Dog(
  _name = "Hugo",
  _breed = "Chihuahua",
)

Blakes_dog.announce()

Ephratas_parrot.announce()
print(
  "name", Ephratas_parrot.name,
  "\n" + "mammal", Ephratas_parrot.mammal,
)
