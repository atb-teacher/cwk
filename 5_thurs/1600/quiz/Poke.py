class Pokemon:
  def __init__(self, _name, _type):
    self.name = _name
    self.type = _type

my_pokemon = [
  Pokemon(
    _name = "Pikachu",
    _type = "Electric",
  ),
  Pokemon(
    _name = "Charizard",
    _type = "Fire",
  )
]

"""
for one_poke in my_pokemon:
  print(
    f"I have a pokemon named {one_poke.name}. It is a {one_poke.type} type of pokemon"
  )
"""
for index in [0, 1]:
  print(
    f"I have a pokemon named {my_pokemon[index].name}.",
    f"It is a {my_pokemon[index].type} type of pokemon."
  )
