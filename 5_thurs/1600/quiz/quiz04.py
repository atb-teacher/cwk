class Question:
    def __init__(
        self,
        _prompt,
        _a,
        _b,
        _c,
        _d,
        _answer,
    ):
        self.prompt = _prompt
        self.a = _a
        self.b = _b
        self.c = _c
        self.d = _d
        self.answer = _answer
        
Dereks_q = Question(
    _prompt = "What is the smallest state in the USA?",
    _a = "Rhode Island",
    _b = "Hawaii",
    _c = "Connecticut",
    _d = "Maine",
    _answer = "Rhode Island",
)

Adithyas_q = Question(
    _prompt = "What was the original 1st colony called in the USA?",
    _a = "Georgetown",
    _b = "Virginia",
    _c = "Jamestown",
    _d = "New York",
    _answer = "Jamestown",
)

Ephratas_q = Question(
    _prompt = "The name 'Canada' comes from an Iroquois word meaning what?",
    _a = "hat",
    _b = "ice or snow",
    _c = "house or island",
    _d = "settlement or village",
    _answer = "settlement or village",
)

Blakes_q = Question(
    _prompt = "Which country is bigger than the USA?",
    _a = "Brazil",
    _b = "Mexico",
    _c = "Russia",
    _d = "China",
    _answer = "Russia",
)


text_prompt = codesters.Text(
    "",
    0, # x
    200, # y
)

text_a = codesters.Text(
    "",
    -125, # x
    100, # y
)

text_b = codesters.Text(
    "",
    125, # x
    100, # y
)



question_index = 0

questions = [Dereks_q, Adithyas_q, Ephratas_q, Blakes_q]

text_prompt.set_text(
    questions[question_index].prompt
)

stage.wait(2)

question_index += 1

text_prompt.set_text(
    questions[question_index].prompt
)

stage.wait(2)

question_index += 1

text_prompt.set_text(
    questions[question_index].prompt
)

stage.wait(2)

question_index += 1

text_prompt.set_text(
    questions[question_index].prompt
)



"""

def set_q():
    global question_index
    
    text_prompt.set_text(active_q.prompt)
    
    text_a.set_text(f"A) {active_q.a}")
    text_b.set_text(f"B) {active_q.b}")

"""



