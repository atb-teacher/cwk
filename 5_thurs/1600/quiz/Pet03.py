# my own pet blueprint

class Pet:
  def __init__(self, _name, _mammal):
    self.name = _name
    self.mammal = _mammal

  def announce(self):
    print(
      f"{self.name} is a mammal? {self.mammal}"
    )

class Dog(Pet):
  def __init__(self, _name, _breed):
    super().__init__(
      _name = _name,
      _mammal = True,
    )
    self.breed = _breed

  def announce(self):
    print(
      f"{self.name} is a {self.breed} type of dog"
    )

Dereks_dog = Dog(
  _name = "Garfield the Second",
  _breed = "Golden Retriever",
)

Ephratas_parrot = Pet(
  _name = "Ephrata Jr.",
  _mammal = False,
)

Adithyas_dog = Dog(
  _name = "Monkey",
  _breed = "German Shepherd",
)

Dereks_dog.announce()
Ephratas_parrot.announce()
Adithyas_dog.announce()
print(
  "name", Adithyas_dog.name,
  "\nmammal", Adithyas_dog.mammal,
  "\nbreed", Adithyas_dog.breed,
)
print(
  "name", Ephratas_parrot.name,
  "\nmammal", Ephratas_parrot.mammal,
  # "\nbreed", Ephratas_parrot.breed, # error
)
