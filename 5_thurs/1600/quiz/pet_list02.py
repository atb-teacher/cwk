# my own pet blueprint

class Pet:
  def __init__(self, _name, _mammal):
    # print("Inside Pet __init__")
    # print(f"{_name=} {_mammal=}")
    self.name = _name
    self.mammal = _mammal

  def announce(self):
    print(
      f"{self.name} is a mammal? {self.mammal}"
    )

Ephratas_parrot = Pet(
  _name = "Ephrata Jr.",
  _mammal = False,
)

Adithyas_dog = Pet(
  _name = "Monkey",
  _mammal = True,
)

Alexs_sloth = Pet(
  _name = "Snorlax",
  _mammal = True,
)

pets = [ # list of pets
  Ephratas_parrot,
  Adithyas_dog,
  Alexs_sloth,
]

print(
  pets[2].name
)

