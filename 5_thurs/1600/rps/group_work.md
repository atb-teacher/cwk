# Group Work

Start with [this file](rps_codesters_rock_03.py). This file outputs "you win", no matter what the user and computer choose. Try to change this file so that the output says "win," "lose," or "draw," depending on the choices. Use [this file](rps2.py) as a hint.