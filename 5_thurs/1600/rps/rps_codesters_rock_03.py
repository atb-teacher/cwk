chosen_sprite_endgame = None # new line

sprites_bugs_life = []
for sprite_name in ["rock", "paper", "scissors"]:
    new_sprite = codesters.Sprite(sprite_name)
    sprites_bugs_life.append(new_sprite)

x_pos = -150
for one_sprite in sprites_bugs_life:
    one_sprite.go_to(
        x_pos, # x
        100, # y
    )
    x_pos += 150

def click_get_even(sprite): # runs whenever we click
    print("chose a sprite!")
    global chosen_sprite_endgame
    chosen_sprite_endgame = sprite

#sprites_bugs_life[2].event_click(click_get_even)

for one_sprite_star_wars in sprites_bugs_life:
    one_sprite_star_wars.event_click(click_get_even)

while chosen_sprite_endgame == None: # loop that waits for a click
    stage.wait(.1)
print("free from loop!")

for one_sprite_mando in sprites_bugs_life: # hides sprites not clicked
    if one_sprite_mando is not chosen_sprite_endgame:
        one_sprite_mando.hide()

comp_weapon = random.choice(["rock", "paper", "scissors"]) # new
user_weapon = chosen_sprite_endgame.get_image_name() # new

output = codesters.Text(
    f"Computer chose {comp_weapon}",
    0, # x
    0, # y
)

stage.wait(2)

output.set_text("You won!")
