chosen_sprite_endgame = None # new line

sprites_bugs_life = []
for sprite_name in ["rock", "paper", "scissors"]:
    new_sprite = codesters.Sprite(sprite_name)
    sprites_bugs_life.append(new_sprite)
    
x_pos = -150
for one_sprite in sprites_bugs_life:
    one_sprite.go_to(
        x_pos, # x
        100, # y
    )
    x_pos += 150

def click(sprite):
    global chosen_sprite_endgame
    chosen_sprite_endgame = sprite
    
for one_sprite_star_wars in sprites_bugs_life:
    one_sprite_star_wars.event_click(click)

while chosen_sprite_endgame == None:
    stage.wait(.1)
    
for one_sprite_mando in sprites_bugs_life:
    if one_sprite_mando is not chosen_sprite_endgame:
        one_sprite_mando.hide()
