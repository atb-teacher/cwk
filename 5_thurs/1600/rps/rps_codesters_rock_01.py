"""
new_sprite = "rock"
codesters.Sprite(new_sprite)
new_sprite = "paper"
codesters.Sprite(new_sprite)
new_sprite = "scissors"
codesters.Sprite(new_sprite)
"""
sprites = []
for sprite_name in ["rock", "paper", "scissors"]:
    new_sprite = codesters.Sprite(sprite_name)
    sprites.append(new_sprite)
    
x_pos = -150
for one_sprite in sprites:
    one_sprite.go_to(
        x_pos, # x
        100, # y
    )
    x_pos += 150

def click(sprite):
    sprite.say("Hello, name")
    # add other actions...
    
for one_sprite in sprites:
    one_sprite.event_click(click)
