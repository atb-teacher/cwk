package space.codekingdoms.alexteacher8.commandexample;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	public void onRunCommand(String command, String[] args) {
		if (command.equals("/favoritefood")) {
			sendMessage("takis");
		}
		else if (command.equals("/favoritecolor")) {
			sendMessage("orange");
		}
		else if (command.equals("/menu") || command.equals("/help")) {
			sendMessage("Type '/favoritefood' for my favorite food");
			sendMessage("Type '/favoritecolor' for my favorite color");
		}
	}
	
}
