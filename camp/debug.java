PLAYER:

package space.codekingdoms.alexteacher8.debugging;

import com.codekingdoms.nozzle.utils.Random;
import com.codekingdoms.nozzle.utils.Direction;
import java.lang.Math;
import java.lang.Integer;
import java.lang.Float;
import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	
	public void onChat( String message ) {
		
		if (message.equals("whereami")) {
			chat("X: " + getLocation().getX());
			chat("Y: " + getLocation().getY());
			chat("Z: " + getLocation().getZ());
		}
		if (message.equals("players")) {
			for (Player player : getGame().getPlayerList()) {
				chat(player.getName());
			}
		}
		if (message.equals("held")) {
			chat("Item Name: " + getItemInMainHand().getType().toString());
		}
		if (message.equals("testing")) {
			getGame().testMethod(this);
		}
	}
}

GAME:

package space.codekingdoms.alexteacher8.debugging;

import com.codekingdoms.nozzle.base.BaseGame;

public class Game extends BaseGame {
	public void testMethod(Player player) {
		player.chat("This message is from the game file!");
	} 
	
}
