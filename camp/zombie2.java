PLAYER:

package space.codekingdoms.alexteacher8.zombiefight;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	
	public void onRunCommand( String command, String[] args ) {
		
		if (command.equals("/start")) {
			
			getGame().setPlayer(this);
			getGame().onCodeUpdate();
			
		}
		
	
	}
	
	
}

GAME:

package space.codekingdoms.alexteacher8.zombiefight;

import com.codekingdoms.nozzle.base.BaseGame;
import org.bukkit.Location;

public class Game extends BaseGame {
	
	Player currPlayer;
	
	public void setPlayer( Player player ) {
		
		currPlayer = player;
	
	}
	
	public void spawnZombie() {
		
		Zombie z = new Zombie();
		z.spawn(world, currPlayer.getLocation());
	
	}
	
	public void onCodeUpdate() {
		
		world.setTime(1000);
		world.setSpawnLocation(0, 70, 0);
		removeAllMobs();
		disableMobSpawning();
		setInterval(
			
			() -> {
				
				spawnZombie();
				
			}
			
			
		, 5, 5);
	
	}
	
	
}


ZOMBIE:

package space.codekingdoms.alexteacher8.zombiefight;

import com.codekingdoms.nozzle.base.BaseZombie;
import org.bukkit.Material;

public class Zombie extends BaseZombie {
	
	public void onSpawn() {
		
		setBaby(true);
		equipItem(Material.WOOD_SWORD);
	
	}
	
	
}
