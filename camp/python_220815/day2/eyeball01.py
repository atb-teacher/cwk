class Eye:
    def __init__(self):
        self.eyeball = codesters.Circle(
            0, # x
            0, # y
            100, # diameter,
            "white",
        )
        self.eyeball.set_outline_color("black")
        self.pupil = codesters.Circle(
            0, # x
            0, # y
            20, # diameter
            "black",
        )
    
    def look(self):
        x_pos = stage.mouse_x() - self.eyeball.x
        y_pos = stage.mouse_y() - self.eyeball.y
        
        hypotenuse = math.sqrt(
            x_pos ** 2 + y_pos ** 2   
        )
        
        x_pos = x_pos/hypotenuse
        y_pos = y_pos/hypotenuse
        
        self.pupil.go_to(
            x_pos * 10 + self.eyeball.x,
            y_pos * 10 + self.eyeball.y,
        )
        
my_eye = Eye()
stage.event_mouse_move(my_eye.look)
