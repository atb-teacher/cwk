import time
import random

nums = [random.randint(1000, 2000)
        for i in range(100)]

before_times = time.time()
for num in nums:
    num * num
after_times = time.time()
print(f"{(after_times - before_times) * 1000} times")

before_pow = time.time()
for num in nums:
    num ** 2
after_pow = time.time()
print(f"{(after_pow - before_pow) * 1000} pow")

