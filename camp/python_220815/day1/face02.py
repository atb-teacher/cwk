class Face:
    def __init__(self):
        self.head = codesters.Circle(
            0, # x
            100, # y
            300, # diameter
            "gray", # color
        )
        
        self.left_eye = codesters.Circle(
            -75, # x
            150, # y
            50, # diameter
            "white", # color
        )
        self.left_eye.set_outline_color("black")
        
        self.left_pupil = codesters.Circle(
            -75, # x
            150, # y
            10, # diameter,
            "black", # color
        )
        
        self.left_eyelid = codesters.Circle(
            -75, # x
            150, # y
            50, # diameter
            "gray", # color
        )
        self.left_eyelid.hide()
        self.left_eyelid.move_to_front()
        
    def wink(self):
        self.left_eyelid.show()
        stage.wait(0.1)
        self.left_eyelid.hide()
        
my_face = Face()

while True:
    stage.wait(
        random.randint(2, 5)
    )
    my_face.wink()
