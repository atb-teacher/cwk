class Face:
    def __init__(self):
        self.head = codesters.Circle(
            0, # x
            100, # y
            300, # diameter
            "gray", # color
        )
        
        self.left_eye = codesters.Circle(
            -75, # x
            150, # y
            50, # diameter
            "white", # color
        )
        self.left_eye.set_outline_color("black")
        
        self.left_pupil = codesters.Circle(
            -75, # x
            150, # y
            10, # diameter,
            "black", # color
        )
        
        
        
my_face = Face()

