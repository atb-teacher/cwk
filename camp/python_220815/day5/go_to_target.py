from collections import namedtuple # IGNORE
import random
import time # Like stage.wait()

# Represents codesters.Sprite
class Sprite:
  def __init__(self, x, y):
    self.x = x
    self.y = y

def set_target(self, in_target):
  self.target = in_target
  
def go_to_target(self):
  if self.target is not None:
    if abs(self.x - self.target.x) < 5:
      return
    elif self.x > self.target.x:
      self.set_x_speed(-5)
    elif self.x < self.target.x:
      self.set_x_speed(5)

# codesters.Sprite.set_target = set_target
# codesters.Sprite.go_to_target = go_to_target
Sprite.set_target = set_target
Sprite.go_to_target = go_to_target
# so every codesters.Sprite can do these two things

Enemy = namedtuple("Enemy", "x y".split()) # IGNORE

# represents list of enemys
enemies = [
  Enemy(
    x = random.randint(-250, 250),
    y = random.randint(-200, 200),
  ) for i in range(20)
]

# how we prioritize enemies
def prioritize_enemy(_enemy):
  points = 0
  if _enemy.y < -100:
    points += abs(_enemy.y) * 1000
  points -= abs(hero.x - _enemy.x)
  # print(_enemy.x, _enemy.y, points)
  return points

# our helper sprite
helper = Sprite(
  x=0,
  y=-150,
)
helper.target = None

while True:
  time.sleep(1)
  # similar to stage.wait(1)

  enemies.sort(
    key = prioritize_enemy, reverse = True,
  )
  found_target = enemies[0] # we found the target we want
  helper.set_target(found_target) # give the target to the helper
  helper.go_to_target() # tell the helper to move to the target
