# =============== SETUP STAGE ===============

stage.disable_all_walls()

# =============== SETUP SPRITE CLASS ===============

def fire(self,
    direction = 1,
    collision_function = None,
    ):
    
    bullet = codesters.Sprite(
        "locationpin",
        350,
        350,
    )
    
    bullet.set_rotation(180)
    bullet.set_size(0.5)
    
    bullet.go_to(
        self.get_x(),
        self.get_y(),
    )
    
    bullet.set_y_speed(5)
    
    if collision_function is None:
        def bullet_collision_heroic(bullet, other):
            if other.allignment == "evil":
                other.health -= 1
                stage.remove_sprite(bullet)
                stage.remove_sprite(other)
        bullet.event_collision(bullet_collision_heroic)
    else:
        bullet.event_collision(collision_function)
        
codesters.Sprite.fire = fire

# =============== CREATING  ===============

hero = codesters.Sprite(
    "anemone", # image name,
    0, # x
    -200, # y
    )
hero.allignment = "good"
    
helper = codesters.Sprite(
    "shiny_ship_5cf", # image name
    350, # x
    350, # y
)
helper.allignment = "good"

helper.set_size(0.2)
helper.set_rotation(90)

helper.go_to(
    0, # x
    -200, # y
)

def left_key(sprite):
    sprite.move_left(20)

hero.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)

hero.event_key("right", right_key)

def make_fire(sprite):
    sprite.fire()

hero.event_key("space", make_fire)
