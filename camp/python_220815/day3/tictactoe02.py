grid = []

turn = 0 # NEW
letter_choices = ["X", "O"] # NEW

for i in range(3): # [0, 1, 2]
    row = []
    for j in range(3): # [0, 1, 2]
        row.append("empty")
    grid.append(row)

print(*grid, sep="\n")

squares = []
for y_coord in [100, 0, -100]:
    for x_coord in [-100, 0, 100]:
        one_square = codesters.Rectangle(
            x_coord,
            y_coord,
            96,
            96,
            "white",
        )
        one_square.set_outline_color("black")
        one_square.set_line_thickness(4)
        squares.append(one_square)

# TEMP CODE
"""
for i in range(len(squares)):
    codesters.Text(
        i,
        squares[i].x,
        squares[i].y,
    )
""" 
# NOT TEMP CODE
for i in range(len(squares)):
    squares[i].id_num = i
    
def got_clicked(self):
    global turn
    global letter_choices
    chosen_letter = letter_choices[turn % 2]
    turn += 1
    chosen_color = "blue"
    new_letter = codesters.Text(
        chosen_letter,
        self.x,
        self.y,
    )
    new_letter.set_color(chosen_color)
    new_letter.set_size(3)
    

for one_square in squares:
    one_square.event_click(got_clicked)

"""
row1 = []
for j in range(3): # [0, 1, 2]
    row1.append("empty")
grid.append(row1)

row2 = []
for j in range(3): # [0, 1, 2]
    row2.append("empty")
grid.append(row2)

row3 = []
for j in range(3): # [0, 1, 2]
    row3.append("empty")
grid.append(row3)

print(row1, row2, row3, sep="\n")
"""
#print(*grid, sep="\n") 
