PLAYER CODE:

package space.codekingdoms.alexteacher8.commands;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Player extends BasePlayer {
	
	public void onRunCommand( String command, String[] args ) {
		
		if (command.equals("/lightning")) {
			
			world.strikeLightning(getTargetBlock().getLocation());
			
		}
		
		else if (command.equals("/gofast")) {
			
			setWalkSpeed(0.5f);
			
		}
			
		else if (command.equals("/givesword")) {
			
			addItemToInventory(new ItemStack(Material.DIAMOND_SWORD));
			
		}
		
		else if (command.equals("/menu") || command.equals("/help")) {
			
			sendMessage("Type /lightning' for lightning");
			sendMessage("Type '/gofast' to go fast");
			
		}
		
		else if (command.equals("/spawnzombie")) {
			Zombie z = new Zombie();
			z.spawn(world, getTargetBlock().getLocation());
		}
	}
}


ZOMBIE CODE:

package space.codekingdoms.alexteacher8.commands;

import com.codekingdoms.nozzle.base.BaseZombie;
import org.bukkit.Material;

public class Zombie extends BaseZombie {
	
	 onSpawn() {
		
		setBaby(true);
		equipItem(Material.WOOD_SWORD);
	
	}
	
	
}
