# Creating a rectangle

my_blue_rect = codesters.Rectangle(
    0, # x
    100, # y
    100, # width
    50, # height
    "blue", # color
)

# Giving the rectangle something to remember
my_blue_rect.name = "Blu"


# Listing everything that the rectangle remembers (mostly verbs)
"""
print(
    "\n".join(dir(my_blue_rect))
)
"""

# Printing out the name that it remembers
print(
    my_blue_rect.name
)

def say_hi_pasta():
    print("Hello there!")
    
my_blue_rect.say_hi_lasagna = say_hi_pasta

my_blue_rect.say_hi_lasagna()


def say_name_cookie(self):
    print("Hello there, I am", self.name)
    
codesters.Rectangle.say_name_salmon = say_name_cookie

my_blue_rect.say_name_salmon()
