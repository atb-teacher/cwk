class Face:
    def __init__(self):
        self.head = codesters.Circle(
            0, # x
            100, # y
            150, # diameter
            "gray", # color
        )
        self.head.set_outline_color("black")
        self.head.set_line_thickness(2)
        self.left_eye = codesters.Circle(
            -20, # x
            120, # y
            30, # diameter
            "white", # color
        )
        self.left_eye.set_outline_color("black")
        self.left_pupil = codesters.Circle(
            -20, # x
            120, # y
            10, # diameter,
            "black", # color
        )
        
my_face = Face()
