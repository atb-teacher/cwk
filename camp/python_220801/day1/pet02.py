class Pet:
    def __init__(self, name_london, owner_sydney):
        self.name = name_london
        self.owner = owner_sydney
        print("I am a pet being created")
        
my_pet = Pet(name_london = "Zeus", owner_sydney = "Alex teacher")
print(my_pet.name)
print(my_pet.owner)

class Fish(Pet):
    def __init__(self, name_hawaii, owner_japan, friend_emoji):
        super().__init__(
            name_london = name_hawaii,
            owner_sydney = owner_japan,
        )
        self.friend = friend_emoji
    
    def announce_friend(self):
        print("I am friends with", self.friend)
        
my_fish = Fish(
    name_hawaii = "Zephyr",
    owner_japan = "Alex teacher",
    friend_emoji = "Zweper",
)
my_fish.announce_friend()
