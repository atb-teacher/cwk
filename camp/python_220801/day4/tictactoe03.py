# ===== SETUP TURNS =====

current_turn = 'x'

def switch_turn(active_turn):
    if active_turn == 'x':
        return 'o'
    elif active_turn == 'o':
        return 'x'
        


grid = []
for i in range(3):
    row = []
    for j in range(3):
        row.append("empty")
    grid.append(row)
    
print(grid)

squares = []
for y_coord in [100, 0, -100]:
    for x_coord in [-100, 0, 100]:
        one_square = codesters.Rectangle(
            x_coord,
            y_coord,
            96,
            96,
            "white",
        )
        one_square.set_outline_color("black")
        one_square.set_line_thickness(4)
        squares.append(one_square)
        
        
for i in range(9):
    squares[i].id = i
    # squares[i].say(i)

def click(sprite):
    global current_turn
    row = sprite.id // 3
    col = sprite.id % 3
    if grid[row][col] == "empty":
        grid[row][col] = current_turn
        letter = codesters.Text(
            current_turn,
            sprite.get_x(),
            sprite.get_y(),
        )
        letter.set_size(4) # new line
    current_turn = switch_turn(current_turn) # new line
    print(grid)
    
for one_square in squares:
    one_square.event_click(click)

