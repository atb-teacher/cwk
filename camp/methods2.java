package space.codekingdoms.alexteacher8.methods;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Player extends BasePlayer {
	
	public void onRespawn() {
		
		setup();
	
	}
	
	public void onJoin() {
		
		setup();
	
	}
	
	public void setup() {
		
		clearInventory();
		addItemToInventory(new ItemStack(Material.STONE_AXE));
		addItemToInventory(new ItemStack(Material.TORCH));
	
	}
	
	public void onRightClick() {
		changeBlock(Material.STONE);
	}
	
	public void changeBlock(Material newMaterial) {
		getGame().setBlockTypeAtLocation(newMaterial, getTargetBlock().getLocation());
	}
	
}

