absentees = {'Sept.': 0, 'Oct.': 2, 'Nov.': 1, 'Dec.': 0, 'Jan.': 2, 'Feb.': 0, 'Mar.': 2, 'Apr.': 0}

counter = 0
for one_month in "Sept. Oct. Nov. Dec. Jan. Feb. Mar. Apr.".split():
     counter = counter + absentees[one_month]
print(counter) # 7

absent_sum = sum(
    [absentees[one_month] for one_month in "Sept. Oct. Nov. Dec. Jan. Feb. Mar. Apr.".split()]
)
print(absent_sum) # 7

"""
>>> [i for i in range(5)]
[0, 1, 2, 3, 4]
>>> my_list = []
>>> for i in range(5):
...     my_list.append(i)
... 
>>> my_list
[0, 1, 2, 3, 4]
>>> 

"""
