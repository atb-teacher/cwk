#!/usr/bin/env python
# coding: utf-8

# In[1]:


with open('ClassData.csv', encoding='windows-1252') as f:
    for one_line in f.readlines():
        print(one_line.split(','))


# In[2]:


import matplotlib.pyplot as plt
import pandas as pd
ClassData = pd.read_csv("ClassData.csv", encoding='windows-1252')
ClassData = ClassData.drop(34)
ClassData.dtypes


# In[3]:


ClassData.head(5)


# In[4]:


ClassData['Total Absentees'] = ClassData['Sept.'] + ClassData['Oct.'] + ClassData['Nov.'] + ClassData['Dec.'] + ClassData['Jan.'] + ClassData['Feb.'] + ClassData['Mar.'] + ClassData['Apr.']
# ClassData['Total Absentees'] = int(sum([ClassData[x] for x in "Sept. Oct. Nov. Dec. Jan. Feb. Mar. Apr.".strip().split()]))


# In[6]:


ClassData.tail()


# In[ ]:





# In[7]:


ClassData.loc[:'Height (in)'].plot.hist(bins=5, color='red')


# In[ ]:




