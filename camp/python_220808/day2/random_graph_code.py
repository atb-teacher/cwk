import random

# https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html#Notes

'''
Example types of representation:
'--r' - red dash line
'^g' - green triangle
'-m' - solid magenta line
'Dk' - black diamond (k -> black)

goal -- make a function that returns a random symbol/color that matplotlib can use

'''

symbol_dict = {
  "dash_line": "--",
  "triangle": "^",
  "solid_line": "-",
  "diamond": "D",
}

color_dict = {
  "red": "r",
  "black": "k",
  "magenta": "m",
  "green": "g",
}

print(
  random.choice(
    list(color_dict.keys())
  )
)
