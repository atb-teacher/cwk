# ==================== IMPORTS ====================

import os # used to check if the database exists

import requests # Lets us grab info from the web

from bs4 import BeautifulSoup # Used to analyse the xml

import sqlite3 # Used to create and query the database

import pandas as pd # Used to turn data into a dataframe

import matplotlib.pyplot as plt # Used to graph the data from the dataframe

import re # so that we can use regex

# ==================== GLOBAL VARIABLES ====================

DELETE_DB = False

WRITE_XML = False

# ==================== CREATING THE DB ====================

if DELETE_DB:
    os.remove("nightvale.db")

if not os.path.exists("nightvale.db"): # If the database doesn't exist, create it
    connection = sqlite3.connect("nightvale.db")
    cursor = connection.cursor()

    pod_url = "http://feeds.nightvalepresents.com/welcometonightvalepodcast"
    
    data = requests.get(pod_url)
    
    soup = BeautifulSoup(data.content, features="xml")
    descriptions = soup.findAll('description')
    descriptions = [str(tag) for tag in descriptions] # convert to a list of strings
    
    tags = '|'.join([ # '|' means 'or'
        "&lt;p&gt;", # paragraph start tag
        "&lt;/p&gt;", # paragraph end tag
        "&lt;a.+/a&gt;", # anchortag and everything inside
        "<description>",
        "</description>",
        "Music\:",
        "Logo\:",
        "Weather\:",
    ])

    for i in range(len(descriptions)):
        descriptions[i] = re.sub(tags, ' ', descriptions[i])

    if WRITE_XML:
        with open('descriptions.xml', 'w') as f:
            for one_line in descriptions:
                f.write(one_line)
    
    word_counter = {}
    blocked_chars = set(('<', '>', '=', '&'))
    strip_punctuation = ('.', ',', ':', ')', '(', '"', '-', '“', '”')

    for one_line in descriptions: # loop over all "<descriptions></descriptions> tags
        for one_word in str(one_line).split(): # split to get words

            one_word = one_word.lower()

            # strip the periods, commas, and parentheses off the left and right sides
            for one_punc in strip_punctuation:
                one_word = one_word.strip(one_punc)

            # found_char checks if we found a blocked character
            found_char = False
            for one_letter in one_word:
                if one_letter in blocked_chars:
                    found_char = True

            if not found_char and one_word: # if we didn't find a blocked character
                # update our dictionary
                if one_word in word_counter.keys():
                    word_counter[one_word] += 1
                else:
                    word_counter[one_word] = 1
    
    word_counter_list = list(word_counter.items()) # convert the dictionary to a list of tuples

    word_counter_list.sort(key=lambda one_tuple: one_tuple[1]) # sort the list of tuples by word count

    # create the table
    cursor.execute("CREATE TABLE words (wordid INTEGER PRIMARY KEY AUTOINCREMENT, word TEXT, count INTEGER)")
    
    # add the last 100 rows to the database
    cursor.executemany("INSERT INTO words(word, count) values (?, ?)", word_counter_list[-100:])

    connection.commit()

else:
    connection = sqlite3.connect("nightvale.db")
    cursor = connection.cursor()


# ==================== CREATING THE DATAFRAME ====================

dataframe = pd.read_sql_query("SELECT * from words ORDER BY count DESC LIMIT 5", connection)

# ==================== PLOTTING THE DATA ====================

plt.bar(
    dataframe['word'],
    dataframe['count'],
    color="green",
)
plt.xlabel("Word")
plt.ylabel("Word Counts")
plt.title("Word Counts for Episode Descriptions of \"Welcome to Night Vale\"")
plt.show()
