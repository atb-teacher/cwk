import csv
import sqlite3

connection = sqlite3.connect("product.db")

# cursor manages commands
cursor = connection.cursor()

col1 = 'categoryid'
col1_type = 'INTEGER PRIMARY KEY'
col2 = 'categoryname'
col2_type = 'TEXT'
cursor.execute(f"CREATE TABLE category ({col1} {col1_type}, {col2} {col2_type})")

with open("Category.csv") as cat_file:
  for one_row in csv.DictReader(cat_file):
    new_pk = one_row["PK"]
    new_name = one_row["Name"]
    cursor.execute(f"INSERT INTO category VALUES ({new_pk}, '{new_name}')")

names = cursor.execute("SELECT categoryname FROM category").fetchall()
print(names)

col1 = 'productid'
col1_type = 'INTEGER PRIMARY KEY'
col2 = 'productname'
col2_type = 'TEXT'
col3 = 'categoryid'
col3_type = 'INTEGER'
constraint = 'FOREIGN KEY(categoryid) REFERENCES category(categoryid)'


cursor.execute(f"CREATE TABLE product ({col1} {col1_type}, {col2} {col2_type}, {col3} {col3_type}, {constraint})")


with open("Product.csv") as prod_file:
  for one_row in csv.DictReader(prod_file):
    new_pk = one_row["PK"]
    new_name = one_row["Name"]
    new_cat = one_row["Category (FK)"]
    cursor.execute(f"INSERT INTO product VALUES ({new_pk}, '{new_name}', {new_cat})")

cursor.execute(f"INSERT INTO product VALUES ({new_pk}, '{new_name}', {new_cat})")
names = cursor.execute("SELECT productname, categoryname FROM category JOIN product WHERE category.categoryid == product.categoryid").fetchall()
print(names)
