"""

===== SAMPLE CODE 1 =====

  * Opening the .csv file
  * Looping over the lines
  * Using .split to split based on commas
  
>>> with open('Product.csv') as f:
...     for one_line in f.readlines():
...             print(one_line.strip().split(','))
... 
['PK', 'Name', 'Category (FK)']
['1', 'Minecraft', '3']
['2', 'Roblox', '3']
['3', 'To Build a Fire', '4']
['4', 'Wall-E', '1']
['5', 'Stranger Things', '2']


===== SAMPLE CODE 2 =====

  * Using the "csv" library
  * Creating an empty list
  * Looping over every row
  * Auto converting rows to dictionaries
  * Putting every dictionary into the list
  
>>> import csv
>>> with open("Product.csv") as f:
...     full_product_list = list(
...             csv.DictReader(f)
...     )
... 
>>> full_product_list
[{'PK': '1', 'Name': 'Minecraft', 'Category (FK)': '3'}, {'PK': '2', 'Name': 'Roblox', 'Category (FK)': '3'}, {'PK': '3', 'Name': 'To Build a Fire', 'Category (FK)': '4'}, {'PK': '4', 'Name': 'Wall-E', 'Category (FK)': '1'}, {'PK': '5', 'Name': 'Stranger Things', 'Category (FK)': '2'}]


===== SAMPLE CODE 3 =====

  * Start with a list of dictionaries
  * Find the dictionary with a specific name
  * Print all info from that dictionary

>>> full_product_list
[{'PK': '1', 'Name': 'Minecraft', 'Category (FK)': '3'}, {'PK': '2', 'Name': 'Roblox', 'Category (FK)': '3'}, {'PK': '3', 'Name': 'To Build a Fire', 'Category (FK)': '4'}, {'PK': '4', 'Name': 'Wall-E', 'Category (FK)': '1'}, {'PK': '5', 'Name': 'Stranger Things', 'Category (FK)': '2'}]

>>> for one_row in full_product_list:
...     if one_row['Name'] == 'Roblox':
...             print(f"{one_row['Name']} is in category {one_row['Category (FK)']}")
... 
Roblox is in category 3



* Consider using this library: https://docs.python.org/3/library/csv.html
* Create a repl
  * Let the repl ask the user for which name they want to look up
  * Print the row info related to that name

EXAMPLE:

Hello! Here you can look up product info.

What's the name of the product you would like info on ('q' to quit)?

Name: Wall-E

Wall-E is in category 3

Give me the next product name or (q)

Name: q

Bye!
"""
