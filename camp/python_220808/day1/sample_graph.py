import numpy as np
import matplotlib.pyplot as plt
names = ["Gary","Cindy","Jenna","Fred"]
cell_phones = [30, 8, 7, 25]
# Plot the simple bar graph
plt.title("Cell Phones Chart")
plt.xlabel("Name")
plt.ylabel("Phones")
plt.bar(names,cell_phones,color="green")
plt.show()
plt.savefig("graph.png")
