'''
There's a study where participants have to take a survey. 5 times a day they have to say how they are feeling. You need to use a dictionary to track what they say, and how many times they say each thing.

Make a repl that updates a dictionary.
'''

'''
> "happy"

{ "happy": 3,
"sad": 2,
"bored": 1,
"content": 5
}

'''
