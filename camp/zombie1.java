GAME:

package space.codekingdoms.alexteacher8.zombiefight;

import com.codekingdoms.nozzle.base.BaseGame;
import org.bukkit.Location;

public class Game extends BaseGame {
	
	public void spawnZombie() {
		
		Zombie z = new Zombie();
		z.spawn(world, new Location(world, -10, 70, 10));
	
	}
	
	public void onCodeUpdate() {
		world.setTime(1000);
		world.setSpawnLocation(0, 70, 0);
		removeAllMobs();
		disableMobSpawning();
		setInterval(
			() -> {
			spawnZombie();
			}
			, 5, 5);
	}
}

ZOMBIE:

package space.codekingdoms.alexteacher8.zombiefight;

import com.codekingdoms.nozzle.base.BaseZombie;
import org.bukkit.Material;

public class Zombie extends BaseZombie {
	
	 public void onSpawn() {
		
		setBaby(true);
		equipItem(Material.WOOD_SWORD);
	
	}
	
}
