package space.codekingdoms.alexteacher8.magicwand;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import com.codekingdoms.nozzle.utils.ProjectileType;

public class Player extends BasePlayer {
	
	public void onUseItem() {
		
		ItemStack stick = new ItemStack(Material.STICK);
		
		ItemStack blaze = new ItemStack(Material.BLAZE_ROD);
		
		if (getItemInMainHand().equals(stick)) {
			
			throwProjectile(ProjectileType.LARGE_FIREBALL);
			
		}
		
		if (getItemInMainHand().equals(blaze)) {
			throwProjectile(ProjectileType.SPECTRAL_ARROW);
		}
	}
	
}
