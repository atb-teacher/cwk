PLAYER:

package space.codekingdoms.alexteacher8.goodsword;

import com.codekingdoms.nozzle.base.BasePlayer;
import org.bukkit.Particle;
import com.codekingdoms.nozzle.utils.ArmorSet;

public class Player extends BasePlayer {
	
	public void onRunCommand( String command, String[] args ) {
		
		if (command.equals("/sword")) {
			
			addItemToInventory(getGame().makeSword());
			
		}
		
		else if (command.equals("/armor")) {
			
			equipFullArmorSet(ArmorSet.DIAMOND);
			
		}
		
	
	}
	
	public void onRightClick() {
		
		String itemName = getGame().getDisplayNameOfItem(getItemInMainHand());
		if ((itemName != null) && itemName.equals("Magical Super Sword")) {
			
			world.strikeLightning(getTargetBlock().getLocation());
			spawnParticle(Particle.EXPLOSION_HUGE, getTargetBlock().getLocation(), 10);
			spawnParticle(Particle.SMOKE_NORMAL, getTargetBlock().getLocation(), 10);
			
		}
		
	
	}
	
	
}



GAME:

package space.codekingdoms.alexteacher8.goodsword;

import com.codekingdoms.nozzle.base.BaseGame;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

public class Game extends BaseGame {
	
	public ItemStack makeSword() {
		
		ItemStack magicSword = new ItemStack(Material.DIAMOND_SWORD, 1);
		setDisplayNameOfItem(magicSword, "Magical Super Sword");
		magicSword.addEnchantment(Enchantment.FIRE_ASPECT, 2);
		magicSword.addEnchantment(Enchantment.KNOCKBACK, 2);
		magicSword.addEnchantment(Enchantment.DAMAGE_ARTHROPODS, 5);
		magicSword.addEnchantment(Enchantment.LOOT_BONUS_MOBS, 3);
		magicSword.addEnchantment(Enchantment.MENDING, 1);
		magicSword.addEnchantment(Enchantment.DAMAGE_ALL, 5);
		magicSword.addEnchantment(Enchantment.DAMAGE_UNDEAD, 5);
		return magicSword;
		
	}
	
	
}
