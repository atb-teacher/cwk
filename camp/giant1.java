PLAYER:

package space.codekingdoms.alexteacher8.spawngiant;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	public void onRunCommand(String command, String[] args) {
		if (command.equals("/spawngiant")) {
			Giant g = new Giant();
			g.spawn(world, getTargetBlock().getLocation());
	}
}

GIANT:

package space.codekingdoms.alexteacher8.spawngiant;

import com.codekingdoms.nozzle.base.BaseGiant;

public class Giant extends BaseGiant {
	
	
}
