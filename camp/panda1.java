PLAYER: 
package space.codekingdoms.alexteacher8.spawnpanda;

import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	public void onRunCommand(String command, String[] args) {
		if (command.equals("/spawnpanda")) {
			Panda p = new Panda();
			p.spawn(world, getTargetBlock().getLocation());
		}
	}
	
}

PANDA:

package space.codekingdoms.alexteacher8.spawnpanda;

import com.codekingdoms.nozzle.base.BasePanda;

public class Panda extends BasePanda {
	
}
