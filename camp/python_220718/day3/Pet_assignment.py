class Pet:# blueprint part
    def __init__(self, _name, _age, _type):# instructions to create yourself
    # customize name and age
        self.name = _name
        self.age = _age # save name and age
        self.type = _type
    
    def print_info(self):
        print(
            f"{self.name} is a {self.age} year old {self.type}"
        )

    def do_a_trick(self):
        print(
            f"You see the {self.type} do a backflip!"    
        )

# make a pet from blueprint
my_pet = Pet(_name="Kung Fu Panda", _age=5, _type="Panda")
my_pet.print_info() # Kung Fu Panda is a 5 year old Panda
my_pet.do_a_trick() # You see the Panda do a backflip!

"""
In this example, the "Dog" class and the "Robot" class
both inherit from the "Pet" class.

Try to make a new class called "Wolf" or "Falcon" or
another cool pet that inherits from the "Pet" class.

You can look to "Dog" and "Robot" for hints on the
syntax.
"""




