class Person:
    def __init__(self, _name, _color, _arm_rotation):
        self.name = _name
        self.left_arm = codesters.Rectangle(
            -25, # x
            50, # y
            20, # width
            40, # height
            _color, # color
        )
        self.left_arm.rotate_about(-1 * _arm_rotation, -20, 75)
        self.right_arm = codesters.Rectangle(
            25, # x
            50, # y
            20, # width
            40, # height
            _color, # color
        )
        self.right_arm.rotate_about(_arm_rotation, 20, 75)
        
        self.torso = codesters.Rectangle(
            0, # x
            45, # y
            20, # width
            60, # height
            _color, # color
        )
        self.head = codesters.Circle(
            0, 100, 40, _color,
        )
        
    def introduce(self):
        self.head.say(f"Hi, I'm {self.name}")

test_person = Person("Alex", "blue", 30)
test_person.introduce()

