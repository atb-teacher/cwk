class Person:
    def __init__(self, _name, _color):
        self.name = _name
        self.left_arm = codesters.Rectangle(
            -25, # x
            50, # y
            20, # width
            40, # height
            _color, # color
        )
        self.left_arm.set_rotation(-20)
        self.right_arm = codesters.Rectangle(
            25, # x
            50, # y
            20, # width
            40, # height
            _color, # color
        )
        self.right_arm.set_rotation(20)
        
        self.torso = codesters.Rectangle(
            0, # x
            45, # y
            20, # width
            60, # height
            _color, # color
        )
        self.head = codesters.Circle(
            0, 100, 40, _color,
        )
        
    def introduce(self):
        self.head.say(f"Hi, I'm {self.name}")

test_person = Person("Alex", "blue")
test_person.introduce()
