class Person:
    def __init__(self, _name, _home_city, _fav_media, _arm_rotation=30, _color="purple"):
        self.name = _name
        self.home_city = _home_city
        self.fav_media = _fav_media
        self.left_arm = codesters.Rectangle(
            -25, # x
            50, # y
            20, # width
            40, # height
            _color, # color
        )
        self.left_arm.rotate_about(-1 * _arm_rotation, -20, 75)
        self.right_arm = codesters.Rectangle(
            25, # x
            50, # y
            20, # width
            40, # height
            _color, # color
        )
        self.right_arm.rotate_about(_arm_rotation, 20, 75)
        
        self.torso = codesters.Rectangle(
            0, # x
            45, # y
            20, # width
            60, # height
            _color, # color
        )
        self.head = codesters.Circle(
            0, 100, 40, _color,
        )
        
    def introduce(self):
        # self.name, self.home_city, self.fav_media
        self.head.say(f"Hi, I'm {self.name}. I like {self.fav_media}. I live in {self.home_city}.")

test_person = Person(
	_name="Alex",
	_home_city="Portland",
	_fav_media="Mario 64",
)
test_person.introduce()
