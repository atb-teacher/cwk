class Pet:
    def __init__(self, _name, _age):
        self.name = _name
        self.age = _age
        
my_pet = Pet(_name="Blaze", _age=1)
print(f"My pet's name is {my_pet.name}.")
print(f"My pet's age is {my_pet.age}.")

