print("#", "=" * 20, "SETUP ENEMY", "=" * 20)

# ==================== SETUP ENEMY ====================

class EnemyCobra: # EnemeyCobra blueprint
    def __init__(self, _health):
        self.sprite = codesters.Sprite("snake2")
        self.sprite.set_size(.2)
		self.health = _health
		self.sprite.say(self.health)

	def announce_health(self):
		self.sprite.say(self.health)


one_enemy = EnemyCobra(_health = 3) # "Follow the blueprint and make an EnemyCobra

while one_enemy.health > 0:
    stage.wait(1)
    one_enemy.health -= 1
    one_enemy.announce_health()

"""
GOALS:
  * A bunch of different types of enemies
    * Different levels of health
    * Different movement patterns
"""
