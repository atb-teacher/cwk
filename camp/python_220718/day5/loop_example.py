"""
for y_coord in [100, 0, -100]:
    for x_coord in [-100, 0, 100]:
        print(f"I am at x={x_coord}, y={y_coord}")
"""

grid = []
for i in range(3):
    row = []
    for j in range(3):
        row.append("empty")
    grid.append(row)