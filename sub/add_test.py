num1 = random.randint(1, 5)
num2 = random.randint(1, 5)
keep_asking = True
while keep_asking == True:
    user_answer = input(f"What is {num1} + {num2}? ")
    user_answer = int(user_answer)
    if num1 + num2 == user_answer:
        print("correct!")
        keep_asking = False # ends the loop
