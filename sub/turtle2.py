t = turtle.Turtle()
t.set_speed(100)


def make_square():
    for number in [0, 1, 2, 3]:
        t.forward(100)
        t.left(90)

def make_circle():
    for i in range(360):
        t.forward(1)
        t.left(1)

def make_triangle():
    for number in [0, 1, 2]:
        t.forward(100)
        t.left(120)


t.penup() # stops from drawing
t.goto(-100, 0)
t.pendown()
t.goto(100, 0)
t.goto(0, 80)
t.goto(-100, 0)
t.penup()
