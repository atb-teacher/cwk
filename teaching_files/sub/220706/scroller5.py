# ===== SETUP STAGE =====

stage.disable_all_walls() 
stage.set_gravity(10)

# ===== SETUP HERO =====

hero = codesters.Sprite("alien2", -200, -175)
hero.set_gravity_on()
hero.set_size(.5) # changed
hero.jumps = 2
hero.alive = True
hero.health = 5

# ===== SETUP HEALTH TEXT =====

health_text = codesters.Text(
        f"Health: {hero.health}",
        -175,
        175
)

def update_health_text():
    health_text.set_text(f"Health: {hero.health}")

# ===== SETUP JUMP =====

def space_bar_roblox(): # function/set of instructions
    if hero.jumps > 0:
        hero.jumps = hero.jumps - 1
        hero.say(hero.jumps)
        hero.jump(15)
        hero.set_gravity_on()

stage.event_key("space", space_bar_roblox)

# ===== SETUP GROUND =====

ground = codesters.Rectangle(
    0, # x position
    -225, # y position
    500, # width
    50, # height
    "limegreen",
)
ground.set_gravity_off()

# ===== SETUP COLLISION =====

def collision_harry_potter(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
        
    if my_var == "limegreen":
        sprite.set_y_speed(0)
        sprite.set_gravity_off()
        sprite.set_y(
            ground.get_y() + \
            ground.get_height()/2 + \
            sprite.get_height()/2
        )
        hero.jumps = 2
        hero.say(hero.jumps)

    if my_var == "blue":
        hero.health = hero.health - 1
        update_health_text()
        
hero.event_collision(collision_harry_potter)

# ===== SETUP OBSTACLE =====

counter = 0
while hero.alive:
    counter = counter + 1
    if counter % 20 == 0:
        obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")
        obstacle.set_gravity_off()
        obstacle.set_x_speed(-5)
    stage.wait(.1)
