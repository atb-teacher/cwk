# Sub Guide

## Intro

We've been working on `Simple Side Scroller` using replit and codesters. The files are [here](/4_wed/1430/scroller/), and the files named `scroller` [here](/4_wed/1430/). The replit is [here](https://replit.com/@AlBurns/wed-1430?v=1). I have two versions of the codesters file: [scroller0.5](/4_wed/1430/scroller0.5.py), which is nice because it connects all the things we've been learning in `replit` to our main scroller project, and [scroller3](/4_wed/1430/scroller3.py), which is further along. You could discuss `scroller0.5`, but I think it'd just be easier to work on `scroller3` and making progress on that. I've been going pretty slowly, so don't feel rushed!

## Presentations

It's best if we don't make them present today. There are 3 students in this class. 2 students I've only had 1 class with, and the 3rd we made a "guess the number" lab since it was a 1-on-1 session. They won't have much to show if they choose to present. If they do want to present, let them present whatever they want, or explain a small file we made in replit.

## Making the file

### Scroller 3

Start with [scroller3.py](scroller3.py). It's totally fine if they directly copy this file if they're behind.

I add weird suffixes to any functions that are also arguments, which is why the jump function is called `space_bar_roblox`. Sorry if that's not readable.

I used `alien2` for this game, but I feel like that sprite has a really wonky hitbox, so feel free to change it.

Topics to review:

* We give the hero a double jump, which is why when we setup the hero we set `hero.jumps` to 2, and when the user hits the spacebar we check `if hero.jumps > 0:`.

* We set `hero.alive = True`, so that lower on we could say `while hero.alive`. It'd be good to review that `while hero.alive == True` and `while hero.alive` are the same thing effectively.

* Do we want the gravity to be on while the hero is jumping? While the hero is standing still?

* In the "Setup Collision" section, when the hero touches the lime green floor, they lose any y speed, any gravity, and their position is set to the floor's position + 1/2 the floor's heigh + 1/2 their height.

```python
# ===== SETUP STAGE =====

stage.disable_all_walls() 
stage.set_gravity(10)

# ===== SETUP HERO =====

hero = codesters.Sprite("alien2", -200, -175)
hero.set_gravity_on()
hero.set_size(.5) # changed
hero.jumps = 2
hero.alive = True

# ===== SETUP JUMP =====

def space_bar_roblox(): # function/set of instructions
    if hero.jumps > 0:
        hero.jumps = hero.jumps - 1
        hero.say(hero.jumps)
        hero.jump(15)
        hero.set_gravity_on()

stage.event_key("space", space_bar_roblox)

# ===== SETUP GROUND =====

ground = codesters.Rectangle(
    0, # x position
    -225, # y position
    500, # width
    50, # height
    "limegreen",
)
ground.set_gravity_off()

# ===== SETUP COLLISION =====

def collision_harry_potter(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
        
    if my_var == "limegreen":
        sprite.set_y_speed(0)
        sprite.set_gravity_off()
        sprite.set_y(
            ground.get_y() + \
            ground.get_height()/2 + \
            sprite.get_height()/2
        )
        hero.jumps = 2
        hero.say(hero.jumps)
        
hero.event_collision(collision_harry_potter)

# ===== SETUP OBSTACLE =====

while hero.alive:
    obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")
    obstacle.set_gravity_off()
    obstacle.set_x_speed(-5)
    stage.wait(2)
```

### Scroller 4

Next is [scroller4.py](scroller4.py).

Here, instead of the game loop having a `stage.wait(2)`, we replace it with `stage.wait(.1)` so that the game loop can run more quickly. This will make it easier to make future adjustments. We also use a game loop counter, along with `if counter % 20 == 0:`, to make sure that we only make an obstacle every 20th game loop. If this is confusing, go back to the `replit` files.

What would happen if we changed the number `20`? What would happen if we changed it back to `stage.wait(2)` with all the other code we've changed?

```python
# ===== SETUP OBSTACLE =====

counter = 0          # added line
while hero.alive:
    obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")     # deleted line
    obstacle.set_gravity_off()     # deleted line
    obstacle.set_x_speed(-5)     # deleted line
    stage.wait(2)     # deleted line
    counter = counter + 1          # added line
    if counter % 20 == 0:          # added line
        obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")          # added line
        obstacle.set_gravity_off()          # added line
        obstacle.set_x_speed(-5)          # added line
    stage.wait(.1)          # added line
```

### Scroller 5

Next is [scroller5.py](scroller5.py).

Here we give the hero health, and we create a textbox. We will get a bug where we lose too much health from colliding with a blue box.

```python
hero.jumps = 2
hero.alive = True
hero.health = 5          # added line

          # added line
# ===== SETUP HEALTH TEXT =====          # added line

          # added line
health_text = codesters.Text(          # added line
        f"Health: {hero.health}",          # added line
        -175,          # added line
        175          # added line
)          # added line

          # added line
def update_health_text():          # added line
    health_text.set_text(f"Health: {hero.health}")          # added line

# ===== SETUP JUMP =====

@ -48,6 +60,10 @@ def collision_harry_potter(sprite, hit_sprite):
        )
        hero.jumps = 2
        hero.say(hero.jumps)

          # added line
    if my_var == "blue":          # added line
        hero.health = hero.health - 1          # added line
        update_health_text()          # added line
        
hero.event_collision(collision_harry_potter)

```

### Scroller 6

Next is [scroller6.py](scroller6.py).

To fix the "lose too much health" bug, we can give the hero a period of invincibility.


```python
hero.jumps = 2
hero.alive = True
hero.health = 5
hero.invincible = False          # added line

# ===== SETUP HEALTH TEXT =====

@ -61,9 +62,12 @@ def collision_harry_potter(sprite, hit_sprite):
        hero.jumps = 2
        hero.say(hero.jumps)

    if my_var == "blue":     # deleted line
    if my_var == "blue" and not hero.invincible:          # added line
        hero.invincible = True          # added line
        hero.health = hero.health - 1
        update_health_text()
        stage.wait(2)          # added line
        hero.invincible = False           # added line
        
hero.event_collision(collision_harry_potter)
```

### Scroller 7

Next is [scroller7.py](scroller7.py).

We can make the game loop end when the hero takes too much damage.

What are the series of events that lead to the game loop ending? Make them go through it line by line.

```python
def collision_harry_potter(sprite, hit_sprite):
        hero.invincible = True
        hero.health = hero.health - 1
        update_health_text()
        if hero.health <= 0:          # added line
            hero.alive = False          # added line
        stage.wait(2)
        hero.invincible = False 
        
@ -81,3 +83,5 @@ while hero.alive:
        obstacle.set_gravity_off()
        obstacle.set_x_speed(-5)
    stage.wait(.1)

end_text = codesters.Text("End")          # added line
```

### Scroller 8

Next is [scroller8.py](scroller8.py).

Next, we can remove all the sprites from the stage at the end.

```python
while hero.alive:
    stage.wait(.1)

end_text = codesters.Text("End")
for one_sprite in [hero, ground, health_text]:          # added line
    stage.remove_sprite(one_sprite)          # added line
```

### Scroller 9

Next is [scroller9.py](scroller9.py).

Next, we can give the player a bit more movement. What does `if hero.get_y_speed != 0:` mean? (it means there's y movement, so the player is not standing on the ground)

How do we determine how far left and right the player can move?

```python
def space_bar_roblox(): # function/set of instructions

stage.event_key("space", space_bar_roblox)

# ===== SETUP MOVEMENT =====          # added line

def drop():          # added line
    if hero.get_y_speed != 0:          # added line
        hero.set_y_speed(-20)          # added line

stage.event_key("down", drop)          # added line

def right_key(sprite):          # added line
    if sprite.get_x() < -150:          # added line
        sprite.move_right(20)          # added line

hero.event_key("right", right_key)          # added line

def left_key(sprite):          # added line
    if sprite.get_x() > -250:          # added line
        sprite.move_left(20)          # added line

hero.event_key("left", left_key)          # added line

# ===== SETUP GROUND =====

ground = codesters.Rectangle(
```

### Scroller 10

Next is [scroller10.py](scroller10.py).

Here we make the obstacles more difficult.

I usually make them come up with random integers, and the do the math to figure out what the height would be in different scenarios. But `height` is between `0` and `-175`, and always divisible by `25`.

```python
hero.event_collision(collision_harry_potter)
counter = 0
while hero.alive:
    counter = counter + 1
    if counter % 20 == 0:     # deleted line
        obstacle = codesters.Rectangle(150, -175, 50, 50, "blue")     # deleted line
    if counter % 5 == 0:          # added line
        height = random.randint(0, 7) * -25          # added line
        obstacle = codesters.Rectangle(150, height, 50, 50, "blue")          # added line
        obstacle.set_gravity_off()
        obstacle.set_x_speed(-5)
    stage.wait(.1)
```

### Scroller 11

Next is [scroller11.py](scroller11.py).

Finally, make double jump feel better with a `jump_lock` attribute.

```python
hero.alive = True
hero.health = 5
hero.invincible = False
hero.jump_lock = False          # added line

# ===== SETUP HEALTH TEXT =====

@ -27,7 +28,8 @@ def update_health_text():
# ===== SETUP JUMP =====

def space_bar_roblox(): # function/set of instructions
    if hero.jumps > 0:     # deleted line
    if hero.jumps > 0 and not hero.jump_lock:          # added line
        hero.jump_lock = True          # added line
        hero.jumps = hero.jumps - 1
        hero.say(hero.jumps)
        hero.jump(15)
@ -35,6 +37,11 @@ def space_bar_roblox(): # function/set of instructions

stage.event_key("space", space_bar_roblox)

def release_key(sprite):          # added line
    sprite.jump_lock = False          # added line
              # added line
hero.event_key_release("space", release_key)          # added line

          # added line
# ===== SETUP MOVEMENT =====

def drop():
```
