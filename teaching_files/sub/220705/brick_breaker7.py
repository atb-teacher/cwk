# ===== SETUP BRICKS =====

bricks = []
for y_pos in [200, 180, 160, 140]:
    for x_pos in [-200, -150, -100, -50, 0, 50, 100, 150, 200]:
        bricks.append(codesters.Rectangle(
            x_pos,# x pos
            y_pos,# y pos
            45,# width
            15,# height
            "darkred",
        ))

# ===== SETUP BALL =====

ball = codesters.Circle(
        3,    # x
        -100, # y
        25,   # diameter
        "black",
)

ball.set_x_speed(
    random.randint(-2, 2)
)

ball.set_y_speed(
    random.randint(-2, 2)
)

# ===== SETUP PADDLE =====

paddle = codesters.Rectangle(
    0, # x
    -150, # y
    100,# width
    15, # height
    "yellow", # color
)
paddle.set_outline_color("black")

# ===== SETUP MOVEMENT =====

def left_key(sprite):
    sprite.move_left(20)

paddle.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)

paddle.event_key("right", right_key)

# ===== SETUP COLLISION =====

def collision(main_ball, other_shape):
    other_shape_color = other_shape.get_color() 
    if other_shape_color == "darkred":
        main_ball.set_y_speed(
            main_ball.get_y_speed() * -1
        )
        bricks.remove(other_shape)
        stage.remove_sprite(other_shape)
        if len(bricks) == 0:
            end_game()
    if other_shape_color == "yellow":
        main_ball.set_y_speed(
            main_ball.get_y_speed() * -1
        )
        
ball.event_collision(collision)

# ===== SETUP GAME END =====

def end_game():
    stage.remove_sprite(paddle)
    stage.remove_sprite(ball)
    end_text = codesters.Text("You win!")
