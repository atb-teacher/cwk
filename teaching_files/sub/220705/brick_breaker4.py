# ===== SETUP BRICKS =====

bricks = []
for y_pos in [200, 180, 160, 140]:
    for x_pos in [-200, -150, -100, -50, 0, 50, 100, 150, 200]:
        bricks.append(codesters.Rectangle(
            x_pos,# x pos
            y_pos,# y pos
            45,# width
            15,# height
            "darkred",
        ))

# ===== SETUP BALL =====

ball = codesters.Circle(
        3,    # x
        -100, # y
        25,   # diameter
        "black",
)

# ===== SETUP PADDLE =====

paddle = codesters.Rectangle(
    0, # x
    -150, # y
    100,# width
    15, # height
    "yellow", # color
)
paddle.set_outline_color("black")

# ===== SETUP MOVEMENT =====

def left_key(sprite):
    sprite.move_left(20)

paddle.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)

paddle.event_key("right", right_key)
