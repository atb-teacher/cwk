```python3
```
```python3
diff --git a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker2.py b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker3.py
index 15659e7..85a1fb9 100644
--- a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker2.py
+++ b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker3.py
@@ -17,9 +17,10 @@ ball = codesters.Circle(
 )
 
 paddle = codesters.Rectangle(
    # x     # deleted line
    # y     # deleted line
    # width     # deleted line
    # height     # deleted line
    # color     # deleted line
    0, # x          # added line
    -150, # y          # added line
    100,# width          # added line
    15, # height          # added line
    "yellow", # color          # added line
 )
paddle.set_outline_color("black")          # added line
```
```python3
diff --git a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker3.py b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker4.py
index 85a1fb9..b5d68b3 100644
--- a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker3.py
+++ b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker4.py
@@ -1,3 +1,5 @@
# ===== SETUP BRICKS =====          # added line

          # added line
 bricks = []
 for y_pos in [200, 180, 160, 140]:
     for x_pos in [-200, -150, -100, -50, 0, 50, 100, 150, 200]:
@@ -9,6 +11,8 @@ for y_pos in [200, 180, 160, 140]:
             "darkred",
         ))
 
# ===== SETUP BALL =====          # added line

          # added line
 ball = codesters.Circle(
         3,    # x
         -100, # y
@@ -16,6 +20,8 @@ ball = codesters.Circle(
         "black",
 )
 
# ===== SETUP PADDLE =====          # added line

          # added line
 paddle = codesters.Rectangle(
     0, # x
     -150, # y
@@ -24,3 +30,15 @@ paddle = codesters.Rectangle(
     "yellow", # color
 )
 paddle.set_outline_color("black")

          # added line
# ===== SETUP MOVEMENT =====          # added line

          # added line
def left_key(sprite):          # added line
    sprite.move_left(20)          # added line

          # added line
paddle.event_key("left", left_key)          # added line

          # added line
def right_key(sprite):          # added line
    sprite.move_right(20)          # added line

          # added line
paddle.event_key("right", right_key)          # added line
```
```python3
diff --git a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker4.py b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker5.py
index b5d68b3..1f535e2 100644
--- a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker4.py
+++ b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker5.py
@@ -20,6 +20,14 @@ ball = codesters.Circle(
         "black",
 )
 
ball.set_x_speed(          # added line
    random.randint(-2, 2)          # added line
)          # added line

          # added line
ball.set_y_speed(          # added line
    random.randint(-2, 2)          # added line
)          # added line

          # added line
 # ===== SETUP PADDLE =====
 
 paddle = codesters.Rectangle(
```
```python3
diff --git a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker5.py b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker6.py
index 1f535e2..fadb6b9 100644
--- a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker5.py
+++ b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker6.py
@@ -50,3 +50,20 @@ def right_key(sprite):
     sprite.move_right(20)
 
 paddle.event_key("right", right_key)

          # added line
# ===== SETUP COLLISION =====          # added line

          # added line
def collision(main_ball, other_shape):          # added line
    other_shape_color = other_shape.get_color()           # added line
    if other_shape_color == "darkred":          # added line
        main_ball.set_y_speed(          # added line
            main_ball.get_y_speed() * -1          # added line
        )          # added line
        bricks.remove(other_shape)          # added line
        stage.remove_sprite(other_shape)          # added line
    if other_shape_color == "yellow":          # added line
        main_ball.set_y_speed(          # added line
            main_ball.get_y_speed() * -1          # added line
        )          # added line
                  # added line
ball.event_collision(collision)          # added line
```
```python3
diff --git a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker6.py b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker7.py
index fadb6b9..79ebf93 100644
--- a/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker6.py
+++ b/home/atb-teaching/cwk/teaching_files/sub/220705/brick_breaker7.py
@@ -61,9 +61,18 @@ def collision(main_ball, other_shape):
         )
         bricks.remove(other_shape)
         stage.remove_sprite(other_shape)
        if len(bricks) == 0:          # added line
            end_game()          # added line
     if other_shape_color == "yellow":
         main_ball.set_y_speed(
             main_ball.get_y_speed() * -1
         )
         
 ball.event_collision(collision)

          # added line
# ===== SETUP GAME END =====          # added line

          # added line
def end_game():          # added line
    stage.remove_sprite(paddle)          # added line
    stage.remove_sprite(ball)          # added line
    end_text = codesters.Text("You win!")          # added line
```
