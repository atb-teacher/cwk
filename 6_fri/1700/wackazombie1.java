// GAME CODE

public class Game extends BaseGame {
	
	public void startGame( Player player ) {
		
		broadcastMessage("starting the game");
		// Zombie z = new Zombie();
		Location loc = player.getLocation();
		// z.spawn(world, loc);
		setInterval(
			() -> {
				removeAllMobs();
				Zombie z = new Zombie();
				z.spawn(world, loc);
			}, 0, 5);
		startTimer(60);
		
	}
	public void onTimerExpire() {
		stopAllTimeouts();
	}
	
}
