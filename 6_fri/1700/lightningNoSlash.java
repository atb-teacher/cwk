package space.codekingdoms.alexteacher8.lightning2022;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import com.codekingdoms.nozzle.base.BasePlayer;

public class Player extends BasePlayer {
	
	public void onChat( String message ) {
		
		if (message.equals("getmakelightning")) {
			
			world.strikeLightning(getTargetBlock().getLocation());
			
		}
		
		if (message.equals("getinvisible")) {
			
			applyPotionEffect(PotionEffectType.INVISIBILITY, 600, 1);
			
		}
		
		if (message.equals("getheal")) {
			
			setFoodLevel(20);
			setHealth(20);
			
		}
		
		if (message.equals("getspeed")) {
			
			setWalkSpeed(0.5f);
			
		}
		
		if (message.equals("getsword")) {
			
			addItemToInventory(new ItemStack(Material.DIAMOND_SWORD));
			
		}
	}
}
