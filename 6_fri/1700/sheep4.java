
import com.codekingdoms.nozzle.utils.Random;
import com.codekingdoms.nozzle.utils.Direction;
import java.lang.Math;
import java.lang.Integer;
import java.lang.Float;
import org.bukkit.Location;
import org.bukkit.World;
import com.codekingdoms.nozzle.base.BaseSheep;

public class Sheep extends BaseSheep {
	
	public Location sheepLoc = new Location(world, 0, 70, 0);
	public void onSheared( String playerName ) {
		double newX = sheepLoc.getX() + Random.generateInteger(-15, 15);
		double newY = sheepLoc.getY();
		double newZ = sheepLoc.getZ() + Random.generateInteger(-15, 15);
		sheepLoc = new Location(world, newX, newY, newZ);
		Sheep s = new Sheep();
		s.spawn(world, sheepLoc);
	
	}
	
	
}
