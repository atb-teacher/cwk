PLAYER CODE:

public class Player extends BasePlayer {
	
	public void onChat( String message ) {
		
		if (message.equals("start")) {
			
			getGame().setLoc(getLocation());
			
		}
		
		if (message.equals("distance")) {
			
			double xDist = getGame().loc.getX() - getLocation().getX();
			double zDist = getGame().loc.getZ() - getLocation().getZ();
			double distance = Math.sqrt((xDist * xDist) + (zDist * zDist));
			sendMessage("Your distance is: " + distance);
			
		}
		
	
	}
	
	public void onJoin() {
		
		getGame().removeAllMobs();
	
	}
	
	public void onRightClick() {
		
		double xDist = getGame().loc.getX() - getLocation().getX();
		double zDist = getGame().loc.getZ() - getLocation().getZ();
		double distance = Math.sqrt((xDist * xDist) + (zDist * zDist));
		sendMessage("Your distance is: " + distance);
	
	}
	
	public void onPlaceBlock( Block block ) {
		
		Location blockLoc = block.getLocation();
		double blockX = blockLoc.getX();
		double blockZ = blockLoc.getZ();
		if (getGame().checkWin(blockX, blockZ)) {
			
			equipFullArmorSet(ArmorSet.DIAMOND);
	
		}
	}
}


GAME CODE:


public class Game extends BaseGame {
	
	public Location loc;
	
	boolean gameRunning = false;
	
	public void setLoc( Location inLoc ) {
		
		double newX = inLoc.getX() + Random.generateDouble(-100, 100);
		double newY = inLoc.getY();
		double newZ = inLoc.getZ() + Random.generateDouble(-100, 100);
		loc = new Location(world, newX, newY, newZ);
		gameRunning = true;
	
	}
	
	public boolean checkWin( double inX, double inZ ) {
		
		if (( inX == loc.getX() ) && ( inZ == loc.getZ() )) {
			
			return true;
			
		}
		
		else {
			return false;
		 }
	}
}
