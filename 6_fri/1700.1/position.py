class Position:
  def __init__(self, _x_pos, _y_pos):
    self.x_pos = _x_pos
    self.y_pos = _y_pos

  def print_position(self):
    print(
      f"at x={self.x_pos}, y={self.y_pos}"
    )

  def move_left(self):
    self.x_pos = self.x_pos - 10

my_pos = Position(100, 50)
my_pos.print_position()
my_pos.move_left()
my_pos.print_position()
