t = turtle.Turtle()

"""
t.penup()
t.goto(20, 100)
t.pendown()
for i in range(4): # this is a loop
    t.forward(100) # this will run 4 times
    t.left(90) # this will run 4 times
"""

# t.pendown()
# t.penup()

"""
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)
"""

"""
# make a square
for i in range(4): # this is a loop
    t.forward(100) # this will run 4 times
    t.left(90) # this will run 4 times


# make an equilateral triangle
for i in range(3):
    t.forward(100)
    t.left(120)
"""

"""
for i in range(36):
    for i in range(3):
        t.forward(100)
        t.left(120)
    t.left(10)
"""

"""
for i in range(36):
    t.forward(1)
    t.left(10)
t.penup()
t.goto(0, -100)
"""


# stretch goals
#   Make a function that toggles the pen
#   between up and down

