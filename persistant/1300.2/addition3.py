num1 = random.randint(1, 5)
num2 = random.randint(1, 5)

sprite = codesters.Sprite("alien1")
user_answer = sprite.ask(f"What is {num1} + {num2}")
sprite.say("You guessed "  + user_answer, 2)
user_answer = int(user_answer) # convert from str to int
correct_answer = num1 + num2
if user_answer == correct_answer:
        sprite.say("Correct!", 2)
