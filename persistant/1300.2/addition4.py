num1 = random.randint(1, 5)
num2 = random.randint(1, 5)

running = True

sprite = codesters.Sprite("alien1")

while running == True:
    user_answer = sprite.ask(f"What is {num1} + {num2}")
    # sprite.say("You guessed "  + user_answer, 2)
    user_answer = int(user_answer) # convert from str to int
    correct_answer = num1 + num2
    if user_answer == correct_answer:
        sprite.say("Correct!", 1)
        running = False
    else:
        sprite.say("Incorrect!", 1)
        running = True
