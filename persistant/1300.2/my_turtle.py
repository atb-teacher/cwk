t = turtle.Turtle()

# t.pendown()
# t.penup()

"""
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)
t.forward(100)
t.left(90)
"""

# make a square
for i in range(4): # this is a loop
    t.forward(100) # this will run 4 times
    t.left(90) # this will run 4 times
    
# make an equilateral triangle
for i in range(3):
    t.forward(100)
    t.left(120)
