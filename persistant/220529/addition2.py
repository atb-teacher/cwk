num1 = random.randint(1, 10)
num2 = random.randint(1, 10)

sprite = codesters.Sprite("alien1")
user_answer = sprite.ask(f"what is {num1} + {num2}?")
user_answer_int = int(user_answer)
if user_answer_int == num1 + num2:
    sprite.say("correct!")
else:
    sprite.say("incorrect!")
