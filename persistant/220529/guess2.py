lower = 10
higher = 100
wait_time = 2
random_number = random.randint(lower, higher)
print(random_number)

guessed_numbers = []

sprite = codesters.Sprite("alien1")
running = True
while running:
    user_answer = sprite.ask(f"Can you guess the number between {lower} and {higher}?")
    user_answer_int = int(user_answer)
    if user_answer_int in guessed_numbers:
        sprite.say(f"You already guessed {user_answer_int}!")
        stage.wait(wait_time)
    else:
        guessed_numbers.append(user_answer_int)
        print(guessed_numbers)
        if user_answer_int == random_number:
            sprite.say("correct!")
            running = False
        elif user_answer_int > random_number:
            sprite.say("too high!")
            stage.wait(wait_time)
        else:
            sprite.say("too low!")
            stage.wait(wait_time)
            
"""
Stretch goals:
  * Add "you already guessed that" comment
Advanced goals:
  * Make the alien say "warmer" or "colder"
Super Advanced goals:
  * Come up with a random number, make the computer guess
"""

