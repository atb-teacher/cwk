t = turtle.Turtle()

"""
# make a square
t.forward(50)
t.left(360 * (1/4))
t.forward(50)
t.left(360 * (1/4))
t.forward(50)
t.left(360 * (1/4))
t.forward(50)
t.left(360 * (1/4))
"""

# make a square
def make_square():
    for i in range(4):
        t.forward(50)
        t.left(90)

def make_triangle():
    for i in range(3):
        t.forward(50)
        t.right(120)

def make_circle():
    for i in range(360):
        t.forward(1)
        t.left(1)

"""
for i in range(360):
    t.forward(1)
    t.left(1)
    make_triangle()
"""

def make_shape(sides=4):
    # confusing
    interior_angles = (sides - 2) * 180
    angle = interior_angles / sides
    for i in range(sides):
        t.forward(5)
        t.right(180 - angle)

make_shape(100)


# print('\n'.join(dir(t)))
# dir lists everything out
"""
this is a comment
it goes on multiple lines
it is really a string
but we use it like a comment
"""

