random_number = random.randint(1, 10)
print(random_number)

sprite = codesters.Sprite("alien1")
running = True
while running:
    user_answer = sprite.ask(f"Can you guess the number?")
    user_answer_int = int(user_answer)
    if user_answer_int == random_number:
        sprite.say("correct!")
        running = False
    elif user_answer_int > random_number:
        sprite.say("too high!")
        stage.wait(1)
    else:
        sprite.say("too low!")
        stage.wait(1)
        
"""
Stretch goals:
  * Add "you already guessed that" comment
  * Add upper bound and lower bound for random number
"""
