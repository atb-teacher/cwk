t = turtle.Turtle()

"""
# make a square
t.forward(50)
t.left(360 * (1/4))
t.forward(50)
t.left(360 * (1/4))
t.forward(50)
t.left(360 * (1/4))
t.forward(50)
t.left(360 * (1/4))
"""

# make a square
for i in range(4):
    t.forward(50)
    t.left(90)

for i in range(3):
    t.forward(50)
    t.right(120)


# print('\n'.join(dir(t)))
# dir lists everything out
"""
this is a comment
it goes on multiple lines
it is really a string
but we use it like a comment
"""

