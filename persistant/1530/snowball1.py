sprite = codesters.Sprite("robot")

def create_snowball():
    snowball = codesters.Circle(0, 0, 10, "grey")
    x = stage.click_x() # asking the x-coord of the click
    y = stage.click_y() # asking the y-coord of the click
    x = x / 10 # slowing it down
    y = y / 10 # slowing it down
    snowball.set_x_speed(x)
    snowball.set_y_speed(y)
    

"""
def click():
    x = stage.click_x()
    y = stage.click_y()
    sprite.glide_to(x, y)
    # add other actions...
"""    
stage.event_click(create_snowball)
