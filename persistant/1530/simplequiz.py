correct_answers = ['a', 'b', 'c']
user_answers = []
for index in [0, 1, 2]:
    user_answers.append(input(f"What is the letter of the alphabet at position {index + 1}?"))
print("you guessed", user_answers)
score = 0
for index in [0, 1, 2]:
    if user_answers[index] == correct_answers[index]:
        score = score + 1
print(f"Your score was {score}")
