sprite = codesters.Sprite("robot", -100, 100)

def create_snowball():
    snowball = codesters.Circle(
        sprite.get_x(),
        sprite.get_y(), 
        10, "grey")
    x = stage.click_x() - sprite.get_x() # asking the x-coord of the click
    y = stage.click_y() - sprite.get_y()# asking the y-coord of the click
    x = x / 10 # slowing it down
    y = y / 10 # slowing it down
    snowball.set_x_speed(x)
    snowball.set_y_speed(y)
    

"""
def click():
    x = stage.click_x()
    y = stage.click_y()
    sprite.glide_to(x, y)
    # add other actions...
"""
def print_hi():
    print("hi there! you clicked at " + 
    f"{stage.click_x()}, {stage.click_y()}")

stage.event_click(create_snowball)
stage.disable_all_walls()
