sprite = codesters.Sprite("alien1")
sprite.set_size(0.5)
sprite.grounded = False
text = codesters.Text("grounded", -200, 200)
floor = codesters.Rectangle(0, -250, 500, 50, "darkred")
p1 = codesters.Rectangle(-100, -100, 100, 25, "darkred")
p2 = codesters.Rectangle(-100, 25, 100, 25, "darkred")
p3 = codesters.Rectangle(-100, 150, 100, 25, "darkred")

sprite.active_platform = None

def check_above(in_sprite, in_platform):
    if in_platform == None:
        return True
    x = in_sprite.get_x()
    left_side = in_platform.get_x() - (in_platform.get_width()/2)
    right_side = in_platform.get_x() + (in_platform.get_width()/2)
    if x > right_side or x < left_side:
        return False
    else:
        return True
    
def fall(sprite):
    if not check_above(sprite, sprite.active_platform):
        sprite.grounded = False
        sprite.set_gravity_on()

floor.set_gravity_off()
p1.set_gravity_off()
p2.set_gravity_off()
p3.set_gravity_off()

stage.set_gravity(10)

def left_key_water():
    sprite.move_left(20)
    fall(sprite)

stage.event_key("left", left_key_water)

def right_key_soda():
    sprite.move_right(20)
    fall(sprite)

stage.event_key("right", right_key_soda)

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "darkred":
        sprite.set_y_speed(0)
        if sprite.get_y() > hit_sprite.get_y():
            sprite.grounded = True
            text.set_text("grounded")
            sprite.set_gravity_off()
            sprite.active_platform = hit_sprite
            sprite.say("I am the sprite", 2)
            sprite.active_platform.say("I am under the sprite", 2)
        
sprite.event_collision(collision)

def space_bar():
    if sprite.grounded:
        sprite.jump(15)
        sprite.set_gravity_on()
        sprite.grounded = False
        text.set_text("not grounded")
        

stage.event_key("space", space_bar)
