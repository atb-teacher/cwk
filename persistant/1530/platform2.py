sprite = codesters.Sprite("alien1")
sprite.set_size(0.5)

floor = codesters.Rectangle(0, -250, 500, 50, "darkred")
floor.set_gravity_off()
stage.set_gravity(10)

def left_key_water():
    sprite.move_left(20)

stage.event_key("left", left_key_water)

def right_key_soda():
    sprite.move_right(20)

stage.event_key("right", right_key_soda)

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "darkred":
        sprite.set_gravity_off()
        sprite.set_y_speed(0)
        
sprite.event_collision(collision)

def space_bar():
    sprite.jump(5)
    sprite.set_gravity_on()
    # add other actions...
    
stage.event_key("space", space_bar)

