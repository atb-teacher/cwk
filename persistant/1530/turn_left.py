import time
directions = ['North', 'West', 'South', 'East']
direction_index = 0
while True:
    print(f"Facing {directions[direction_index % 4]}")
    time.sleep(1)
    direction_index += 1


    if direction_index == 4:
        direction_index = 0

