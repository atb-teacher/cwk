question_list = []
answer_list = []
correct_answer_list = []


question = codesters.Text("question", 0, 200)
a = codesters.Text("a", -100, 100)
b = codesters.Text("b", 100, 100)
c = codesters.Text("c", -100, -100)
d = codesters.Text("d", 100, -100)



question_list.append(
    "Who is the main character in the Legend of Zelda games?"
)
answer_list.append(
    [
        "Zelda",
        "Ganon",
        "Navi",
        "Link",
    ]
)

correct_answer_list.append("d")

question_list.append(
    "Which fruit is yellow?"
)
answer_list.append(
    [
        "Apple",
        "Banana",
        "Orange",
        "Strawberry",
    ]
)
correct_answer_list.append("b")

question_list.append(
    "Which minecraft ore looks like red specs?"
)
answer_list.append(
    [
        "Redstone",
        "Gold",
        "Emerald",
        "Diamond",
    ]
)
correct_answer_list.append("a")



print("question_list", question_list)
print("answer_list", answer_list)
print("correct_answer_list", correct_answer_list)


# a.set_text("Hello Codesters!")

question_session = 0
question.set_text(question_list[question_session])
a.set_text(answer_list[question_session][0])




