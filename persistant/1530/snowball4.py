# CREATE SPRITES

sprite = codesters.Sprite("robot", -200, -200)
enemy = codesters.Sprite("anemone", 150, 150)

# HANDLE GRAVITY

# stage.set_gravity(5)
sprite.set_gravity_off()
enemy.set_gravity_off()
enemy.set_y_speed(0)

# HANDLE WALLS

stage.disable_all_walls()

# HANDLE HEALTH

sprite.health = 10
enemy.health = 10

def create_snowball():
    """let's the bot throw snowballs"""
    snowball = codesters.Circle(
        sprite.get_x(),
        sprite.get_y(), 
        10, "white")
    snowball.set_outline_color("black")
    x = stage.click_x() - sprite.get_x() # asking the x-coord of the click
    y = stage.click_y() - sprite.get_y()# asking the y-coord of the click
    x = x / 10 # slowing it down
    y = y / 10 # slowing it down
    snowball.set_x_speed(x)
    snowball.set_y_speed(y)

def enemy_snowball():
    """let's the enemy throw snowballs"""
    enemy_snowball = codesters.Circle(
        enemy.get_x(),
        enemy.get_y(),
        10, "white")
    enemy_snowball.set_outline_color("black")
    enemy_snowball.set_x_speed(
        random.randint(-5, -1)    
    )
    enemy_snowball.set_y_speed(
        random.randint(-5, -1)    
    )

stage.event_click(create_snowball)

def collision(sprite, hit_sprite):
    """handle collision btw sprite and snowball"""
    my_var = hit_sprite.get_color() 
    if my_var == "red":
        sprite.say("Ouch!")
        
    if my_var == "green":
        sprite.say("Goal!")
        
    # add any other actions...
    
sprite.event_collision(collision)
enemy.event_collision(collision)

def announce_health(in_sprite):
    in_sprite.say(f"Health: {in_sprite.health}")

# GAME LOOP (WILL BE A WHILE LOOP SOON)

while True:
    enemy_snowball()
    stage.wait(1)
    announce_health(sprite)
    announce_health(enemy)
