question_list = []
answer_list = []
correct_answer_list = []


question = codesters.Text("question", 0, 200)
a = codesters.Text("a", -100, 100)
b = codesters.Text("b", -150, 150)
c = codesters.Text("c", -150, 150)
d = codesters.Text("d", -150, 150)



question_list.append(
    "Who is the main character in the Legend of Zelda games?"
)
answer_list.append(
    [
        "Zelda",
        "Ganon",
        "Navi",
        "Link",
    ]
)

# Same code above and below
# answer_list.append(["Zelda", "Ganon", "Navi", "Link",])

correct_answer_list.append("d")
print("question_list", question_list)
print("answer_list", answer_list)
print("correct_answer_list", correct_answer_list)


# a.set_text("Hello Codesters!")

question_session = 1
question.set_text(question_list[0])



