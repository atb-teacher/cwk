# CREATE SPRITES

sprite = codesters.Sprite("robot", -200, -200)
enemy = codesters.Sprite("anemone", 150, 150)

# HANDLE WALLS

stage.disable_all_walls()

# HANDLE HEALTH

sprite.health = 10
enemy.health = 10


def barrier_collision(barrier, snowball):
    my_var = snowball.get_color() 
    if my_var == "white":
        stage.remove_sprite(snowball)
        barrier.health = barrier.health - 1
        if barrier.health <= 0:
            stage.remove_sprite(barrier)
    
barriers = []
barrier1 = codesters.Rectangle(0, -50, 100, 25, "darkred")
barrier1.health = 5
barriers.append(barrier1)
barrier2 = codesters.Rectangle(-50, 0, 25, 100, "darkred")
barrier2.health = 5
barriers.append(barrier2)

barrier1.event_collision(barrier_collision)
barrier2.event_collision(barrier_collision)


def normalize(x_param, y_param):
    """Make the snowball go the same speed wherever you click on the map"""
    x = x_param / math.sqrt(x_param**2 + y_param**2)
    y = y_param / math.sqrt(x_param**2 + y_param**2)
    x = x * 5 # speeding it up
    y = y * 5 # speeding it up
    return x, y

def create_snowball():
    """let's the bot throw snowballs"""
    snowball = codesters.Circle(
        sprite.get_x(),
        sprite.get_y(),
        10, "white")
    snowball.set_outline_color("black")
    x = stage.click_x() - sprite.get_x() # asking the x-coord of the click
    y = stage.click_y() - sprite.get_y()# asking the y-coord of the click
    x, y = normalize(x, y)
    snowball.set_x_speed(x)
    snowball.set_y_speed(y)
    snowball.owner = "robot"

def enemy_snowball():
    """let's the enemy throw snowballs"""
    enemy_snowball = codesters.Circle(
        enemy.get_x(),
        enemy.get_y(),
        10, "white")
    enemy_snowball.set_outline_color("black")
    x = sprite.get_x() - enemy.get_x()
    y = sprite.get_y() - enemy.get_y()
    x, y = normalize(x, y)
    enemy_snowball.set_x_speed(x)
    enemy_snowball.set_y_speed(y)
    enemy_snowball.owner = "anemone"

stage.event_click(create_snowball)

def collision(in_sprite, snowball):
    """handle collision btw sprite and snowball"""
    if in_sprite.get_image_name() != snowball.owner:
        in_sprite.health = in_sprite.health - 1
        stage.remove_sprite(snowball)
        
sprite.event_collision(collision)
enemy.event_collision(collision)

def announce_health(in_sprite):
    in_sprite.say(f"Health: {in_sprite.health}")

def up_key(sprite):
    if sprite.get_y() < -50:
        sprite.move_up(20)
        
def right_key(sprite):
    if sprite.get_x() < -50:
        sprite.move_right(20)        

def down_key(sprite):
    sprite.move_down(20)
    
def left_key(sprite):
    sprite.move_left(20)
    
sprite.event_key("up", up_key)
sprite.event_key("right", right_key)
sprite.event_key("down", down_key)
sprite.event_key("left", left_key)

# GAME LOOP (WILL BE A WHILE LOOP SOON)

counter = 0
while True:
    counter += 1
    if counter >= 9:
        counter = 0
        enemy_snowball()
    stage.wait(.1)
    announce_health(sprite)
    announce_health(enemy)
    if sprite.health < 0 or enemy.health < 0:
        break 
    
# END OF THE GAME

stage.remove_sprite(sprite)
stage.remove_sprite(enemy)
end_text = codesters.Text("", 0, 0)
if sprite.health == enemy.health:
    end_text.set_text("Tie!")
elif sprite.health > enemy.health:
    end_text.set_text("You Won!!! :D")
else:
    end_text.set_text("You Lost!!! (ur bad) >:(")




