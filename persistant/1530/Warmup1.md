# Warmup #1 (Scratch)

1. Make a spaceship sprite

1. Make it so that the ship can move up and down, or make it so that it can move in all 4 directions

1. Make an enemy that flies around

1. Give the ship the ability to shoot the enemy

1. Make it so that the enemy disappears when it's hit by a laser shot

1. Use cloning to make a bunch of enemies

1. Give your ship health

1. Keep score

1. Use stage/broadcast to make an end screen
