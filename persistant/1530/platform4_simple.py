sprite = codesters.Sprite("alien1") # this creates the alien
sprite.set_size(0.5) 

floor = codesters.Rectangle(0, -150, 200, 50, "darkred")
floor.set_gravity_off()
stage.set_gravity(10)

def left_key_water():
    sprite.move_left(20)
    if sprite.get_x() < floor.get_x() - (floor.get_width()/2):
        sprite.set_gravity_on()

stage.event_key("left", left_key_water)

def right_key_soda():
    sprite.move_right(20)
    if sprite.get_x() > floor.get_x() + (floor.get_width()/2):
        sprite.set_gravity_on()

stage.event_key("right", right_key_soda)

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_color() 
    if my_var == "darkred":
        sprite.set_gravity_off()
        sprite.set_y_speed(0)
        
sprite.event_collision(collision) # whenever a sprite hits something
# run the "collision" set of instructions which will
# set gravity off and stop the up/down momentum

def space_bar():
    sprite.jump(5)
    sprite.set_gravity_on()
    
stage.event_key("space", space_bar)
# if the player presses the space bar,
# the player jumps with 5 power
# we turn on gravity, so that the player falls
