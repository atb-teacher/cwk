# CREATE SPRITES

sprite = codesters.Sprite("robot", -200, -200)
enemy = codesters.Sprite("anemone", 175, 175)

# HANDLE GRAVITY

# stage.set_gravity(5)
sprite.set_gravity_off()
enemy.set_gravity_off()
enemy.set_y_speed(0)

# HANDLE WALLS

stage.disable_all_walls()

def create_snowball():
    
    snowball = codesters.Circle(
        sprite.get_x(),
        sprite.get_y(), 
        10, "white") # changed
    snowball.set_outline_color("black") # new
    x = stage.click_x() - sprite.get_x() # asking the x-coord of the click
    y = stage.click_y() - sprite.get_y()# asking the y-coord of the click
    x = x / 10 # slowing it down
    y = y / 10 # slowing it down
    snowball.set_x_speed(x)
    snowball.set_y_speed(y)

# print_hi taught us about using stage.click_x() and stage.click_y()
# We could use it with this code:
#   stage.event_click(print_hi)
"""
def print_hi():
    sprite.say("hi there! you clicked " + 
    f"{stage.click_y() - sprite.get_y()} above me")
"""

stage.event_click(create_snowball)

# GAME LOOP (WILL BE A WHILE LOOP SOON)

for i in range(5):
    enemy_snowball = codesters.Circle(
        enemy.get_x(),
        enemy.get_y(),
        10, "white")
    enemy_snowball.set_outline_color("black")
    enemy_snowball.set_x_speed(-2)
    enemy_snowball.set_y_speed(-2)
    stage.wait(1)
