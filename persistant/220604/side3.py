# ========== SETUP STAGE ==========

stage.set_gravity(10)
stage.disable_all_walls()

# ========== SETUP HERO ==========

hero = codesters.Sprite("alien1")
hero.set_size(.5)
hero.set_position(-200, -150)
hero.grounded = False
hero.health = 5
hero.invincible = False

# ========== SETUP GROUNDED TEXT ==========

grounded_text = codesters.Text(
    f"Grounded: {hero.grounded}", -175, 200)
    
def refresh_text_grounded():
    grounded_text.set_text(f"Grounded: {hero.grounded}")
    
# ========== SETUP HEALTH TEXT ==========

health_text = codesters.Text(
    f"Health: {hero.health}", -175, 180)
    
def refresh_text_health():
    health_text.set_text(f"Health: {hero.health}")

# ========== SETUP FLOOR ==========

floor = codesters.Rectangle(
    0, # x pos
    -250, # y pos
    500, # width
    50, # height
    "darkred"
)
floor.set_gravity_off()

# ========== SETUP COLLISION ==========

def collision_starwars(sprite, hit_sprite):
    hit_color = hit_sprite.get_color() 
    if hit_color == "darkred":
        hero.grounded = True
        refresh_text_grounded()
        sprite.set_gravity_off()
        sprite.set_y_speed(0)
        sprite.set_y(hit_sprite.get_y() + \
        (hit_sprite.get_height()/2) + \
        (sprite.get_height()/2))
    if hit_color == "blue":
        if not hero.invincible:
            hero.invincible = True
            hero.health -= 1
            refresh_text_health()
            """
            for i in range(10):
                stage.wait(.1)
                hero.hide()
                stage.wait(.1)
                hero.show()
            """
            stage.wait(2)
            hero.invincible = False
    if hit_sprite.get_image_name() == "cat":
        hero.health += 2
        stage.remove_sprite(hit_sprite)
        refresh_text_health()
    
hero.event_collision(collision_starwars)

def collision_mando(sprite, hit_sprite):
    hit_color = hit_sprite.get_color() 
    if hit_color == "darkred":
        sprite.set_y_speed(sprite.get_y_speed() * -1)

# ========== SETUP MOTION ==========

def space_bar():
    if hero.grounded:
        hero.grounded = False
        hero.jump(15)
        hero.set_gravity_on()
        refresh_text_grounded()
    
stage.event_key("space", space_bar)


def drop():
    if not hero.grounded:
        hero.set_y_speed(-20)

stage.event_key("down", drop)

def right_key(sprite):
    if sprite.get_x() < -150:
        sprite.move_right(20)

hero.event_key("right", right_key)

def left_key(sprite):
    if sprite.get_x() > -250:
        sprite.move_left(20)

hero.event_key("left", left_key)



# ========== SETUP GAME LOOP ==========

# Game Loop
counter = 0
while True:
    counter = counter + 1
    stage.wait(.1)
    if counter % 8 == 0:
        obstacle = codesters.Rectangle(
            200, # x
            random.randint(-150, 150), # y
            25, # width
            50, # height
            "blue",
        )
        obstacle.set_x_speed(-5)
        # obstacle.set_gravity_off()
        obstacle.event_collision(collision_mando)
    if counter % 8 == 4:
        if random.randint(1, 5) == 5:
            cat = codesters.Sprite("cat", 400, 400)
            cat.set_size(.3)
            cat.go_to(200, random.randint(-150, 150))
            cat.set_x_speed(-5)
            cat.event_collision(collision_mando)
