# ========== SETUP STAGE ==========

stage.set_gravity(10)
stage.disable_all_walls()

# ========== SETUP HERO ==========

hero = codesters.Sprite("alien1")
hero.set_size(.5)
hero.set_position(-200, -150)
hero.grounded = False

# ========== SETUP GROUNDED TEXT ==========

grounded_text = codesters.Text(
    f"Grounded: {hero.grounded}", -175, 200)
    
def refresh_text_grounded():
    grounded_text.set_text(f"Grounded: {hero.grounded}")

# ========== SETUP FLOOR ==========

floor = codesters.Rectangle(
    0, # x pos
    -250, # y pos
    500, # width
    50, # height
    "darkred"
)
floor.set_gravity_off()

# ========== SETUP JUMP ==========

def collision_starwars(sprite, hit_sprite):
    hit_color = hit_sprite.get_color() 
    if hit_color == "darkred":
        hero.grounded = True
        refresh_text_grounded()
        sprite.set_gravity_off()
        sprite.set_y_speed(0)
        sprite.set_y(hit_sprite.get_y() + \
        (hit_sprite.get_height()/2) + \
        (sprite.get_height()/2))
    
    
hero.event_collision(collision_starwars)


def space_bar():
    hero.jump(15)
    hero.set_gravity_on()
    hero.grounded = False
    refresh_text_grounded()
    
stage.event_key("space", space_bar)

