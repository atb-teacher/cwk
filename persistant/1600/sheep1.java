package space.codekingdoms.alexteacher8.magicsword;

import com.codekingdoms.nozzle.base.BaseSheep;

public class Sheep extends BaseSheep {
	public static void onSheared( String playerName ) {
		if (getGame().score > -1) {
			getGame().score += 1;
			getGame().randomLocation(getLocation());
			Sheep s = new Sheep();
			s.spawn(world, getGame().randomLoc);
		}
	}
}
