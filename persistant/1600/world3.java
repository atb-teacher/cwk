
public class Game extends BaseGame {
	public Location randomLoc;
	public int score;
	
	public void randomLocation( Location inLocation ) {
		
		double newX = inLocation.getX() + 5;
		double newY = inLocation.getY() + 5;
		double newZ = inLocation.getZ() + 5;
		randomLoc = new Location(world, newX, newY, newZ);
		//return randomLoc;
	}
	
	public void start( Location location ) {
		
		score = 0;
		startTimer(60);
		randomLocation(location);
		Sheep s = new Sheep();
		s.spawn(world, randomLoc);
	
	}
	
	public void onTimerExpire() {
		
		broadcastMessage("Score: " + score);
		score = -1;
	
	}
	
	
}
