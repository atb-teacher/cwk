
public class Player extends BasePlayer {
	public Location randomLocation( Location inputLocation ) {
		double newX = inputLocation.getX() + Random.generateInteger(-15, 15);
		double newY = inputLocation.getY();
		double newZ = inputLocation.getZ() + Random.generateInteger(-15, 15);
		return new Location(world, newX, newY, newZ);
	
	}
	public void onRunCommand( String command, String[] args ) {
		if (command.equals("/debug")) {
			chat(randomLocation(new Location(world, 0, 0, 0)).getX());
		}
	}
}
