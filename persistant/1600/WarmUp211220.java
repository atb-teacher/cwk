ZOMBIE:

public class Zombie extends BaseZombie {
	public void onKilledByPlayer( String playerName ) { // changed
		Zombie z = new Zombie();
		z.spawn(world, getGame().randomLocation(getGame().randomLoc));
	}
}

GAME:

public class Game extends BaseGame {
	
	public Location randomLoc;
	
	public void start( Location inputLocation ) {
		
		Zombie z = new Zombie();
		z.spawn(world, randomLocation(inputLocation));
	
	}
	
	public Location randomLocation( Location inputLocation ) {
		
		double newX = inputLocation.getX() + Random.generateInteger(-15, 15);
		double newY = inputLocation.getY();
		double newZ = inputLocation.getZ() + Random.generateInteger(-15, 15);
		randomLoc = new Location(world, newX, newY, newZ);
		return randomLoc;
	
	}
	
	
}

PLAYER:

public class Player extends BasePlayer {
	public void onRunCommand( String command, String[] args ) {
		if (command.equals("/start")) {
			getGame().start(getLocation());
		}
	}
}
