import java.awt.*;
import java.awt.event.*;

public class EmployeeSalary
{
    public static void main(String args[]) {
        
        Frame f = new Frame();
        f.setTitle("Cool Project.");
        f.setSize(500, 500);
        f.setVisible(true);
        
        f.setBackground(Color.cyan);
        
        f.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });
        
        Button button1 = new Button("Submit");
        f.add(button1);
        Font myFont = new Font("Arial", Font.BOLD, 15);
        button1.setFont(myFont);
        button1.setBounds(215, 250, 80, 30);
        button1.setBackground(Color.pink);
        
        TextField number_input = new TextField("", 20);
        number_input.setBounds(200, 110, 200, 20);
        f.add(number_input);

        TextField salary = new TextField("", 40);
        salary.setBounds(150, 350, 200, 100);
        // setBounds(int x-coordinate, int y-coordinate, int width, int height)
        f.add(salary);
        
        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String n1 = number_input.getText();
                double number = Double.parseDouble(n1);
                double result = number * 2;
                salary.setText("That number doubled is" + result);
            }
        });

        //f.setLayout(new FlowLayout());
        f.setLayout(null);
    }
}
