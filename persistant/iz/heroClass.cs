using System;
using System.Collections.Generic;  

class Program {
  public static void Main (string[] args) {
    genericClass userClass = new Pyro(); // this can be improved on
    string userChoice = "necro";
    if (userChoice == "pyro") {
      userClass = new Pyro();
    }
    if (userChoice == "necro") {
      userClass = new Necro();
    }
    Console.WriteLine(userClass.fightLevel);
  }
}

class Pyro: genericClass {
  public Pyro() {
    className = "Pyro";
    fightLevel = 10;
    charmLevel = -5;
  }
}

class Necro: genericClass {
  public Necro() {
    className = "Necro";
    fightLevel = 5;
    charmLevel = -5;
  }
}

class genericClass {
  public string className;
  public int fightLevel;
  public int charmLevel;
}
