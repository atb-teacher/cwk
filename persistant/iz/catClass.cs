using System;

class Program {
  public static void Main (string[] args) {
    // string answer = "game";
    // char[] displayWord = [];
    Cat myCat = new Cat();
    Console.WriteLine(myCat.hunger);
    myCat.makeNoise();
    Cat myCat2 = new Cat();
    myCat2.hunger = 999;
    myCat2.makeNoise();
  }
}

class Cat {
  public int hunger;
  public Cat() {
    hunger = 0;
  }
  public void makeNoise() {
    if (hunger < 5) {
      Console.WriteLine("purr");
    } else {
      Console.WriteLine("meow");
    }
  }
}
