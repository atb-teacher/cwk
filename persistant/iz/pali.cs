using System;

class Program {
  public static void Main (string[] args) {
    Console.WriteLine ("Give me a word");
    string userInput = Console.ReadLine();
    char[] inputChars = userInput.ToCharArray();
    // converts to an array
    int middleFloored = (int)Math.Floor(inputChars.Length/2.0);
    // gets the middle number, rounded down
    for (int i=0; i<middleFloored; i++) {
      // loops from 0 to the middle number
      Console.WriteLine(inputChars[i] == inputChars[inputChars.Length - i - 1]); // prints out if both letters are the same
    }
  }
}
