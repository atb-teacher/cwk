import java.awt.*;
import java.awt.Graphics2D;
import java.awt.event.*;
import javax.swing.*;

public class TicTacToeGame extends JFrame {
	int ROWS = 3;
	int COLS = 3;

	int CELL_SIZE = 100;
	int CANVAS_WIDTH = CELL_SIZE * COLS;
	int CANVAS_HEIGHT = CELL_SIZE * ROWS;
	int GRID_WIDTH = 8;
	int GRID_WIDTH_HALF = GRID_WIDTH / 2;

	int CELL_PADDING = CELL_SIZE / 6;
	int SYMBOL_SIZE = CELL_SIZE - CELL_PADDING * 2;
	int SYMBOL_STROKE_WIDTH = 8;

	public enum Seed {
		EMPTY, CROSS, O,
	}
	private Seed currentPlayer;
	private Seed[][] board;
	
	public enum GameState {
		PLAYING, DRAW, CROSS_WON, O_WON,
	}

	private GameState currentState;
	private DrawCanvas canvas;

	class DrawCanvas extends JPanel {
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			setBackground(Color.WHITE);
			g.setColor(Color.LIGHT_GRAY);
			for (int row=1; row<ROWS; row++) {
				g.fillRoundRect(0, CELL_SIZE * row - GRID_WIDTH_HALF,
					CANVAS_WIDTH-1, GRID_WIDTH, GRID_WIDTH, GRID_WIDTH);
			}
			for (int col=1; col<COLS; col++) {
				g.fillRoundRect(CELL_SIZE * col - GRID_WIDTH_HALF, 0,
					GRID_WIDTH, CANVAS_HEIGHT-1, GRID_WIDTH, GRID_WIDTH);
			}


			// new stuff
			Graphics2D g2d = (Graphics2D)g;
			g2d.setStroke(new BasicStroke(
				SYMBOL_STROKE_WIDTH,
				BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_ROUND
			));
			for (int T = 0; T < ROWS; T++) {
				for (int H = 0; H < COLS; H++) {
					int x1 = H * CELL_SIZE + CELL_PADDING;
					int y1 = T * CELL_SIZE + CELL_PADDING;
					if (board[T][H] == Seed.CROSS) {
						g2d.setColor(Color.RED);
						int x2 = (H + 1) * CELL_SIZE - CELL_PADDING;
						int y2 = (T + 1) * CELL_SIZE - CELL_PADDING;
						g2d.drawLine(x1, y1, x2, y2);
						g2d.drawLine(x2, y1, x1, y2);
					}
					else if (board[T][H] == Seed.O) {
						g2d.setColor(Color.BLUE);
						g2d.drawOval(x1, y1, SYMBOL_SIZE, SYMBOL_SIZE);
					}
				}
			}
		}
	}

	public TicTacToeGame() {
		canvas = new DrawCanvas();
		canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(canvas, BorderLayout.CENTER);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setTitle("Tic Tac Toe");
		setVisible(true);
		
		board = new Seed[ROWS][COLS];
		initGame();
	}
	
	public void initGame() {
		for (int row=0; row<ROWS; row++) {
			for (int col=0; col < COLS; col++) {
				board[row][col] = Seed.EMPTY;
			}
		}
		currentState = GameState.PLAYING;
		currentPlayer = Seed.CROSS;
	}

	public static void main(String[] args) {
		new TicTacToeGame();
	}
}
