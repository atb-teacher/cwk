class Pet:
	def __init__(self, owner, name, size, sleepiness):
		self.name = name
		self.owner = owner
		self.size = size
		self.sleepiness = sleepiness

class HouseCat(Pet):
	def __init__(self, owner, name):
		super().__init__(owner=owner, name=name, size="medium", sleepiness=9)

kona = HouseCat(owner="Alex", name="Kona")

print(f"{kona.name} is a housecat owned by {kona.owner}.")
print(f"{kona.name} is a {kona.size}-sized pet.")
