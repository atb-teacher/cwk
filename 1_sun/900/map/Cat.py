# Cat.py

class Cat:
    def __init__(self, _name, _int=5, _hunger=10 ):
        self.name = _name
        self.intelligence = _int
        self.hunger = _hunger

    def feed(self):
        self.hunger += 1

    def make_noise(self):
        self.hunger -= 1
        if self.hunger > 5:
            print("purrr")
        else:
            print("meow")

    def __repr__(self):
        return "🐱"

my_cat = Cat( _name="Kona", _int=6 )

print(my_cat)
