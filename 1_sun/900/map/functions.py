def noisy_add(num1, num2):
    print(f"ADDING {num1} AND {num2}!")
    return num1 + num2

def bad_add(num1, num2):
    print(num1 + num2)

"""
print(
    noisy_add(
        noisy_add(2, 3), noisy_add(1, 5)
    )
)
"""
print(
    bad_add(2, 3) + (1 + 5)
)
