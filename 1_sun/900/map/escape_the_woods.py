# STATES

class Room:
    def __init__(
        self,
        main_description,
        n_description,
        s_description,
        e_description,
        w_description,
		n_room = None,
		s_room = None,
		e_room = None,
		w_room = None,
        ):
        self.main_description = main_description
        self.n_description = n_description
        self.s_description = s_description
        self.e_description = e_description
        self.w_description = w_description
        self.n_room = n_room
        self.s_room = s_room
        self.e_room = e_room
        self.w_room = w_room

class GenericWoods(Room):
	def __init__(self,
        	main_description = "You are surrounded by trees.",
        	n_description = "You see shaded trees, along with a pathway.",
        	s_description = "You see dark-green trees, and also a pathway.",
        	e_description = "A pathway surrounded by trees. You hear animal noises",
        	w_description = "Trees and a pathway. You hear nothing.",
	):
		super().__init__(
			main_description = main_description,
        		n_description = n_description,
        		s_description = s_description,
        		e_description = e_description,
        		w_description = w_description,
				n_room = self,
				s_room = self,
				e_room = self,
				w_room = self,
		)
	
	def set_room(self, direction, room):
		setattr(self, f"{direction}_room", room)	

	def description_add(self, direction, addendum):
		setattr(self, f"{direction}_description",
			getattr(self, f"{direction}_description") + addendum
		)

	def __repr__(self):
		return self.main_description
				
		
root_woods = GenericWoods(
	n_description = "You see a shimmer of light up north, along a pathway"
)		

woods_n = GenericWoods(
    main_description = "Looks like you made some progress",
    e_description = "A pathway surrounded by trees, with a shimmering light"
)


root_woods.set_room('n', woods_n)
root_woods.set_room('s', root_woods)
root_woods.set_room('e', root_woods)
root_woods.set_room('w', root_woods)


GAME_RUNNING = True

active_room = root_woods

# GAME LOOP

def ask_go():
    user_response = input("go this direction? ").lower()
    return user_response in ['y', 'yes']

while GAME_RUNNING:
    print(active_room)
    print("What do you want to do?")
    user_choice = input(": ")
    if user_choice.lower() in ('h', 'help'):
        print('look (n)orth, (s)outh, (e)ast, (w)est')
        print('(q) to quit')
    if user_choice.partition(" ")[0].lower() in ["l", "look"]:
        direction = user_choice.partition(" ")[2]

        if direction in ['n', 'north']:
            print(active_room.n_description)
            if ask_go():
                if active_room.n_room is None:
                    print("Cannot go that way")
                else:
                    active_room = active_room.n_room

        elif direction in ['s', 'south']:
            print(active_room.s_description)
            if ask_go():
                if active_room.s_room is None:
                    print("Cannot go that way")
                else:
                    active_room = active_room.s_room

        elif direction in ['e', 'east']:
            print(active_room.e_description)
            if ask_go():
                if active_room.e_room is None:
                    print("Cannot go that way")
                else:
                    active_room = active_room.e_room

        elif direction in ['w', 'west']:
            print(active_room.w_description)
            if ask_go():
                if active_room.w_room is None:
                    print("Cannot go that way")
                else:
                    active_room = active_room.w_room

    if user_choice.lower() in ('q', 'quit'):
        GAME_RUNNING = False

