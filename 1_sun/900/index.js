//var, let, and const.

//OBJECTS
// red square
var player;
var playerSize = 10;

// pillars / obstacles
var obstacles;

// floor
var canvas = document.getElementById("myCanvas-friedchicken");
var ctx = canvas.getContext("2d");
var floor = 20;
console.log(floor);

// score
var scoreText = document.getElementById("score");
var scoreSpan = document.getElementById("score-number");
var score;

// prompt text

//NON-OBJECT GLOBALS
var gameOver = false; //True or false
var scroll // movement to left

//FUNCTIONS
function start() {
	score = 0;
	player = {
		x_pos: canvas.width/4,
		y_pos: floor + playerSize,
		y_vel: 0,
	}
	drawBackground();
	drawGround();
	drawPlayer();
}

function drawBackground() {
	ctx.beginPath();
	ctx.rect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle = "skyblue";
	ctx.fill();
}

function drawGround() {
	ctx.beginPath();
	ctx.rect(0, canvas.height, canvas.width, floor * -1);
	ctx.fillStyle = "black";
	ctx.fill();
}

function drawPlayer() {
	console.log(player.x_pos);
	console.log(player.y_pos);
	ctx.beginPath();
	ctx.rect(player.x_pos, canvas.height - player.y_pos, playerSize, playerSize);
	ctx.fillStyle = "red";
	ctx.fill();
}

function updateState() {
	pass;
}


function keyDownHandler(e) {
	if (e.keyCode == 65) {
		if (gameOver == true) {
			start();
			gameOver = false; // start the game
		} else {  
			player.y_vel = -4;
		}
	}
}

document.addEventListener("keydown", keyDownHandler, false);

function frameUpdate() {
	if (gameOver == false) {
		updateState();
		drawPlayer();
		requestAnimationFrame(frameUpdate);
	}
}

start();
requestAnimationFrame(frameUpdate);




/*
 * setInterval example
function incrementMyCounter() {
	myCounter++;
	if (myCounter%100 == 0) {
		console.log(myCounter);
	}
}

setInterval(incrementMyCounter, 10);
*/
/*
let myCounter = 0;

function incrementMyCounter() {
	myCounter++;
	if (myCounter%100 == 0) {
		console.log(myCounter);
	}
	requestAnimationFrame(incrementMyCounter);
}

requestAnimationFrame(incrementMyCounter);
*/
