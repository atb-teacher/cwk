# Web Scraping Plan

## Types

* HTML (not needing js)
  * Wikipedia, Amazon, etc.
  * Can be parsed with BeautifulSoup library or regex
* JSON API
  * reddit, twitter
  * can be parsed with JSON library or regex
* data stored in csv or similar
  * govt data, example data off of gitlab
  * can be parsed with csv library or regex
* HTML/JS
  * let's not do this, I don't want to learn Selenium

## Analyzing the data
  * Can be shown in a graph with the pandas library
