var canvas = document.querySelector("#my-canvas-endgame");
var ctx = canvas.getContext("2d");
var turnText = document.querySelector("#turnText");
/*
 * Goals:
 * make it so you can't click on the same square twice
 *
 */

const ROWS = 3;
const COLS = 3;
const LINE_THICKNESS = 20;
const CELL_WIDTH = canvas.width / COLS;
const CELL_HEIGHT = canvas.height / ROWS;
const PADDING = (CELL_WIDTH + CELL_HEIGHT)/10;

var playerTurn = {
	O: 0,
	CROSS: 1,
	EMPTY: 2,
};

var playerArr = ["O", "X"];

const board = [];

for (let i=0; i<ROWS; i++) {
	let newRow = [];
	for (let j=0; j<COLS; j++) {
		newRow.push(playerTurn.EMPTY);
	}
	board.push(newRow);
}

var gameStates = {
	PLAYING: 0,
	TIE: 1,
	CROSS_WON: 2,
	O_WON: 3,
}

var gameState = gameStates.PLAYING;

console.log(board);

var activePlayer = playerTurn.CROSS;

function drawGrid() {
	// DRAWING ROW SEPARATORS
	for (let i=1; i< ROWS; i++) {
		let rowHeight = (CELL_HEIGHT * i) - (LINE_THICKNESS / 2);
		ctx.beginPath();
		ctx.rect(
			0, // starting x pos
			rowHeight, // starting y pos
			canvas.width, // width
			LINE_THICKNESS, // height
		);
		ctx.fillStyle = "skyblue";
		ctx.fill();
	}
	// DRAWING COL SEPARATORS
	for (let i=1; i< COLS; i++) {
		let colWidth = (CELL_WIDTH * i) - (LINE_THICKNESS / 2);
		ctx.beginPath();
		ctx.rect(
			colWidth, // starting x pos
			0, // starting y pos
			LINE_THICKNESS, // width
			canvas.height, // height
		);
		ctx.fillStyle = "skyblue";
		ctx.fill();
	}

};

function genPlaces() {
	for (let i=0; i<ROWS; i++) {
		for (let j=0; j<COLS; j++) {
			yield [i, j];
		}
	}
}

function foundEmptyCell(inBoard) {
	let foundEmpty = false;
	let placeGen = genPlaces();
	let nextPlace = placeGen.next().value;
	while (foundEmpty === false && nextPlace != undefined) {
		if (inBoard[nextPlace[0]][nextPlace[1]] == playerTurn.EMPTY) {
			foundEmpty = true;
		}
		nextPlace = placeGen.next().value;
	}
	
	return foundEmpty
}

function foundEmptyCell(inBoard) {
	let foundEmpty = false;
	for (let i=0; i<ROWS; i++) {
		for (let j=0; j<COLS; j++) {
			if (inBoard[i][j] === playerTurn.EMPTY) {
				foundEmpty = true;
			}
		}
	}
	return foundEmpty
}

canvas.addEventListener("click", function(event) {
	//console.log(`x: ${event.x}, y: ${event.y}`);
	//console.log(event);
	let foundEmpty = foundEmptyCell(board);
	if (gameState === gameStates.PLAYING && foundEmpty) {
		let x_pos = event.x - canvas.offsetLeft;
		let y_pos = event.y - canvas.offsetTop;
		let col = Math.floor(x_pos / CELL_WIDTH); 
		let row = Math.floor(y_pos / CELL_HEIGHT);
		console.log(`col: ${col}`);
		console.log(`row: ${row}`);
		if (board[row][col] === playerTurn.EMPTY) {
			board[row][col] = activePlayer;
			console.log(board);
			if (checkWinBoard(board, activePlayer)) {
				turnText.innerText = `${playerArr[activePlayer]} won!`;
				if (activePlayer == playerTurn.CROSS) {
					gameState = gameStates.CROSS_WON;
				} else {
					gameState = gameStates.O_WON;
				}
			}
			
			if (activePlayer === playerTurn.CROSS) {
				drawX(col, row);
				activePlayer = playerTurn.O;
			}
			else {
				drawY(col, row);
				activePlayer = playerTurn.CROSS;
			}
			if (gameState == gameStates.PLAYING) {
				turnText.innerText = `${playerArr[activePlayer]}'s turn!`;
			}
		}
	} else {
		// runs if you click but the gameState is not gameStates.PLAYING
		for (let i=0; i<ROWS; i++) {
			for (let j=0; j<COLS; j++) {
				board[i][j] = playerTurn.EMPTY;
			}
		}
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		drawGrid();
		activePlayer = playerTurn.CROSS;
		gameState = gameStates.PLAYING;
		turnText.innerText = `${playerArr[activePlayer]}'s turn!`;
	}
});

function drawX(col, row) {
	ctx.beginPath();
	ctx.lineWidth = 15;
	ctx.strokeStyle = "red";
	ctx.moveTo(
		(col * CELL_WIDTH) + PADDING, // x position
		(row * CELL_HEIGHT) + PADDING, // y position
	);
	ctx.lineTo(
		((col + 1) * CELL_WIDTH) - PADDING, // x position
		((row + 1) * CELL_HEIGHT) - PADDING, // y position
	);
	ctx.moveTo(
		(col * CELL_WIDTH) + PADDING, // x position
		((row + 1) * CELL_HEIGHT) - PADDING, // y position
	);
	ctx.lineTo(
		((col + 1) * CELL_WIDTH) - PADDING, // x position
		(row * CELL_HEIGHT) + PADDING, // y position
	);
	ctx.stroke();
	/*
	ctx.beginPath();
	ctx.moveTo(
		0, 0
	);
	ctx.lineTo(
		100, 100
	);
	ctx.stroke();
	*/
};

function drawY(col, row) {
	ctx.beginPath();
	ctx.lineWidth = 15;
	ctx.strokeStyle = "blue";
	ctx.arc(
		(col * CELL_WIDTH) + (CELL_WIDTH/2),
		(row * CELL_HEIGHT) + (CELL_HEIGHT/2),
		(CELL_WIDTH - (PADDING * 2))/2, // width
		0, // starting angle
		2 * Math.PI, // ending angle
		false,
	)
	ctx.stroke();
};

function checkTie() {
	let foundEmpty = false;	
	for (let i=0; i<ROWS; i++) {
		for (let j=0; j<COLS; j++) {
			if (board[i][j] == playerTurn.EMPTY) {
				foundEmpty = true;
			}
		}
	}
	return !foundEmpty;
}

function checkWin(c) {
	if (c.inBoard[c.yPos][c.xPos] != c.player) {
		return false;
	}
	else {
		if (c.counter == c.winAmount) {
			return true;
		}
		else return checkWin({
			inBoard: c.inBoard,
			xPos: c.xPos + c.xDelta,
			yPos: c.yPos + c.yDelta,
			counter: c.counter + 1,
			player: c.player,
			winAmount: c.winAmount,
			xDelta: c.xDelta,
			yDelta: c.yDelta,
		})
	}
	

}

function checkWinBoard(board, activePlayer) {
	foundWinner = false;

	// check columns
	for (let startX=0; startX < COLS; startX++) {
		if (checkWin({
			inBoard: board,
			xPos: startX, // change
			yPos: 0,
			counter: 1,
			player: activePlayer,
			winAmount: 3,
			xDelta: 0,
			yDelta: 1,
		})) {
			foundWinner = true;
			break;
		}
	}
	if (foundWinner) {
		return foundWinner;
	}

	// check rows
	for (let startY=0; startY < ROWS; startY++) {
		if (checkWin({
			inBoard: board,
			yPos: startY,
			xPos: 0,
			counter: 1,
			player: activePlayer,
			winAmount: 3,
			xDelta: 1,
			yDelta: 0,

		})) {
			foundWinner = true;
			break;
		}
	}
	if (foundWinner) {
		return foundWinner;
	}
	
	// check top left to botom right
	if (checkWin({
			inBoard: board,
			yPos: 0,
			xPos: 0,
			counter: 1,
			player: activePlayer,
			winAmount: 3,
			xDelta: 1,
			yDelta: 1,

		})) {
		foundWinner = true;
	}

	// check top right to bottom left
	if (checkWin({
			inBoard: board,
			yPos: 0,
			xPos: 2,
			counter: 1,
			player: activePlayer,
			winAmount: 3,
			xDelta: -1,
			yDelta: 1,

		})) {
		foundWinner = true;
	}
	return foundWinner;
}

/*
 *
 * board[2][2] = playerTurn.O
 *
for (let i=0; i<ROWS; i++) {
	let newRow = [];
	for (let j=0; j<COLS; j++) {
		newRow.push(playerTurn.EMPTY);
	}
	board.push(newRow);
}
*/

/*
 *
 *
function draw() {
  var canvas = document.getElementById('canvas');
  if (canvas.getContext) {
	      var ctx = canvas.getContext('2d');

	      ctx.beginPath();
	      ctx.moveTo(75, 50);
	      ctx.lineTo(100, 75);
	      ctx.lineTo(100, 25);
	      cfter the game is over, clicking resets the board
	      tx.fill();
	    }
}

*/
// https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes
drawGrid();


/*
 *Goals:
 make it display draws
 */
