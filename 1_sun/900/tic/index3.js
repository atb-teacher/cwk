canvas = document.querySelector("#my-canvas-endgame");
var ctx = canvas.getContext("2d");

const ROWS = 3;
const COLS = 3;
const LINE_THICKNESS = 20;
const CELL_WIDTH = canvas.width / COLS;
const CELL_HEIGHT = canvas.width / ROWS;
const PADDING = (CELL_WIDTH + CELL_HEIGHT)/10

const board = [];

var playerTurn = {
  O: 0,
  CROSS: 1, 
  EMPTY: 2,
};

for (let i=0; i<ROWS; i++){
  let newRow = [];
  for (let j=0; j<COLS; j++){
    newRow.push(playerTurn.EMPTY);
  }
  board.push(newRow);
}
console.log(board)

var activePlayer = playerTurn.CROSS;

function drawGrid() {
  console.log("a")
	// DRAWING ROW SEPARATORS
	for (let i=1; i< ROWS; i++) {
		let rowHeight = (CELL_HEIGHT * i) - (LINE_THICKNESS / 2);
		ctx.beginPath();
		ctx.rect(
			0, // starting x pos
			rowHeight, // starting y pos
			canvas.width, // width
			LINE_THICKNESS, // height
		);
		ctx.fillStyle = "skyblue";
		ctx.fill();
	}
	// DRAWING COL SEPARATORS
	for (let i=1; i< COLS; i++) {
		let colWidth = (CELL_WIDTH * i) - (LINE_THICKNESS / 2);
		ctx.beginPath();
		ctx.rect(
			colWidth, // starting x pos
			0, // starting y pos
			LINE_THICKNESS, // width
			canvas.height, // height
		);
		ctx.fillStyle = "skyblue";
		ctx.fill();
	}

};

canvas.addEventListener("click", function(event) {
	//console.log(`x: ${event.x}, y: ${event.y}`);
	//console.log(event);
	let x_pos = event.x - canvas.offsetLeft;
	let y_pos = event.y - canvas.offsetTop;
	let col = Math.floor(x_pos / CELL_WIDTH); 
	let row = Math.floor(y_pos / CELL_HEIGHT);
	console.log(`col: ${col}`);
	console.log(`row: ${row}`);
  if (board[row][col] == playerTurn.EMPTY) {
    board[row][col] = activePlayer;
    console.log(board);
    checkWinBoard(board, activePlayer)
      //console.log('Did ${activePlayer} win? ${playerWon}');
    
      if (activePlayer === playerTurn.CROSS){
        drawX(col, row);
        activePlayer = playerTurn.O;
      }
    else {
      drawY(col, row);
      activePlayer = playerTurn.CROSS;
    }
  }
});

function drawX(col, row) {
	ctx.beginPath();
  ctx.lineWidth = 15;
  ctx.strokeStyle = "red";
	ctx.moveTo(
		(col * CELL_WIDTH) + PADDING, // x position
		(row * CELL_HEIGHT) + PADDING, // y position
	);
	ctx.lineTo(
		((col + 1) * CELL_WIDTH) - PADDING, // x position
		((row + 1) * CELL_HEIGHT) - PADDING, // x position
	);
  
  ctx.moveTo(
    ((col * 1) * CELL_WIDTH) + PADDING, // x position
		((row + 1) * CELL_HEIGHT) - PADDING, // x position
	);
  
	ctx.lineTo(
		((col + 1) * CELL_WIDTH) - PADDING, // x position
		((row * 1) * CELL_HEIGHT) + PADDING, // x position
	);
	ctx.stroke();
};


function drawY(col, row){
  console.log((CELL_WIDTH - (PADDING * 2))/2)
  ctx.beginPath();
  ctx.lineWidth = 15;
  ctx.strokeStyle = "blue";
  ctx.arc(
    (col * CELL_WIDTH) + (CELL_WIDTH/2),
    (row * CELL_HEIGHT) + (CELL_HEIGHT/2),
    (CELL_WIDTH - (PADDING * 2))/2, // width
    0, // starting angle
    2 * Math.PI, // ending angle
    true,
  )
  ctx.stroke();
};

function checkTie() {
  let foundEmpty = false;
  for (let i=0; i<board.lenght; i++){
      for (let j=0; j<board[9].length; j++){
              if (board[i][j] == playerTurn.EMPTY){
                foundEmpty = true;
              }
      }
  }
  return !foundEmpty;
  
}

function checkWin(c){
	console.log(c);
  if (c.inBoard[c.xPos][c.yPos] != c.player){
    return false;
  }
  else {
    if (c.counter == c.winAmount){
      return true;
    }
    else return checkWin({
      inBoard: c.inBoard,
      xPos: c.xPos + c.xDelta,
      yPos: c.yPos + c.yDelta,
      counter: c.counter + 1,
      player: c.player,
      winAmount: c.winAmount,
      xDelta: c.xDelta,
      yDelta: c.yDelta,
    })
  }
}

function checkWinBoard(board, activePlayer) {
  let foundWinner = false;
  for (let startX=0; startX < COLS; startX++) {
    if (checkWin({
      inBoard: board,
      xPos: startX,
      yPos: 0,
      counter: 1,
      player: activePlayer,
      winAmount: 3,
      xDelta: 0,
      yDelta: 1,
    })){
      foundWinner = true;
      break;
    }
  }
  console.log(`Did ${activePlayer} win? ${foundWinner}`);
}

drawGrid()
