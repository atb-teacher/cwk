person = codesters.Sprite("person1")
finished = False
while not finished:
    user_answer = person.ask("What is 2+2? ")
    if user_answer == "4":
        person.say("Correct!")
        finished = True
    else:
        person.say("Wrong!")
