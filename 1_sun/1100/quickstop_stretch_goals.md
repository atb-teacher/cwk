# Quickstop Stretch Goals
* Make an end screen if they go into the negatives
* Make a ball that slows down the cat, and one that speeds up the cat
* Let the user choose difficulty at the beginning by pushing an "easy" or "difficult" button
* One ball gives a 20 second "multiplier" bonus


## Advanced goals
* Make a game with two teleporting cats