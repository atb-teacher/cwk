# modulus_example.py
import time

counter = 0
while True:
  print(counter)
  time.sleep(.2)
  if counter % 5 == 0:
    print("I often speak")
  if counter % 17 == 0:
    print("I rarely speak")
  counter = counter + 1
