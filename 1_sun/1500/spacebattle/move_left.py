class Sprite:
  def __init__(self, _x_pos, _y_pos):
    self.x_pos = _x_pos
    self.y_pos = _y_pos

  def move_left(self, _x_change):
    self.x_pos = self.x_pos - _x_change

  def print_position(self):
    print(
      "This sprite is at",
      self.x_pos,
      self.y_pos
    )

s1 = Sprite(_x_pos=100, _y_pos=0)
s1.print_position()
for letter in "abc":
  s1.move_left(_x_change=20)
s1.print_position()
