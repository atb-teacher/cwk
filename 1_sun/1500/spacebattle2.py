# ===== SETUP STAGE =====

stage.set_background("space4")
stage.disable_all_walls()

# ===== SETUP SPRITES =====

ufo = codesters.Sprite("ufo", 500, 500)
ufo.set_size(.3)

ufo.go_to(
    200, # x position 
    0, # y position
)

shuttle = codesters.Sprite("spaceshuttle", 500, 500)
shuttle.set_size(.3)
shuttle.go_to(
    -200, # x position 
    0, # y position
)

# ===== SETUP MOVEMENT =====

def left_key_jurassic(sprite):
    sprite.move_left(20)
    sprite.set_rotation(0)

ufo.event_key("left", left_key_jurassic)

def right_key_spiderman(sprite):
    sprite.move_right(20)
    sprite.set_rotation(180)

ufo.event_key("right", right_key_spiderman)


def up_key_avengers(sprite):
    sprite.move_up(20)
    sprite.set_rotation(270)

ufo.event_key("up", up_key_avengers)


def down_key_strangerthings(sprite):
    sprite.move_down(20)
    sprite.set_rotation(90)

ufo.event_key("down", down_key_strangerthings)

boundary = 175
for i in range(50):
    obstacle = codesters.Rectangle(
        random.randint(boundary * -1, boundary * 1), # x_pos
        random.randint(boundary * -1, boundary * 1), # y_pos
        random.randint(10, 30), # width
        random.randint(10, 30), # height
        "darkred"
    )

while True:
    # temp code
    ufo.say(
        ufo.get_rotation()    
    )
    # end temp code
    stage.wait(2)
    green = codesters.Sprite("green_laser_34a", 500, 500)
    green.set_size(.3)
    green.go_to(
        ufo.get_x(),
        ufo.get_y()
    )
    if ufo.get_rotation() == 0:
        green.set_y_speed(-5)
    elif ufo.get_rotation() == 180:
        green.set_y_speed(5)
    elif ufo.get_rotation() == 90:
        green.set_rotation(90)
        green.set_x_speed(5)


