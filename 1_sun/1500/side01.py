stage.set_gravity(10)

hero = codesters.Sprite("alien1_masked", -200, -150)
hero.set_size(.5)
hero.grounded = False
hero.jumps = 2 # new
hero.say(hero.jumps) # new

grounded_text = codesters.Text(
    f"Grounded: {hero.grounded}",
    -160, # x
    200 # y
)

def update_grounded_text():
    grounded_text.set_text(f"Grounded: {hero.grounded}")
 
floor = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "red", # color
)

def collision_minecraft(_hero, _rect):
    rect_color = _rect.get_color() 
    if rect_color == "red":
        hero.jumps = 2 # new
        hero.say(hero.jumps) # new
        _hero.set_gravity_off()
        _hero.set_y_speed(0)
        _hero.grounded = True
        update_grounded_text()
    
hero.event_collision(collision_minecraft)

def space_bar_nether():
    if hero.jumps > 0: # changed
        hero.jumps -= 1 # new
        hero.say(hero.jumps)
        hero.jump(15)
        hero.grounded = False
        update_grounded_text()
        hero.set_gravity_on()

    
stage.event_key("space", space_bar_nether)

def right_key():
    if hero.get_x() < -100:
        hero.move_right(20)

stage.event_key("right", right_key)

def left_key():
    if hero.get_x() > -225:
        hero.move_left(20)
        
stage.event_key("left", left_key)

def down_key():
    if hero.grounded == False:
        hero.set_y_speed(-15)

stage.event_key("down", down_key)




