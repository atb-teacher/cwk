unsorted_nums = list(range(10))

random.shuffle(unsorted_nums)

print(unsorted_nums)

# prints something like [6, 1, 7, 3, 4, 9, 0, 2, 8, 5]



smallest_num_index = 0
smallest_num = unsorted_nums[0]

for i, current_num in enumerate(unsorted_nums):
    if current_num < smallest_num:
        smallest_num = current_num
        smallest_num_index = i

print(f"The smallest num is {smallest_num} at  index {smallest_num_index}")

'''
goal #1: find the smallest number in the list

'''
