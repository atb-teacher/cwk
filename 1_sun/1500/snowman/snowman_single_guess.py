# snowman_single_guess.py

answer = "television"

lines = ["_"] * len(answer)

print(lines)

guessed_letter = "e"

for index, letter in enumerate(answer):
  if letter == guessed_letter:
    lines[index] = guessed_letter

print(lines)

guessed_letter = "o"

for index, letter in enumerate(answer):
  if letter == guessed_letter:
    lines[index] = guessed_letter

print(lines)
