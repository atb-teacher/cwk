# =============== MAKE LIST OF WORDS ===============

words = """
television
roblox
tiger
Pneumonoultramicroscopicsilicovolcanoconiosis
desktop
""".strip().split('\n')
print(words)

# ignore .strip()
# .split is going to create a list
# every line in the string will be an item in the list

# =============== CHOOSE A WORD ===============

answer = random.choice(words) # desktop
answer = "desktop"

# ========== MAKE UNDERSCORE REPRESENTATION ==========

repr = ["_"] * len(answer) # ["_", "_", "_", "_", "_", "_", "_"]

repr_string = " ".join(repr) # "_ _ _ _ _ _ _"

# ========== MAKE WRONG LETTERS ==========

wrong_list = []

wrong_string = "Wrong:"

wrong_visual = codesters.Text(
    wrong_string,
    75, # x
    225, # y
)

# ========== CREATE CODESTERS TEXT FOR REP ==========

lines = codesters.Text(
    repr_string, # string
    -100, # x
    100, # y
)

# ========== CREATE SNOWMAN STEPS ==========

snowman_phase = 1

def draw_snowman():
    global snowman_phase
    
    if snowman_phase == 1:
        snowman_head = codesters.Circle(
            175, # x
            150, # y
            75, # diameter
            "white", # color
        )
        snowman_head.set_outline_color("black")
        snowman_head.set_line_thickness(5)
        
    if snowman_phase == 2:
        snowman_body = codesters.Circle(
            175, # x
            80, # y
            100, # diameter
            "white", # color
        )
        snowman_body.set_outline_color("black")
        snowman_body.set_line_thickness(5)
        snowman_body.move_to_back()
    
    if snowman_phase == 3:
        snowman_legs = codesters.Circle(
            175, # x
            0, # y
            125, # diameter
            "white", # color
        )
        snowman_legs.set_outline_color("black")
        snowman_legs.set_line_thickness(5)
        snowman_legs.move_to_back()
    
    if snowman_phase == 4:
        hat_base = codesters.Ellipse(
            175, # x
            190, # y
            100, # width
            50, # height
            "black",
        )
        hat_main = codesters.Rectangle(
            175, # x
            220, # y
            60, # width
            80, # height
            "black", # color
        )
        
        

    snowman_phase += 1

while "_" in repr:

    guess = stage.ask("Guess a letter:")
    
    found_guessed_letter = False
    
    # for index, letter in enumerate(answer):
    for index, letter in enumerate(answer):
        if letter == guess:
            found_guessed_letter = True
            repr[index] = guess
            repr_string = " ".join(repr)
            lines.set_text(repr_string)

    if (found_guessed_letter == False 
    and guess not in wrong_list):
        wrong_list.append(guess)
        wrong_string = "Wrong:\n" + ",".join(wrong_list)
        wrong_visual.set_text(wrong_string)
        draw_snowman()
        

