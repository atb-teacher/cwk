# Spacebattle Goals

* Make it so that the ship can't pass through a red block
* Make it so that a laser can't pass through a red block
* Make it so that both ships can move
* Make it so that a ship loses health when hit by a laser
