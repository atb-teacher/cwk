stage.disable_all_walls()

good = codesters.Sprite(
    "snowman",
    -160,
    -150,
)

good.set_size(.5)

evil = codesters.Sprite(
    "alien5",
    160,
    150,
)

evil.set_size(.5)

def click():
    x = stage.click_x()
    y = stage.click_y()
    print(x, y)
    new_snowball = codesters.Circle(
        # -160,
        # -150,
        good.get_x(), # x pos
        good.get_y(), # y pos
        15,# diameter
        "white",# color
    )
    new_snowball.set_outline_color("black")
    new_snowball.set_x_speed(
        (x - good.get_x()) * .1    
    )
    new_snowball.set_y_speed(
        (y - good.get_y()) * .1
    )
    

stage.event_click(click)

def up_key(sprite):
    sprite.move_up(20)

good.event_key("up", up_key)
