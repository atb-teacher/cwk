sprite = codesters.Sprite("earth")
num1 = random.randint(1, 5)
num2 = random.randint(1, 5)
user_answer = sprite.ask(f"Add these numbers: {num1} and {num2}")
user_answer = int(user_answer)
if user_answer > num1 + num2:
    sprite.say("too big!")
if user_answer == num1 + num2:
    sprite.say("correct!")
if user_answer < num1 + num2:
    sprite.say("too small!")
