// script.js
const myCanvas = document.querySelector("Canvas");
const ctx = myCanvas.getContext('2d');
console.log(myCanvas.width);

let myBoard = [];

for (let i=1; i<10; i++) {
	newRow = [];
	for (let j=1; j<10; j++) {
		newRow.push(j);
	}	
	myBoard.push(newRow);
}
console.log(myBoard);

// make a grid

function drawLine(c) {
	// c should have c.startX, c.startY, c.endX, c.endY, c.horizontal, c.thickness
	let xDelta = 0;
	let yDelta = 0;
	if (c.horizontal) {
		yDelta = c.thickness;
	} else {
		xDelta = c.thickness;
	}
	ctx.rect(
		c.startX + xDelta,
		c.startY + yDelta,
		Math.abs(c.endX - c.startX) - xDelta,
		Math.abs(c.endY - c.startY) - yDelta,
	);
	/*
	console.log("startx");
	console.log( c.startX + xDelta);
	console.log("starty");
	console.log( c.startY + yDelta);
	console.log("endx");
	console.log( Math.abs(c.endX - c.startX) - xDelta);
	console.log("endy");
	console.log( Math.abs(c.endY - c.startY) - yDelta);
	*/
	ctx.fill();
}
/*
drawLine({
	startX: 50,
	startY: 100,
	endX: 450,
	endY: 100,
	horizontal: true,
	thickness: 2,
})
*/
let thickness = 2;
let cellWidth = myCanvas.width/9;
for (let i=0; i<= myCanvas.width; i+= cellWidth) {
	if (i % (cellWidth * 3) == 0) {
		thickness = 4;
	} else {
		thickness = 2;
	}
	drawLine({
		startX: i,
		startY: 0,
		endX: i,
		endY: myCanvas.height,
		horizontal: false,
		thickness: thickness,
	})
}
let cellHeight = myCanvas.height/9;
for (let i=0; i<= myCanvas.height; i+= cellHeight) {
	if (i % (cellHeight * 3) == 0) {
		thickness = 4;
	} else {
		thickness = 2;
	}
	drawLine({
		startX: 0,
		startY: i,
		endX: myCanvas.width,
		endY: i,
		horizontal: true,
		thickness: thickness,
	})
}
