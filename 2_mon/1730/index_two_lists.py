emoji_list = [
  "🐻", # index 0
  "🦒", # index 1
  "🐕", # index 2
  "🐼", # index 3
]

location_list = [
  # (x position, y position)
  (-100, -100), # index 0
  (100, -100), # index 1
  (-100, 0), # index 2
  (100, 0), # index 3
]

for index in [0, 1, 2, 3]:
  print(
    emoji_list[index], "at", location_list[index]
  )
