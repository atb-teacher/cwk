from Character import CharacterBehindCard
# from Character.py get some code
emoji_list = [
  "🐻",
  "🦒",
  "🐕",
  "🐼",
]

behind_card_list = []
for num in [0, 1]:
  for one_emoji in emoji_list:
    behind_card_list.append(
      CharacterBehindCard(
        _unique_char = one_emoji
      )
    )
print(behind_card_list)
