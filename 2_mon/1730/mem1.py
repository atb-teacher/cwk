sprites = []
for num in [0, 1]:
    """
    for one_sprite in "applecore football bat".split():
        sprites.append(codesters.Sprite(one_sprite))
    """
    sprites.append(
        codesters.Sprite("applecore")
    )
    sprites.append(
        codesters.Sprite("football")
    )
    sprites.append(
        codesters.Sprite("bat")
    )

random.shuffle(sprites)

locations = []

for y_pos in [0, 100]:
    for x_pos in [-100, 0, 100]:
        locations.append((x_pos, y_pos))
        
print(locations)
"""
index = 0
for y_pos in [0, 100]:
    for x_pos in [-100, 0, 100]:
        sprites[index].go_to(x_pos, y_pos)
        index = index + 1
"""


