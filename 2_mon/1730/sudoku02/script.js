// script.js
const myCanvas = document.querySelector("Canvas");
const ctx = myCanvas.getContext('2d');
console.log(myCanvas.width);


let nums = `_68__2479
4_3__8___
_216__53_
8___47913
7____3__4
23419__87
_4752___1
_8_4___2_
_________`

let selectedRow = null;
let selectedCol = null;

let nums_rows = nums.split('\n');

let myBoard = [];
for (let i=0; i<nums_rows.length; i++) {
	console.log('hello')
	newRow = [];
	for (let j=0; j<nums_rows[0].length; j++) {
		console.log('hi');
		newRow.push(nums_rows[i][j]);
	}	
	myBoard.push(newRow);
}
console.log(myBoard);

// make a grid

function drawLine(c) {
	// c should have c.startX, c.startY, c.endX, c.endY, c.horizontal, c.thickness
	let xDelta = 0;
	let yDelta = 0;
	if (c.horizontal) {
		yDelta = c.thickness;
	} else {
		xDelta = c.thickness;
	}
	ctx.rect(
		c.startX + xDelta,
		c.startY + yDelta,
		Math.abs(c.endX - c.startX) - xDelta,
		Math.abs(c.endY - c.startY) - yDelta,
	);
	ctx.fill();
}
let thickness = 2;
let cellWidth = myCanvas.width/9;
for (let i=0; i<= myCanvas.width; i+= cellWidth) {
	if (i % (cellWidth * 3) == 0) {
		thickness = 4;
	} else {
		thickness = 2;
	}
	drawLine({
		startX: i,
		startY: 0,
		endX: i,
		endY: myCanvas.height,
		horizontal: false,
		thickness: thickness,
	})
}
let cellHeight = myCanvas.height/9;
for (let i=0; i<= myCanvas.height; i+= cellHeight) {
	if (i % (cellHeight * 3) == 0) {
		thickness = 4;
	} else {
		thickness = 2;
	}
	drawLine({
		startX: 0,
		startY: i,
		endX: myCanvas.width,
		endY: i,
		horizontal: true,
		thickness: thickness,
	})
}

ctx.font = "30px serif";
for (let i=0; i<myBoard.length; i++) {
	oneRow = myBoard[i];
	for (let j=0; j<myBoard[0].length; j++) {
		oneNum = oneRow[j];
		if (oneNum != "_") {
			ctx.strokeText(
				oneNum, // number being written
				Math.floor(cellWidth * j + (cellWidth/2)) - 10, // x pos
				Math.floor(cellHeight * i + (cellHeight/2)) + 15, // y pos
			);
		}
	}
}
myCanvas.onclick = function(e) {
	let clickedCol = Math.floor(e.offsetX/cellWidth);
	selectedCol = clickedCol;
	console.log(clickedCol);
	let clickedRow = Math.floor(e.offsetY/cellHeight);
	selectedRow = clickedRow;
	console.log(clickedRow);
	ctx.fillStyle = "rgba(153, 204, 255, 50)";
	ctx.fillRect(
		clickedCol * cellWidth + 10,
		clickedRow * cellHeight + 10,
		cellWidth - 20,
		cellHeight - 20,
	);
	console.log(
		selectedCol,
		selectedRow,
	);
}

document.onkeydown = function(e) {
	if (selectedCol != null && selectedRow != null) {
		console.log(`Put the number ${e.key} at ${selectedCol}, ${selectedRow}`);
                ctx.strokeText(
                        e.key, // number being written
                        Math.floor(cellWidth * selectedCol + (cellWidth/2)) - 10, // x pos
                        Math.floor(cellHeight * selectedRow + (cellHeight/2)) + 15, // y pos
                );

	}
}
