# Character.py
class CharacterBehindCard:
  def __init__(self, _unique_char):
    self.unique_char = _unique_char

  def get_char(self):
    return self.unique_char

  def __repr__(self): # new
    return self.unique_char + "!" # new
"""
bunny = CharacterBehindCard(
  _unique_char = "🐇"
)
print(
  bunny.get_char()
)
frog = CharacterBehindCard(
  _unique_char = "🐸"
)
print(
  frog.get_char()
)
"""
