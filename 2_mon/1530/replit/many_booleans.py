user_age = input("What is your age?") # similar to blue ask/answer in scratch
user_age_int = int(user_age) # convert to a number
above_seven = user_age_int > 7
below_thirty = user_age_int < 30

user_game = input("What is the best game?")

good_games = ["Batman Arkham Asylum", "GTA", "Minecraft", "Roblox"]

likes_good_game = user_game in good_games

if all([
  above_seven,
  below_thirty,
  likes_good_game,
]):
  print("You're cool!")