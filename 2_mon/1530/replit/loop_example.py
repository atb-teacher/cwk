print("Hello, my name is Alex")

# while loop example
# exit by pushing ctrl + c
import time

counter = 0
# while True:
while 1 == 1: # forever loop
  print(counter)
  time.sleep(1)
  counter = counter + 1
