stage.disable_all_walls()
stage.set_gravity(10)

hero = codesters.Sprite(
    "alien1", # image
    0, # x
    0, #y
)

hero.set_size(0.5) # make them smaller


floor = codesters.Rectangle(
    0, # x 
    -250, # y
    500, # width
    50, # height
    "red", # color
)
floor.set_gravity_off()

def calc_y(in_height): # pink block
    return floor.get_top() + (height//2)

height = 100

while True:
    obstacle = codesters.Rectangle(
        300, # x
        calc_y(height), # y
        50, # width
        height, # height
        "blue",
    )
    obstacle.set_gravity_off()
    
    obstacle.set_x_speed(-5)
    
    stage.wait(2)
    

