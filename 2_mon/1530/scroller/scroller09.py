# ===== SETUP STAGE =====

stage.disable_all_walls()
stage.set_gravity(10)

# ===== SETUP HERO =====

hero = codesters.Sprite(
    "alien1", # image
    -200, # x
    -100, # y
)

hero.set_size(0.5) # make them smaller
hero.grounded = False
hero.underground = False
hero.alive = True

# ===== SETUP GROUNDED TEXT =====

grounded_text = codesters.Text(
    "Grounded: False",
    -160, # x
    220, # y
)

def update_grounded_text():
    grounded_text.set_text(f"Grounded: {hero.grounded}")

# ===== SETUP MOVEMENT =====

    
def space_bar_pizza():
    if hero.grounded == True:
        hero.grounded = False
        update_grounded_text()
        hero.jump(15)
        hero.set_gravity_on()

stage.event_key("space", space_bar_pizza)

def left_key(sprite):
    if sprite.get_x() > -230:
        sprite.move_left(20)

hero.event_key("left", left_key)

def right_key(sprite):
    if sprite.get_x() < -50:
        sprite.move_right(20)

hero.event_key("right", right_key)

def down_key(sprite):
    
    if sprite.grounded == True:
        sprite.underground = True
        print("go down")
        sprite.set_y(
            floor.get_top() - (hero.get_height()//2) + 20
        )
     
hero.event_key("down", down_key)

# ===== SETUP FLOOR =====

floor = codesters.Rectangle(
    0, # x 
    -250, # y
    500, # width
    50, # height
    "red", # color
)
floor.set_gravity_off()

# ===== SETUP COLLISION =====

def collision_caesar_salad(sprite, other_thing):
    """instructions for when
    our alien hits something
    sprite = alien
    other_thing = whatever we hit
    """
    other_thing_color = other_thing.get_color()
    if all([
        other_thing_color == "red",
        hero.underground == False,
        hero.alive == True,
    )]:
        hero.grounded = True
        update_grounded_text()
        hero.set_gravity_off() # stop falling
        hero.set_y_speed(0) # stop falling
        hero.set_y( # tell the hero to go to this height
            calc_y( # top of floor + half the height
                hero.get_height() # ask the hero their height
            ) - 12 # subtract 12 (too high)
        )

hero.event_collision(collision_caesar_salad)

def calc_y(in_height): # pink block
    """This pink block calculates
    how high to put the blue obstacles"""
    return floor.get_top() + (height//2)

# ===== MAKE OBSTACLES =====

height = 100 # OBSTACLE HEIGHT

while True: # FOREVER LOOP
    obstacle = codesters.Rectangle(
        300, # x
        calc_y(height), # y
        50, # width
        height, # height
        "blue",
    )
    obstacle.set_gravity_off()
    
    obstacle.set_x_speed(-5)
    
    stage.wait(2)



"""
GOALS:
* Add health/score
  * Make them take damage
* Able to move a bit
* Able to drop down quickly

ADVANCED:
* Allow double jump
* Add more obstacles
  * Obstacles could be up high (maybe a bird)
"""


