# ===== SETUP STAGE =====

stage.disable_all_walls()
stage.set_gravity(10)

# ===== SETUP HERO =====

hero = codesters.Sprite(
    "alien1", # image
    -200, # x
    -100, # y
)

hero.set_size(0.5) # make them smaller

# ===== SETUP FLOOR =====

floor = codesters.Rectangle(
    0, # x 
    -250, # y
    500, # width
    50, # height
    "red", # color
)
floor.set_gravity_off()

# ===== SETUP COLLISION =====

# def hero_collision(sprite, other_thing):

def collision_caesar_salad(sprite, other_thing):
    other_thing_color = other_thing.get_color()
    if other_thing_color == "red":
        hero.set_gravity_off()
        hero.set_y_speed(0)
        hero.set_y( # tell the hero to go to this height
            calc_y( # top of floor + half the height
                hero.get_height() # ask the hero their height
            ) - 12 # subtract 12 (too high)
        )

hero.event_collision(collision_caesar_salad)

# ===== MAKE OBSTACLES =====

def calc_y(in_height): # pink block
    """This pink block calculates
    how high to put the blue obstacles"""
    return floor.get_top() + (height//2)

height = 100 # OBSTACLE HEIGHT

while True: # FOREVER LOOP
    obstacle = codesters.Rectangle(
        300, # x
        calc_y(height), # y
        50, # width
        height, # height
        "blue",
    )
    obstacle.set_gravity_off()
    
    obstacle.set_x_speed(-5)
    
    stage.wait(2)



"""
GOALS:
* Make the alien land on the floor
* Make the alien able to jump
* Add more obstacles
* Add health/score

ADVANCED:
* Allow double jump
* Added down button

"""

