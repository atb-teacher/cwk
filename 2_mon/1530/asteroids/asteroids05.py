# ===== SETUP SOUND =====

# NOTE: SOUND IS OPTIONAL
"""
def make_noise(): # OPTIONAL
    Bb_sound = codesters.Sound("piano_bb")
    D_sound = codesters.Sound("piano_d")
    F_sound = codesters.Sound("piano_f")
    
    for one_sound in [Bb_sound, D_sound, F_sound]:
        one_sound.play()
"""

# ===== SETUP STAGE =====
stage.set_background("space2")
stage.disable_all_walls() # stop bouncing

# ===== SETUP HERO =====

ship_sprite = codesters.Sprite(
    "spaceship3", # sprite name
    0, # x
    -200, # y
)

ship_sprite.set_size(0.3)

ship_sprite.rotation_offset = -57
ship_sprite.set_rotation(
    ship_sprite.rotation_offset    
)

ship_sprite.health = 5

# ===== SETUP HEALTH TEXT =====

health_text = codesters.Text(
    "Health: 5",
    -180, # x
    200, # y
)

health_text.set_color("white")

def update_health():
    ship_sprite.health = ship_sprite.health - 1
    health_text.set_text(
        f"Health: {ship_sprite.health}"
    )

# ===== SETUP COLLISION =====

def ship_collision(ship, other_thing):
    if other_thing.get_image_name() == "asteroid3":
        stage.remove_sprite(other_thing)
        update_health()
        
ship_sprite.event_collision(ship_collision)

# ===== SETUP MOVEMENT =====


def left_key_WOF(sprite):
    sprite.move_left(20)

ship_sprite.event_key("left", left_key_WOF)


def right_key_BTAS(sprite):
    sprite.move_right(20)

ship_sprite.event_key("right", right_key_BTAS)

while True:
    one_asteroid = codesters.Sprite(
        "asteroid3", # image name
        500, # x
        500, # y
    )
    one_asteroid.set_size(0.2)
    one_asteroid.go_to(
        random.randint(-200, 200), # x
        300, # y
    )
    one_asteroid.set_y_speed(-5)
    stage.wait(2)

"""
MAIN GOALS
* When you hit an asteroid, the game is over
* Add end screen

STRETCH GOALS
* Music
* Can shoot a laser
* Can get powerups
"""
