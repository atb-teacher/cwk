# ===== SETUP STAGE =====
stage.set_background("space2")

# ===== SETUP HERO =====

ship_sprite = codesters.Sprite(
    "spaceship3", # sprite name
    0, # x
    -200, # y
)

ship_sprite.set_size(0.3)
ship_sprite.offset = -57
ship_sprite.set_rotation(
    ship_sprite.offset    
)

# ===== SETUP MOVEMENT =====

def left_key(sprite):
    sprite.move_left(20)

ship_sprite.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)

ship_sprite.event_key("right", right_key)




