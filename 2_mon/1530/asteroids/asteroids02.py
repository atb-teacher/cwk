# ===== SETUP STAGE =====
stage.set_background("space2")
stage.disable_all_walls() # stop bouncing

# ===== SETUP HERO =====

ship_sprite = codesters.Sprite(
    "spaceship3", # sprite name
    0, # x
    -200, # y
)

ship_sprite.set_size(0.3)

ship_sprite.rotation_offset = -57
ship_sprite.set_rotation(
    ship_sprite.rotation_offset    
)

# ===== SETUP MOVEMENT =====

def left_key_WOF(sprite):
    sprite.move_left(20)

ship_sprite.event_key("left", left_key_WOF)

def right_key_BTAS(sprite):
    sprite.move_right(20)

ship_sprite.event_key("right", right_key_BTAS)


while True:
    one_asteroid = codesters.Sprite(
        "asteroid3", # image name
        500, # x
        500, # y
    )
    one_asteroid.set_size(0.2)
    one_asteroid.go_to(
        random.randint(-200, 200), # x
        300, # y
    )
    one_asteroid.set_y_speed(-5)
    stage.wait(2)

