green_alien = codesters.Sprite("alien3")

block = codesters.Rectangle(
    -200,
    0, 
    100, 
    50, 
    random.choice(["blue", "red", "green",]),
)

block.set_x_speed(2)

def collision_minecraft(sprite, other_thing):
    
    other_thing_color = other_thing.get_color()
    
    if other_thing_color == "blue":
        green_alien.say("I got hit by a blue block")
    
    if other_thing_color == "red":
        green_alien.set_size(0.9)
    
    if other_thing_color == "green":
        green_alien.hide()

        
green_alien.event_collision(collision_minecraft)


