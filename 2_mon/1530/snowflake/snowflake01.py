# ===== SETUP =====
stage.set_background("winter")
stage.disable_all_walls()

# ==== MAKE FLAKES =====
while True:
    sprite = codesters.Sprite(
        "snowflake3", # image
        random.randint(-200, 200), # x
        275, # y
        )
    
    sprite.set_y_speed(-2)
    stage.wait(1)