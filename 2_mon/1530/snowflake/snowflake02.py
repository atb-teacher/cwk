# ===== SETUP =====
stage.set_background("winter")
stage.disable_all_walls()

# ==== MAKE FLAKES =====
flakes = []

while True:
    sprite = codesters.Sprite(
        "snowflake3", # image
        random.randint(-200, 200), # x
        275, # y
        )
    flakes.append(sprite)
    sprite.set_y_speed(-2)
    for one_flake in flakes:
        one_flake.set_x_speed(
            random.randint(-1, 1)
        )
    stage.wait(1)
