# ===== SETUP =====
stage.set_background("winter")
stage.disable_all_walls()

# make snowman
sprite = codesters.Sprite(
    "snowman",
    0, # x
    -100, # y
)

def left_key_friedrice(sprite):
    sprite.move_left(20)
    # add other actions...
    
sprite.event_key("left", left_key_friedrice)

def right_key_spaghetti(sprite):
    sprite.move_right(20)
    # add other actions...
    
sprite.event_key("right", right_key_spaghetti)


# ==== MAKE FLAKES =====
flakes = []

while True:
    sprite = codesters.Sprite(
        "snowflake3", # image
        random.randint(-200, 200), # x
        275, # y
        )
    flakes.append(sprite)
    sprite.set_y_speed(-2)
    for one_flake in flakes:
        one_flake.set_x_speed(
            random.randint(-1, 1)
        )
    stage.wait(1)

