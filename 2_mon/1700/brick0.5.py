# ===== SETUP SCORE =====

score = 0

# ===== SETUP TEXT =====

score_text = codesters.Text(
    f"Score: {score}",
    0, # x pos
    200, # y pos
)

# NEW
def update_score_text():
    global score
    score_text.set_text(f"Score: {score}")

for y_location in [0, 100, 200]: # ADDED
    for x_location in [-150, 0, 150]:
        new_rec = codesters.Rectangle(
            x_location, # x pos
            y_location, # y pos # CHANGED
            100, # width
            50, # height
            "darkred"
        )
        new_rec.set_outline_color("black")
        new_rec.set_line_thickness(5)


"""
# ===== TEMPORARY CODE ======
while True:
    stage.wait(1)
    score = score + random.randint(-2, 5)
    update_score_text()
"""
