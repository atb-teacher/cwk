sprite = codesters.Sprite("alien1") # this is the part that makes the sprite


def left_key_worldcup(sprite): 
    sprite.move_left(20) # this makes the alien move left
    
sprite.event_key("left", left_key_worldcup)

"""
sprite.event_key attaches the "left" key and the
"left_key_worldcup" set of instuctions
"""

# This is a one-line comment

"""
this is a multi-line comment
.
.
.
.
you can write a lot btw three quotes
"""
