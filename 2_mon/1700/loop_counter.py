counter1 = 0

for letter in "abc":
  counter1 = counter1 + 1
  
print("counter1", counter1)

counter2 = 0
for letter1 in "abcd":
  for letter2 in "efghijk":
    counter2 = counter2 + 1
print("counter2", counter2)
