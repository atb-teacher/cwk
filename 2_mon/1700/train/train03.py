player = codesters.Sprite(
    "alien1_masked", # sprite
    -200, # x
    0, # y
)

player.set_size(0.5)

def right_key_wking(sprite):
    if player.x < 220:
        player.set_x(
            player.x + 20
        )
    player.say(
        player.x
    )
    
player.event_key("right", right_key_wking)

def left_key_avatar(sprite):
    if player.x > -220:
        player.set_x(
            player.x - 20
        )
    player.say(
        player.x
    )

player.event_key("left", left_key_avatar)

def up_key_cuphead(sprite):
    if player.y < 220:
        player.set_y(
            player.y + 20
        )
    
player.event_key("up", up_key_cuphead)

def down_key_poke(sprite):
    if player.y > -220:
        player.set_y(
            player.y - 20
        )

player.event_key("down", down_key_poke)

# TEMP CODE

height = 70
spacing = 10

traincar1 = codesters.Rectangle(
    0, # x
    200 - (height + spacing) * 0, # y
    20, # width
    height, # height
    "blue", # color
)

traincar2 = codesters.Rectangle(
    0, # x
    200 - (height + spacing) * 1, # y
    20, # width
    height, # height
    "blue", # color
)

traincar3 = codesters.Rectangle(
    0, # x
    200 - (height + spacing) * 2, # y
    20, # width
    height, # height
    "blue", # color
)


for num in [0, 1, 2]:
    traincar3 = codesters.Rectangle(
        50, # x
        200 - (height + spacing) * num, # y
        20, # width
        height, # height
        "purple", # color
    )
    


