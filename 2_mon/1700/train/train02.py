player = codesters.Sprite(
    "alien1_masked", # sprite
    -200, # x
    0, # y
)

player.set_size(0.5)

def right_key_wking(sprite):
    if player.x < 220:
        player.set_x(
            player.x + 20
        )
    player.say(
        player.x
    )
    
player.event_key("right", right_key_wking)

def left_key_avatar(sprite):
    if player.x > -220:
        player.set_x(
            player.x - 20
        )
    player.say(
        player.x
    )

player.event_key("left", left_key_avatar)

def up_key_cuphead(sprite):
    if player.y < 220:
        player.set_y(
            player.y + 20
        )
    
player.event_key("up", up_key_cuphead)

def down_key_poke(sprite):
    if player.y > -220:
        player.set_y(
            player.y - 20
        )

player.event_key("down", down_key_poke)

