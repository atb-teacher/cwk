stage.disable_all_walls()

player = codesters.Sprite(
    "alien1_masked", # sprite
    -200, # x
    0, # y
)

player.set_size(0.5)

def right_key_wking(sprite):
    if player.x < 220:
        player.set_x(
            player.x + 20
        )
    player.say(
        player.x
    )
    
player.event_key("right", right_key_wking)

def left_key_avatar(sprite):
    if player.x > -220:
        player.set_x(
            player.x - 20
        )
    player.say(
        player.x
    )

player.event_key("left", left_key_avatar)

def up_key_cuphead(sprite):
    if player.y < 220:
        player.set_y(
            player.y + 20
        )
    
player.event_key("up", up_key_cuphead)

def down_key_poke(sprite):
    if player.y > -220:
        player.set_y(
            player.y - 20
        )

player.event_key("down", down_key_poke)

# TEMP CODE
"""
class TrainData:
    def __init__(self, _speed, _color, _damage):
        self.speed = _speed
        self.color = _color
        self.damage = _damage
        
class FastTrain(TrainData):
    def __init__(self):
        super().__init__(10, "blue", 1)

class ScaryTrain(TrainData):
    def __init__(self):
        super().__init__(5, "red", 3)

"""

"""
traincar1 = codesters.Rectangle(
    0, # x
    200 - (height + spacing) * 0, # y
    20, # width
    height, # height
    "blue", # color
)

traincar2 = codesters.Rectangle(
    0, # x
    200 - (height + spacing) * 1, # y
    20, # width
    height, # height
    "blue", # color
)

traincar3 = codesters.Rectangle(
    0, # x
    200 - (height + spacing) * 2, # y
    20, # width
    height, # height
    "blue", # color
)
"""

"""
for num in [0, 1, 2]:
    traincar3 = codesters.Rectangle(
        50, # x
        200 - (height + spacing) * num, # y
        20, # width
        height, # height
        "purple", # color
    )
    traincar3.set_y_speed(-5)
"""

goal_area = codesters.Rectangle(
    250, # x
    0, # y
    20, # width
    500, # height
    "lightgreen",
)

def collision_function(alien, other):
    other_color = other.get_color()
    if other_color == "lightgreen":
        alien.say("I win!")
    if other_color in ["blue", "green", "purple"]:
        alien.say("ouch!")

player.event_collision(collision_function)

def make_train_going_down():
    height = 70
    spacing = 10
    x_pos = random.randint(-5, 5) * 40
    color = random.choice(["blue", "green", "purple"])
    for num in [0, 1, 2]:
        traincar3 = codesters.Rectangle(
            x_pos, # x
            400 - (height + spacing) * num, # y
            20, # width
            height, # height
            color, # color
        )
        traincar3.set_y_speed(-5)

while True:
    stage.wait(1)
    make_train_going_down()


