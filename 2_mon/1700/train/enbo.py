stage.disable_all_walls()

player = codesters.Sprite(
    "alien1_masked", # sprite
    -200, # x
    0, # y
)

player.set_size(0.5)

def right_key_wking(sprite):
    if player.x < 220:
        player.set_x(
            player.x + 20
        )
    player.say(
        player.x
    )
    
player.event_key("right", right_key_wking)

def left_key_avatar(sprite):
    if player.x > -220:
        player.set_x(
            player.x - 20
        )
    player.say(
        player.x
    )

player.event_key("left", left_key_avatar)

def up_key_cuphead(sprite):
    if player.y < 220:
        player.set_y(
            player.y + 20
        )
    
player.event_key("up", up_key_cuphead)

def down_key_poke(sprite):
    if player.y > -220:
        player.set_y(
            player.y - 20
        )

player.event_key("down", down_key_poke)


class TrainData:
    def __init__(self, _speed, _color, _damage):
        self.speed = _speed
        self.color = _color
        self.damage = _damage
        
class FastTrain(TrainData):
    def __init__(self):
        super().__init__(
            _speed = 10,
            _color = "blue",
            _damage = -1,
        )

class ScaryTrain(TrainData):
    def __init__(self):
        super().__init__(5, "red", 3)

height = 70
spacing = 10

goal_area = codesters.Rectangle(
    250, # x
    0, # y
    20, # width
    500, # height
    "lightgreen",
)

def collision_function(alien, other):
    other_color = other.get_color()
    if other_color == "lightgreen":
        alien.say("I win!")
    if other_color in ["blue", "green", "purple"]:
        alien.say("ouch!")

player.event_collision(collision_function)

def make_train_going_down():
    height = 70
    spacing = 10
    rand_train = random.choice([FastTrain, ScaryTrain])()
    x_pos = random.randint(-5, 5) * 40
    color = random.choice(["blue", "green", "purple"])
    for num in [0, 1, 2]:
        traincar3 = codesters.Rectangle(
            x_pos, # x
            400 - (height + spacing) * num, # y
            20, # width
            height, # height
            color, # color
        )
        traincar3.set_y_speed(-1 * rand_train.speed)

while True:
    stage.wait(1)
    make_train_going_down()


