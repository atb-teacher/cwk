# ===== SETUP STAGE =====

stage.set_background("space2")

obstacle_color = random.choice(
    ["skyblue", "purple", "crimson"]
)

# ===== SETUP SPRITES =====

sprite_ufo = codesters.Sprite(
    "ufo", # image
    -225, # x
    225, # y
)
sprite_ufo.set_size(0.25)

sprite_ship = codesters.Sprite(
    "spaceship", # image
    225, # x
    -225, # y
)
sprite_ship.set_size(0.25)

# ===== SETUP UFO MOVEMENT =====

def left_key_ufo(sprite):
    sprite.set_rotation(90)
    sprite.move_left(20)

sprite_ufo.event_key("left", left_key_ufo)

def right_key_ufo(sprite):
    sprite.set_rotation(-90)
    sprite.move_right(20)
    
sprite_ufo.event_key("right", right_key_ufo)

def up_key_ufo(sprite):
    sprite.set_rotation(0)
    sprite.move_up(20)
    
sprite_ufo.event_key("up", up_key_ufo)

def down_key_ufo(sprite):
    sprite.set_rotation(180)
    sprite.move_down(20)
    
sprite_ufo.event_key("down", down_key_ufo)

# ===== SETUP OBSTACLES =====

for i in range(25): # loop 25 times
    sprite = codesters.Rectangle(
        
        random.randint(-200, 200), # x 
        random.randint(-200, 200), # y
        random.randint(25, 50), # width
        random.randint(25, 50), # height
        obstacle_color,
    )

