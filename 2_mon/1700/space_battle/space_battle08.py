# ===== SETUP STAGE =====

stage.disable_all_walls()
stage.set_background("space2")

obstacle_color = random.choice(
    ["skyblue", "purple", "crimson"]
)

# ===== SETUP SPRITES =====

sprite_ufo = codesters.Sprite(
    "ufo", # image
    -225, # x
    225, # y
)
sprite_ufo.set_size(0.25)
sprite_ufo.set_rotation(0)

sprite_ship = codesters.Sprite(
    "spaceship", # image
    225, # x
    -225, # y
)
sprite_ship.set_size(0.25)
sprite_ship.offset = 45
sprite_ship.set_rotation(90 + sprite_ship.offset)


# ===== SETUP UFO MOVEMENT =====

def left_key_ufo(sprite):
    sprite.set_rotation(90)
    sprite.move_left(20)

sprite_ufo.event_key("a", left_key_ufo)

def right_key_ufo(sprite):
    sprite.set_rotation(-90)
    sprite.move_right(20)
    
sprite_ufo.event_key("d", right_key_ufo)

def up_key_ufo(sprite):
    sprite.set_rotation(0)
    sprite.move_up(20)
    
sprite_ufo.event_key("w", up_key_ufo)

def down_key_ufo(sprite):
    sprite.set_rotation(180)
    sprite.move_down(20)
    
sprite_ufo.event_key("s", down_key_ufo)

# ===== SETUP UFO SHOT =====
def ufo_shot_collision(ufo_shot, other_thing):
    global obstacle_color
    other_thing_color = other_thing.get_color()
    if other_thing_color == obstacle_color:
        ufo_shot.set_x_speed(
            ufo_shot.get_x_speed() * -1
        )
        ufo_shot.set_y_speed(
            ufo_shot.get_y_speed() * -1
        )

def shoot_ufo(sprite):
    ufo_shot = codesters.Sprite(
        "green_laser_34a", # image
        500, # x
        500, # y
    )
    
    ufo_shot.event_collision(ufo_shot_collision)
    
    ufo_shot.set_size(0.2)
    
    ufo_shot.set_rotation(
        sprite.get_rotation()
    )
    
    if sprite.get_rotation() == 0:
        ufo_shot.set_y_speed(-2)
    elif sprite.get_rotation() == 90:
        ufo_shot.set_x_speed(2)
    elif sprite.get_rotation() == 180:
        ufo_shot.set_y_speed(2)
    elif sprite.get_rotation() in (-90, 270):
        ufo_shot.set_x_speed(-2)
    
    ufo_shot.set_position(
        sprite.get_x(),
        sprite.get_y(),
    )

sprite_ufo.event_key("f", shoot_ufo)

# ===== SETUP SPACESHIP SHOT =====

def ship_shot_collision(ship_shot, other_thing):
    global obstacle_color
    other_thing_color = other_thing.get_color()
    if other_thing_color == obstacle_color:
        ship_shot.set_x_speed(
            ship_shot.get_x_speed() * -1
        )
        ship_shot.set_y_speed(
            ship_shot.get_y_speed() * -1
        )


def shoot_spaceship(sprite):
    ship_shot = codesters.Sprite(
        "red_laser_19d", # image
        500, # x
        500, # y
    )
    
    ship_shot.event_collision(ship_shot_collision)
    
    ship_shot.set_size(0.2)
    
    ship_shot.set_rotation(
        sprite.get_rotation() - sprite.offset
    )
    
    print(sprite.get_rotation())
    
    if sprite.get_rotation() == 0 + sprite.offset: # looks up
        ship_shot.set_y_speed(2)
    elif sprite.get_rotation() == 90 + sprite.offset: 
        ship_shot.set_x_speed(-2)
    elif sprite.get_rotation() == 180 + sprite.offset:
        ship_shot.set_y_speed(-2)
    elif sprite.get_rotation() == 270 + sprite.offset:
        ship_shot.set_x_speed(2)

    ship_shot.set_position(
        sprite.get_x(),
        sprite.get_y(),
    )

sprite_ship.event_key("space", shoot_spaceship)

# ===== SETUP SHIP MOVEMENT =====

def left_key_ship(sprite):
    sprite.set_rotation(90 + sprite_ship.offset)
    sprite.move_left(20)

sprite_ship.event_key("left", left_key_ship)

def right_key_ship(sprite):
    sprite.set_rotation(270 + sprite_ship.offset)
    sprite.move_right(20)

sprite_ship.event_key("right", right_key_ship)

def up_key_ship(sprite):
    sprite.set_rotation(0 + sprite_ship.offset)
    sprite.move_up(20)

sprite_ship.event_key("up", up_key_ship)

def down_key_ship(sprite):
    sprite.set_rotation(180 + sprite_ship.offset)
    sprite.move_down(20)

sprite_ship.event_key("down", down_key_ship)

# ===== SETUP OBSTACLES =====

area = 180
for i in range(20): # loop 25 times
    sprite = codesters.Rectangle(
        
        random.randint(-1 * area, area), # x 
        random.randint(-1 * area, area), # y
        random.randint(25, 50), # width
        random.randint(25, 50), # height
        obstacle_color,
    )

"""
GOALS:
* Add health
* Score
* Shoot others
* Other player can shoot
* Obstacles block shots
"""

