# ===== SETUP STAGE =====

stage.set_gravity(10) # NEW CODE
stage.disable_all_walls()

# ===== MAKE THE FLOOR =====

floor = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "darkred", # color
)

floor.name = "main floor"

floor.set_gravity_off() # NEW CODE

# ===== MAKE THE PLAYER =====

player = codesters.Sprite(
    "knight2", # image name
    -200, # x
    0, # y
)

player.name = "hero"

player.set_y_speed(-5)

player.set_size(.7) # 70% of previous size

def when_collision(main_sprite, hit_sprite):
    if hit_sprite.name == "main floor":
        
        # stopping falling
        main_sprite.set_y_speed(0)
        main_sprite.set_gravity_off()
        
        
        # positioning knight
        proper_height = hit_sprite.get_top() + \
        (main_sprite.get_height() // 2) # NEW CODE
        main_sprite.set_y(proper_height) # NEW CODE 
        

player.event_collision(when_collision)

def space_bar_video_games(in_sprite):
    in_sprite.jump(10)
    in_sprite.set_gravity_on()
    
player.event_key("space", space_bar_video_games)

def left_key(sprite):
    sprite.move_left(20)

player.event_key("left", left_key)

def right_key(sprite):
    if sprite.get_x() < 0:
        sprite.move_right(20)

player.event_key("right", right_key)

def enemy_collision(enemy, other):
    if other.name == "hero":
        enemy.say("gotcha!")
    elif other.name == "main floor":
        enemy.set_y_speed(
            abs(
                enemy.get_y_speed()
            )
        )

while True:
    stage.wait(1)
    enemy_rect = codesters.Rectangle(
        200, # x
        random.randint(-50, 250), # y
        50, # width
        50, # height
        "blue", # color    
    )
    enemy_rect.set_x_speed(-10)
    enemy_rect.name = "enemy"
    enemy_rect.event_collision(enemy_collision)
