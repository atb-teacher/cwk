# ===== MAKE THE FLOOR =====

floor = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "darkred", # color
)

floor.name = "main floor"# NEW CODE

# ===== MAKE THE PLAYER =====

player = codesters.Sprite(
    "knight2", # image name
    -200, # x
    0, # y
)

player.set_y_speed(-5)

player.set_size(.7) # 70% of previous size

def when_collision(main_sprite, hit_sprite):
    if hit_sprite.name == "main floor":
        main_sprite.set_y_speed(0)

player.event_collision(when_collision)




# RANDOM CODE: 

"""
# "falling code"
player.set_size(10)
while True:
    player.set_size(.9)
    stage.wait(0.1)
"""

