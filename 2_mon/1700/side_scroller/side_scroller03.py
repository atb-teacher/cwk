# ===== SETUP STAGE =====

stage.set_gravity(10) # NEW CODE

# ===== MAKE THE FLOOR =====

floor = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "darkred", # color
)

floor.name = "main floor"

floor.set_gravity_off() # NEW CODE

# ===== MAKE THE PLAYER =====

player = codesters.Sprite(
    "knight2", # image name
    -200, # x
    0, # y
)

player.set_y_speed(-5)

player.set_size(.7) # 70% of previous size

def when_collision(main_sprite, hit_sprite):
    if hit_sprite.name == "main floor":
        main_sprite.set_y_speed(0)
        main_sprite.set_gravity_off() # NEW CODE

player.event_collision(when_collision)

def space_bar_video_games(in_sprite):
    in_sprite.jump(10) # CHANGED 5 -> 10
    in_sprite.set_gravity_on() # NEW CODE
    
player.event_key("space", space_bar_video_games)


