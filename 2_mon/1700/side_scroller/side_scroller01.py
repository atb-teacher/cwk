# ===== MAKE THE FLOOR =====

floor = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "darkred", # color
)

# ===== MAKE THE PLAYER =====

player = codesters.Sprite(
    "knight2", # image name
    -200, # x
    0, # y
)

