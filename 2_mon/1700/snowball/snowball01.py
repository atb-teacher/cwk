hero = codesters.Sprite(
    "costumed_person1", # image name
    -175, # x
    -175, # y
)
hero.set_size(.4) # shrink it by half

villain = codesters.Sprite(
    "709bea192d0e4c09b8cba6c9536abff9", # image name
    175, # x
    175, # y
)
villain.set_size(.4)
