ters.Sprite("alien1")


def left_key(sprite):
    sprite.move_left(20)

hero.event_key("left", left_key)

def right_key(sprite):
    sprite.move_right(20)

hero.event_key("right", right_key)

def up_key(sprite):
    sprite.move_up(20)

hero.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)

hero.event_key("down", down_key)

stage.disable_all_walls()


def spawn_warning(width, height, wait):
    warning_rect = codesters.Rectangle(
        500,
        500,
        width,
        height,
        "red"
    )
    warning_rect.set_opacity(.5)
    warning_rect.go_to(0, 0)
    stage.wait(wait)
    stage.remove_sprite(warning_rect)
    
    
def spawn_laser(width, height):
    laser_rect = codesters.Rectangle(
        500,
        500,
        width,
        height,
        "red"
    )
    laser_rect.set_opacity(1)
    laser_rect.go_to(0, 0)
    stage.wait(.5)
    stage.remove_sprite(laser_rect)

while True:
    stage.wait(1)
    spawn_warning(width=100, height=500, wait=1)
    spawn_laser(width=100, height=500)
