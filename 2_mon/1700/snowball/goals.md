# Main Goals
* Make hero heath
* Make game stop

# Stretch Goals
* Make walls
* Stop snowballs from going through walls
  * Delete snowballs when they hit a wall
* Limit fire rate

# Advanced Goals

* Make snowballs bounce off walls

* Make a big laser
  * Make warning zones
    * set opacity

* Make phases
  * Put phases into game loop