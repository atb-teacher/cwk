stage.set_gravity(10)

one_platform = codesters.Rectangle(
    0, # x
    0, # y
    100, # width
    20, # height
    "blue",# color
)

one_platform.set_y_speed(-1)
one_platform.set_x_speed(1)
one_platform.set_gravity_off()

hero = codesters.Sprite(
    "alien2", # sprite
    0, # x
    100, # y
)
hero.set_size(0.4)

def collision_avatar(alien, other_thing):
    if other_thing.get_color() == "blue":
        alien.say("hitting the platform")
        alien.set_gravity_off()
        alien.set_y_speed(
            other_thing.get_y_speed()
        )
        alien.set_x_speed(
            other_thing.get_x_speed()
        )
    if other_thing.get_color() == "red":
        stage.remove_sprite(alien)

hero.event_collision(collision_avatar)

lava = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "red",
)
