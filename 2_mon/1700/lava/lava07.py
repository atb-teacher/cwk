game_running = True

stage.set_gravity(10)

one_platform = codesters.Rectangle(
    0, # x
    0, # y
    100, # width
    20, # height
    "blue",# color
)

one_platform.set_y_speed(-0.3)
one_platform.set_x_speed(1)
one_platform.set_gravity_off()

hero = codesters.Sprite(
    "alien2", # sprite
    0, # x
    100, # y
)
hero.set_size(0.4)
hero.grounded = False

grounded_text = codesters.Text(
    "grounded: False",
    -175,
    200,
)

def update_grounded_text():
    grounded_text.set_text(
        f"grounded: {hero.grounded}"
    )


def space_bar():
    if hero.grounded == True:
        hero.set_gravity_on()
        hero.grounded = False
        update_grounded_text()
        hero.jump(15)

stage.event_key("space", space_bar)


def collision_avatar(alien, other_thing):
    correct_color = other_thing.get_color() == "blue"
    falling = alien.get_y_speed() < 0
    not_grounded = alien.grounded == False
    if correct_color and falling and not_grounded:
        alien.set_gravity_off()
        alien.grounded = True
        update_grounded_text()
        while alien.grounded:
            alien.set_y_speed(
                other_thing.get_y_speed()
            )
            alien.set_x_speed(
                other_thing.get_x_speed()
            )
            stage.wait(0.1)
    if other_thing.get_color() == "red":
        stage.remove_sprite(alien)
        global game_running
        game_running = False

hero.event_collision(collision_avatar)

def collision_platform(platform, other_thing):
    if other_thing.get_color() == "red":
        stage.remove_sprite(platform)

one_platform.event_collision(collision_platform)

lava = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "red",
)

# while game_running:
while game_running == True:
    
    second_platform = codesters.Rectangle(
        0, # x
        200, # y
        100, # width
        20, # height
        "blue",# color
    )
    
    second_platform.set_y_speed(-0.7)
    second_platform.set_x_speed(
        random.choice([-3, -2, -1, 1, 2, 3])
    )
    second_platform.set_gravity_off()
    second_platform.event_collision(collision_platform)
    
    stage.wait(2)
    

game_over_text = codesters.Text(
    "Game Over",
    0,
    0,
)
game_over_text.set_size(2)

"""
GOALS:
* Make the alien pop up a bit when riding the blue platform
  (not touching)
* Add a score
* Add more randomness to the platforms
"""


