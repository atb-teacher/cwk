stage.set_gravity(10)

one_platform = codesters.Rectangle(
    0, # x
    0, # y
    100, # width
    20, # height
    "blue",# color
)

one_platform.set_y_speed(-1)
one_platform.set_x_speed(1)
one_platform.set_gravity_off()

hero = codesters.Sprite(
    "alien2", # sprite
    0, # x
    100, # y
)
hero.set_size(0.4)
hero.grounded = False

grounded_text = codesters.Text(
    "grounded: False",
    -175,
    200,
)

def update_grounded_text():
    grounded_text.set_text(
        f"grounded: {hero.grounded}"
    )


def space_bar():
    if hero.grounded == True:
        hero.set_gravity_on()
        hero.grounded = False
        update_grounded_text()
        hero.jump(15)

stage.event_key("space", space_bar)


def collision_avatar(alien, other_thing):
    if other_thing.get_color() == "blue":
        
        alien.set_gravity_off()
        alien.grounded = True
        alien.set_y_speed(
            other_thing.get_y_speed()
        )
        alien.set_x_speed(
            other_thing.get_x_speed()
        )
        update_grounded_text()
    if other_thing.get_color() == "red":
        stage.remove_sprite(alien)

hero.event_collision(collision_avatar)

def collision_platform(platform, other_thing):
    if other_thing.get_color() == "red":
        stage.remove_sprite(platform)

one_platform.event_collision(collision_platform)

lava = codesters.Rectangle(
    0, # x
    -225, # y
    500, # width
    50, # height
    "red",
)



