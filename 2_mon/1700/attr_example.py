class Thing:
    def __init__(self):
        print("this is a thing being created")

my_thing = Thing()

my_thing.best_food = "s'mores"

my_thing.best_game = "mario 64"

print(
    my_thing.best_food,
    my_thing.best_game
)
if hasattr(my_thing, "best_food"):
    if my_thing.best_food == "s'mores":
        print("You have good taste in food!")

lava = Thing()
lava.id = "lava"

# inside collision function
if hasatter(lava, "id"):
    if lava.id == "lava":
        print("You got hurt by lava")
