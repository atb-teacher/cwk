stage.set_background("space2")

hero = codesters.Sprite(
    "ufo", # image
    200,# x
    0,# y
)
hero.set_size(0.4)

def up_key(sprite):
    sprite.move_up(20)

hero.event_key("up", up_key)

def down_key(sprite):
    sprite.move_down(20)

hero.event_key("down", down_key)

"""
sprite = codesters.Sprite("alien1")
sprite = codesters.Sprite("meteor1")
"""