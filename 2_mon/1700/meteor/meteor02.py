# ===== SETUP =====
stage.set_background("space2")
stage.disable_all_walls()

# ===== MAKE HERO =====
hero = codesters.Sprite(
    "ufo", # image
    200,# x
    0,# y
)
hero.set_size(0.4)

# ===== SETUP MOVEMENT =====
def up_key_superbowl(sprite):
    sprite.move_up(20)

hero.event_key("up", up_key_superbowl)

def down_key_avatar(sprite):
    sprite.move_down(20)

hero.event_key("down", down_key_avatar)

# ===== GAME LOOP =====
while True:
    stage.wait(2) # WAIT 2 SECS
    
    # ===== MAKE METEOR =====
    meteor = codesters.Sprite(
        "meteor1",
        -200, # x
        random.randint(-175, 175), # y
    )
    meteor.set_x_speed(2)
    meteor.set_size(0.5)


"""
# GOALS
## MAIN GOALS
* Make aliens
* Make score
* Make aliens increase score

## STRETCH GOALS
"""

