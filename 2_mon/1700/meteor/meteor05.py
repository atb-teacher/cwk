# ===== SETUP =====
stage.set_background("space2")
stage.disable_all_walls()
game_running = True

# ===== MAKE HERO =====
hero = codesters.Sprite(
    "ufo", # image
    200,# x
    0,# y
)
hero.set_size(0.4)
hero.score = 0

# ===== SETTING UP SCORE =====

score_text = codesters.Text(
    "score: 0",
    -175, # x
    175, # y
)
score_text.set_color("white")

def update_score():
    score_text.set_text(
        f"score: {hero.score}"
    )

# ===== SETUP COLLISION ======

def collision(sprite, hit_sprite):
    my_var = hit_sprite.get_image_name() 
    if my_var == "meteor1":
        stage.remove_sprite(hero)
        global game_running
        game_running = False
    if my_var == "alien1":
        # hero.score = hero.score + 1
        hero.score += 1
        update_score()
        stage.remove_sprite(hit_sprite)
        
hero.event_collision(collision)

# ===== SETUP MOVEMENT =====
def up_key_superbowl(sprite):
    sprite.move_up(20)

hero.event_key("up", up_key_superbowl)

def down_key_avatar(sprite):
    sprite.move_down(20)

hero.event_key("down", down_key_avatar)

# ===== GAME LOOP =====
counter = 0
while game_running == True:
    stage.wait(0.5) # WAIT 0.5 SECS
    counter = counter + 1
    print(counter)
    if counter % 4 == 0:
        # ===== MAKE METEOR =====
        meteor = codesters.Sprite(
            "meteor1",
            -200, # x
            random.randint(-175, 175), # y
        )
        meteor.set_x_speed(2)
        meteor.set_size(0.5)
    
    if counter % 15 == 0:
        
        alien = codesters.Sprite(
            "alien1",
            -200, # x
            random.randint(-175, 175), # y
        )
        alien.set_x_speed(2)
        alien.set_size(0.3)


