# ===== SETUP STAGE =====

stage.disable_all_walls()

# ===== SETUP HERO =====

hero = codesters.Sprite(
    "ski_5b5",
    0, # x
    150, # y
)
hero.set_size(0.3)

# ===== SETUP OBSTACLE =====

def gen_tree_string():
    randnum = random.choice("12347")
    return "pinetree" + randnum

def make_tree():
    one_tree = codesters.Sprite(
        gen_tree_string(), # string
        random.randint(-225, 225), # x
        -250, # y
    )
    
    one_tree.set_y_speed(5)
    one_tree.set_size(0.5)

# ===== GAME LOOP =====

while True:
    stage.wait(2)
    make_tree()

