# ===== SETUP STAGE =====

stage.disable_all_walls()

# ===== SETUP HERO =====

hero = codesters.Sprite(
    "ski_5b5",
    0, # x
    150, # y
)
hero.set_size(0.3)

# ===== SETUP OBSTACLE =====

trees = [] # NEW CODE

def gen_tree_string():
    randnum = random.choice("12347")
    return "pinetree" + randnum

def make_tree():
    one_tree = codesters.Sprite(
        gen_tree_string(), # string
        random.randint(-225, 225), # x
        -250, # y
    )
    
    one_tree.set_y_speed(5)
    one_tree.set_size(0.5)
    trees.append(one_tree) # NEW CODE

# ===== SETUP MOVEMENT =====

left_key_pressed = False
right_key_pressed = False

def left_key_antman():
    hero.set_rotation(-45)
    global left_key_pressed
    left_key_pressed = True

stage.event_key("left", left_key_antman)

def release_left(sprite):
    hero.set_rotation(0)
    global left_key_pressed
    left_key_pressed = False
hero.event_key_release("left", release_left)

def right_key_zelda():
    hero.set_rotation(45)
    for one_tree in trees:
        one_tree.set_x(
            one_tree.get_x() - 10
        )

stage.event_key("right", right_key_zelda)

def release_right(sprite):
    hero.set_rotation(0)
hero.event_key_release("right", release_right)

# ===== GAME LOOP =====

counter = 0
while True:
    counter = counter + 1
    stage.wait(0.05)
    if counter % 20 == 0:
        make_tree()
    if left_key_pressed == True: # TEMP
        x_move = 5
    else:
        x_move = 0
    for one_tree in trees:
        one_tree.set_x_speed(x_move)

"""
GOALS:

* Can crash into tree
* Prune trees
* Stop shaking
  * Replacing set_x/get_x with set_x_speed

ADVANCED:
* Make a monster chase you
* Add jumps
* Make a button to go faster

"""



