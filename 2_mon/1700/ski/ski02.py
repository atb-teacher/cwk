# ===== SETUP STAGE =====

stage.disable_all_walls()

# ===== SETUP HERO =====

hero = codesters.Sprite(
    "ski_5b5",
    0, # x
    150, # y
)
hero.set_size(0.3)

# ===== SETUP OBSTACLE =====
trees = [] # NEW CODE

def gen_tree_string():
    randnum = random.choice("12347")
    return "pinetree" + randnum

def make_tree():
    one_tree = codesters.Sprite(
        gen_tree_string(), # string
        random.randint(-225, 225), # x
        -250, # y
    )
    
    one_tree.set_y_speed(5)
    one_tree.set_size(0.5)
    trees.append(one_tree) # NEW CODE

# ===== SETUP MOVEMENT =====

def left_key_antman():
    hero.set_rotation(-45)
    for one_tree in trees:
        one_tree.set_x(
            one_tree.get_x() + 10
        )

stage.event_key("left", left_key_antman)

def release_left(sprite):
    hero.set_rotation(0)
hero.event_key_release("left", release_left)

def right_key_zelda():
    hero.set_rotation(45)
    for one_tree in trees:
        one_tree.set_x(
            one_tree.get_x() - 10
        )

stage.event_key("right", right_key_zelda)

def release_right(sprite):
    hero.set_rotation(0)
hero.event_key_release("right", release_right)

# ===== GAME LOOP =====

while True:
    stage.wait(2)
    make_tree()



