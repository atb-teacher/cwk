# ===== SETUP STAGE =====

stage.disable_all_walls()
trees = []

# ===== SETUP HERO =====

hero = codesters.Sprite(
    "ski_5b5",
    0, # x
    150, # y
)
hero.set_size(0.3)

print(
    "\n".join(
        dir(hero)
    )
)

# ===== SETUP HERO COLLISION =====

def hero_collision(sprite, other_thing):
    other_thing_name = other_thing.get_image_name()
    if other_thing_name[:4] == "pine":
        stage.remove_sprite(other_thing)
        trees.remove(other_thing)
        for one_tree in trees:
            one_tree.set_y_speed(2)
        stage.wait(2)
        for one_tree in trees:
            one_tree.set_y_speed(5)

hero.event_collision(hero_collision)

# ===== SETUP OBSTACLE =====


def gen_tree_string():
    randnum = random.choice("12347")
    return "pinetree" + randnum

def make_tree():
    one_tree = codesters.Sprite(
        gen_tree_string(), # string
        random.randint(-225, 225), # x
        -250, # y
    )
    
    one_tree.set_y_speed(5)
    one_tree.set_size(0.5)
    trees.append(one_tree) # NEW CODE

# ===== SETUP MOVEMENT =====

left_key_pressed = False
right_key_pressed = False

def left_key_antman():
    hero.set_rotation(-45)
    global left_key_pressed
    left_key_pressed = True

stage.event_key("left", left_key_antman)

def release_left(sprite):
    hero.set_rotation(0)
    global left_key_pressed
    left_key_pressed = False

hero.event_key_release("left", release_left)

def right_key_zelda():
    hero.set_rotation(45)
    global right_key_pressed
    right_key_pressed = True

stage.event_key("right", right_key_zelda)

def release_right(sprite):
    hero.set_rotation(0)
    global right_key_pressed
    right_key_pressed = False

hero.event_key_release("right", release_right)

# ===== Prune Trees =====

def prune_trees():
    for one_tree in trees:
        if one_tree.get_y() > 300 or not one_tree.isvisible():
            stage.remove_sprite(one_tree)
            trees.remove(one_tree)
    print(len(trees))

# ===== GAME LOOP =====

counter = 0
while True:
    counter = counter + 1
    stage.wait(0.05)
    if counter % 20 == 0:
        make_tree()
    
    # left_key_pressed = True
    # right_key_pressed = False
    if left_key_pressed == right_key_pressed:
        x_move = 0
    elif left_key_pressed == True:
        x_move = 5
    elif right_key_pressed == True:
        x_move = -5
    
    for one_tree in trees:
        one_tree.set_x_speed(x_move)
    prune_trees()

"""
GOALS:

* Can crash into tree
* Prune trees

ADVANCED:
* Make a monster chase you
* Add jumps
* Make a button to go faster

"""

