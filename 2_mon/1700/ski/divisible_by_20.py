import time # stage.wait
counter = 0

while True:
    counter += 1
    if counter % 20 == 0:
        print(f"{counter} is divisible by 20")
    time.sleep(0.1) # stage.wait
