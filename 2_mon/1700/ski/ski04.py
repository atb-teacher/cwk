# ===== SETUP STAGE =====

stage.disable_all_walls()

# ===== SETUP HERO =====

hero = codesters.Sprite(
    "ski_5b5",
    0, # x
    150, # y
)
hero.set_size(0.3)


# ===== SETUP OBSTACLE =====

trees = [] # NEW CODE

def gen_tree_string():
    randnum = random.choice("12347")
    return "pinetree" + randnum

def make_tree():
    one_tree = codesters.Sprite(
        gen_tree_string(), # string
        random.randint(-225, 225), # x
        -250, # y
    )
    
    one_tree.set_y_speed(5)
    one_tree.set_size(0.5)
    trees.append(one_tree) # NEW CODE

# ===== SETUP MOVEMENT =====

left_key_pressed = False
right_key_pressed = False

lr_text = codesters.Text(
    "left: False\nright: False", # text
    -180, # x
    200, # y
)

def update_lr():
    lr_text.set_text(
        f"left: {left_key_pressed}\nright: {right_key_pressed}"   
    )

update_lr()
def left_key_antman():
    hero.set_rotation(-45)
    global left_key_pressed
    left_key_pressed = True
    update_lr()
    
stage.event_key("left", left_key_antman)

def release_left(sprite):
    hero.set_rotation(0)
    global left_key_pressed
    left_key_pressed = False
    update_lr()
    
hero.event_key_release("left", release_left)

def right_key_zelda():
    hero.set_rotation(45)
    global right_key_pressed
    right_key_pressed = True
    update_lr()

stage.event_key("right", right_key_zelda)

def release_right(sprite):
    hero.set_rotation(0)
    global right_key_pressed
    right_key_pressed = False
    update_lr()
    
hero.event_key_release("right", release_right)

# ===== GAME LOOP =====

counter = 0
while True:
    counter = counter + 1
    stage.wait(0.05)
    if counter % 20 == 0:
        make_tree()
    
    if left_key_pressed == right_key_pressed:
        x_move = 0
    elif left_key_pressed == True:
        x_move = 5
    elif right_key_pressed == True:
        x_move = -5
    
    for one_tree in trees:
        one_tree.set_x_speed(x_move)

"""
GOALS:

* Can crash into tree
* Prune trees
* Stop shaking
  * Replacing set_x/get_x with set_x_speed

ADVANCED:
* Make a monster chase you
* Add jumps
* Make a button to go faster

"""

