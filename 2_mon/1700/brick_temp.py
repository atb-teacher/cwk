# ===== SETUP COLLISION =====

def collision_thor(sprite, hit_sprite):
    global score
    my_color = hit_sprite.get_color() 
    if my_color == "darkred":
        stage.remove_sprite(hit_sprite)
        score = score + 1
        score_text.set_text(f"Score: {score}")
        sprite.set_y_speed(
            sprite.get_y_speed() * -1
        )
    
ball.event_collision(collision_thor)

# ===== SETUP PADDLE =====

paddle = codesters.Rectangle(
	30, # x
	-180, # y
	50, # width
	50, # height
	"blue", # color
)

